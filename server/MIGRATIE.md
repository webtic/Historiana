Queries uit te voeren:

// oude redirect records zonder created veld weggooien
MATCH (n:Redirect) where not exists(n.created) delete n

# cleanups gedaan op newlive 26.6.2020

# deze en varianten om ongewenste entries te verwijderen

match (a:ContentItem {title:'The theatres of war'})-[r*1..3]-(b:ContentItem {title:'How the War was Fought'}) unwind r as x delete x

match (c:ContentItem) where c.embed_code contains "http:/" set c.embed_code=replace(c.embed_code, 'http:/', 'https:/')

=== 21-6-2022 adding content-types
create (:ContentType {name: "Image", icon: "image", uuid: "c8f1a815-97d5-483e-8cc6-9ba5c3e219d7"});
create (:ContentType {name: "Text", icon: "file-alt", uuid: "b5d4d134-8128-4119-b0fa-5eedd0ca08b6"});
create (:ContentType {name: "Video", icon: "video", uuid: "f996e95b-e866-4305-9dc2-549cf9591066"});
create (:ContentType {name: "Sound", icon: "volume", uuid: "ceffd1cc-1390-4cc8-9124-563e9df80671"});



===
