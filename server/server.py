"""Historiana API server"""

# import logging
import hashlib
import os
import os.path
import uuid
import sys
import time

import py2neo

from config import CONFIG

from json import dumps

from log import log

from tic import api_response, status

# When running as a daemon we have a ascii stdout; reset it to utf8
if CONFIG.SITE and CONFIG.SITE.get("utf8console"):
    import codecs
    sys.stdout = codecs.getwriter("utf-8")(sys.stdout.buffer, "strict")

from graph import g, graph


from sanic import Sanic, Blueprint
from sanic.response import json, html, HTTPResponse
import sanic
import datetime
import logging


start = datetime.datetime.now()
__version__ = (1, 0, 0)
__requests__ = 0


if CONFIG.opbeat:
    print("** enabling OPBEAT")
    import opbeat

    client = opbeat.Client(
        organization_id=ob["organization_id"],
        app_id=ob["app_id"],
        secret_token=ob["secret_token"],
        debug=True,
        async_mode=True,
    )

    from sanic.handlers import ErrorHandler
    from sanic.exceptions import SanicException

    # custom error handler to report errors to Opbeat
    # based on example:
    # https://github.com/channelcat/sanic/blob/master/examples/exception_monitoring.py
    class CustomHandler(ErrorHandler):
        def default(self, request, exception):
            # Here, we have access to the exception object
            # and can do anything with it (log, send to external service, etc)
            client.capture_exception()
            # Some exceptions are trivial and built into Sanic (404s, etc)
            if not isinstance(exception, SanicException):
                print(">>>> ", exception, "<<<<<< ")

            # Then, we must finish handling the exception by returning
            # our response to the client
            # For this we can just call the super class' default handler
            return super().default(request, exception)

    handler = CustomHandler()

    # performance monitoring not available for Sanic..
    # improvise on some examples
    # https://opbeat.com/docs/articles/performance-monitoring-in-falcon/
    import opbeat.instrumentation.control

    opbeat.instrumentation.control.instrument()

from sanic.log import access_logger
class AssetFilter(logging.Filter):
    def filter(self, record):
        #print(">>>>>> ", record.request)
        #return not ("/ua/" in record.request and record.status == 200)
        return not ("/ua/" in record.request)

access_logger.addFilter(AssetFilter())

app = Sanic(__name__)

from sanic.exceptions import NotFound
from sanic import response



@app.exception(NotFound)
def ignore_404s(request, exception):
    # return text("Yep, I totally found the page: {}".format(request.url))
    return response.HTTPResponse(status=404)


# ------------------------------------------------------------------------
@app.middleware("request")
async def set_remote_addr(request):
    """every request passes this function"""

    global __requests__
    __requests__ = __requests__ + 1

    """create a remote_addr attribute on the current request for logging purposes"""
    if "x-forwarded-for" in request.headers:
        request.headers["remote_addr"] = request.headers["x-forwarded-for"]
    else:
        request.headers["remote_addr"] = request.ip


# ------------------------------------------------------------------------


if CONFIG.SITE.get("rollbar"):
    from sanic_rollbar import SanicRollbar

    app.config["ROLLBAR_TOKEN"] = "8e4c2567b2e246479b486e72a5c0018d"
    app.config["ROLLBAR_ENV"] = CONFIG.SITE.get("ROLLBAR_ENV")
    SanicRollbar(app)

from api.data import DATA
app.blueprint(DATA)

from api.partners import PARTNER
app.blueprint(PARTNER)

from api.eactivity import EACTIVITY
app.blueprint(EACTIVITY)

from api.my_shares import MYSHARES
app.blueprint(MYSHARES)

from api.hcollection import COLLECTION
app.blueprint(COLLECTION)

from api.glossary import GLOSSARY
app.blueprint(GLOSSARY)

from api.language import LANGUAGE
app.blueprint(LANGUAGE)

from api.viewpoint import VIEWPOINT
app.blueprint(VIEWPOINT)

from api.sitemap import SITEMAP
app.blueprint(SITEMAP)

from api.viewpoints import VIEWPOINTS
app.blueprint(VIEWPOINTS)

from api.hlogging import LOG
app.blueprint(LOG)

from api.system import SYSTEM
app.blueprint(SYSTEM)

from api.search import SEARCH
app.blueprint(SEARCH)

from api.narrative import NARRATIVE
app.blueprint(NARRATIVE)

# support for /api/narratives/ (plural)
from api.narrative import NARRATIVES
app.blueprint(NARRATIVES)

# support for /api/narratives/ (plural)
from api.tags import TAGS
app.blueprint(TAGS)

# support for /api/ocr
from api.ocr import OCR
app.blueprint(OCR)


from admin import (
    admin_root,
    generic,
    generic_list,
    generic_delete,
    generic_edit,
    generic_get,
    dashboard,
    collections,
    learningactivities,
    narratives,
    viewpoints,
    get_form_meta,
    add_form_option,
    delete_form_option,
    get_admin_contenttypes,
    add_admin_contenttype,
    delete_admin_contenttype,
)
from admin.learningactivities import *

ADMIN = Blueprint("admin", url_prefix="/admin")
ADMIN.add_route(admin_root, "/", methods=["GET"], name="admin-root")
ADMIN.add_route(dashboard, "/dashboard", methods=["GET"])
ADMIN.add_route(collections.index, "/collections", methods=["GET"], name="collections-index")
ADMIN.add_route(collections.delete, "/collection", methods=["DELETE"], name="collections-delete" )
ADMIN.add_route(collections.delete_thumb, "/collectionThumb", methods=["DELETE"])

ADMIN.add_route(learningactivities.index, "/learningactivities", methods=["GET"], name="la-index")
ADMIN.add_route(learningactivities.delete, "/learningactivities", methods=["DELETE"], name="las-delete")

ADMIN.add_route(narratives.index, "/narratives", methods=["GET"], name="narratives-index")
ADMIN.add_route(narratives.delete, "/narratives", methods=["DELETE"], name="narratives-delete")

ADMIN.add_route(viewpoints.index, "/viewpoints", methods=["GET"], name="viewpoints-index")
ADMIN.add_route(viewpoints.delete, "/viewpoints", methods=["DELETE"], name="viewpoints-delete")


ADMIN.add_route(generic_edit, "/edit/<uuid>", methods=["POST"], name="generic_edit")
ADMIN.add_route(generic_edit, "/edit", methods=["POST"], name="ge_index")
ADMIN.add_route(generic_list, "/list/<nodelabel>", methods=["GET"])
ADMIN.add_route(generic, "/<nodelabel>", methods=["GET"])
ADMIN.add_route(generic_get, "/get/<uuid>", methods=["GET"])
ADMIN.add_route(generic_delete, "/delete/<uuid>", methods=["DELETE"], name="generic-delete")
ADMIN.add_route(get_form_meta, "/form-meta", methods=["GET"])
ADMIN.add_route(add_form_option, "/add/form/<form>", methods=["POST"])
ADMIN.add_route(delete_form_option, "/delete/form/<form>", methods=["POST"], name="delete-form-option")


ADMIN.add_route(la_removeCopyright, "/la/rights/remove", methods=["POST"])
ADMIN.add_route(la_changeCopyright, "/la/rights/change", methods=["POST"])
ADMIN.add_route(getAssets, "/la/assets/<uuid>", methods=["GET"])
ADMIN.add_route(saveAsset, "/la/asset/<uuid>", methods=["POST"])
ADMIN.add_route(deleteAsset, "/la/asset/<uuid>", methods=["DELETE"])
ADMIN.add_route(saveLA, "/la/<uuid>", methods=["POST"])
ADMIN.add_route(createLA, "/la/create", methods=["POST"])
ADMIN.add_route(deleteLA, "/la/delete", methods=["POST"], name="la-delete")
ADMIN.add_route(la_setStatus, "/la/status", methods=["POST"])

ADMIN.add_route(get_admin_contenttypes, "/contenttypes", methods=["GET"])
ADMIN.add_route(add_admin_contenttype, "/contenttype/add", methods=["POST"])
ADMIN.add_route(delete_admin_contenttype, "/contenttype/delete", methods=["POST"], name="content-type-delete")


app.blueprint(ADMIN)


##### tijdelijke modules editor #####

# ---------------------------------------------
@app.route("/admin/module/save", methods=["POST"])
async def module_save(request):
    uuid = request.json.get("uuid")
    rec = g.run(
        "MATCH (m:Module {uuid:{uuid}}) RETURN m", uuid=uuid).evaluate()

    rec["name"] = request.json.get("name")
    rec["slug"] = request.json.get("slug")
    rec["short_intro"] = request.json.get("short_intro")
    rec["introduction"] = request.json.get("introduction")
    rec["status"] = request.json.get("status")

    g.push(rec)
    return api_response(status.OK)


# ---------------------------------------------
@app.route("/admin/moduleitem/save", methods=["POST"])
async def module_item_save(request):
    uuid = request.json.get("uuid")
    rec = g.run(
        "MATCH (m:ContentItem {uuid:{uuid}}) RETURN m", uuid=uuid).evaluate()
    rec["title"] = request.json.get("title")
    rec["slug"] = request.json.get("slug")
    rec["intro"] = request.json.get("intro")
    rec["embed_code"] = request.json.get("embed_code")
    g.push(rec)
    return api_response(status.OK)


# ---------------------------------------------
@app.route("/admin/module/chapterintro/save", methods=["POST"])
async def chapteringtro_save(request):
    uuid = request.json.get("uuid")
    rec = g.run(
        "MATCH (m:ContentItem {uuid:{uuid}}) RETURN m", uuid=uuid).evaluate()
    print("chapter item: ", rec)
    rec["title"] = request.json.get("title")
    rec["slug"] = request.json.get("slug")
    rec["intro"] = request.json.get("intro")
    g.push(rec)
    return api_response(status.OK)


# ---------------------------------------------
@app.route("/admin/modules-index")
async def module_index(request):
    data = g.run(
        "MATCH (m:Module) WITH m ORDER BY m.name RETURN collect(m)").evaluate()
    return sanic.response.json(data)


# ---------------------------------------------
@app.route("/admin/get-module/<uuid>")
async def get_module(request, uuid):
    data = g.run(
        """
    MATCH (m:Module {uuid:{uuid}})-[]-(r)
    WITH collect({r: r, label:labels(r)}) as relations, m
    OPTIONAL MATCH (m)-[:ICONIC_IMAGE]-(icon)
    RETURN {
      module: m,
      icon: icon,
      related: relations
    }
    """,
        uuid=uuid,
    ).evaluate()

    # image

    if data and data["icon"]:
        data["icon"][
            "url"
        ] = f"/objects/{data['icon']['upload_uuid']}/{data['icon']['filename']}"

    return sanic.response.json(data)


# ---------------------------------------------


##### einde module editor #####


from api import BP

app.blueprint(BP)

from api import hist

app.blueprint(hist.HIST, url_prefix="/api/hist")


from sas import sas

app.blueprint(sas.SAS, url_prefix="/api/sas")


SERVER = os.uname()[1]
CONFIGS = {
    "atom.webtic.net": {"url": "http://localhost:8080/"},
    "hidev.webtic.nl": {"url": "http://dev.historiana.eu/"},
    "demo": {"url": "http://demo.historiana.eu/"},
}

URL = CONFIGS.get(SERVER)

if CONFIG.opbeat:
    # install Opbeat handlers
    app.error_handler = handler

    @app.middleware("request")
    async def print_on_request(request):
        client.begin_transaction(request.path)

    @app.middleware("response")
    async def print_on_response(request, response):
        client.end_transaction(request.path, response.status)


app.static("/uploads", "../uploads", name="uploads")
app.static("/admin/assets", "./admin/assets", name="admin-assets")
app.static("/assets/ext", "./assets/ext", name="assets-ext")
app.static("/assets", "./assets", name="assets")


# user uploaded assets
app.static("/ua", "/Users/paulj/Projects/Historiana/development/server/user_assets", name="user-assets")
app.static("/objects", "/Users/paulj/Projects/Historiana/development/objects", name="objects")


# ================================================================================
@app.route("/api/select/partners")
async def get_partners(request):

    data = g.run(
        """
    MATCH (p:Partner)
    WITH p
    ORDER BY p.name
    RETURN collect(p)
  """
    ).evaluate()

    return sanic.response.json(data)


# ================================================================================
@app.route("/api/select/license")
async def get_licenses(request):

    data = g.run(
        """
    MATCH (c:License)
    WITH c
    ORDER BY c.name
    RETURN collect(c)
  """
    ).evaluate()

    return sanic.response.json(data)

# ================================================================================
@app.route("/api/select/copyright")
async def get_copyrights(request):

    data = g.run(
        """
    MATCH (c:License)
    WITH c
    ORDER BY c.name
    RETURN collect(c)
  """
    ).evaluate()

    return sanic.response.json(data)


# ================================================================================
@app.route("/upload")
async def u(request):
    print("** UPLOAD **", request.method)

    print(json.dumps(request))
    print("====================")
    print("request: ", request.url)
    print("test")
    print(dir(request))

    print("## headers ##")
    # print(json.dumps(request.headers, content_type='text/html', body_bytes='', indent=4))

    print("## body ##")
    # print( json.dumps("%s" % request.body))
    # print("files: ", request.files)
    if request.method == "POST":
        upload = request.files["image"]
        # name = request.files['name']
        # print("name: ", name)
        print("filew: ", upload.type, type(upload.type))
        print("filew: ", upload.name)
        data = upload.body
        f = open("bla.dump", "wb")
        f.write(data)
        f.close()

        sig = hashlib.sha512(data).hexdigest()
        print(sig)
        try:
            ra = request.headers["Remote-Addr"].split(":")[0]
        except:
            ra = None

        upload = {
            "digest": sig,
            "filename": upload.name,
            "remote_addr": ra,
            "mimetype": upload.type,
            "status": "uploaded",
            "extension": os.path.splitext(upload.name)[1],
            "uuid": str(uuid.uuid4()),
            "owner": "anonymous",
            "uploaded": "timestamp()",
        }

        print("ra: ", upload)

        # print("keys: ", upload.keys())

    return html(
        '<form action="" method="post" enctype="multipart/form-data"> \
        <input type="file" name="image"><input type="submit">'
    )


# @app.route("/")
# async def test(request):
#    return json({"hello": "world"})
# app.static('/', '/web/sites/historiana/historiana-thriving/dist')

# @app.route("/admin")
# async def test(request):
#     print("foo: ", request.headers)
#     return json({"foo": "bar"})


@app.route("/foo")
async def test(request):
    print("foo: ", request.headers)
    return json({"foo": "bar"})


# @app.route("/")
# async def homepage(request):
#     """homepage"""
#     return html(open("../historiana-thriving/dist/index.html").read())


async def handler(request, *params):
    print("rr: ", request.url)
    print("params: ", params)
    print("handler!!!")
    return json("df")


# paths = g.run(
#     "MATCH (p:tic_Path)-[:USE_TEMPLATE]-(t:tic_Template) RETURN p.name as uri,t.file as template"
# )
# for p in paths:
#     print("uri: ", p['uri'])
#     app.router.add(uri=p['uri'], methods=['GET'], handler=handler)
# self.router.add(uri=uri, methods=methods, handler=handler).
# print(p)

# print("routers: ", app.router.routes_all)
# for r in app.router.routes_all:
#    print(r)


################################################################################
@app.route("/api/status")
async def get_status(request):


    if CONFIG.development:

        resp = dict(
            requests=__requests__,
            version=str(__version__),
            uptime=str(datetime.datetime.now() - start),
            sanic=sanic.__version__,
            py2neo=py2neo.__version__,
            neo4j=str(g.service.product),
            path=str(sys.path)
        )

    else:

        resp = dict()

    return api_response(status.OK, resp)




################################################################################
# prototype for SourceEditor
@app.route("/api/mytags", methods=["POST"])
async def get_mytags(request):
    member = request.json.get('uuid')
    data = g.run("MATCH (m:Member {uuid:$member})-[:MYTAG]-(t) WITH t ORDER BY t.name RETURN collect(distinct { value: t.uuid, label:t.name} )", member=member).evaluate()
    return api_response(status.OK, data)

################################################################################
# prototype for SourceEditor
@app.route("/api/saveSource", methods=["POST"])
async def saveSource(request):

    member = request.json.get('member')
    record = request.json.get('record')

    print(f"SAVE SOURCE FOR {member}")
    print(dumps(record, indent=4))

    r = g.run("MATCH (s {uuid:$source}) RETURN s", source=record['uuid']).evaluate()
    if record.get('introduction'):
        r['introduction'] = record.get('introduction')

    if record.get('title',''):
        r['title'] = record.get('title')

    if record.get('description',''):
        r['description'] = record.get('description')

    g.push(r)

    # license, language are single connections..
    if record.get('language'):
        lang = g.run("MATCH (s {uuid:$source})-[:HAS_LANGUAGE]-(l) RETURN collect(l.uuid)", source=record['ua']['uuid']).evaluate()
        print("LANG: ", lang, record['language'])
        # if language is different from current; update
        if record['language']['uuid'] not in lang:
            print("SET LANG ", record['ua']['uuid'])

            # remove current language from source
            remove = g.run("""
                MATCH (s {uuid:$source})-[rel:HAS_LANGUAGE]-(l)
                DELETE rel
            """, source=record['ua']['uuid'])

            lang = g.run("""
                MATCH (s {uuid:$source})
                MATCH (l {uuid:$language})
                OPTIONAL MATCH (s)-[x:HAS_LANGUAGE]-(l)
                DELETE x
                CREATE (s)-[:HAS_LANGUAGE]->(l)
            """, source=record['ua']['uuid'], language=record['language']['uuid'])


    if record.get('license'):
        license = g.run("MATCH (s {uuid:$source})-[:HAS_LICENSE]-(l) RETURN collect(l.uuid)", source=record['ua']['uuid']).evaluate()
        # print("LICENSE: ", lang, record['license'])
        # if language is different from current; update
        if record['license']['uuid'] not in license:
            print("SET LICENSE ", record['ua']['uuid'])

            # remove current language from source
            remove = g.run("""
                MATCH (s {uuid:$source})-[rel:HAS_LICENSE]-(l)
                DELETE rel
            """, source=record['ua']['uuid'])

            license = g.run("""
                MATCH (s {uuid:$source})
                MATCH (l {uuid:$license})
                OPTIONAL MATCH (s)-[x:HAS_LICENSE]-(l)
                DELETE x
                CREATE (s)-[:HAS_LICENSE]->(l)
            """, source=record['ua']['uuid'], license=record['license']['uuid'])


    # new_tags = record.get('tags', []) + record.get('myTags', [])
    new_tags = record.get('tags', [])
    print("NT: ", new_tags)

    if new_tags:

        # get current tags for source
        current_tags = g.run("""
            MATCH (s {uuid:$source})-[:HAS_TAG]-(tag) RETURN collect(tag.uuid)
        """, source=record['ua']['uuid']).evaluate()


        # scan existing tags on source
        for tag in current_tags:

            if tag not in new_tags:

                # remove current language from source
                remove = g.run("""
                    MATCH (tag {uuid:$tag})
                    MATCH (s {uuid:$source})-[rel:HAS_TAG]-(tag)
                    DELETE rel
                """, source=record['ua']['uuid'], tag=tag)

        # all tags in current set
        for tag in new_tags:
            # create tag if its not in current
            if tag.get('uuid') not in current_tags:
                create = g.run("""
                    MATCH (tag {uuid:$tag})
                    MATCH (s {uuid:$source})
                    CREATE (s)-[rel:HAS_TAG]->(tag)
                    RETURN s, tag
                """, source=record['ua']['uuid'], tag=tag.get('uuid'))

    return api_response(status.OK)


################################################################################
@app.route("/api/ui-build", methods=["GET"])
async def ui_build(r):
    path = "/web/sites/historiana.eu/site/dist/index.html"

    try:
        v = os.path.getmtime(path)
        val = time.ctime(v)

    except:
        val = "dev"

    return api_response(status.OK, dict(build=val))


################################################################################
@app.route("/api/content-types", methods=["GET"])
async def _get_content_types(request):
    _gr = graph()
    data = _gr.run("MATCH (c:ContentType) WITH c ORDER BY c.name RETURN collect(c)").evaluate()
    return api_response(status.OK, data)

################################################################################
@app.route("/api/set-contenttype", methods=["POST"])
async def _set_content_type(request):
    user = request.json.get('user')
    item = request.json.get('item')
    ctype = request.json.get('ctype')

    _gr = graph()


    print(f"SET CONTENT TYPE user: {user} item: {item} ctype: {ctype}")

    # remove old type if applicable
    _gr.run("""
        MATCH (s {uuid:$item})-[ct:HAS_CONTENT_TYPE]->(c:ContentType)
        DETACH DELETE ct
    """, item=item)

    # create new connection
    res = _gr.run("""
        MATCH (s {uuid:$item})
        MATCH (c:ContentType {uuid:$ctype})
        CREATE (s)-[:HAS_CONTENT_TYPE]->(c)
    """, item=item, ctype=ctype)

    print("SET CONTENT TYPE RES: ", res)

    return api_response(status.OK)




################################################################################
@app.route("/", methods=["OPTIONS", "POST", "GET"])
async def index(request):
    """redirect root requests to the /api context"""
    from index import _html

    return html(_html)
    #    return HTTPResponse(
    #    status=200, content_type="text/html", headers={"Location": "/api"}
    # )

    # return HTTPResponse(
    #    status=308, content_type="text/html", headers={"Location": "/api"}
    # )

################################################################################
def run_server():
    if os.uname().nodename=='atom.local':
      # app.run(host="0.0.0.0", port=9000, dev=True, workers=1, access_log=True)
      app.run(host="0.0.0.0", port=9000, workers=4, dev=True, access_log=True)
    else:
      app.run(host="0.0.0.0", port=9000, workers=8, dev=True, access_log=True)


    # log.info("")
    # log.info("==============================================")
    # log.info("=== START Historiana API {}".format(__version__))
    # log.info("=== sanic  @ {}".format(sanic.__version__))
    # log.info("=== py2neo @ {}".format(py2neo.__version__))
    # log.info("=== neo4j  @ {}".format(g.database.kernel_version))
    # log.info("==============================================")


if __name__ == "__main__":
    run_server()
