import daemon

from server import run_server

with daemon.DaemonContext():
	run_server()


