from graph import g
from collections import Counter

data = g.run("match (ua:UserAsset) return properties(ua) as r")
#data = g.run("match (ua:MySources) return properties(ua) as r")


props = Counter()

for d in data:

	for (k,v) in d['r'].items():
		print(k)
		props[k] = props[k]+1


import json
print(json.dumps(props, indent=4))


data = g.run("match (ua:UserAsset) return collect(distinct ua.type) as r").evaluate()
print("type: ", json.dumps(sorted(data), indent=4))

# data = g.run("match (ua:UserAsset) return collect(distinct [ua.thumb_url, ua.title]) as r").evaluate()
# print("type: ", data)

