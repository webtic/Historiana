from pptx import Presentation
from pptx.parts.image import ImagePart

from pptx.shapes.picture import Picture
from pptx.shapes.placeholder import PlaceholderPicture

import os
import os.path
import tempfile
import hashlib

from graph import g
from py2neo import Node

from tic import NEW_UUID, api_response, status, js_now
from slugify import slugify

import json
from config import CONFIG

import mimetypes

mimetypes.init()


def pptx_to_collection(filename, url=None, user=None):
    # process PPTX into array of images and texts
    data = analyze_pptx(filename, url)

    # check if the title exists; add a (n) for the current one
    titles = g.run(
        "MATCH (c:Collection) where c.name contains {title} RETURN count(c)", data
    ).evaluate()
    data["user"] = user
    if titles > 0:
        data["title"] = data["title"] + " ({})".format(titles + 1)
        data["slug"] = slugify(data["title"])

    g.run(
        """
  MATCH (m:Member {uuid:$user})
  CREATE (c:Collection)<-[:OWNS]-(m)
  SET
    c.uuid = $uuid,
    c.pptx = $pptx,
    c.name = $title,
    c.slug = $slug,
    c.subtitle = $subtitle,
    c.description = $description,
    c.status  = 'powerpoint import',
    c.ts = timestamp()
  RETURN c
""",
        data,
    )

    for slide in data["slides"]:

        # print("silde: ", slide)
        slide["collection"] = data["uuid"]
        # print("EKYS: ", slide.keys())
        # if a slide has no image there is no need to store it as a CollectionItem
        # as these are all based around an image and some text
        if slide.get("url"):
            # create UserAssets (the file is already on the filesystem)
            n = Node("UserAsset", **slide["upload"])
            g.create(n)

            slide["ua"] = n["uuid"]

            # create CollectionItem
            g.run(
                """
              MATCH (c:Collection {uuid:{collection}})
              MATCH (ua:UserAsset {uuid:$ua})
              CREATE (ua)<-[:IS_ASSET]-(ci:CollectionItem)-[:IN_COLLECTION]->(c)
              SET
                ci.uuid = apoc.create.uuid(),
                ci.order = $slide,
                ci.title = $title,
                ci.filename = $filename,
                ci.url = $url,
                ci.description = $text
              RETURN ci
            """,
                slide,
            )

    return api_response(status.OK, data)


# ============================================================================
def analyze_pptx(filename, urlpath):

    pptx_dir = os.path.dirname(filename)
    pptx = os.path.basename(filename)
    p = Presentation(filename)
    slides = []

    # tmpdir = tempfile.mkdtemp()
    # os.chmod(tmpdir, 0o777) #open for webserving
    try:
        images_dir = pptx_dir + "/images"
        os.mkdir(images_dir, 0o777)
    except:
        print("{} ALREADY EXISTS or PERMISSION PROBLEM".format(images_dir))
        pass

    title = ""
    subtitle = ""
    description = ""

    # for each slide
    for cnt, slide in enumerate(p.slides):

        rec = dict(text=[], slide=cnt, filename=None, url=None)

        # for each 'shape' in the slide
        for (o, s) in enumerate(slide.shapes):
            try:
                rec["text"].append(s.text)
            except:
                pass

            # is this an image, then store it on the filesystem
            if type(s) in [Picture, PlaceholderPicture]:
                filename = "{:03d}_{:03d}_{}".format(cnt, o, s.image.filename)
                filename = filename.replace("jpeg", "jpg")
                print(f"create file: {images_dir}/{filename}")
                print(f"config: {CONFIG.USER_UPLOADS}")
                print(f"path: {images_dir}/{filename}")

                asset_file = f"{images_dir}/{filename}"
                asset_url = images_dir.replace(CONFIG.USER_UPLOADS + "/", "")

                f = open(asset_file, "wb")
                f.write(s.image.blob)
                f.close()

                # this dict becomes a UserAsset
                (fname, ext) = os.path.splitext(s.image.filename)

                upload = dict(
                    digest=hashlib.sha256(s.image.blob).hexdigest(),
                    filename=filename,
                    extension=ext.lower(),
                    path=f"{asset_url}/{filename}",
                    type=mimetypes.types_map[ext],
                    uuid=NEW_UUID(),
                    created=js_now(),
                )

                print(json.dumps(upload, indent=4))
                print(120 * "^")

                rec["upload"] = upload
                rec["url"] = f"/ua/{asset_url}/{filename}"

        print(120 * "=")
        print(rec["text"])
        print(120 * "_")

        # alle tekstblokken van 1 slide zit nu in rec['text']

        # cleanup empty blocks
        rec["text"] = [r for r in rec["text"] if r.strip()]

        # assume shortest block is the title
        shortest = min(rec["text"], key=len)

        # create new blocks from \n seperated lines
        # and do some basic parsing
        keep = []
        for (offset, text) in enumerate(rec["text"]):

            lines = [l.strip() for l in rec["text"][offset].splitlines() if l.strip()]

            license = None
            source = None

            for line in lines:
                if line.startswith("Source:"):
                    source = line.strip()
                elif line.startswith("CC BY"):
                    license = line.strip()
                elif line.startswith("Public Domain"):
                    license = line.strip()
                else:
                    keep.append(line)

            # we could do a shortest on keep now but it tends to be fuzzier due to more short blocks

        try:

            rec["title"] = shortest
            rec["license"] = license
            rec["text"] = [l for l in keep if l != shortest]

            # if this is the first slide assume title and subtitle are in this one
            if cnt == 0:
                try:
                    (title, subtitle) = rec["title"].split("\n", 1)
                    description = "\n".join(rec["text"])

                except ValueError:
                    title = rec["title"]
                    subtitle = ""

                title = title.lower().capitalize()

        except:
            rec["title"] = "No title found"

        slides.append(rec)
        print("→ CREATED REC: ")
        print(json.dumps(rec, indent=4))

    # basename because we want a asset path

    return dict(
        uuid=NEW_UUID(),
        pptx=pptx,
        title=title,
        description=description,
        slug=slugify(title),
        subtitle=subtitle,
        path=os.path.basename(pptx_dir) + "/images",
        slides=slides,
    )


if __name__ == "__main__":
    # pptx = "/Users/paulj/Projects/Historiana/development/server/powerpoint/Internment the Bigger Picture in format.pptx"
    pptx = "./Posters of Communist China.pptx"
    data = analyze_pptx(pptx)
    print("data: ", json.dumps(data, indent=4))
