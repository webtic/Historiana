import argparse
import os.path
import requests
import hashlib
import re

from tic import js_now, NEW_UUID
from urllib.parse import urlparse
from openpyxl import load_workbook
from graph import g
from config import CONFIG
from py2neo import Node, Relationship
from lxml import html

from config import Config

CONFIG = Config()


# this is api.mail
from api import mail

log = []
rows = []


def process(excel_file, member, collection):

    wb = load_workbook(filename=excel_file, read_only=True)
    ws = wb.worksheets[0]

    active = False
    for (o, r) in enumerate(ws.iter_rows()):
        cols = []
        for c in r:
            cols.append(c.value)

        # only keep rows with one or more non-empty cols
        if any(cols) and active:
            rows.append(cols)

        # once we have seen Title we keep all following rows
        if cols[0] == "Title":
            active = True

    user = g.run(
        """
        MATCH (m:Member {uuid:$uuid})
        RETURN m
        """,
        uuid=member,
    ).evaluate()

    # optionally create new collection ()
    # we currently get a uuid from an existing one
    # collection = g.run(
    #     """
    #         MATCH (m:Member {uuid:$user})
    #         CREATE (c:Collection)<-[:OWNS]-(m)
    #         SET c.trash=1,
    #             c.name=$name,
    #             c.uuid=apoc.create.uuid(),
    #             c.created=datetime(),
    #             c.status='import via Excel',
    #             c.slug=$slug
    #         RETURN c
    #     """,
    #     user=user["uuid"],
    #     name="Import",
    #     slug="import1",
    # ).evaluate()

    # print("collection: ", collection)

    # process each row
    # the fields in the excel
    # some are translated eg description is introduction in the Excel
    props = [
        "title",
        "description",
        "url",
        "type",
        "period_start",
        "period_end",
        "origin",
        "country",
        "language",
        "local_id",
        "copyright",
        "tags",
    ]

    dest_absolute = f"{CONFIG.USER_UPLOADS}/{member}/{collection}"

    os.makedirs(dest_absolute, exist_ok=True)

    # process each row in the Excel Sheet
    for (offset, item) in enumerate(rows, start=1):
        print("")
        # first fetch the image-url
        fetch = item[props.index("url")]

        # get the basename for entry
        pathname = urlparse(fetch).path
        filename = os.path.basename(pathname)
        keep_filename = False

        # if there is no known image extension in the url we generate a new filename
        if re.search("jpg|png|gif|svg", filename, re.IGNORECASE):
            keep_filename = True

        try:
            req = requests.get(fetch)

        except Exception as e:
            log.append(
                dict(
                    line=offset,
                    code=503,
                    msg=str(e),
                    url=fetch,
                    title=item[props.index("title")],
                )
            )
            continue

        if not req.ok:
            print("LOG ERROR: ", fetch)
            log.append(
                dict(
                    line=offset,
                    code=req.status_code,
                    msg=req.reason,
                    url=fetch,
                    title=item[props.index("title")],
                )
            )

            continue

        # generate a new name with extension based on content-type
        if not keep_filename:
            t = req.headers["Content-Type"]
            # skip any stuff after ;
            tp = t.split(";")
            # get type after the /
            ext = tp[0].split("/")[1]
            filename = f"{NEW_UUID()}.{ext.lower()}"

        rtype = req.headers.get("Content-Type")
        if rtype:
            rtype = rtype.split(";")[0]
            if not "image" in rtype:
                print("*** NO IMAGE ", rtype)
                tree = html.fromstring(req.content)
                og_image = tree.xpath('//meta[@property="og:image"]/@content')[0]
                if og_image:
                    print("found og: ", og_image)

        dest = f"{dest_absolute}/{offset}_{filename}"
        upload_url = f"/{user['uuid']}/{collection}/{offset}_{filename}"
        hashfile = hashlib.sha256()
        print("WRITE: ", dest)
        with open(dest, "wb") as f:
            for chunk in req.iter_content(1024):
                hashfile.update(chunk)
                f.write(chunk)

        (fname, ext) = os.path.splitext(filename)
        upload_uuid = NEW_UUID()

        upload = {
            "filename": filename,
            "extension": ext.lower(),
            "digest": hashfile.hexdigest(),
            "type": req.headers.get("content-type"),
            "uuid": upload_uuid,
            "path": upload_url,
            "created": js_now(),
            "via": "ExcelCollectionUpload",
        }

        # create a UserAsset node for this file,
        # to keep track and to be able to
        # use it as a cover image for the collection.
        n = Node("UserAsset", **upload)
        g.create(n)

        # attach UserAsset to current user
        if user:
            r = Relationship(n, "UPLOADED_BY", user)
            g.create(r)

        ###################################################################
        # CREATE COLLECTION-ITEM
        # connect image to the collection as a CollectionItem
        ci = g.run(
            """
            MATCH (c:Collection {uuid:$collection})
            MATCH (ua:UserAsset {uuid:$asset_uuid})

            CREATE (ua)<-[:IS_ASSET]-(ci:CollectionItem)-[:IN_COLLECTION]->(c)
            SET
            ci.uuid = apoc.create.uuid(),
            ci.order = $order,
            ci.title = $title,
            ci.filename = $filename,
            ci.url = $url,
            ci.description = $description,
            ci.type = $itemtype,
            ci.flag=1
            RETURN ci
        """,
            collection=collection,
            order=offset,
            title=item[props.index("title")],
            filename=filename,
            url=f"/ua/{upload_url}",
            description=item[props.index("description")],
            itemtype=item[props.index("type")],
            local_id=item[props.index("local_id")],
            origin=item[props.index("origin")],
            asset_uuid=upload_uuid,
        ).evaluate()

        # set first image as iconic image
        if offset == 1:

            g.run(
                """
                MATCH (c:Collection {uuid:$collection})
                MATCH (ua:UserAsset {uuid:$asset})
                CREATE (ua)-[:THUMB_FOR]->(c)
                """,
                dict(collection=collection, asset=upload["uuid"]),
            )

        ###################################################################
        # TAGS

        # get tags from input
        tmptags = item[props.index("tags")]
        tags = []
        if tmptags:
            tags = [e.strip() for e in tmptags.split(",")]

        # connect the tags, create if needed
        # doing a 2 stage merge to avoid duplication of Tag entries
        # with different uuids
        for tag in tags:
            g.run(
                """
                MATCH (ci:CollectionItem {uuid:$ci})
                MERGE (t:MyKeywords {name:$tag})
                ON CREATE SET t.uuid=apoc.create.uuid()
                MERGE (ci)-[:MYTAG]-(t)
                RETURN t
                """,
                ci=ci["uuid"],
                tag=tag,
            )

        print("tags: ", tags)

        ######################################################################
        # PERIODS
        print("** periods")
        start = item[props.index("period_start")]
        end = item[props.index("period_end")]

        if start:
            g.run(
                """
                MATCH (ci:CollectionItem {uuid:$ci})
                MERGE (p:Period {name:$period})
                ON CREATE SET p.uuid=apoc.create.uuid()
                MERGE (ci)-[:STARTS]->(p)
                RETURN p
            """,
                ci=ci["uuid"],
                period=start,
            )

        if end:
            g.run(
                """
                MATCH (ci:CollectionItem {uuid:$ci})
                MERGE (p:Period {name:$period})
                ON CREATE SET p.uuid=apoc.create.uuid()
                MERGE (ci)-[:ENDS]->(p)
                RETURN p
            """,
                ci=ci["uuid"],
                period=end,
            )

        ###################################################################
        # COUNTRY

        print("** country")
        country = item[props.index("country")]
        if country:
            # check if this country is known; connect if so
            has_country = g.run(
                """
                MATCH (ci:CollectionItem {uuid:$ci})
                MATCH (c:Country)
                WHERE toLower(c.name)=toLower($country)
                CREATE (ci)-[:IN]->(c)
                RETURN c
            """,
                ci=ci["uuid"],
                country=country,
            ).evaluate()

            # language unknown, create it and keep track of creator
            if not has_country:
                lang = g.run(
                    """
                    MATCH (m:Member {uuid:$member})
                    MATCH (ci:CollectionItem {uuid:$ci})
                    CREATE (c:Country {name:$country})
                    SET c.created=timestamp()
                    SET c.uuid=apoc.create.uuid()
                    SET c.via="ExcelCollectionUpload"
                    CREATE (ci)-[:IN]->(c)<-[:CREATED_BY]-(m)
                    RETURN c
                """,
                    member=user["uuid"],
                    ci=ci["uuid"],
                    country=country,
                ).evaluate()

        ###################################################################
        # LANGUAGE
        # lookup existing; create if not found

        print("** language")

        language = item[props.index("language")]
        if language:

            # check if this language is known; connect if so
            lang = g.run(
                """
                MATCH (ci:CollectionItem {uuid:$ci})
                MATCH (l:Language)
                WHERE toLower(l.name)=toLower($language) OR
                    toLower(l.native)=toLower($language)

                CREATE (ci)-[:HAS_LANGUAGE]->(l)
                RETURN l
            """,
                ci=ci["uuid"],
                language=language.lower(),
            ).evaluate()

            # language unknown, create it and keep track of creator
            if not lang:
                lang = g.run(
                    """
                    MATCH (m:Member {uuid:$member})
                    MATCH (ci:CollectionItem {uuid:$ci})
                    CREATE (l:Language {name:$language})
                    SET l.native=$language
                    SET l.label=$language
                    SET l.created=timestamp()
                    SET l.uuid=apoc.create.uuid()
                    SET l.via="ExcelCollectionUpload"
                    CREATE (ci)-[:HAS_LANGUAGE]->(l)<-[:CREATED_BY]-(m)
                    RETURN l
                """,
                    member=user["uuid"],
                    ci=ci["uuid"],
                    language=language,
                ).evaluate()

        ###################################################################
        # COPYRIGHT NEE LICENSE

        print("** license")

        hlicense = item[props.index("copyright")]
        if hlicense:
            # check if this language is known; connect if so
            hl = g.run(
                """
                MATCH (ci:CollectionItem {uuid:$ci})
                MATCH (l:License)
                WHERE toLower(l.name)=toLower($license) OR
                    toLower(l.code)=toLower($license)
                CREATE (ci)-[:HAS_LICENSE]->(l)
                RETURN l
            """,
                ci=ci["uuid"],
                license=hlicense,
            ).evaluate()

            # licensee unknown, create it and keep track of creator
            if not hl:
                hl = g.run(
                    """
                    MATCH (m:Member {uuid:$member})
                    MATCH (ci:CollectionItem {uuid:$ci})
                    CREATE (l:License {name:$license})
                    SET l.created=timestamp()
                    SET l.uuid=apoc.create.uuid()
                    SET l.via="ExcelCollectionUpload"
                    CREATE (ci)-[:HAS_LICENSE]->(l)<-[:CREATED_BY]-(m)
                    RETURN l
                """,
                    member=user["uuid"],
                    ci=ci["uuid"],
                    license=hlicense,
                ).evaluate()


if __name__ == "__main__":
    print("START EXCEL")
    parser = argparse.ArgumentParser()
    parser.add_argument("excel_path", help="path to Excel file", type=str)
    parser.add_argument("member_uuid", help="uuid of member", type=str)
    parser.add_argument("collection_uuid", help="uuid of collection", type=str)

    args = parser.parse_args()
    process(args.excel_path, args.member_uuid, args.collection_uuid)

    # HARDCODE TEST
    # member_uuid = '83baf2fd-5801-438b-b1c4-54aca6b90097'
    # collection_uuid = '1d4c72d3-4087-40f4-8b79-c7675e93b995'
    # excel_file = "/Users/paulj/Downloads/upload.xlsx"

    member = g.run(
        "MATCH (m:Member {uuid:$member}) RETURN m", member=args.member_uuid
    ).evaluate()
    collection = g.run(
        "MATCH (c:Collection {uuid:$collection}) RETURN c",
        collection=args.collection_uuid,
    ).evaluate()

    # HARDCODE TEST
    # process(
    #     excel_file=excel_file,
    #     member=member_uuid,
    #     collection=collection_uuid,
    # )

    # get name from first available non null value
    name = next(iter([member["name"], member["login"], member["email"]]))

    msg = mail.create(
        "excel_process",
        member["email"],
        dict(
            name=name,
            collection=dict(name=collection["name"], slug=collection["slug"]),
            log=log,
            host=CONFIG.SITE_URL,
            subject=f"[Historiana] Excel Upload Collection processing finished for: {collection['name']}",
        ),
    )

    mail.send(msg)
