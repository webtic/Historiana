import requests
import re
import os.path


from tic import status, get_relative_path, get_hash, url_to_filename
from tasks import mux
from graph import graph
from py2neo import Node, Relationship


from config import Config
CONFIG = Config()


def import_video_from_url(_user, _url, _title="Title for this video.", _description='Description for this video'):

    """import video asset from url"""

    # create new db conx
    g = graph()

    print("** IMPORT VIDEO ASSET FROM URL")
    print (f"  user {_user}")
    print (f"  url  {_url}")
    print (f"  title  {_title}")
    print (f"  description  {_description}")
    print(40*'-')

    if not _user:
        return status.USER_NOTFOUND

    if not _url:
        return status.NO_DATA


    headers = {
        "User-Agent": "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
    }

    # put file into a user-uuid/upload-uuid/filename hierarchy
    relative = []
    relative.append(_user)
    path = "/".join([CONFIG.USER_UPLOADS,]+relative)


    r = requests.get(_url, headers=headers)
    if r.ok:

        # get mimetype
        mimetype = r.headers.get('Content-Type','')
        print(">> TYPE ", mimetype)
        print(">> CD: ", r.headers.get("Content-Disposition"))

        # get filename from request, this fails on non-download links which we will not parse
        reqfile = r.headers.get("Content-Disposition")
        # if not reqfile:
        #     print("** RETURN NO_DOWNLOAD")
        #     return status.NO_DOWNLOAD


        # process the download
        # 1. pass import to MUX and import the same _url via MUX API
        muxmeta = mux.create(_url)

        # 2. download and store the Asset; assume filename has extension
        if reqfile:
            filename = re.findall("filename=\"(.+)\"", reqfile).pop(0)
            _, extension = os.path.splitext(filename)
        else:
            filename, extension = url_to_filename(_url)
            filename = f"{filename}{extension}"


        print("fname: ", filename)
        print("ext: ", extension)
        print("fname from url: ", _url.split("/")[-1])

        dest = path + '/' + filename
        relative = get_relative_path(dest)
        print(">>>>> ", relative)
        offset = 0
        my_dest = dest

        # check for file existance.
        # prefix with number until file doe not exist.
        while True:
            print("CHECK DEST: ", my_dest)
            offset = offset+1
            if not os.path.exists(my_dest):
                break

            else:
                my_dest = f"{path}/{offset}_{filename}"
                continue


        with open(my_dest,"wb") as out:
            out.write(r.content)
            digest = get_hash(r.content)

            # store it on the graph
            rec = g.run(
                """
                MATCH (m:Member {uuid:$user})
                CREATE (ua:UserAsset)-[:UPLOADED_BY]->(m)
                SET ua.created = timestamp(),
                    ua.type = $mimetype,
                    ua.extension = $extension,
                    ua.filename = $filename,
                    ua.path = $relpath,
                    ua.uuid = apoc.create.uuid(),
                    ua.digest = $digest,
                    ua.origin = $origin,
                    ua.thumb_url = $thumb_url,
                    ua.playback_url = $playback_url,
                    ua.mux_id = $mux_id
                RETURN ua
                """,
                user = _user,
                mimetype = mimetype,
                extension = extension,
                filename = filename,
                relpath = relative,
                digest = digest,
                origin = _url,
                mux_id = muxmeta['mux_id'],
                thumb_url = muxmeta['thumb'],
                playback_url = muxmeta['playback']
            ).evaluate()

            # create a MySource too

            source = g.run("""
                MATCH (m:Member {uuid:$user})
                CREATE (s:MySources)<-[:SOURCE]-(m)
                SET s.created = timestamp(),
                    s.url = $url,
                    s.uuid = apoc.create.uuid(),
                    s.description = $description,
                    s.title = $title
                RETURN s
            """,

                user = _user,
                url = muxmeta['playback'],
                title = _title,
                description = _description
            ).evaluate()

            class IsAsset(Relationship): pass
            ac = IsAsset(source, rec)
            g.create(ac)

    return source



    return status.OK