# MUX integration for Historiana
#
# DEVELOPMENT TOKEN
#
MUX_TOKEN_SECRET = 'VMvqksP6vfX2CGOxUVlBdHv9m6QMlIpLHRV7v4Nc2DXbxtoXhGc6FZUGTXxBqcHmmF4HQXFVWSp'
MUX_TOKEN_ID = '35cfc5dd-15d4-4e65-ba13-0bc41694daba'

import time

import mux_python
from mux_python.rest import ApiException

# Authentication Setup
configuration = mux_python.Configuration()
configuration.username = MUX_TOKEN_ID #os.environ['MUX_TOKEN_ID']
configuration.password = MUX_TOKEN_SECRET #os.environ['MUX_TOKEN_SECRET']

# API Client Initialization
assets_api = mux_python.AssetsApi(mux_python.ApiClient(configuration))

# List Assets
# print("Listing Assets: \n")
# try:
#     list_assets_response = assets_api.list_assets()
#     for asset in list_assets_response.data:
#         print('Asset ID: ' + asset.id)
#         print('Status: ' + asset.status)
#         print('Duration: ' + str(asset.duration) + "\n")
# except ApiException as e:
#     print("Exception when calling AssetsApi->list_assets: %s\n" % e)


def index():
    return assets_api.list_assets()


def create(url):
    """create the ASsset on MUX, return its id and some helper-info.
    note: mux is async; status will be updated after this function returns

    """

    # links for testing
    # https://www.openbeelden.nl/files/13/02/1303026.1302205.BADAN_SEHIDUP-FHD00Z01LOR_112960_246200.mp4
    # https://www.youtube.com/watch?v=7Cpq5sPCqKY

    # MUX CREATES:
    # https://image.mux.com/pX6cumHLEf6yU9RzkTHpZTRZ33AdG859ZP1K3djeWPo/thumbnail.jpg?time=6
    # https://image.mux.com/pX6cumHLEf6yU9RzkTHpZTRZ33AdG859ZP1K3djeWPo/low.mp4
    # https://stream.mux.com/pX6cumHLEf6yU9RzkTHpZTRZ33AdG859ZP1K3djeWPo/low.mp4

    input_settings = [mux_python.InputSettings(url=url)]
    create_asset_request = mux_python.CreateAssetRequest(input=input_settings, playback_policy=[mux_python.PlaybackPolicy.PUBLIC], mp4_support="standard")
    create_asset_response = assets_api.create_asset(create_asset_request)

    # print("Created Asset ID: " + create_asset_response.data.id)

    # for simplicity this is done in the foreground for now....
    if create_asset_response.data.status not in ['ready', 'errored']:
        while True:
            asset_response = assets_api.get_asset(create_asset_response.data.id)
            #print("MUX RESP: ", asset_response)
            if asset_response.data.status == 'ready':
                print("Asset Ready! Playback URL: https://stream.mux.com/" + asset_response.data.playback_ids[0].id + ".m3u8")
                break

            elif asset_response.data.status == 'errored':
                print("Asset Ready! Playback URL: https://stream.mux.com/" + asset_response.data.playback_ids[0].id + ".m3u8")
                break

            else:
                print("WAIT for ", asset_response.data.status)
                time.sleep(1)
                continue


    # print("=============================")
    # print(dir(asset_response))
    # print("xxx: ", asset_response.data)
    # print("============================")

    asset_response = assets_api.get_asset(create_asset_response.data.id)

    # print("STATUS: ", asset_response.data.status)
    # status will be updated asynchronously to either ready|preparing|errored


    return dict(
        status = asset_response.data.status,
        mux_id = create_asset_response.data.id,
        thumb = f"https://image.mux.com/{asset_response.data.playback_ids[0].id}/thumbnail.jpg?time=2",
        playback = f"https://stream.mux.com/{asset_response.data.playback_ids[0].id}.m3u8",
        #mp4 = [ e.name for e in asset_response.data.static_renditions.files]
    )
