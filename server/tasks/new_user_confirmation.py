from graph import g
import tic

def start_new_user_confirmation(request, email, uuid):

	print("new user confirmation transaction")
	print("addr: ", request.headers.get('remote_addr'))
	task_uuid = tic.NEW_UUID()

	g.run("""
		CREATE (t:Task)
		SET
			t.created = timestamp(),
			t.action = 'request-email-confirmation',
			t.done = false,
			t.ip = $ip,
			t.email = $email,
			t.uuid = $task_uuid,
			t.member_uuid = $uuid

		""", email=email, uuid=uuid, ip=request.headers.get('remote_addr'), task_uuid=task_uuid)

