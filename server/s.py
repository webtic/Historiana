
from sanic import Sanic, Blueprint
from sanic.response import json, html, HTTPResponse

import sanic
import codecs
import sys


sys.stdout = codecs.getwriter('utf-8')(sys.stdout.buffer, 'strict')

app = Sanic(__name__)

@app.route("/test", methods=['POST','OPTION'])
async def u(request):
    print(request.json)
    return json({})

def run_server():
    print("HISTORIANA API 1.0")
    print("-> Using SANIC ", sanic.__version__)
    app.run(host="0.0.0.0", port=9001, debug=True)
    
if __name__=='__main__':
    run_server()

