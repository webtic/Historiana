# API response codes

BAD_REQUEST = { 'isError': True,   'errorCode': 'bad-api-request' }		#
NO_SESSION  = { 'isError': True,   'errorCode': 'no-session' }		# session info is missing on request
BROKEN_JSON = { 'isError': True,   'errorCode': 'json-data-broken' }		# json is broken on request
NO_DATA 	= { 'isError': True,   'errorCode': 'no-data-found' }		# no valid data found in request
DELETED 	= { 'isError': False,  'errorCode': 'delete-ok' }		# delete was done
NOT_FOUND 	= { 'isError': True,   'errorCode': 'data-not-found' }		# request-data was parsed ok but not found
NO_DOWNLOAD = { 'isError': True,   'errorCode': 'download-not-found', 'msg': 'No download found at specified url.' }		# url import cannot find Content Disposition
OK 	    	= { 'isError': False,  'errorCode': '' }				# request-data was parsed ok but not found
ERROR 		= { 'isError': True,   'errorCode': 'api-error'}
CANNOT_FETCH	= { 'isError': True,   'errorCode': 'cannot-request-remote-source'} # for Adding external web sources
NOT_LOGGED_IN	= { 'isError': True,   'errorCode': 'not-logged-in' } # for Adding external web sources

# not used yet, msg is not handled?
# see api/register calls
USER_EXISTS = {'isError': True, 'errorCode': 'user-exists', 'msg': 'Login name already exists.'}
USER_NOTFOUND = {'isError': True, 'errorCode': 'user-not-found', 'msg': 'Email address not known.'}
NOT_ALLOWED = {'isError': True, 'errorCode': 'action-not-allowed', 'msg': 'Action is not permitted'}
NO_PARTNER_CONNECTION = {'isError': True, 'errorCode': 'no-partner-connection', 'msg': 'This account is not connected to a Partner.'}
