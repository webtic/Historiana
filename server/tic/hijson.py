import json
import datetime

# make a list of possible datetime objects
date_instances = [datetime.datetime, datetime.date]


# add this one if its there (newer py2neo dep)
try:
    from interchange.time import DateTime
    #date_instances.append(interchange.time.Date)
    date_instances.append(DateTime)
except:
    pass


# isinstance wants a tuple
date_instances = tuple(date_instances)


class HiJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, date_instances):
            return obj.__str__()

        if isinstance(obj, complex):
            return [obj.real, obj.imag]

        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)


def HiJSONdump(data):
    return json.dumps(data, cls=HiJSONEncoder)


if __name__ == "__main__":
    print("main")
    from py2neo import Graph

    g = Graph(user="neo4j", password="geen", database="historiana", debug=True)

    data = g.run("RETURN {created:datetime({timezone:'CET'}), name:'iets'}").evaluate()
    print(json.dumps(data, cls=HiJSONEncoder))
