from sanic import response
from config import CONFIG
from tic.hijson import HiJSONdump
import datetime

# ------------------------------------------------------------------------
def api_options_response(data, extra=None, result=None):

    # print("** api_options_response")
    h = {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "Content-Type",
    }
    return response.json({"isError": False}, headers=h)


# ------------------------------------------------------------------------
def new_api_response(status, args={}):

    error_flag = status.get("isError")
    error_code = status.get("errorCode")

    reply = {
        "isError": error_flag,
        "errorCode": error_code,
    }

    # anything passed as something=val is added in the root of the response
    # eg api_response(err.OK, modified=res)
    for kw in args:
        reply[kw] = args[kw]


    status_code = 400 if error_flag else 200


    return response.json(reply, status=status_code, dumps=HiJSONdump)

# ------------------------------------------------------------------------
def api_response(data, result=None, **kwargs):
    """create a JSON reply"""

    msg = data.get("msg")
    error_flag = data.get("isError")
    error_code = data.get("errorCode")

    reply = {
        "isError": error_flag,
        "errorCode": error_code,
    }

    if msg:
        reply["msg"] = msg

    # anything passed as something=val is added in the root of the response
    # eg api_response(err.OK, modified=res)
    for kw in kwargs:
        reply[kw] = kwargs[kw]

    # wrap in data property
    if result:
        reply["data"] = result

    # for development only

    # print("data: ", type(data))
    # if there is isError entry flag the status 400 so Axios sees it as an error
    status_code = 200
    # if type(data) is list:
    #    status = 200
    if type(data) is object or type(data) is dict:
        status_code = 400 if data.get("isError") else 200

    # if CONFIG.development:
    #     print("===========XXXXXX")

    #     h = {
    #         "Access-Control-Allow-Origin": "*",
    #         "Access-Control-Allow-Headers": "Content-Type",
    #     }
    #     return response.json(data, status=status, headers=h)

    # TODO: reflect more meta in status code? eg 201 created and generic errors?
    return response.json(reply, status=status_code, dumps=HiJSONdump)
