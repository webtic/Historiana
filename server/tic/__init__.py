"""
generic functions by WEBtic
"""

from .tic import *
import uuid

# javascript compatible timestamp
NOW = lambda : int(time.time()*1000)

# create uuid's for nodes without plugin
NEW_UUID = lambda : str(uuid.uuid4())

# used in redirect service 
from . import base66

# generic json based response
from .api_response import api_response
from .api_response import new_api_response

#from .mail import send_mail

def get_user_from_session(session):

	# lookup session and match user if possible
	s = g.run("""
	    MATCH (s:Session {sessionId:$sess})
	    OPTIONAL MATCH (s)-[]-(m:Member)
	    RETURN {
	        valid:s.sessionValid,
	        user: m
	    }
	""", sess=session).evaluate()

	return dict(user = s.get('user'), session_valid = s.get('valid'))

