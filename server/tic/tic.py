
import datetime, time

from sanic import response
import time
import uuid
import hashlib

from slugify import slugify
from graph import g
from py2neo.matching import NodeMatcher

from urllib.parse import urlparse
from os.path import basename, splitext

NOW = lambda : int(time.time()*1000)
NEW_UUID = lambda : str(uuid.uuid4())

from config import Config
CONFIG = Config()


# ------------------------------------------------------------------------
def get_relative_path(path):
    return path.replace(CONFIG.USER_UPLOADS, "/ua")


#
def url_to_filename(s):
    url = urlparse(s)
    fname = basename(url.path)
    return splitext(fname)

# ------------------------------------------------------------------------
def get_hash(blob):

    """central hash function to ease future migration"""
    return hashlib.sha256(blob).hexdigest()

# ------------------------------------------------------------------------
def get_password(s):
    """get password hash from string"""
    return hashlib.sha1(s.encode()).hexdigest()



# ------------------------------------------------------------------------
def sanitize(s):

    if s:
        s = s.replace("'","")

    return s



# ------------------------------------------------------------------------
def log_event(member_uuid, msg):

    r = g.run("""
        MATCH (m:Member {uuid:$member_uuid})
        CREATE (e:EventLog {msg:$msg, ts:timestamp()})<-[:_EVENT]-(m)
        RETURN null""", {'member_uuid': member_uuid, 'msg': msg}).evaluate()
    return r


# ------------------------------------------------------------------------
def js_now():
    d = datetime.datetime.utcnow()
    return int(time.mktime(d.timetuple())) * 1000

# ------------------------------------------------------------------------
def ts_to_date(ts):
    """javascript timestamp to date"""
    if ts:
        dt = datetime.datetime.fromtimestamp(float(ts)/1000.0)
        return dt
    else:
        return None    

# ------------------------------------------------------------------------
def date_to_ts(date):
    """date to javascript timestamp"""
    if date:
        d = date.strip()
        try:
            # assume it is just a year
            if len(d)==4:
                t = time.mktime(datetime.datetime.strptime(date, "%Y").timetuple())
            else:
                t = time.mktime(datetime.datetime.strptime(date, "%m/%d/%Y").timetuple())
            return int(t)*1000
        except:
            pass

    return 0

# ------------------------------------------------------------------------   
def get_remote_addr(request):
    # get the remote addr for this request; only interested in the IP# part
    # not the socket; should Sanic store it in a list instead?

    ip = request.headers.get('X-Forwarded-For', None)
    if not ip:
        try:
            ip = request.ip[0]
        except:
            ip = None

    return ip


# ------------------------------------------------------------------------
def log(msg):
    print(js_now(), msg)



# ------------------------------------------------------------------------
def create_slug(string, node, prop):
    """create an unique slug for a specific Property on a specific Node type"""

    slug = slugify(string)
    m = NodeMatcher(g)

    counter = 1
    while True:
        # adapt string by appending -n until not-found
        #print("try: ", {prop:string})
        nodes = m.match(node,  **{prop:slug})
        if nodes.count()>0:
            # just brute-force it for now
            slug = slugify(string) + f'-{counter}'
            counter = counter+1
        else:
            break

    return slug


