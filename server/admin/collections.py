from graph import g
from tic import api_response, status


def index(request):
    print("collections index in admin/collections.py")
    collections = g.run("""
        MATCH (c:Collection)
        OPTIONAL MATCH (c)-[:OWNS]-(m:Member)
        OPTIONAL MATCH (c)-[:OWNS]-(m:Member)-[]-(p:Partner)
        OPTIONAL MATCH (c)-[:IN_COLLECTION]-(entry:CollectionItem)
        OPTIONAL MATCH (c)-[:HAS_LICENSE]->(l:License)
        WITH count(entry) as itemcount,c,l,m,p
        ORDER BY c.ts DESC
        RETURN collect({
            ts: c.ts,
            name: c.name,
            subtitle: c.subtitle,
            status: c.status,
            license: l,
            uuid: c.uuid,
            slug: c.slug,
            member: m,
            partner: p,
            itemcount: itemcount,
            is_published: c.is_published
        })


    """).evaluate()

    return api_response(status.OK, collections)


def delete(request):
    """delete Collection AND its CollectionItems"""
    print("admin/collections.py DELETE")
    collection = request.json.get('collection')

    # TODO: validate if delete is permitted
    user = request.json.get('user')

    g.run("""
        MATCH (c:Collection {uuid:{collection}})
        OPTIONAL MATCH (c)<-[:IN_COLLECTION]-(ci:CollectionItem)
        DETACH DELETE c,ci
    """, collection=collection)
    return api_response(status.OK)

def delete_thumb(request):
    """delete the thumb button"""
    g.run("""
        MATCH (c:Collection {uuid:{collection}})-[t:THUMB_FOR]-(x)
        DELETE t
    """, request.json)


    return api_response(status.OK)


