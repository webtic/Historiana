from sanic.response import json,text, html
from sanic import Blueprint
from tic import api_response, status, NEW_UUID
from py2neo import Node
from graph import g
import time
import os

try:
    from ujson import dumps as json_dumps
except BaseException:
    from json import dumps as json_dumps



from .collections import *
from .narratives import *


async def get_emails(request):
  pass


# --------------------------------------------------------------
async def admin_root(request):
  print(api_response)
  return html('admin index')


# --------------------------------------------------------------
async def get_admin_contenttypes(request):
  data=['de','data']
  data = g.run("MATCH (c:ContentType) WITH c ORDER BY c.name RETURN collect(c)").evaluate()
  return api_response(status.OK, data)

# --------------------------------------------------------------
async def add_admin_contenttype(request):

  # check member

  record = request.json.get('entry')

  if "uuid" in record.keys():
    g.run("MATCH (c:ContentType {uuid:$uuid}) SET c.icon=$icon, c.name=$name RETURN c", **record).evaluate()

  else:

    g.run("CREATE (c:ContentType) SET c.icon=$icon, c.name=$name, c.uuid=apoc.create.uuid() RETURN c", **record).evaluate()


  return api_response(status.OK)

# --------------------------------------------------------------
async def delete_admin_contenttype(request):

  _uuid = request.json.get('uuid')
  _member = request.json.get('member')
  g.run("MATCH (c:ContentType {uuid:$uuid}) DETACH DELETE c", uuid=_uuid).evaluate()
  return api_response(status.OK)






# --------------------------------------------------------------
async def dashboard(request):

  lastlogins = g.run("""
    MATCH(n: Session) - [] - (u: Member)
    WITH {
      session: n.sessionId,
      login: n.ts,
      name: u.name,
      joined: u.joined
    } as rec, n
    order by n.ts DESC LIMIT 10
    RETURN rec
  """)

  #print("l: ",lastlogins.data())

  m = g.run("""
    MATCH (m:Member)
    WITH count(m) as m
    MATCH (c:Collection)
    WITH count(c) as c,m
    MATCH (ci:CollectionItem)
    WITH count(ci) AS ci,c,m
    MATCH (la:LearningActivity)
    WITH count(la) AS la,ci,c,m

    RETURN {
      cards: [
        { title: 'Collections', value: c },
        { title: 'CollectionItems', value: ci },
        { title: 'LearningActivities', value: la },
        { title: 'Members', value: m }
      ]
    }
  """).evaluate()

  reply = {}
  reply.update(m)
  reply["logins"] = [d['rec'] for d in lastlogins.data()]

  return api_response(reply)

# --------------------------------------------------------------
async def generic_edit(request, uuid=None):
  """edit a record"""

  meta = request.json.get('meta')
  data = request.json.get('form')

  if meta and meta['new']:

    # create NEW record
    uuid = NEW_UUID()
    rec = Node(meta['node'], uuid=uuid)
    del data['uuid']
    g.create(rec)

  else:
    # fetch for update
    rec = g.run("MATCH (u {uuid:{uuid}}) RETURN u", uuid=uuid).evaluate()

  # set new properties and save
  rec.update(data)
  a = g.push(rec)
  return api_response(status.OK, rec)


# --------------------------------------------------------------
async def generic_get(request, uuid):
  """get a record by its uuid"""
  #print("--> generic-get ", uuid)
  rec = g.run("MATCH (u {uuid:{uuid}}) RETURN u", uuid=uuid).evaluate()
  if rec is not None:
    return api_response(status.OK, rec)

  return api_response(status.NOT_FOUND)

# --------------------------------------------------------------
async def generic_delete(request, uuid):
  """XXX see notes.md for improvements"""
  print("**** generic delete ****", uuid, request.json)

  # try to convert to integer to match the key in Neo4J for numeric keys
  # try:
  #   uuid = int(uuid)
  # except Exception as e:
  #   print("err: ", e)
  #   pass

  act = g.run("MATCH (n {uuid:{uuid}}) DETACH DELETE n RETURN n", uuid=uuid).evaluate()
  if act is not None:
    return api_response(status.DELETED)

  return api_response(status.NOT_FOUND)

# --------------------------------------------------------------
async def generic_list(request, nodelabel):

  print("genericlist ***")
  start = time.time()
  sort = request.args.get('sort','')        # sort on this field
  order = request.args.get('order', 'ASC')  # order of sorting
  # this field is the key, skipped in meta so it does not show in the data-table
  key = request.args.get('key')
  sorter = "ORDER BY r.{sort} {order}".format(sort=sort,order=order) if sort else ''

  reply = {}
  reply['sort'] = sort
  reply['order'] = order

  # query twice to get ALL records regardless property combinations
  # combining a match and count gives an overview of all different property combinations
  # eg:
  #   MATCH (r:License) return count(r), keys(r)
  # 6 ["name", "url", "uuid", "icon", "code"]
  # 1 ["name", "url", "uuid", "icon", "code", "default"]
  # 1 ["code", "name", "uuid"]
  #
  # MATCH (r:License)
  #     WITH count(r) as count
  #     MATCH (r:License)
  #     WITH {
  #       meta: keys(r),
  #       count: count(keys(r))
  #     } as meta
  #     return meta
  #
  # right now we do not do anything with this situation; Once data is stable this should not
  # happen but dealing with it would be nice.

  # query = """
  #     MATCH (r:{nodelabel})
  #     WITH count(r) as count
  #     MATCH (r:{nodelabel})
  #     OPTIONAL MATCH (r)-[rel]-(x)
  #     WITH count, r, collect(distinct type(rel)) as types, collect(distinct labels(x)) as labels
  #     RETURN {{
  #       relations: labels,
  #       meta: keys(r),
  #       count: count,
  #       xxx: count
  #     }}
  # """.format(nodelabel=nodelabel)
  # print("aa: ", query)

  # info = g.run(query).evaluate()
  # reply['meta'] = [ x for x in info['meta'] if x!=key] if info else []
  # reply['count'] = info['count'] if info else 0
  # reply['key'] = key

#      OPTIONAL MATCH (r)-[rel]-(x)
# WITH r, collect(distinct type(rel)) as types, collect(distinct labels(x)) as labels

  # retrieve all the meta info we need for this nodelabel
  query = """
    MATCH (r:{nodelabel})
      WITH count(r) as count
      MATCH (r:{nodelabel})
      OPTIONAL MATCH (r)-[rel]-(x)
      WITH
        count,
        collect(distinct keys(r)) as keys,
        collect(distinct type(rel)) as types,
        collect(distinct labels(x)) as labels
      UNWIND keys AS key
      UNWIND key AS k
      WITH DISTINCT k, count,labels, types
      WITH collect(k) AS keys, count, labels, types
      RETURN {{
        fields: keys,
        count: count,
        relations: apoc.coll.flatten(labels),
        types: apoc.coll.flatten([types])
      }}
  """.format(nodelabel=nodelabel)

  # use apoc to unwind relations and types; trying to do it via UNWIND
  # did not work out well; edge cases give empty results

  info = g.run(query).evaluate()
  if info is not None:
    reply.update(info)
  else:
    reply['count'] = 0
    reply['fields'] = []

  reply['key'] = key

  # fetch all the data/meta for this nodelabel
  # we sent this in reply.records
  query = """
      MATCH (r:{nodelabel})
      WITH r {sorter}
      RETURN {{
        count: count(r),
        data: collect(r),
        timestamp: timestamp(),
        sort: '{sort}',
        order: '{order}'
      }}
  """.format(nodelabel=nodelabel, sorter=sorter, sort=sort, order=order)

  records = g.run(query).evaluate()

  reply['records'] = records
  stop = time.time()
  reply["elapsed"] = stop-start
  reply['actions'] = [
      {
        'name': 'edit',
        'label': 'Edit',
        'url': 'edit/{uuid}'
      },
      {
        'name': 'delete',
        'label': 'Delete',
        'url': 'delete/{uuid}',
        'type': 'danger',
        'confirm': True
      }

  ]
  #print("== RESP == ", reply)
  return api_response(reply)
# --------------------------------------------------------------
async def generic(request, nodelabel):
  print("admin generic for: ", nodelabel)
  return api_response({})



# --------------------------------------------------------------
async def delete_form_option(request, form):

  # add an option to a form
  print("--- delete to form: ", form)
  print("--- ", request.json)
  value = request.json.get('value')
  field = request.json.get('field')

  # q = f"MATCH (a:{form}) WHERE a.{field}={{value}} DETACH DELETE a"
  q = """
    MATCH (a:{form}) WHERE a.{field}={{value}}
    DETACH DELETE a
  """.format(form=form, field=field, value=value)
  print("Q: ", q)
  data = g.run(q,value=value).evaluate()
  return await get_form_meta(request)


# --------------------------------------------------------------
# alle info voor formulier elementen
async def add_form_option(request, form):

  # add an option to a form
  print("--- add to form: ", form)
  print("--- ", request.json)
  value = request.json.get('value')
  field = request.json.get('field')

  # q = f"CREATE (a:{form}) SET a.{field}={{value}} RETURN a"
  q = "CREATE (a:{form}) SET a.{field}={{value}} RETURN a".format(form=form, field=field, value=value)
  print("q: ", q)
  r = g.run(q, value=value).evaluate()

  return await get_form_meta(request)

# --------------------------------------------------------------
# alle info voor formulier elementen
async def get_form_meta(request):

  print("** api-admin-selects")

  data = g.run("""
    MATCH (d:ActivityDuration)
    WITH d
    ORDER BY d.label
    WITH {
        title: 'Approximate time',
        node: 'ActivityDuration',
        props: 'label',
        options: collect(distinct {label: d.label}),
        actions: {
          add: {
            method: '/admin/add/form/ActivityDuration',
            field: 'label'
          },
          delete: {
            method: '/admin/delete/form/ActivityDuration',
            field: 'label'
          }

        }
    } as duration


    MATCH (a:Age)
    WITH duration, a
    ORDER BY a.label
    WITH duration, {
        title: 'Indicative age',
        node: 'Age',
        props: 'label',
        options: collect(distinct {label: a.label}),
        actions: {
          add: {
            method: '/admin/add/form/Age',
            field: 'label'
          },
          delete: {
            method: '/admin/delete/form/Age',
            field: 'label'
          }
        }
    } as age

    MATCH (s:Status)
    WITH duration, age, s
    ORDER BY s.name
    WITH duration, age, collect(s) as status

    MATCH (t:Tag)
    WITH duration, age, t, status
    ORDER BY t.name
    WITH duration, age, status,
    {
        title: 'Tags',
        node: 'Tag',
        props: 'label',
        options: collect(distinct {label: t.name}),
        actions: {
          add: {
            method: '/admin/add/form/Tag',
            field: 'name'
          },
          delete: {
            method: '/admin/delete/form/Tag',
            field: 'name'
          }
        }
    } as tag

    WITH duration, age, tag, status


    MATCH (th:HistoricalThinking)
    WITH duration, age, tag, th, status
    ORDER BY th.label
    WITH duration, age, tag, status,
    {
        title: 'Historical thinking',
        node: 'HistoricalThinking',
        props: 'label',
        options: collect(distinct {label: th.label}),
        actions: {
          add: {
            method: '/admin/add/form/HistoricalThinking',
            field: 'name'
          },
          delete: {
            method: '/admin/delete/form/HistoricalThinking',
            field: 'name'
          }
        }
    } as thinking

    WITH duration, age, tag, thinking, status

    MATCH (tm:TeachingMethods)
    WITH duration, age, tag, thinking, tm, status

    ORDER BY tm.label
    WITH duration, age, tag, thinking, status,
    {
        title: 'Teaching methods',
        node: 'TeachingMethods',
        props: 'label',
        options: collect(distinct {label: tm.label}),
        actions: {
          add: {
            method: '/admin/add/form/TeachingMethods',
            field: 'name'
          },
          delete: {
            method: '/admin/delete/form/TeachingMethods',
            field: 'name'
          }
        }
    } as methods

    WITH duration, age, tag, thinking, methods, status

    MATCH (tc:TeachingChallenges)
    WITH duration, age, tag, thinking, methods, tc, status

    ORDER BY tc.label
    WITH duration, age, tag, thinking, methods, status,
    {
        title: 'Teaching challenges',
        node: 'TeachingChallenges',
        props: 'label',
        options: collect(distinct {label: tc.label}),
        actions: {
          add: {
            method: '/admin/add/form/TeachingMethods',
            field: 'name'
          },
          delete: {
            method: '/admin/delete/form/TeachingMethods',
            field: 'name'
          }
        }
    } as challenges

    WITH duration, age, tag, thinking, methods, challenges, status

    MATCH (p:Partner)
    OPTIONAL MATCH (p)-[:IS_LOGO]-(logo)
    OPTIONAL MATCH (p)-[:IS_ICON]-(icon)
    WITH p,logo,icon, duration, age, tag, thinking, methods, challenges, status
    ORDER BY p.name

    WITH duration, age, tag, thinking, methods, challenges, status,
    COLLECT(p {
        .*,
        logo: logo.url,
        icon: icon.url
    }) AS selectPartners


    WITH duration, age, tag, thinking, methods, challenges, selectPartners, status

    MATCH (l:License)
    WITH l, duration, age, tag, thinking, methods, challenges, selectPartners, status
    ORDER BY l.name
    WITH duration, age, tag, thinking, methods, challenges, selectPartners, status,
    COLLECT(l) as selectLicense

    MATCH (lt:LinkType)
    WITH lt, duration, age, tag, thinking, methods, challenges, selectPartners, selectLicense, status
    ORDER BY lt.name

    WITH duration, age, tag, thinking, methods, challenges, selectPartners, selectLicense, status,
    COLLECT(lt) as linkTypes



    RETURN {
      age: age,
      duration: duration,
      tag: tag,
      thinking: thinking,
      methods: methods,
      challenges: challenges,
      partners: selectPartners,
      licenses: selectLicense,
      linktypes: linkTypes,
      status: status

    }
  """).evaluate()


  for d in data['linktypes']:
    print("--> ", d['icon'])
    d['icon'] = d['icon'].replace("/assets/images/icons/", "/static/la/")
    print("icon: ", d['icon'])

  return api_response(status.OK, data)
