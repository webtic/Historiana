# this is the LA specific admin code
#
# however: the /api/la and /api/la/<uuid> calls to get index and one specific LA
# are in api.py
#
# these are used in the admin to show the list of LA's and to retrieve all props
# for edit


from graph import g
from tic import api_response, status, NOW, NEW_UUID, new_api_response
from ujson import dumps as json_dumps


def index(request):
    """list of Learning Activities for Admin"""

    data = g.run(
        """
    MATCH (l:LearningActivity)
    RETURN collect(l) AS data
  """
    ).evaluate()

    return new_api_response(status.OK, dict(data=data))


def delete(request, METHOD=["DELETE"]):
    """delete a Learning Activities for Admin"""
    _uuid = request.json.get("uuid")
    _user = request.json.get("user")

    g.run(
        """
        MATCH (n:LearningActivity {uuid:$uuid})
        DETACH DELETE n
    """,
        uuid=_uuid,
    )

    return api_response(status.OK)


def deleteLA(request):

    uuid = request.json.get("uuid")
    print("DELETE LA", uuid)
    g.run("MATCH (la:LearningActivity {uuid:$uuid}) DETACH DELETE la", uuid=uuid)
    return api_response(status.OK, uuid)


def createLA(request):
    uuid = NEW_UUID()
    print("CREATE LA", uuid)
    g.run("CREATE (la:LearningActivity) SET la.uuid=$uuid", uuid=uuid)
    return api_response(status.OK, uuid)


def deleteAsset(request, uuid):
    print("DELETE ASSET ", uuid)
    # print("data: ", request.json)
    l = request.json.get("link")
    link_uuid = request.json.get("mylink")
    interest = l["interest"].lower()
    print("uid: ", l["uuid"])

    if interest == "teacher":

        s = g.run(
            """
      MATCH (la:LearningActivity {uuid:$la})-[r:INTEREST_TEACHER]-(a {uuid:$link})
      DETACH DELETE a
    """,
            la=uuid,
            link=link_uuid,
        )

    if interest == "student":

        s = g.run(
            """
      MATCH (la:LearningActivity {uuid:$la})-[r:INTEREST_STUDENT]-(a {uuid:$link})
      DETACH DELETE a
    """,
            la=uuid,
            link=link_uuid,
        )

    print("REMOVE: ", s)

    # if interest=='student':
    return api_response(status.OK)


def saveAsset(request, uuid):
    print("-> SAVE ASSET FOR LA: ", uuid)
    print("data: ", request.json)
    # only title and linktype are of interest in the post data
    # we save the title omn the Asset object not the Link object
    # this sh/could be changed

    # first we delete the ASSET_FOR and then create the
    # LA-LINK-ASSET/LINKTYPE

    data = dict(
        la=uuid,
        asset=request.json.get("uuid"),
        linktype=request.json.get("linktype"),
        title=request.json.get("title"),
    )

    # print("--> DATA: ", data)

    p = g.run(
        """
    MATCH (la:LearningActivity {uuid:$la})<-[new:ASSET_FOR]-(a:UserAsset {uuid:$asset})
    MATCH (lt:LinkType {uuid:$linktype})
    CREATE (link:Link)-[:LINK_TYPE]->(lt)
    CREATE (link)-[:LINK_ASSET]->(a)
    SET a.title = $title
    SET link.uuid = apoc.create.uuid()
    DETACH DELETE new
    RETURN [lt.interest, link.uuid]
  """,
        **data
    ).evaluate()

    data["link"] = p[1]

    if p[0].lower() == "teacher":
        k = g.run(
            """
      MATCH (la:LearningActivity {uuid:$la})
      MATCH (link:Link {uuid:$link})
      CREATE (la)-[r:INTEREST_TEACHER]->(link)
      RETURN r
    """,
            data,
        ).evaluate()
        print("t: ", k)

    elif p[0].lower() == "student":
        k = g.run(
            """
      MATCH (la:LearningActivity {uuid:$la})
      MATCH (link:Link {uuid:$link})
      CREATE (la)-[r:INTEREST_STUDENT]->(link)
      RETURN r
    """,
            data,
        ).evaluate()
        print("t: ", k)

        # """
        # CASE lt.interest
        # 	WHEN 'teacher'
        # 	THEN CREATE (la)-["INTEREST_TEACHER]->(link)
        # 	WHEN 'student'
        # 	THEN CREATE (la)-["INTEREST_STUDENT]->(link)
        # END
        # """

    return api_response(status.OK)


def getAssets(request, uuid):
    """get the assets for this LA - to curate into student/teacher/link"""

    # :ASSET_FOR is nieuw; voor assets die nog geen titel etc hebben
    # oude LA's hebben ook Link (die naar een asset linkt)
    # hoe herkennen we assets zonder link?

    # letop: nieuwe LA's krijgen UserAsset terwijl oude Asset gekoppeld hebben

    assets = g.run(
        """

    // een oude asset die al via Link is gekoppeld
    MATCH (la {uuid:$uuid})-[:INTEREST_TEACHER|INTEREST_STUDENT]-(l:Link)-[:LINK_ASSET]-(a)
    MATCH (l)-[:LINK_TYPE]-(lt:LinkType)
    WITH COLLECT( distinct a {
        .*,
        link: lt,
        mylink: l.uuid
    }) as existingItems

    OPTIONAL MATCH (la:LearningActivity {uuid:$uuid})<-[:ASSET_FOR]-(a:UserAsset)
    WITH existingItems, la, COLLECT(a {
      .*,
      title: '',
      linktype: ''
    }) as newItems


    RETURN {
      new: newItems,
      existing: existingItems
    }
  """,
        uuid=uuid,
    ).evaluate()

    if assets:
        return api_response(status.OK, assets)

    return api_response(status.NOT_FOUND)


def saveLA(request, uuid):
    print("** SAVE LA ** ", uuid)
    data = request.json.get("record")
    related = request.json.get("related")

    if related:
        g.run(
            """
      MATCH (la:LearningActivity {uuid:$uuid})-[r:LA_RELATED]-(al:LearningActivity)
      DELETE r
      RETURN la
    """,
            uuid=uuid,
        )

        for r in related:
            g.run(
                """
        MATCH (la:LearningActivity {uuid:$uuid})
        MATCH (al:LearningActivity {uuid:$r})
        CREATE (la)-[r:LA_RELATED]->(al)
      """,
                uuid=uuid,
                r=r,
            )

            print("create rel: ", r)

    # print(json_dumps(data))

    # for k in data.keys():
    # 	print("k: ", k)

    la = g.run(
        """
          MATCH (la:LearningActivity {uuid:$uuid})
          RETURN la
        """,
        uuid=uuid,
    ).evaluate()

    # base properties to process
    process = [
        "title",
        "author",
        "slug",
        "summary",
        "intro",
        "outcomes",
        "copyright",
        "acknowledgements",
    ]

    for p in process:
        # print("process: ", p, )
        la[p] = data.get(p)
        # print("d: ", data.get(p))

    # set or modify timestamp
    la["modified"] = NOW()
    g.push(la)

    la = g.run(
        """
          MATCH (la:LearningActivity {uuid:$uuid})
          MATCH (la)-[arel:HAS_DURATION]-(:ActivityDuration)
          MATCH (la)-[brel:TAG]-(:Tag)
          MATCH (la)-[crel:IN_AGE]-(:Age)
          DELETE arel, brel, crel
        """,
        uuid=uuid,
    ).evaluate()

    for r in data["duration"]:
        s = g.run(
            """
      MATCH (la:LearningActivity {uuid:$uuid})
      MATCH (dur:ActivityDuration {label:$label})
      CREATE (la)-[:HAS_DURATION]->(dur)
    """,
            uuid=uuid,
            label=r,
        ).evaluate()

    for r in data["age"]:
        s = g.run(
            """
      MATCH (la:LearningActivity {uuid:$uuid})
      MATCH (dur:Age {label:{label}})
      CREATE (la)-[:IN_AGE]->(dur)
    """,
            uuid=uuid,
            label=r,
        ).evaluate()

    for r in data["tags"]:
        print("create tag: ", r)
        s = g.run(
            """
      MATCH (la:LearningActivity {uuid:$uuid})
      MATCH (dur:Tag {name:$label})
      CREATE (la)-[:TAG]->(dur)
    """,
            uuid=uuid,
            label=r,
        ).evaluate()

    return api_response(status.OK)


# -----------------------------------------------------------------------
async def la_setStatus(request):
    """set the status of a Learning Activity"""

    la = request.json.get("la")
    la_status = request.json.get("status")
    print("la.   : ", la)
    print("status: ", la_status)

    # s = g.run("""
    # 	MATCH (la:LearningActivity {uuid:{la}})-[x:HAS_STATUS]-(s:Status)
    # 	DELETE x
    # 	WITH la
    # 	MATCH (s:Status {uuid:{status}})
    # 	WITH la, s
    # 	CREATE p = (la)-[:HAS_STATUS {created:timestamp()}]->(s)
    # 	RETURN p
    # """, la=la, status=la_status).evaluate()

    s = g.run(
        """
    MATCH (la:LearningActivity {uuid:$la})-[x:HAS_STATUS]-(s:Status)
    DELETE x
    WITH DISTINCT la
    MATCH (la:LearningActivity {uuid:$la}), (s:Status {uuid:$status})
    WITH la,s
    CREATE p = (la)-[:HAS_STATUS {created:timestamp()}]->(s)
    RETURN p
  """,
        la=la,
        status=la_status,
    ).evaluate()

    print("p: ", s)

    return api_response(status.OK)


# -----------------------------------------------------------------------
async def la_removeCopyright(request):
    """remove partner from item"""
    print("LA REMOVE COPYTIHGHT")
    item = request.json.get("item")
    copyright = request.json.get("copyright")

    g.run(
        """
        MATCH (i:LearningActivity {uuid:$item})
        MATCH (l:License {uuid:$copyright})
        MATCH (i)-[r:HAS_LICENSE]-(l)
        DELETE r
    """,
        item=item,
        copyright=copyright,
    )

    return api_response(status.OK)


# ------------------------------------------------------------------------
async def la_changeCopyright(request):
    print("LA CHANGE COPYTIHGHT: ", request.json)

    g.run(
        """
        MATCH (c:LearningActivity {uuid:$c})-[x]-(l:License)
        DELETE x
    """,
        c=request.json.get("item"),
    ).evaluate()

    g.run(
        """
        MATCH (i:LearningActivity {uuid:$c})
        MATCH (l:License {uuid:$l})
        CREATE (i)-[:HAS_LICENSE]->(l)

        """,
        c=request.json.get("item"),
        l=request.json.get("copyright"),
    ).evaluate()

    return api_response(status.OK)
