from graph import g
from tic import api_response, status


def index(request):
    print("narratives index in admin/narratives.py")
    collections = g.run("""
        MATCH (c:ViewpointCollection)
        OPTIONAL MATCH (c)-[:OWNS]-(m:Member)
        OPTIONAL MATCH (c)-[:OWNS]-(m:Member)-[]-(p:Partner)
        OPTIONAL MATCH (c)-[:HAS_LICENSE]->(l:License)
        WITH c,l,m,p
        ORDER BY c.ts DESC
        RETURN collect({
            title: c.title,
            text: c.introduction,
            license: l,
            uuid: c.uuid,
            slug: c.slug,
            member: m,
            partner: p,
            created: c.created,
            is_published: c.is_published
        })


    """).evaluate()

    return api_response(status.OK, collections)


def delete(request):
    """delete Collection AND its CollectionItems"""
    print("admin/narratives.py DELETE")
    narrative = request.json.get('uuid')

    # TODO: validate if delete is permitted
    user = request.json.get('user')

    g.run("""
        MATCH (n:Narrative {uuid:$uuid})
        DETACH DELETE n
    """, uuid=narrative)
    return api_response(status.OK)

