# SAS
# Search and Select Europeana integration

from sanic.response import json
from sanic import Blueprint
from ujson import dumps as json_dumps

import requests

from tic import status, api_response
from graph import g

SAS = Blueprint("sas")


class config:
    WSKEY = "VBCZAGGDZI"
    DEBUG = True
    url = "https://api.europeana.eu/api/v2/search.json"


@SAS.route("/get-partner", methods=["POST"])
async def get_partner(request):
    # due to data duplication for now we need to use the uuid
    # ideally we use an unique slug

    uuid = request.json.get("uuid")
    slug = request.json.get("slug")

    sp = g.run(
        """
        MATCH (p:SearchPartner {uuid:{uuid}})-[:HAS_LOGO]->(l:NewUpload)
        RETURN {
            name: p.name,
            text: p.long,
            short: p.short,
            code: p.code,
            logo: '/uploads/'+l.uuid+'_'+l.filename
        } as sp
    """,
        uuid=uuid,
    ).evaluate()
    return api_response(status.OK, sp)


@SAS.route("/get-data-provider", methods=["GET"])
async def get_data_provider(request):

    # search = request.args.get("s", None)

    p = {
        "wskey": config.WSKEY,
        "query": "*",
        "facet": "DEFAULT",
        "profile": "portal",
        "rows": 1,
    }
    r = requests.get(config.url, params=p)

    data = r.json()["facets"]
    names = [
        {"name": x["label"]}
        for x in [b["fields"] for b in data if b["name"] == "DATA_PROVIDER"][0]
    ]
    print("names: ", names)

    return api_response(status.OK, items=names)


@SAS.route("/get", methods=["POST"])
async def get_from_api(request):
    # get a record from the API by it's EU-id ; doing this in our API server hides our Europeana WSKEY to clients
    eu_id = request.json.get("id")
    url = "http://api.europeana.eu/api/v2/record{}.json?wskey={}".format(
        eu_id, config.WSKEY
    )
    r = requests.get(url)
    return api_response(status.OK, r.json())


@SAS.route("/get-best-image", methods=["POST"])
async def get_best_image(request):
    # get a record from the API by it's EU-id ; doing this in our API server hides our Europeana WSKEY to clients
    # find the best image url for this asset in the data and return it's URL to the client
    eu_id = request.json.get("id")
    url = "http://api.europeana.eu/api/v2/record{}.json?wskey={}".format(
        eu_id, config.WSKEY
    )
    r = requests.get(url)
    reply = r.json()
    # print(json_dumps(reply, indent=4))
    obj = reply.get("object")
    image_url = None
    # first try
    # object > aggregations > edmIsShowmBy
    try:
        ag = obj.get("aggregations")
        ag = ag.pop()
        image_url = ag.get("edmIsShownBy")
        if image_url:
            # print("OK object > aggregations > edmIsShowmBy: ", image_url)
            return api_response(status.OK, url=image_url)

    except:
        print("FAILED object > aggregations > edmIsShowmBy")
        pass

    # object > europeanaAggregation > edmPreview
    # this is really the thumb which is always there
    try:
        edm = obj.get("europeanaAggregation")
        image_url = edm["edmPreview"]
        if image_url:
            # print("OK object > europeanaAggregation > edmPreview: ", image_url)
            return api_response(status.OK, url=image_url)

    except:
        print("FAILED object > europeanaAggregation > edmPreview")
        pass

    return api_response(status.OK, url=image_url)


@SAS.route("/browse-partner", methods=["POST"])
async def browse_partner(request):

    partner = request.json.get("partner")
    s = request.json.get("searchFor", "*")
    rows = request.json.get("showMax", 25)
    cursor = request.json.get("cursor", "*")
    print("--> BROWSE : ", partner)

    p = {
        "wskey": config.WSKEY,
        "query": s,
        "facet": "DEFAULT",
        "profile": "portal",
        "rows": rows,
        "cursor": cursor,
        "qf": 'DATA_PROVIDER:"{}"'.format(partner),
    }

    print("p: ", p)
    r = requests.get(config.url, params=p)
    data = r.json()
    print(data)
    return api_response(status.OK, data)


@SAS.route("/search", methods=["GET", "POST"])
async def search(request):
    print("-->SAS SEARCH")
    print(request.json)
    s = request.json.get("searchFor", "*")
    rows = request.json.get("showMax", 25)
    cursor = request.json.get("cursor", "*")
    # cursor = 'AoE+LzkyMDU0L1VSTl9OQk5fU0lfZG9jX1Y3RTRUS09V'
    print("zoek naar: ", s, " cursor: ", cursor)
    p = {
        "wskey": config.WSKEY,
        "query": str(s),
        "facet": "DEFAULT",
        #'profile':  'portal',
        "profile": "minimal",  # was rich
        "rows": rows,
        "media": True,  # grotere media beschikbaar
        "cursor": cursor,
        "type": "IMAGE",
    }

    partner = request.json.get("partner")
    if partner:
        p["qf"] = "DATA_PROVIDER:{}".format(partner)

    print("PARAMS: ", p)

    r = requests.get(config.url, params=p)
    print("RESULT: ", r)
    # print(r.json())

    is_error = False
    error_msg = ""
    try:
        data = r.json()
        print("data:")
    except Exception as e:
        is_error = True
        error_msg = str(e)

    if "error" in data.keys():
        is_error = True
        error_msg = data["error"]

    if is_error:
        return api_response(status.ERROR, msg=error_msg)

    # print("cursor: ", data['nextCursor'])
    # for i in data['items']:
    #     print("link: ", i['link'])
    #     print('id:   ', i['id'])
    #     print("gen:  ", "http://api.europeana.eu/api/v2/record{}.json?wskey={}".format(i['id'], config.WSKEY))

    return api_response(status.OK, data)


if __name__ == "__main__":
    get_data_provider()
