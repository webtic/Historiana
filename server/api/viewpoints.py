from graph import g
from sanic import Blueprint, response
from tic import api_response, status, js_now, get_user_from_session, NEW_UUID
from slugify import slugify
from os.path import dirname


VIEWPOINTS = Blueprint("viewpoints", url_prefix="/api/viewpoints")


# ------------------------------------------------------------------------
@VIEWPOINTS.route("/update-order", methods=["POST"])
async def update_order(request):
    """set values on presenting order"""

    entries = request.json.get('vps')
    print("col: ", request.json.keys())

    collection=request.json.get('uuid')
    for (order, entry) in enumerate(entries):
        print(entry['uuid'])
        res = g.run("""
            MATCH (vp:Viewpoint {uuid:$e})-[o:IN]->(c:ViewpointCollection {uuid:$collection})
            SET o.order = $order
            RETURN o.order
        """,

        e=entry['uuid'],
        collection=collection,
        order=order).evaluate()
        print("res: ", res)


    return api_response(status.OK)



# ------------------------------------------------------------------------
@VIEWPOINTS.route("/delete/collection/", methods=["POST"])
async def delete_collection(request):
    print("** delete_collection ")
    user = request.json.get('user')
    collection = request.json.get('collection')

    if not user:
        print("no user")
        return api_response(status.USER_NOTFOUND)

    if not collection:
        print("no collection")
        return api_response(status.NO_DATA)

    print("user: ", user)
    print("item: ", collection)

    data = g.run("""
        MATCH (m:Member {uuid:$user})-[:OWNS]->(vc:ViewpointCollection {uuid:$uuid})
        DETACH DELETE vc
    """, uuid=collection, user=user)

    return api_response(status.OK)


# ------------------------------------------------------------------------
@VIEWPOINTS.route("/delete/", methods=["POST"])
async def delete(request):

    user = request.json.get('user')
    item = request.json.get('item')

    if not user:
        return api_response(status.USER_NOTFOUND)

    if not item:
        return api_response(status.NO_DATA)

    print("user: ", user)
    print("item: ", item)

    data = g.run("""
        MATCH (m:Member {uuid:$user})-[:OWNS]->(vc:ViewpointCollection)<-[:IN]-(vp {uuid:$uuid})
        DETACH DELETE vp
    """, uuid=item, user=user)

    return api_response(status.OK)



# ------------------------------------------------------------------------
@VIEWPOINTS.route("/toggle/visibility", methods=["POST"])
async def toggle_visibility(request):


    session = request.json.get("session")
    collection = request.json.get("collection")

    if not any([session, collection]):
        return api_response(status.NO_DATA)

    # get user via session
    s = g.run(
        """
        MATCH (s:Session {sessionId:$session})
        OPTIONAL MATCH (s)-[]-(m:Member)
        RETURN {
            valid:s.sessionValid,
            user: m
        }
    """,
        session=session,
    ).evaluate()

    user = s.get("user")

    if not user:
        return api_response(status.USER_NOTFOUND)

    if not user['is_admin']:
        return api_response(status.NOT_ALLOWED)


    p = g.run("""MATCH (p:ViewpointCollection {uuid:$collection}) RETURN p""", collection=collection).evaluate()

    if not p:
        return api_response(status.NO_DATA)


    if 'is_visible' in p.keys():
        p['is_visible'] = not p['is_visible']
    else:
        p['is_visible'] = True

    g.push(p)

    return api_response(status.OK)



# ------------------------------------------------------------------------
@VIEWPOINTS.route("/vpsave", methods=["POST"])
async def save_viewpoint(request):

    # todo: validate ownership
    vp = request.json.get('vp')
    if not vp:
        return api_response(status.NO_DATA)

    vp["slug"] = slugify(vp.get('title',''))

    data = g.run("""
        MATCH (vp:Viewpoint {uuid:$uuid})
        SET
            vp.modified=timestamp(),
            vp.title = $title,
            vp.slug = $slug,
            vp.introduction = $introduction,
            vp.text = $text
        RETURN vp
    """,
        uuid=vp.get("uuid"),
        title=vp.get("title"),
        slug=vp.get("slug"),
        introduction=vp.get("introduction"),
        text=vp.get("text")
    ).evaluate()

    return api_response(status.OK)


# ------------------------------------------------------------------------
@VIEWPOINTS.route("/addtoCollection", methods=["POST"])

async def add_to_collection(request):
    uuid = request.json.get('collection')
    if not uuid:
        return api_response(status.NO_DATA)

    print("#########")
    print("add vp to collection: ", uuid)

    vp = request.json.get('vp')
    collection = request.json.get('collection')
    vp['uuid'] = NEW_UUID()

    print("VP: ", vp)
    print("--")
    print("collection: ", collection)

    vp["slug"] = slugify(vp.get('title',''))

    data = g.run("""
        MATCH (vpc:ViewpointCollection {uuid:$collection})
        CREATE (vp:Viewpoint)-[:IN]->(vpc)
        SET vp.uuid = $uuid,
        vp.title = $title,
        vp.slug = $slug,
        vp.introduction = $introduction,
        vp.text = $text
        RETURN vp
    """,
        uuid=vp.get("uuid"),
        title=vp.get("title"),
        slug=vp.get("slug"),
        introduction=vp.get("introduction"),
        text=vp.get("text"),
        collection=collection
    ).evaluate()

    # data = g.run("""
    #   MATCH (vpc:ViewpointCollection {uuid:$uuid})
    #   OPTIONAL MATCH (vpc)-[x:THUMB_FOR]-(icon)
    #   DETACH DELETE x
    # """, uuid=uuid).evaluate()

    return api_response(status.OK)



# ------------------------------------------------------------------------
@VIEWPOINTS.route("/deleteIcon", methods=["POST"])

async def deleteIcon(request, slug=None):
    uuid = request.json.get('uuid')
    if not uuid:
        return api_response(status.NO_DATA)

    data = g.run("""
        MATCH (vpc:ViewpointCollection {uuid:$uuid})
        OPTIONAL MATCH (vpc)-[x:THUMB_FOR]-(icon)
        DETACH DELETE x
    """, uuid=uuid).evaluate()


    return api_response(status.OK)



# ------------------------------------------------------------------------
@VIEWPOINTS.route("/xslug/<slug>", methods=["GET"])
async def xget_by_slug(request, slug=None):
    print("get for slug; ", slug)
    data = g.run("""
        MATCH (vpc:ViewpointCollection {slug:$slug})
        OPTIONAL MATCH (vpc)<-[:OWNS]-(m:Member)
        OPTIONAL MATCH (vpc)<-[o:IN]-(vp:Viewpoint)
        OPTIONAL MATCH (vpc)-[:THUMB_FOR]-(icon)

        WITH vpc {
            .*,
            owner: m.uuid,
            vps: collect(vp),
            iconic: '/ua/'+icon.path
        } AS vp,o

        ORDER BY o.order
        RETURN collect(vp)


    """, slug=slug).evaluate()


# ------------------------------------------------------------------------
@VIEWPOINTS.route("/slug/<slug>", methods=["GET"])
async def get_by_slug(request, slug=None):
    print("get for slug; ", slug)
    data = g.run("""
        MATCH (vpc:ViewpointCollection {slug:$slug})
        OPTIONAL MATCH (vpc)<-[:OWNS]-(m:Member)
        OPTIONAL MATCH (vpc)<-[o:IN]-(vp:Viewpoint)
        OPTIONAL MATCH (vpc)-[:THUMB_FOR]-(icon)
        WITH vp, m, icon, vpc
        ORDER BY o.order
        WITH collect(vp)  AS vps, m, icon, vpc


        RETURN vpc {
            .*,
            owner: m.uuid,
            vps: vps,
            iconic: '/ua/'+icon.path
        } AS vp




    """, slug=slug).evaluate()

    #return api_response(status.OK, data.pop())
    return api_response(status.OK, data)


# ------------------------------------------------------------------------
@VIEWPOINTS.route("/my", methods=["POST"])
async def my_viewpoints_index(request):
    print("** my_viewpoints_index")
    # return all ViewppintCollections for the index
    member = request.json.get('user')
    if not member:
        return api_response(status.USER_NOTFOUND)


    # iconic value is not used at the moment, is a mental reminder for the syntax
    data = g.run("""
        MATCH (m:Member {uuid:$member})
        MATCH (vp:ViewpointCollection)-[:OWNS]-(m:Member)
        OPTIONAL MATCH (vp)-[:THUMB_FOR]-(icon)
        WITH vp {
            .*,
            owner: m.uuid,
            icon: icon,
            iconic: '/ua/'+icon.path,
            url: '/viewpoints/'+vp.slug
        } AS entry
        RETURN collect(entry)

        """, member=member).evaluate()
    return api_response(status.OK, data)



# ------------------------------------------------------------------------
@VIEWPOINTS.route("/", methods=["GET"])
async def index(request):
    # return all ViewppintCollections for the index

    # iconic value is not used at the moment, is a mental reminder for the syntax
    data = g.run("""
        MATCH (vp:ViewpointCollection)
        OPTIONAL MATCH (vp)-[:OWNS]-(m:Member)
        OPTIONAL MATCH (vp)-[:THUMB_FOR]-(icon)
        WITH vp {
            .*,
            owner: m.uuid,
            icon: icon,
            iconic: '/ua/'+icon.path,
            url: '/viewpoints/'+vp.slug
        } AS entry
        RETURN collect(entry)

        """).evaluate()
    return api_response(status.OK, data)


# ------------------------------------------------------------------------
@VIEWPOINTS.route("/add", methods=["POST"])
async def add(request):
    print("** ADD VIEWPOINT")

    vp = request.json.get("data")
    sess = request.json.get("session")

    if not any([vp,sess]):
        return api_response(status.NO_DATA)

    print("get: ", vp)
    user = get_user_from_session(sess)
    print("user: ", user)
    uuid = NEW_UUID()
    if user['session_valid']:
        print("CREATE VIEWPOINT")

        vp = g.run("""
            MATCH (u:Member {uuid:$user})
            CREATE (vp:ViewpointCollection)<-[:OWNS]-(u)
            SET vp.uuid=$uuid,
                vp.slug=$slug,
                vp.title=$title,
                vp.introduction=$introduction,
                vp.text=$text,
                vp.created=timestamp()
            """,
            uuid=uuid,
            slug=slugify(vp['title']),
            user=user['user']['uuid'],
            title=vp.get('title'),
            introduction=vp.get('introduction'),
            text=vp.get('text')
            )



    return api_response(status.OK)


# ------------------------------------------------------------------------
@VIEWPOINTS.route("/edit", methods=["POST"])
async def edit(request):


    vp = request.json.get("data")
    sess = request.json.get("session")

    if not any([vp,sess]):
        return api_response(status.NO_DATA)

    user = get_user_from_session(sess)

    if user['session_valid']:
        print("CREATE VIEWPOINT")


        vp = g.run("""
            MATCH (u:Member {uuid:$user})-[:OWNS]->(vp:ViewpointCollection {uuid:$uuid})
            SET
                vp.title=$title,
                vp.slug=$slug,
                vp.introduction=$introduction,
                vp.text=$text,
                vp.modified=timestamp()
            """,
            uuid=vp['uuid'],
            slug=slugify(vp.get('title')),
            user=user['user']['uuid'],
            title=vp.get('title'),
            introduction=vp.get('introduction'),
            text=vp.get('text')
            )


    return api_response(status.OK)
