

from graph import g
from sanic import Blueprint, response
from tic import new_api_response, status
from .europeana import europeana_search
import datetime

SEARCH = Blueprint("search", url_prefix="/api/search")

# old generic code for site-wide search
@SEARCH.route("/eu", methods=["GET"])
async def eusearch(request):

    searchFor = request.args.get('searchFor','').lower()
    member = request.args.get('uuid')
    search_europeana = request.args.get('eu', True)

    eu = await europeana_search(searchFor) if search_europeana else False

    print(f"** SEARCH FOR {searchFor} / eu: {eu}")

    types = g.run("""
        MATCH (n:CollectionItem) WHERE toLower(n.title) contains $s
        RETURN collect(distinct labels(n)[0])
    """, s=searchFor).evaluate()

    print("******* ", types)


    # res = g.run("""
    #     MATCH (n:CollectionItem) WHERE toLower(n.title) contains $s
    #     WITH n, collect(distinct labels(n)[0]) as types
    #     RETURN collect({type: labels(n)[0], node:n})
    # """, s=searchFor).evaluate()


    # for now we only search CollectionItems but present them to the UI as a source
    # so they get formatted correctly within the <Card>

    res = g.run("""
        MATCH (n:CollectionItem) WHERE toLower(n.title) contains $s
        OPTIONAL MATCH (n)-[:IN_COLLECTION]-(c:Collection)
        WITH c, n, collect(distinct labels(n)[0]) as types
        RETURN collect({type: 'Source', csstype:'source', node:n, collection:c})
    """, s=searchFor).evaluate()


    types = sorted(types)
    hcount = len(res)

    # add Europeana results if there are any
    if eu:
        types.append('Europeana')
        for item in eu:
            res.append(dict(type="Source", csstype="euresult", node=item.to_dict()))


    reply = {
        'searchstring': searchFor,
        'count': hcount,
        'eucount': len(eu) if eu else 0,
        'data' : res,
        'types': types
    }


    return new_api_response(status.OK, reply)

# ==================================================================================================
@SEARCH.route("/", methods=["GET"])
async def search(request):
    searchFor = request.args.get('searchFor','').lower()
    member = request.args.get('member')
    search_europeana = request.args.get('eu', False)

    eu = await europeana_search(searchFor) if search_europeana else False

    print(f"** SEARCH FOR {searchFor} / eu: {eu}")


    # res = g.run("""
    #     MATCH (n:CollectionItem) WHERE toLower(n.title) contains $s
    #     WITH n, collect(distinct labels(n)[0]) as types
    #     RETURN collect({type: labels(n)[0], node:n})
    # """, s=searchFor).evaluate()


    # for now we only search CollectionItems but present them to the UI as a source
    # so they get formatted correctly within the <Card>
    types = []
    dataset = []

    # dataset.append( dict(
    #     type='LearningActivity',
    #     uuid=42, title="ELA TEST",
    #     url="//xs4all.nl",
    #     thumb=dict(url="http://localhost:8080/static/img/Homepage/ww1.jpg", uuid=43),
    #     text="This is the text for this learning activity (teaser)",
    #     owner=dict(name="demo", uuid="xxxx"),
    #     related=[]
    # ))

    # --------------------
    # MySources
    res = g.run("""
        MATCH (n:MySources) WHERE toLower(n.title) contains $s
        OR n.description contains $s
        OPTIONAL MATCH (n)-[:SOURCE]-(m:Member)
        OPTIONAL MATCH (n)-[:HAS_CONTENT_TYPE]-(ctype:ContentType)
        OPTIONAL MATCH (n)-[:IsAsset]-(asset:UserAsset)

        WITH [] as related, n,m,ctype, asset
        RETURN collect({
            type: 'Source',
            content_type: CASE ctype WHEN null THEN {name:null,icon:null, uuid:null} ELSE ctype END,
            thumb: CASE asset WHEN null THEN {xxuuid: n.uuid, url: n.url} ELSE {uuid:asset.uuid, url:asset.thumb_url} END,
            related:related,
            uuid: n.uuid,
            title: n.title,
            text: n.description,
            url: n.url,
            owner:{uuid:m.uuid, name:m.name}})
    """, s=searchFor).evaluate()

    if res:
        types.append('Source')
        for e in res:

            if isinstance(e['text'], list):
                e['text'] = " ".join(e['text'])

            dataset.append(e)


    # --------------------
    # CollectionItems
    res = g.run("""
        MATCH (n:CollectionItem) WHERE toLower(n.title) contains $s
        OR n.description contains $s
        OPTIONAL MATCH (n)-[:IN_COLLECTION]-(c:Collection)-[:HAS_PARTNER]-(p:Partner)
        OPTIONAL MATCH (n)-[:HAS_CONTENT_TYPE]-(ctype:ContentType)

        WITH { type: 'Collection', uuid: c.uuid, title: c.name } as related, p, c, n, ctype
        RETURN collect({
            uuid: n.uuid,
            type: 'CollectionItem',
            related:related,
            title: n.title,
            text: n.description,
            url: n.url,
            thumb: { uuid: n.uuid, url: n.url},
            owner:{uuid:p.uuid, name:p.name, icon:p.icon},
            content_type: CASE ctype WHEN null THEN {name:null,icon:null, uuid:null} ELSE ctype END

        })
    """, s=searchFor).evaluate()

    if res:
        types.append('CollectionItem')
        for e in res:
            # cleanup empty records
            if not e['related']['uuid']:
                print("empty: ", e)
                e['related'] = []
            dataset.append(e)


    # --------------------
    # Collection
    res = g.run("""
        MATCH (n:Collection) WHERE toLower(n.name) contains $s
        OR n.description contains $s
        OPTIONAL MATCH (n)-[:IN_COLLECTION]-(s:CollectionItem)
        OPTIONAL MATCH (n)-[:HAS_PARTNER]-(p:Partner)
        OPTIONAL MATCH (n)-[:THUMB_FOR]-(thumb)
        WITH collect({uuid: s.uuid, title: s.title, url: s.url, text:s.description}) as sources, n, thumb, p

        RETURN collect({
            type: 'Collection',
            acknowledgements: n.acknowledgements,
            title: n.name,
            text: n.description,
            url: n.slug,
            uuid: n.uuid,
            thumb: { uuid: thumb.uuid, url: '/ua/'+thumb.path},
            owner:{uuid:p.uuid, name:p.name, icon:p.icon},
            sources: sources
        })
    """, s=searchFor).evaluate()

    if res:
        types.append('Collection')
        for e in res:
            dataset.append(e)


    # --------------------
    # Narrative
    res = g.run("""
        MATCH (n:Narrative) WHERE toLower(n.title) contains $s
        OR n.teaser contains $s
        OPTIONAL MATCH (n)-[:HAS_PARTNER]-(p:Partner)
        OPTIONAL MATCH (n)-[:THUMB_FOR]-(thumb)
        WITH n, thumb, p

        RETURN collect({
            type: 'Narrative',
            acknowledgements: n.acknowledgement,
            title: n.title,
            text: n.teaser,
            url: n.slug,
            uuid: n.uuid,
            thumb: { uuid: thumb.uuid, url: '/ua/'+thumb.path},
            owner:{uuid:p.uuid, name:p.name, icon:p.icon}
        })
    """, s=searchFor).evaluate()

    if res:
        types.append('Narrative')
        for e in res:
            dataset.append(e)


    # --------------------
    # ViewpointCollection
    res = g.run("""
        MATCH (n:ViewpointCollection) WHERE toLower(n.title) contains $s
        OR n.text contains $s
        OR n.introduction contains $s
        OPTIONAL MATCH (n)-[:HAS_PARTNER]-(p:Partner)
        OPTIONAL MATCH (n)-[:THUMB_FOR]-(thumb)
        WITH n, thumb, p

        RETURN collect({
            type: 'ViewpointCollection',
            title: n.title,
            text: n.text,
            url: '/viewpoints/'+n.slug,
            uuid: n.uuid,
            thumb: { uuid: thumb.uuid, url: '/ua/'+thumb.path},
            owner:{uuid:p.uuid, name:p.name, icon:p.icon}
        })
    """, s=searchFor).evaluate()

    if res:
        types.append('ViewpointCollection')
        for e in res:
            dataset.append(e)



    # --------------------
    # LearningActivity
    res = g.run("""
        MATCH (n:LearningActivity) WHERE toLower(n.title) contains $s
        OR n.intro contains $s
        OR n.summary contains $s

        OPTIONAL MATCH (n)-[:HAS_PARTNER]-(p:Partner)
        OPTIONAL MATCH (n)-[:OWNS]-(m:Member)
        OPTIONAL MATCH (n)-[:ICONIC_IMAGE]-(thumb)
        WITH n, thumb, p

        RETURN collect({
            type: 'LearningActivity',
            acknowledgements: n.acknowledgements,
            title: n.title,
            text: n.intro,
            url: '/learning-activity/'+n.slug,
            uuid: n.uuid,
            thumb: { uuid: thumb.uuid, url: '/objects/'+thumb.uuid+'/'+thumb.filename},
            owner:{uuid:p.uuid, name:p.name, icon:p.icon}
        })
    """, s=searchFor).evaluate()

    if res:
        types.append('LearningActivity')
        for e in res:
            dataset.append(e)


    # clean up by unique url for now
    urls = []
    for d in dataset:
        if d['url'] in urls:
            d['dup'] = True


        else:
            urls.append(d['url'])



    # types = sorted(types)
    hcount = len(dataset)

    # add Europeana results if there are any
    # if eu:
    #     types.append('Europeana')
    #     for item in eu:
    #         res.append(dict(type="Source", csstype="euresult", node=item.to_dict()))


    reply = {
        'searchstring': searchFor,
        'count': hcount,
        'eucount': len(eu) if eu else 0,
        'data' : [d for d in dataset if not d.get('dup', False)],
        'types': types
    }


    return new_api_response(status.OK, reply)


