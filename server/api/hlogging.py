#
# Historiana Logging module
#

from sanic import Blueprint, response
from tic import api_response, status, js_now, NOW
from slugify import slugify
from os.path import dirname
from graph import g

LOG = Blueprint("log", url_prefix="/api/log")

# ------------------------------------------------------------------------
@LOG.route("/notify-webmaster", methods=["POST"])
async def notify_webmaster(request):
	print(">>> ", request.ip)
	print(dir(request))

	data = request.json
	print("data:", data)

	g.run("""
		CREATE (e:EventLog)
		SET
			e.ts=datetime(),
			e.remote_addr=$ip,
			e.msg=$msg,
			e.source='notify-webmaster'
	""", dict(ip=request.ip, msg=data.get('msg')))

	return api_response(status.OK, data)

