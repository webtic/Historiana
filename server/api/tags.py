from graph import g
from sanic import Blueprint, response
from py2neo import Node, Relationship
from tic import new_api_response, status, create_slug
from slugify import slugify

import uuid
import datetime
import json
import urllib.parse

TAGS = Blueprint("tags", url_prefix="/api/tags")


def is_uuid(input_str):
    try:
        uuid_obj = uuid.UUID(input_str)
        return True
    except ValueError:
        return False



#######################################################################
@TAGS.get("/item/<_uuid:str>")
async def get_tags_for_item(request, _uuid):

    tags = g.run("""
        MATCH (n {uuid:$uuid})-[rel:HTAGGED]-(x:Tag)
        RETURN collect(x)
    """, uuid=_uuid).evaluate()

    return new_api_response(status.OK, dict(data=tags))





#######################################################################
@TAGS.post("/setItem")
async def set_tags_for_item(request):
    """set the tags for a specific item"""
    _uuid = request.json.get('uuid') # item
    _tags = request.json.get('tags')

    print(">> TAGS.setItem")

    print("uuid: ", _uuid)
    print("tags to set: ", _tags)

    # remove current Historiana tags
    g.run("""
        MATCH (n {uuid:$uuid})-[rel:HTAGGED]-(x:Tag)
        DETACH DELETE rel

    """, uuid=_uuid).evaluate()

    for tag in _tags:
        print("create tag", tag)
        g.run("""
            MATCH (n {uuid:$uuid})
            MATCH (tag:Tag {uuid:$tag})
            CREATE (n)-[:HTAGGED]->(tag)
            RETURN n
        """, uuid=_uuid, tag=tag).evaluate()


    return new_api_response(status.OK, dict(data={}))


#######################################################################
@TAGS.get("/all/<parent:str>")
def get_all_tags_as_tree(request, parent='Historiana'):
    print("### GET TAGS FOR ", parent)
    # {name:'Teaching & Learning'}
    data = g.run("""
        MATCH p = (t:Tag {name:$parent})-[:CHILD*1..3]-(x)
        WITH p,t,x
        ORDER BY lower(t.name), lower(x.name)
        WITH collect(p) as paths
        CALL apoc.convert.toTree(paths)
        YIELD value
        RETURN value
    """, parent=parent).evaluate()

    # print("### TAGS FOUND ", data)

    # skip root; return first child
    return new_api_response(status.OK, dict(data=data))
    #return new_api_response(status.OK, dict(data=data['child']))


#######################################################################
@TAGS.get("/lookup")
def lookup(request):
    s = request.args.get("s")

    print("lookup: ", s)
    data = g.run("""
        MATCH (t:Tag) WHERE t.name contains $search
        WITH t ORDER BY t.name
        OPTIONAL MATCH (t)-[:CHILD]->(c:Tag)
        OPTIONAL MATCH (t)-[:THUMB_FOR]-(asset)
        WITH c,t,asset
        ORDER BY c.name
        WITH collect({label: c.name, value:c.uuid, text:c.text}) as children, t, asset
        RETURN collect(
            distinct
            {
                uuid: t.uuid,
                name:t.name,
                text:t.text,
                children: children,
                iconic: {url:'/ua/'+asset.path, uuid:asset.uuid}
            })
    """, search=s).evaluate()

    return new_api_response(status.OK, dict(data=data))

#######################################################################
@TAGS.get("/")
@TAGS.get("/get/<tag>", name="get-by-tag")
def get_index(request, tag=None):

    if tag:
        tag = urllib.parse.unquote(tag)
        print("Get childs of: ", tag)


        # geeft lijst van specifieke node met childs
        # alle childs 2 niveaus diep krijg je door [:CHILD*1..2]
        # en  [:CHILD*] geeft alle niveaus
        # maar zonder de hierarchie.
        # zie bijv https://gist.github.com/jexp/5c1092933781ec4ea2a3
        # of de documentatie voor apoc.convert.toTree
        # https://neo4j.com/apoc/4.3/overview/apoc.convert/apoc.convert.toTree/
        #
        data = g.run("""
            MATCH (t:Tag {name:$tag}) WITH t ORDER BY t.name
            OPTIONAL MATCH (t)-[:CHILD]->(c:Tag)
            OPTIONAL MATCH (t)-[:THUMB_FOR]-(asset)
            WITH c,t,asset
            ORDER BY c.name
            WITH collect({label: c.name, value:c.uuid, text:c.text}) as children, t, asset
            RETURN distinct
                {
                    value: t.uuid,
                    label: t.name,
                    text: t.text,
                    children: children,
                    iconic: {url: '/ua/'+asset.path, uuid:asset.uuid}
                }
        """, tag=tag).evaluate()
    else:
        # get list of nodes; first WHERE avoids repeating child nodes in the root
        data = g.run("""
            MATCH (t:Tag) WITH t ORDER BY t.name
            WHERE NOT (t)<-[:CHILD]-()
            OPTIONAL MATCH (t)-[:CHILD]->(c:Tag)
            OPTIONAL MATCH (t)-[:THUMB_FOR]-(asset)
            WITH c,t,asset
            ORDER BY c.name
            WITH collect(c) as children, t, asset
            RETURN collect(
                distinct
                {
                    uuid: t.uuid,
                    name:t.name,
                    text:t.text,
                    children: children,
                    iconic: {url:'/ua/'+asset.path, uuid:asset.uuid}
                })
        """).evaluate()

    return new_api_response(status.OK, dict(data=data))



@TAGS.route("/create", methods=["POST"])
def create(request):
    """simple tag create; use just the string, this is the root-level + in Admin/Tags"""

    _tag = request.json.get('tag','').lower().strip()
    _member = request.json.get('member')

    if not _tag:
        return new_api_response(status.NO_DATA)

    if not _member:
        return api.response(status.USER_NOTFOUND)


    # we do system-wide tags; ignore member for now...
    # print(f"tag: {_tag} create by member: {_member}")

    data = g.run("""
        MERGE (t:Tag {name:$_tag})
        ON CREATE
            SET t.created = timestamp()
            SET t.uuid = apoc.create.uuid()
        RETURN t


    """, _tag=_tag).evaluate()


    return new_api_response(status.OK, dict(data=data))

#######################################################################
@TAGS.route("/add", methods=["POST"])
def add(request):

    _tag = request.json.get('tag','').strip()
    _member = request.json.get('member')
    _parent = request.json.get('parent')

    if not _tag:
        return new_api_response(status.NO_DATA)

    if not _member:
        return api.response(status.USER_NOTFOUND)


    print(f"tag: {_tag} create by member: {_member}")

    # we do system-wide tags; ignore member for now...

    # data = g.run("""
    #     MERGE (t:Tag {name:$_tag})
    #     ON CREATE
    #         SET t.created = timestamp()
    #         SET t.uuid = apoc.create.uuid()
    #     RETURN t
    # """, _tag=_tag).evaluate()

    data = g.run("""
        CREATE (t:Tag {name:$_tag})
        SET t.created = timestamp()
        SET t.uuid = apoc.create.uuid()
        RETURN t
    """, _tag=_tag).evaluate()

    # _parent is either a uuid or a string (Historiana or partner)
    if _parent=='Historiana':
        _parent = g.run("MATCH (t:Tag {name:$name}) RETURN t.uuid", name=_parent).evaluate()

    if not is_uuid(_parent):
        # neither a normal uuid or the string Historiana
        # so this must be a partner code; look it up and get its uuid

        # make sure the root exists
        root = g.run("MATCH (t:Tag {name:$name}) RETURN t.uuid", name=_parent).evaluate()
        print("root for ",_parent, "  is ", root)

        if not root:
            print(f"root {_parent} not found {root} ; create!")
            create_root = g.run("""
                CREATE (t:Tag {name:$name})
                SET t.uuid=apoc.create.uuid()
                RETURN t.uuid""", name=_parent).evaluate()
        else:
            _parent = root


    if _parent:
        # create child relation
        print(f"create tag relation to {_parent}")
        print("data: ", data)
        rel = g.run("""
            MATCH (t:Tag {uuid:$parent})
            MATCH (c:Tag {uuid:$child})
            CREATE (t)-[:CHILD]->(c)
        """, parent=_parent, child=data['uuid']).evaluate()



    return new_api_response(status.OK, dict(data=data))


#######################################################################
# DELETE TAG
@TAGS.route("/delete", methods=["POST"])
def delete_tag(request):
    _tag = request.json.get('tag')
    _member = request.json.get('member')

    print("DELETE TAG ", _tag)

    g.run("""
        MATCH (c:Tag {uuid:$tag})
        DETACH DELETE c
    """, tag=_tag)

    return new_api_response(status.OK, dict())




#######################################################################
# DELETE ICONIC IMAGE FOR TAG
@TAGS.route("/delete-iconic", methods=["POST"])
def delete_iconic(request):
    _tag = request.json.get('tag')
    _member = request.json.get('member')


    """delete the thumb button"""
    g.run("""
        MATCH (c:Tag {uuid:$tag})-[t:THUMB_FOR]-(x)
        DELETE t
    """, tag=_tag)

    return new_api_response(status.OK, dict())



#######################################################################
# UPDATE TAGS
@TAGS.route("/update", methods=["POST"])
def update(request):
    """update props for a tag, the icon is done seperately"""

    _tag = request.json.get('tag')
    _member = request.json.get('member')
    tag = g.run("MATCH (t:Tag {uuid:$tag}) RETURN t ", tag=_tag.get('uuid')).evaluate()
    tag['text'] = _tag.get('text')
    tag['name'] = _tag.get('name').strip()
    tag['modified'] = datetime.datetime.now()
    g.push(tag)
    return new_api_response(status.OK, dict())


#######################################################################
@TAGS.route("/get-root/<parent:str>", methods=["GET"])
def _get_root(request, parent):

    # data = g.run("""
    #     MATCH p = (t:Tag {name:$parent})-[:CHILD*1]-(x)
    #     WITH p,t,x
    #     ORDER BY t.name, x.name
    #     WITH collect(p) as paths
    #     CALL apoc.convert.toTree(paths)
    #     YIELD value
    #     RETURN value
    # """, parent=parent).evaluate()


    data = g.run("""
        MATCH (t:Tag {name:$parent})-[:CHILD*1]-(x)
        OPTIONAL MATCH (x)-[:HTAGGED]-(anything)
        WITH x, count(anything) as cnt
        ORDER BY x.name
        RETURN collect({name:x.name, uuid:x.uuid, count:cnt})
    """, parent=parent).evaluate()

    # print("### TAGS FOUND ", data)

    # skip root; return first child
    return new_api_response(status.OK, dict(data=data))




