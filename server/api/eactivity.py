from graph import g
from sanic import Blueprint, response
from tic import api_response, status, NEW_UUID
from slugify import slugify
from os.path import dirname
from ujson import dumps as json_dumps

# for add-to-my-historiana
import tempfile
import os

EACTIVITY = Blueprint("eactivity", url_prefix="/api/ea")


# ------------------------------------------------------------------------
@EACTIVITY.route("/")
def ea_test(request):
    return api_response(status.OK, "Hello from /api/ea")


# ------------------------------------------------------------------------
@EACTIVITY.route("/student/start", methods=["POST"])
def ea_start(request):
    """start an activity, register new as new member if needed"""

    share = request.json.get("share")
    email = request.json.get("email", "").lower()

    if not email or not share:
        return api_response(status.NO_DATA)

    # is email address known?
    member_exists = g.run(
        "MATCH (m:Member) WHERE m.email=toLower({email}) RETURN m", dict(email=email)
    ).evaluate()

    is_new_member = False if member_exists else True
    if member_exists:
        # get uuid for existing member
        member = member_exists["uuid"]

    else:
        # create new member, get uuid
        member = NEW_UUID()
        m = g.run(
            """
            CREATE (m:Member)
            SET
                m.uuid = $member,
                m.email = $email,
                m.created = timestamp(),
                m.memo = 'ea_register',
                m.can_create=false,
                m.is_teacher=false,
                m.is_staff=false,
                m.is_confirmed=false,
                m.joined=timestamp()
            RETURN m
        """,
            dict(member=member, email=email),
        ).evaluate()

    # get activity for the share-uuid
    activity = g.run(
        """
        MATCH (share:MyShares {uuid:$share})<-[SHARED]-(a:MyActivity)
        OPTIONAL MATCH (a)-[r:BLOCK]-(b:MyBlocks)
        OPTIONAL MATCH (ua:UserAsset)-[]-(a)
        WITH collect(b) as blocks, a, ua
        RETURN
            {
                activity: properties(a),
                image: CASE ua WHEN ua THEN collect(properties(ua))[0] ELSE {} END,
                blocks: blocks
            }
    """,
        dict(share=share),
    ).evaluate()

    # get existing answer if any
    answer = g.run(
        """
        MATCH (s:MyShares {uuid:$share})
        MATCH (m:Member {uuid:$member})
        MATCH (s)-[:ANSWERS]->(sa:StudentAnswers)-[:ANSWERS]->(m)
        RETURN COLLECT (sa)[0] as answers
    """,
        dict(share=share, member=member),
    ).evaluate()

    # create an empty StudentAnswers
    if not answer:
        answer_uuid = NEW_UUID()
        answer = g.run(
            """
            MATCH (s:MyShares {uuid:$share})
            MATCH (m:Member {uuid:$member})
            CREATE (s)-[:ANSWERS]->(sa:StudentAnswers)-[:ANSWERS]->(m)
            SET
                sa.uuid={answer},
                sa.created=timestamp(),
                sa.memo="created by student/start",
                sa.answers='{}',
                sa.submitted=false

            RETURN sa as answers
        """,
            dict(share=share, member=member, answer=answer_uuid),
        ).evaluate()

    # BUG: here we somehow ADD the data to the status.OK object

    try:
        thumb = f"/ua/{activity['image']['path']}"
    except Exception:
        thumb = ''
        pass

    data = dict(
        user=member,
        answer=answer,
        activity=activity["activity"],
        thumb=thumb,
        blocks=activity["blocks"],
        isNewMember=is_new_member,
    )

    return api_response(status.OK, result=data)


# ------------------------------------------------------------------------
@EACTIVITY.route("/share/view", methods=["POST"])
def ea_shareview(request):
    """start an activity, register new as new member if needed"""

    share = request.json.get("share")

    # get activity for the share-uuid
    activity = g.run(
        """
        MATCH (share:MyShares {uuid:$share})<-[SHARED]-(a:MyActivity)
        OPTIONAL MATCH (a)-[r:BLOCK]-(b:MyBlocks)
        OPTIONAL MATCH (ua:UserAsset)-[]-(a)
        OPTIONAL MATCH (m:Member)<-[:IS_OWNER]-(a)
        WITH collect(b) as blocks, a, ua, m
        RETURN
            {
                owner: m.uuid,
                activity: properties(a),
                image: CASE ua WHEN ua THEN collect(properties(ua))[0] ELSE {} END,
                blocks: blocks
            }
    """,
        dict(share=share),
    ).evaluate()

    # if not found via share-uuid try it as activity-uuid
    if not activity:
        activity = g.run(
            """
            MATCH (a:MyActivity {uuid:$uuid})
            OPTIONAL MATCH (a)-[r:BLOCK]-(b:MyBlocks)
            OPTIONAL MATCH (ua:UserAsset)-[]-(a)
            OPTIONAL MATCH (m:Member)<-[:IS_OWNER]-(a)
            WITH collect(b) as blocks, a, ua, m
            RETURN
                {
                    owner: m.uuid,
                    activity: properties(a),
                    image: CASE ua WHEN ua THEN collect(properties(ua))[0] ELSE {} END,
                    blocks: blocks
                }
        """,
            uuid=share,
        ).evaluate()

    if not activity:
        return api_response(status.NO_DATA)

    try:
        thumb = f"/ua/{activity['image']['path']}"
    except Exception:
        thumb = ''
        pass

    data = dict(
        owner=activity["owner"],
        activity=activity["activity"],
        thumb=thumb,
        blocks=activity["blocks"],
    )

    return api_response(status.OK, result=data)


# ------------------------------------------------------------------------
@EACTIVITY.route("/student/save", methods=["OPTIONS", "POST"])
async def save_student_answers(request):
    """save the answers given on a start-activity"""
    # print("** SAVE STUDENT ANSWERS **")
    r = request.json
    uuid = r.get("sa", None)
    answers = json_dumps(r.get("answers", None))

    if not all([uuid, answers]):
        return api_response(status.NO_DATA)

    g.run(
        """
        MATCH (sa:StudentAnswers {uuid:$uuid})
        SET
            sa.answers={answers},
            sa.updated=timestamp(),
            sa.submitted=false

    """,
        uuid=uuid,
        answers=answers,
    )

    # print(">>>>>>>> ", status.OK)
    # BUG: ea/student/start adds data to status.OK, wipe it here
    return api_response(status.OK, result="")


# ------------------------------------------------------------------------
@EACTIVITY.route("/student/submit", methods=["OPTIONS", "POST"])
async def submit_student_answers(request):
    """save the answers given on a start-activity"""
    # print("** SAVE STUDENT ANSWERS **")
    r = request.json
    uuid = r.get("sa", None)
    answers = json_dumps(r.get("answers", None))

    if not all([uuid, answers]):
        return api_response(status.NO_DATA)

    sa = g.run(
        """
        MATCH (sa:StudentAnswers {uuid:$uuid})
        SET
            sa.answers={answers},
            sa.updated=timestamp(),
            sa.submitted = true
        RETURN sa
    """,
        uuid=uuid,
        answers=answers,
    ).evaluate()

    # print(">>>>>>>> ", status.OK)
    # BUG: ea/student/start adds data to status.OK, wipe it here
    return api_response(status.OK, record=sa)


# ------------------------------------------------------------------------
# @EACTIVITY.route("/add", methods=["OPTIONS", "POST"])
# async def add_to_my_historiana(request):
#     """add an eActivity to My Historiana"""
#     activity_uuid = request.json.get("activity")
#     member_uuid = request.json.get("member")

#     if not all([activity, member]):
#         return api_response(status.NO_DATA)

#     g.run(
#         """
#         MATCH (a:MyActivity {uuid:$activity_uuid})
#         MATCH (m:Member {uuid:$member_uuid})

#     """,
#         dict(activity_uuid=activity_uuid, member_uuid=member_uuid),
#     ).evaluate()


# ------------------------------------------------------------------------
@EACTIVITY.route("/add-to-my-historiana", methods=["POST"])
async def add_to_my_historiana(request):

    # call apoc.export.cypher.query("MATCH p=(a:Test)-[]-()
    # RETURN p","/tmp/thefile",{format:'plain',cypherFormat:'updateStructure'})
    # copy an object to the user specified.

    activity_uuid = request.json.get("activity")
    member_uuid = request.json.get("member")
    new_uuid = NEW_UUID()

    print("** add to my historiana, existing: ", activity_uuid)
    print("this will become: ", new_uuid)
    print("for member: ", member_uuid)

    if not all([activity_uuid, member_uuid]):
        return api_response(status.NO_DATA)

    # get the import directory from the Neo4J config
    q = "CALL dbms.listConfig() yield name,value where name='dbms.directories.import' return value+'/'"
    prefix = g.run(q).evaluate()

    # create temp file for Neo4J to write its Cypher export to
    # we want strings, not the default bytes
    f = tempfile.NamedTemporaryFile(
        mode="w+", prefix=prefix
    )
    os.chmod(f.name, 0o666)
    filename = os.path.basename(f.name)

    # first we get the activity and its blocks as Cypher statements using APOC
    # the result is written to the tempfile
    q = f"""
        CALL apoc.export.cypher.query(
            "MATCH p=(a:MyActivity {{uuid:'{activity_uuid}'}})-[:BLOCK]-()
            RETURN p","{filename}",{{format:'plain',cypherFormat:'create'}}
        )
        """

    # print("===================================")
    # print("q: ", q)

    try:
        r = g.run(q)
    except Exception as e:
        #print*("ERROR ON EXPORT")
        #print("R: ", r)
        return api_response(status.ERROR, msg=str(e))

    # temp file is now filled with Cypher statements for the existing card
    # now execute the Cypher to create the new objects; the Activity and its blocks
    # this way a new copy is made, we do the image via a relation later.

    lines = []
    for line in f.readlines():
        # replace the old uuid with a new one for this user
        cmd = line.replace(activity_uuid, new_uuid)
        lines.append(cmd)

    # print(60 * '-')
    # print("code: ", "".join(lines))
    # print(60 * '-')

    # write new version to a file for Neo4J to read
    # we want strings, not the default byte
    with tempfile.NamedTemporaryFile(mode="w+", prefix=prefix + "new_") as out:
        os.chmod(out.name, 0o666)
        out.write("".join(lines))
        out.flush()

        out_filename = os.path.basename(out.name)
        # print("READ CYPHER FROM ", out_filename)
        q = f"CALL apoc.cypher.runFile('{out_filename}') yield row, result"
        # print("READ CYPHER: ", q)
        res = g.run(q).evaluate()
        print("RESULT: ", res)

    # for debug: write a copy of the export
    # with open("/tmp/cypher", "w") as extra:
    #    extra.write("".join(lines))
    #    extra.close()

    # now attach image and current user to the new object
    has_image = g.run(
        """
        MATCH (a:MyActivity {uuid:$activity_uuid})-[x:HAS_IMAGE]-(ua:UserAsset)
        RETURN ua
    """,
        activity_uuid=activity_uuid,
    ).evaluate()

    print("IMAGE: ", has_image)
    if has_image:
        try:
            ok = g.run(
                """
                MATCH (old:MyActivity {uuid:$activity_uuid})
                MATCH (new:MyActivity {uuid:$new_uuid})
                MATCH (m:Member {uuid:$member_uuid})
                OPTIONAL MATCH (old)-[:HAS_IMAGE]-(ua:UserAsset)
                MERGE (m)<-[:IS_OWNER]-(new)
                MERGE (new)-[:HAS_IMAGE]->(ua)
                MERGE (new)-[r:COPIED_FROM]->(old)
                SET new.is_published=false
                SET new.created=timestamp()
                SET r.timestamp=timestamp()
                RETURN new
            """,
                dict(
                    activity_uuid=activity_uuid,
                    new_uuid=new_uuid,
                    member_uuid=member_uuid,
                ),
            ).evaluate()

            print("COMPLETE: ", ok)
        except Exception as e:
            print("ERROR ON IMAGE: ", str(e))
            return api_response(status.ERROR, msg=str(e))

    else:
        ok = g.run(
            """
            MATCH (old:MyActivity {uuid:$activity_uuid})
            MATCH (new:MyActivity {uuid:$new_uuid})
            MATCH (m:Member {uuid:$member_uuid})
            MERGE (m)<-[:IS_OWNER]-(new)
            MERGE (new)-[r:COPIED_FROM]->(old)
            SET new.is_published=false
            SET new.created=timestamp()
            SET r.timestamp=timestamp()
            RETURN new
        """,
            dict(
                activity_uuid=activity_uuid,
                new_uuid=new_uuid,
                member_uuid=member_uuid,
            ),
        ).evaluate()

        print("COMPLETE [no-image]: ", ok)

    return api_response(status.OK, activity=new_uuid)


# ------------------------------------------------------------------------
@EACTIVITY.route("/review", methods=["POST"])
async def review_answers(request):

    # get all the answers for a specific share-uuid

    share_uuid = request.json.get("share")

    # get activity for the share-uuid
    activity = g.run(
        """
        MATCH (share:MyShares {uuid:$share})<-[SHARED]-(a:MyActivity)
        MATCH (share)-[:ANSWERS]->(answers:StudentAnswers)-[]-(member:Member)
        OPTIONAL MATCH (a)-[r:BLOCK]-(b:MyBlocks)
        OPTIONAL MATCH (ua:UserAsset)-[]-(a)
        WITH collect(b) as blocks, a, ua, answers, member
        WITH collect( { answer: answers.uuid,
                        submitted: answers.submitted,
                        submitdate: answers.updated,
                        member: {
                            uuid:member.uuid,
                            email:member.email,
                            name:member.name,
                            login:member.login
                        }
        }) as answerset, a, ua, blocks
        RETURN {
            answers: answerset,
            activity: properties(a),
            image: CASE ua WHEN ua THEN collect(properties(ua))[0] ELSE {} END,
            blocks: blocks
        }
        """,
        dict(share=share_uuid),
    ).evaluate()

    if not activity:
        return api_response(status.NO_DATA)

    return api_response(status.OK, activity)


# ------------------------------------------------------------------------
@EACTIVITY.route("/get-answer", methods=["POST"])
async def get_answer(request):

    # get all the answers for a specific answer-uuid

    answer_uuid = request.json.get("answer")

    # get activity for the share-uuid
    activity = g.run(
        """
        MATCH (answers:StudentAnswers {uuid:$answer_uuid})-[]-(member:Member)
        WITH { answer: answers,
                        member: {
                            uuid:member.uuid,
                            email:member.email,
                            name:member.name,
                            login:member.login
                        }
        } as answerset
        RETURN answerset
        """,
        dict(answer_uuid=answer_uuid),
    ).evaluate()

    if not activity:
        return api_response(status.NO_DATA)

    return api_response(status.OK, activity)
