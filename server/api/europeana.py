from dataclasses import dataclass
from dataclasses_json import dataclass_json

import requests


WSKEY = "VBCZAGGDZI"
EUROPEANA = "https://api.europeana.eu/api/v2/search.json"

def get_providers():
    # search = request.args.get("s", None)

    p = {
        "wskey": WSKEY,
        "query": "*",
        "facet": "DEFAULT",
        "profile": "portal",
        "f.PROVIDER.facet.limit": "1000",
        "f.DATA_PROVIDER.facet.limit": "10000",
        "f.RIGHTS.facet.limit": "0",
        "f.COUNTRY.facet.limit": "0",
        "f.LANGUAGE.facet.limit": "0",
        "rows": 1,
    }

    print("get_providers start request--")
    r = requests.get(EUROPEANA, params=p)
    print("get_providers done request--", r.status_code)


    data = r.json()["facets"]

    dataproviders = [
        x["label"]
        for x in [b["fields"] for b in data if b["name"] == "DATA_PROVIDER"][0]
    ]

    providers = [
        x["label"]
        for x in [b["fields"] for b in data if b["name"] == "PROVIDER"][0]
    ]

    return dict(dataproviders=dataproviders, providers=providers)

#########################
# public search version
async def europeana_search(searchFor):

    rows = 250  # request.json.get("showMax", 25)
    cursor = '*'  # request.json.get("cursor", "*")

    @dataclass_json
    @dataclass
    class Record:
        title: str = ''
        _id: str = ''
        _type: str = ''
        eulink: str = ''
        url: str = ''
        description: str = ''

    print(f"EUROPEANA zoek naar {searchFor} met cursor: {cursor}")
    params = {
        "wskey": WSKEY,
        "query": str(searchFor),
        "facet": "DEFAULT",
        "profile": "rich",  # was rich
        "rows": rows,
        "media": True,  # grotere media beschikbaar
        "cursor": cursor,
        "type": "IMAGE",
    }

    # partner = request.json.get("partner")
    # if partner:
    #     p["qf"] = "DATA_PROVIDER:{}".format(partner)

    # print("PARAMS: ", p)

    r = requests.get(EUROPEANA, params=params)
    #print("RESULT: ", r.json())
    #import json
    # with open("out.json", "w") as x:
    #    x.write(json.dumps(r.json(), indent=4))

    # we only process the first batch of items for now.
    # this is 100 results max, cursor can be used to fetch more

    results = []
    reply = r.json()
    types = set()

    for item in reply['items']:
        rec = Record()

        # get fields we need from each EU item
        #fk = list(item['dcTitleLangAware'].keys()).pop()
        #rec.title = "\n".join(item['dcTitleLangAware'][fk])
        rec.title = "\n".join(item["title"])
        rec._type = item["type"]
        types.add(item["type"])
        rec._id = item["id"]
        rec.eulink = item["link"]
        try:
            rec.url = item["edmPreview"].pop()
        except:
            rec.url = None


        rec.description = "\n".join(item.get("dcDescription", ''))

        if rec.url:
            results.append(rec)

        #import json
        #print(json.dumps(rec.to_dict(), indent=4))

    return dict(types=list(types), results=results)


#########################
# partner search version

async def partner_search(searchFor, dataproviders=[], providers=[]):

    rows = 250  # request.json.get("showMax", 25)
    cursor = '*'  # request.json.get("cursor", "*")

    @dataclass_json
    @dataclass
    class Record:
        title: str = ''
        uuid: str = ''
        _id: str = ''
        _type: str = ''
        eulink: str = ''
        url: str = ''
        description: str = ''

    print(f"EUROPEANA zoek naar {searchFor} met cursor: {cursor}")
    params = {
        "wskey": WSKEY,
        "query": str(searchFor),
        "facet": "DEFAULT",
        "profile": "minimal",  # was rich
        "rows": rows,
        "media": True,  # grotere media beschikbaar
        "cursor": cursor,
        "type": "IMAGE",
    }

    if dataproviders:
        qf = "DATA_PROVIDER:" + ",".join(dataproviders)
        params['qf'] = qf

    # De Europeana API gaat er van uit dat je de qf parameter meerdere keren
    # in het request kan zetten, dat is handig maar de params bij ons zijn een dict...
    # voor nu slaan we de PROVIDER dan maar even over.

    # if providers:
    #     qf = "PROVIDER:" + ",".join(providers)
    #     params['qf'] = qf


    r = requests.get(EUROPEANA, params=params)
    #print("RESULT: ", r.json())
    #import json
    # with open("out.json", "w") as x:
    #    x.write(json.dumps(r.json(), indent=4))

    # we only process the first batch of items for now.
    # this is 100 results max, cursor can be used to fetch more

    results = []
    reply = r.json()
    types = set()


    if reply.get('items'):
        for offset, item in enumerate(reply.get('items')):
            rec = Record()

            # get fields we need from each EU item
            #fk = list(item['dcTitleLangAware'].keys()).pop()
            #rec.title = "\n".join(item['dcTitleLangAware'][fk])
            rec.title = "\n".join(item["title"])
            rec._type = item["type"]
            types.add(item["type"])
            rec._id = item["id"]
            rec.eulink = item["link"]
            rec.url = item["edmPreview"].pop()
            rec.description = "\n".join(item.get("dcDescription", ''))
            rec.uuid = f"eu_{offset:08d}"
            results.append(rec.to_dict())

            #import json
            #print(json.dumps(rec.to_dict(), indent=4))

    return dict(types=list(types), results=results)


#########################
# get an object

def json_extract(obj, key):
    """Recursively fetch values from nested JSON."""
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        print("#### ", obj, arr, key)
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (dict, list)):
                    extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    values = extract(obj, arr, key)
    return values


async def get_eu(eu_link):

    r = requests.get(eu_link)
    eu_reply = r.json()

    reply = {}

    # get crap from aggregations; assume there is only one object in the aggregations array

    aggregations_to_keep = [
        'edmDataProvider',
        'edmIsShownBy',
        'edmProvider',
        'edmDataProvider',
        'edmRights'
    ]

    proxies_to_keep = [
        'edmType',
        'year',
        'dcCoverage',
        'dcDescription',
        'dcRights',
        'dcTitle',
        'dcType',
        'dctermsCreated',


    ]

    for proxy in eu_reply['object']['proxies']:

        for a in proxies_to_keep:
            content = ''
            item = proxy.get(a)

            if isinstance(item, dict):

                if item.get('def') and len(item.keys())==1:
                    content = item.get('def')
                else:
                    content = item

            elif isinstance(item, str):
                content = item

            if content:
                # morph arrays with 1 entry to string
                if isinstance(content, list):
                    if len(content)==1:
                        reply[a] = content.pop()
                else:
                    reply[a] = content


    aggregations = eu_reply['object']['aggregations'][0] if eu_reply['object']['aggregations'] else None
    if aggregations:
        for a in aggregations_to_keep:
            content = ''
            if isinstance(aggregations[a], dict):
                # assume all aggregation properties have the `def` form
                try:
                    content = aggregations[a].get('def','')[0]
                except IndexError:
                    pass
            elif isinstance(aggregations[a], str):
                content = aggregations[a]

            if content:
                reply[a] = content


    return reply





if __name__ == '__main__':
    #print("EUSEARCH")
    #results = europeana_search('napoleon')

    print( get_eu("https://api.europeana.eu/record/90402/SK_A_296.json?wskey=VBCZAGGDZI"))





