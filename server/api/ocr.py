from graph import g
from sanic import Blueprint, response
from py2neo import Node, Relationship
from tic import new_api_response, status, create_slug
from slugify import slugify


import uuid
import datetime
import json
import requests
import io
import pytesseract

import PIL

OCR = Blueprint("ocr", url_prefix="/api/ocr")


# example code to add a background to a transparent png
# https://stackoverflow.com/questions/31084753/pytesseract-on-base64-string-python/31085530
#
# from PIL import Image
# import base64
# import pytesseract
# import io

# imgstring = 'data: image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGcAAAAVCAYAAABbq/AzAAAACXBIWXMAAA7EAAAOxAGVKw4bAAADiUlEQVRoge3YTWgeVRQG4IcQJJQSQihBNIQiXYUSpJSgIF1IkSKllCJFQggutBQRQRfFH3RTRFwVERdBRHcuREVEupASRIP4C7VoFSmWSq2gtRGjNm21Ls79+k0mM3cmLrpxXhjm++aec973/sy55w4dOnTo0KFDhw4FLOIe3IWrmWtbwWcfLuDhinj7avzvTjyLDXp24F2cxx84gQMVdgfxHVbwFe5tqaEObXxynDCAQ/g2af+8hjM3ftcwIzoPQ5iouObEQG1INvP4PvlVBT+IYxVxhlJ7Vad6GMICHsQkNqWO/I7Zgt1s6twejGEvfhMLrI2GKjT5NHHCs6l/dyTtM8nnzkL/msbvGhbFTOdwDIfT7514GyN4ryb403glE+8JvN/AWcY8Xi/8P45HK+J+0FJDFZp8mjgHxRu1o2TzkH62aDN+YFS8tlMZQdP4S6yCMhZqgr8oVlAdtuFvDGdsyngHL6Xfw0L31pq4Qy00VCHn04bzlmSzsWQznmw2lJ7XjZ8BbMclfJ0R/CRexS8ZmzJGcb/+nnFcrJ6B1P4lroiJb8I4nhGD8FzhGZwu2Z5OHJtbaFiv7jacvTG6qWSzKdmMZbjX4D78kGnfisvYUtNeN/PDYr8Yxo2iCDiH5ws2ZxN/HY7ob8insKvQtj09Hyz5DOkXLm00rEd3G04iZR3Vn8xpkdKuWjtptW+O1HAyI/Y1vJFpzwYvYUakx17nTjb4Dor0MIH9YgPtpZxJ1emjl6YnGzTMWF2NzbTQ3ZZzRKTfc6KIOSoKiMu4oeRbO36DWK4g62GLGJTbM8LXg2/EKtuIpXRfzthfSe3LOJPub+EpkUr+ERNXTMkT6X6mQcObuLnw/NcWuttyLuGBUoxZkcovZXhWYQA/6efDMh7Hh/ikbcAGTCe+Jf38++M6/Iur7k98ZnWqk/5/oX7SexouJu7edbGF7v/KOYBH8HKGoxJjqqu1cVES5g5s1L+Wh3GbyNvjYm+5oH+QnEq8oxW+t4rSfkqkiDHsFmltvmC3R2zcu5LdbnHm2N9SQxWafJo4iYwwIhbTtCiXF1S/AI3bwqfWnnNeEAepJuRK6VOi4vlZnGmKK+4QPq6JuVmcZ86KBXI+2R6wtoNz4iS+Ik7txYFv0lCFNj45TuJguqL/ZeMxa/eaHhonZ07/C8H1wonE26EFPhKfSK4H9ia+Dh06dOjw/8G/sXcmUir28IcAAAAASUVORK5CYII='
# imgstring = imgstring.split('base64,')[-1].strip()
# pic = io.StringIO()
# image_string = io.BytesIO(base64.b64decode(imgstring))
# image = Image.open(image_string)

# # Overlay on white background, see http://stackoverflow.com/a/7911663/1703216
# bg = Image.new("RGB", image.size, (255,255,255))
# bg.paste(image,image)

# print(pytesseract.image_to_string(bg))

# # Save the image passed to pytesseract for debugging purposes
# bg.save('pic.png')



#######################################################################
@OCR.route("/", methods=["GET"])
def get_index(request):

    data = dict()
    return new_api_response(status.OK, dict(data=data))


#######################################################################
@OCR.route("/", methods=["POST"])
def process(request):

    # fetch image url ; convert to Image and process with tesseract
    url = request.json.get('url')
    if not url:
        return new_api_response(status.NO_DATA)

    i = requests.get(url)
    print("## ",i.status_code)
    #print("## ",i.content)

    try:
        img = io.BytesIO(i.content)
        image = PIL.Image.open(img)
        text = pytesseract.image_to_string(image)
    except PIL.UnidentifiedImageError:

        return new_api_response(status.NO_DATA, dict(content=str(i.content)))


    return new_api_response(status.OK, dict(text=text))


