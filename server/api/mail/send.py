
import dns.resolver # from dnspython
import os
import jinja2

# hacked smtplib.py which also stores status of sent messsages
# not just the errors
from . import mysmtplib

from dns.exception import DNSException
from sanic.response import json

# message is an instance of EmailMessage() created with mail.create()
def send(message):
    
    try:
        to_addr = message['To'].addresses[0]
    except:
        to_addr = None
        return {
            'isError': True,
            'code': 500,
            'msg': 'Cannot determine TO address',
        }

    # status is reported via dict with email-string as key
    email = f'{to_addr.username}@{to_addr.domain}'

    mxs = []
    # for x in dns.resolver.resolve(to_addr.domain, 'MX'):
    #     print("x: ", x)
    try:
        for x in dns.resolver.resolve(to_addr.domain, 'MX'):
            mxs.append((x.preference, f'{x.exchange}'))

    except dns.resolver.NXDOMAIN:
        # NO DOMAIN
        resp = {'isError': True, 'msg': 'Domain not found; No mail sent.'}
        return resp

    except dns.resolver.Timeout:
        # NO DOMAIN
        resp = {'isError': True, 'msg': 'Timeout; No mail sent.'}
        return resp

    except DNSException as e:
        # DONT KNOW
        resp = {'isError': True, 'msg': e}
        return resp

    try:
        (prio, best) = sorted(mxs)[0]
    except Exception as e:
        best = None

    if not best:
        resp = {'isError': True, 'msg': 'No MX found for domain'}
        return resp

    print(f" {to_addr} DELIVERY VIA SMTP: {best} ")
    try:
        s = mysmtplib.SMTP(best, 25)
        s.set_debuglevel(0)
        # s.connect(best, 25)
        s.starttls()
        status = s.send_message(message)

        # get status for this email
        (code, smsg) = status[email]

        resp = {
            'isError': False,
            'code': code,
            'msg': smsg.decode(),
            'host': best
        }
        s.quit()
        return resp
    except Exception as e:

        resp = {
            'isError': True,
            'code': 500,
            'msg': str(e),
            'host': best
        }
        print("send.py CANNOT SEND ", e)

        return resp

    return {}
