#
# create an email


import os
import sys
import jinja2

from . import mysmtplib

from . import send

from email.message import EmailMessage
from email.headerregistry import Address
from email.mime.text import MIMEText
from email.utils import make_msgid

from config import Config
CONFIG = Config()

def create(template_name, to, values):
    """
        template_name: Jinja template name, full path calculated here
        to:  single email address
		values: other template values in dict: subject, etc
    """

    template_folder = CONFIG.SITE.get('mail_templates')

    if not "@" in to:
        resp = {"isError": True, "msg": "Email address should have a @; No mail sent."}
        return resp

    try:
        loader = jinja2.FileSystemLoader(searchpath=template_folder)
        env = jinja2.Environment(loader=loader)
        template = env.get_template(f"{template_name}.html")
    except Exception as e:
        print(f"TEMPLATE FOLDER {template_folder} MISSING")
        raise e

    msg = EmailMessage()
    msg["Message-Id"] = make_msgid()
    msg["Content-ID"] = "logo"
    msg["X-Attachment-Id"] = "logo"
    values["logo_id"] = "logo"  # add this value to the template values

    # Create a text/plain message
    # msg.set_content(message)
    msg.set_content("")
    # Create text/html message
    msg.add_alternative(template.render(values), subtype="html")

    # add the logo
    with open(f"{template_folder}/email-logo.png", "rb") as img:
        msg.get_payload()[1].add_related(
            img.read(), "image", "png", cid=f'<{values["logo_id"]}>'
        )

    msg["Subject"] = values.get("subject")
    msg["From"] = Address("Historiana", "web", "historiana.eu")
    msg["To"] = Address(addr_spec=to)

    return msg
