from graph import g
from sanic import Blueprint, response
from py2neo import Node, Relationship
from tic import new_api_response, status, create_slug
from slugify import slugify

import uuid
import datetime
import json


NARRATIVE = Blueprint("narrative", url_prefix="/api/narrative")
NARRATIVES = Blueprint("narratives", url_prefix="/api/narratives")

#######################################################################
# INDEX
@NARRATIVES.route("/my", methods=["POST"])
async def my_narratives(request):
    member = request.json.get("member")
    print("** ")
    print("** my-narratives for: ", member)
    data = g.run("""

        MATCH (member:Member {uuid:$member})
        MATCH (n:Narrative)-[:OWNS]-(member)
        OPTIONAL MATCH (n)-[:ICONIC_IMAGE]-(icon)
        OPTIONAL MATCH (source)-[:HTAGGED]-(tag:Tag)
        WITH collect(tag) AS tags, member, n, source, icon
        RETURN collect( distinct n
        {
            tags: tags,
            uuid: n.uuid,
            title: n.title,
            slug: n.slug,
            icon_url: icon.url,
            teaser: n.teaser,
            owner: member.uuid,
            is_published: n.is_published
        })
    """, member=member).evaluate()
    print("** data: ", data)
    return new_api_response(status.OK, dict(data=data))


#######################################################################
# INDEX
@NARRATIVES.route("/", methods=["GET"])
async def index(request):

    data = g.run("""
        MATCH (n:Narrative)
        OPTIONAL MATCH (n)-[:ICONIC_IMAGE]-(icon)
        OPTIONAL MATCH (n)-[:OWNS]-(member:Member)
        RETURN collect(
        {
            uuid: n.uuid,
            title: n.title,
            slug: n.slug,
            icon_url: icon.url,
            teaser: n.teaser,
            owner: member.uuid
        })
    """).evaluate()

    if not data:
        return new_api_response(status.NO_DATA)

    return new_api_response(status.OK, dict(data=data))



#######################################################################
# DELETE
@NARRATIVE.route("/delete", methods=["POST"])
async def delete(request):
    _user = request.json.get('user')
    _uuid  = request.json.get('narrative')

    data = g.run("""
        MATCH (m:Member {uuid:$member})
        MATCH (n:Narrative {uuid:$uuid})-[:OWNS]-(m)
        DETACH
        DELETE n
    """, uuid=_uuid, member=_user).evaluate()

    return new_api_response(status.OK, {})


#######################################################################
# GET NARRATIVE
@NARRATIVE.route("/<slug>", methods=["GET"])
async def get_narrative(request, slug):

    print("### SLIG ", slug)
    if not slug:
        return new_api_response(status.NO_DATA)

    summary = g.run("""
        MATCH (n:Narrative {slug:$slug})
        OPTIONAL MATCH (n)-[s:IS_SUMMARY]-(sb:NBlock)
        WITH s,sb
        ORDER BY s.offset
        RETURN collect(sb) AS summary
    """, slug=slug).evaluate()

    # transform content to json for UI
    for s in summary:
        s['content'] = json.loads(s['content'])
        # remove text attrribute for now
        # if s.get('text'):
        #     del s['text']


    blocks = g.run("""
        MATCH (n:Narrative {slug:$slug})
        OPTIONAL MATCH (n)-[s:IS_CHAPTER]-(b:NBlock)
        WITH s,b
        ORDER BY s.offset
        RETURN collect(b) AS blocks
    """, slug=slug).evaluate()

    for b in blocks:
        b['content'] = json.loads(b['content'])
        b['summary'] = json.loads(b['summary'])
        b['icon'] = json.loads(b['icon'])


    n = g.run("""
        MATCH (n:Narrative {slug:$slug})
        OPTIONAL MATCH (n)-[r:IS_CHAPTER]-(nb:NBlock)
        OPTIONAL MATCH (n)-[OWNS]-(m:Member)
        OPTIONAL MATCH (n)-[:ICONIC_IMAGE]-(icon)
        OPTIONAL MATCH (n)-[:HAS_LICENSE]-(license:License)
        WITH m, n, icon, nb, r, license
        ORDER BY r.offset
        WITH m, n, icon, license, collect(nb) AS content
        RETURN n {
            .*,
            icon: icon,
            content: content,
            meta: {
                created: n.created,
                modified: n.modified,
                owner: m.uuid,
                license: license
            }
        }
    """, slug=slug).evaluate()

    reply = dict()
    reply.update(n)
    reply.update(dict(summary=summary))
    reply.update(dict(content=blocks))

    if n:
        return new_api_response(status.OK, dict(data=reply))

    return new_api_response(status.NOT_FOUND, {})

#######################################################################
@NARRATIVE.route("/test/", methods=["GET"])
async def test(request):

    with open("../narratives/ww1_narrative.json","r") as ww1:
        data = json.load(ww1)


    return new_api_response(status.OK)



#######################################################################
# SAVE / UPDATE NARRATIVE
@NARRATIVE.route("/", methods=["POST"])
async def process(request):
    """process a narrative, assuming a complete narrative structure in _n"""

    _user = request.json.get('user')
    _n  = request.json.get('narrative')
    _uuid = _n.get('uuid')

    print("## narrative POST")
    print(f"## user: {_user}\n## narrative: {_uuid}")

    user = g.run("MATCH (m:Member {uuid:$uuid}) RETURN m", uuid=_user).evaluate()
    if not user:
        return new_api_response(status.NO_SESSION, {})


    if _uuid:
        narrative = g.run("MATCH (n:Narrative {uuid:$uuid}) RETURN n", uuid=_n['uuid']).evaluate()
    else:
        narrative = None


    _slug = _n.get('slug') if _n.get('slug') else create_slug(_n.get('title'),"Narrative", "slug")
    _meta = _n.get('meta')

    license = None
    if _meta and _meta.get('license'):
        _license = _meta.get('license')
        if _license:
            _license = _license.get('uuid')
            license = g.run("MATCH (l:License {uuid:$uuid}) RETURN l", uuid=_license).evaluate()
            print("HAS LICENSE : ", license)


    if not narrative:
        # create a NEW narrative
        print("CREATE NEW")

        # use _uuid if it is there allowing for client-side uuid generation
        # not sure if we want to keep it but for prototyping this is handy
        n = dict(
            title = _n.get('title'),
            slug = _slug,
            teaser = _n.get('teaser'),
            acknowledgement = _n.get('acknowledgement'),
            created = datetime.datetime.now(),
            uuid = _uuid if _uuid else str(uuid.uuid4())
        )

        # summary []
        # icon (url/uuid/caption) :  altijd een bestaand uuid
        # content
        # meta


        narrative = Node("Narrative", **n)
        g.create(narrative)
        rel = Relationship(user, "OWNS", narrative)
        g.create(rel)

    else:

        # delete current blocks
        g.run("MATCH (n:Narrative {uuid:$uuid})-[:IS_CHAPTER]-(nb:NBlock) DETACH DELETE nb", uuid=narrative['uuid']).evaluate()
        g.run("MATCH (n:Narrative {uuid:$uuid})-[l:HAS_LICENSE]-(License) DETACH DELETE l", uuid=narrative['uuid']).evaluate()

        # this is an update; set new props

        narrative['title'] =  _n.get('title')
        narrative['slug'] =  _slug
        narrative['teaser'] =  _n.get('teaser')
        narrative['acknowledgement'] =  _n.get('acknowledgement')
        narrative['modified'] = datetime.datetime.now()
        g.push(narrative)


    # process rest

    if license:
        rel = Relationship(narrative, "HAS_LICENSE", license)
        g.create(rel)



    _icon = _n.get('icon')
    print("** ICON: ", _icon)
    if _icon:
        # is dit altijd een MySources? Of kan het ook een asset zijn?

        # remove old ICONIC IMAGE
        g.run("MATCH (n:Narrative {uuid:$uuid})-[l:ICONIC_IMAGE]-(a) DETACH DELETE l", uuid=narrative['uuid']).evaluate()


        icon = g.run("MATCH (asset {uuid:$uuid}) RETURN asset", uuid=_icon['uuid']).evaluate()
        if icon:
            print("create REL")
            rel = Relationship(icon, "ICONIC_IMAGE", narrative)
            g.create(rel)
        else:

            print("NO ICON")



    def create_block(parent, block):
        print("## ", msg, kw)

    def get_text(blocks):
        """helper to get text properties from the blocks"""
        text = []
        for block in blocks:
            for entry in block.get('content',[]):
                text.append(entry.get('text',''))

        # \n as seperator is not shown in Neo4J UI
        return "||".join(text)



    # content blokjes
    # for now we store all paragraphs within the block
    # in future we might need to create an NBlock for each
    # paragraph (when we need to relate stuff on paragraph level)
    for i,c in enumerate(_n.get("content")):

        block = dict(
            icon = json.dumps(c.get('icon')),
            summary = json.dumps(c.get('summary')),
            type = c.get('type'),
            title = c.get('title', ''),
            content = json.dumps(c.get('content')),
            text = get_text(c.get('content'))
        )

        nb = Node("NBlock", **block)
        g.create(nb)

        rel = Relationship(narrative, "IS_CHAPTER", nb)
        rel['offset'] = i
        g.create(rel)

        # store the icon relation for this block
        _icon = c.get('icon')
        if _icon:
            # relate icon
            icon = g.run("MATCH (i {uuid:$uuid}) RETURN i", uuid=_icon['uuid']).evaluate()
            if icon:
                rel = Relationship(nb, "ICONIC_IMAGE", icon)
                g.create(rel)



    # summary
    # remove current blocks
    g.run("""
        MATCH (n:Narrative {uuid:$uuid})-[:IS_SUMMARY]-(nb:NBlock)
        DETACH DELETE nb""", uuid=narrative['uuid']).evaluate()

    # process the blocks
    for i,c in enumerate(_n.get("summary")):
        print(f"SUMMARY #{i}") # {json.dumps(c, indent=4)}")
        block = dict(
            type = c.get('type'),
            content = json.dumps(c.get('content')),
            text = "".join([e['text'] for e in c.get('content')])
        )

        #print("CREATE BLOCK ", block)

        nb = Node("NBlock", **block)
        g.create(nb)

        rel = Relationship(narrative, "IS_SUMMARY", nb)
        rel['offset'] = i
        g.create(rel)

    return new_api_response(status.OK, dict(uuid=narrative['uuid'], slug=narrative['slug']))
