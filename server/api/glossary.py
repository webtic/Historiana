from graph import g
from sanic import Blueprint, response
from tic import api_response, status
from slugify import slugify

GLOSSARY = Blueprint("glossary", url_prefix="/api/glossary")


@GLOSSARY.route("/", methods=["GET"])
async def index(request):

    data = g.run(
        """
    MATCH (g: Glossary)
    RETURN collect(g)
    """
    ).evaluate()

    return api_response(status.OK, data)


@GLOSSARY.route("/", methods=["POST"])
async def add(request):
    print("## ADD DEF")
    params = request.json
    data = g.run(
        """
        CREATE (g:Glossary)
        SET 
            g.word = toLower($word),
            g.definition = $definition,
            g.created = timestamp(),
            g.uuid = apoc.create.uuid()
        RETURN g
        """,
        params,
    ).evaluate()

    # connect Glossory definition to a user if specified
    user = request.json.get("user")
    if user:
        print("connect to user ****", data)
        data = g.run(
            """
            MATCH (g:Glossary {uuid:$glossary})
            MATCH (u:Member {uuid:$user})
            CREATE (g)-[:BY]->(u)
        """,
            dict(glossary=data["uuid"], user=user),
        )

    return api_response(status.OK, data)
