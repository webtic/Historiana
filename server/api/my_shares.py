# API entries for my-shares

from sanic import Blueprint, response
from tic import api_response, status, NEW_UUID
from graph import g

MYSHARES = Blueprint("my_shares", url_prefix="/api/my/shares")

# ------------------------------------------------------------------------
@MYSHARES.route("/", methods=["GET"], name="shares-index")
def my_shares_index(request):
    return api_response(status.OK, "my-shares-index")


# ------------------------------------------------------------------------
@MYSHARES.route("/", methods=["POST"], name="my-shares-index")
def my_shares_index(request):

    member = request.json.get("member")
    print(60 * "-")
    data = g.run(
        """
            MATCH (m:Member {uuid:$member})-[]-(a:MyActivity)-[]-(s:MyShares)
            OPTIONAL MATCH (s)-[:ANSWERS]-(submitted_answers:StudentAnswers {submitted:True})
            OPTIONAL MATCH (s)-[:MYTAG]-(t)
            OPTIONAL MATCH (s)-[short:SHORTLINK_FOR]-(:Redirect)
            OPTIONAL MATCH (ua:UserAsset)-[:HAS_IMAGE]-(a)
            OPTIONAL MATCH (answers:StudentAnswers)-[:ANSWERS]-(s)-[]-(student:Member)

            WITH s,a,t,short,ua, count(answers) as answers, submitted_answers

            with collect({
                name: t.name,
                uuid: t.uuid,
                label: labels(t)[0]
            }) as stags,s,a,short,ua, answers, submitted_answers
            ORDER BY s.created DESC

            WITH count(submitted_answers) as sacount, stags,s,a,short,ua, answers

            RETURN collect({
                image: { url: '/ua/' + ua.path},
                share: s,
                short: short.url,
                title: a.title,
                activity: a.uuid,
                tags: stags,
                answers: answers,
                submitted: sacount

            }) as shares
            """,
        member=member,
    ).evaluate()

    # print(data.data())
    # return json({'status':'ok', 'data': data.data()[0]})

    return api_response(status.OK, shares=data)


# ------------------------------------------------------------------------
@MYSHARES.route("/delete", methods=["POST"])
def delete(request):

    member = request.json.get("member")
    uuids = request.json.get("uuids")

    # validate parameters
    if not all([member, uuids]):
        return api_response(status.NO_DATA)

    res = g.run(
        """
        MATCH (m:Member {uuid:$member})-[:OWNS]->(s:MyShares)
        WHERE s.uuid in $shares
        WITH s, count(s) as total
        DETACH
        DELETE s
        RETURN total
    """,
        member=member,
        shares=uuids,
    ).evaluate()

    # return num of shares deleted
    return api_response(status.OK, res)


# ------------------------------------------------------------------------
@MYSHARES.route("/status", methods=["POST"])
async def review_answers_status(request):
    """return status for each student"""

    # TODO: validate member?
    owner = request.json.get("owner")
    share_uuid = request.json.get("share")

    # get activity for the share-uuid
    activity = g.run(
        """
        MATCH (share:MyShares {uuid:$share})<-[SHARED]-(a:MyActivity)
        MATCH (share)-[:ANSWERS]->(answers:StudentAnswers)-[]-(member:Member)
        WITH answers, member
        WITH collect( {
                        submitted: answers.submitted,
                        submitdate: answers.updated,
                        member: {
                            uuid:member.uuid,
                            email:member.email,
                            name:member.name,
                            login:member.login
                        }
        }) as answerset
        RETURN answerset
        """,
        dict(share=share_uuid),
    ).evaluate()

    if not activity:
        return api_response(status.NO_DATA)

    return api_response(status.OK, activity)
