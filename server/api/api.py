# coding=utf-8
# -*- coding: utf-8 --*-
#!/usr/local/bin/python3

"""Historiana API"""

import os
import sys
import pprint
import hashlib
import requests
import tempfile
import subprocess

from urllib.parse import urlparse

from log import log
from sanic.response import json, text
from sanic import Blueprint
from ujson import dumps as json_dumps
from py2neo import Graph, Node, Relationship #, NodeMatcher
from graph import g, graph
# from . import models

import tic
import tasks
from tic import status, api_response

# this is api.mail
from api import mail

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.utils import formatdate

import smtplib
from string import Template

from config import Config
CONFIG = Config()

import re

TAG_RE = re.compile(r'<[^>]+>')
def remove_tags(text):
    return TAG_RE.sub('', text)



BP = Blueprint('api')
VERSION = 1.1

HEADERS = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers':
    'Origin, X-Requested-With, Content-Type, Accept, Cache-Control, X-Session',
    'Access-Control-Allow-Methods': 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
}

@BP.route("/api/test", methods=['GET'])
async def bp_test(request):
    data = ["dit is een test", "dududu"]
    return json(data, headers=HEADERS)


# ------------------------------------------------------------------------
@BP.route('/api', methods=['OPTIONS', 'POST', 'GET'])
async def bp_root(request):
    """index of api"""
    return json({"api": "historiana", "version": VERSION, "config": str(CONFIG), "encoding": sys.getdefaultencoding()})


# ------------------------------------------------------------------------
@BP.route('/api/s3crets', methods=['OPTIONS', 'POST'])
async def s3crets(request):
    r = g.run("MATCH (m:Member) RETURN m ORDER BY m.joined DESC")
    data = [ rec['m'] for rec in r.data() ]
    reply = {'status': 'ok', 'data': data}
    return json(reply)

# ------------------------------------------------------------------------
@BP.route('/api/sendtestmail', methods=['OPTIONS', 'POST'])
async def sendtestmail(request):

    print("XXXX: ", CONFIG.SITE_URL)

    msg = MIMEMultipart('alternative')
    me = 'HistorianaThriving <clio@historiana.eu>'
    you = 'info@webtic.nl'
    msg['From'] = me
    msg['To'] = you
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = 'Historiana TEST message'
    msg['X-uuid'] = tic.NEW_UUID()
    print('XX: ', msg['X-uuid'])
    template = Template(open("./mail_templates/shareLink.html").read())
    send_html = template.substitute(HOST=CONFIG.SITE['url'], MARKER=msg['X-uuid'])
    send_text = 'de standaard tekst'
    part1 = MIMEText(send_text, 'plain')
    part2 = MIMEText(send_html, 'html', 'utf-8')

    msg.attach(part1)
    msg.attach(part2)
    try:
        s = smtplib.SMTP('smtp.ziggo.nl', timeout=10)
        s.set_debuglevel(0)
        status = s.sendmail(me, [you], msg.as_string())
    except Exception as e:
        print("SENDMAIL: ", e)

    reply = {'status': 'ok'}
    return json(reply)

# ------------------------------------------------------------------------
@BP.route('/api/remove/activities', methods=['OPTIONS', 'POST'])
async def remove_activities(request):
    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    member = request.json.get('user')
    activities = request.json.get('activities')
    print("GOT: ", activities)
    for act in activities:
        print("Process: ", act)
        c = dict(user=request.json.get('user'), activity=act)
        r = g.run("""
           MATCH (m:Member {uuid:$user})-[]-(item:MyActivity {uuid:$activity})
            DETACH DELETE item RETURN item
        """, **c).evaluate()

    return json([])


# ------------------------------------------------------------------------
@BP.route('/api/remove', methods=['OPTIONS', 'POST'])
async def remove_activity(request):
    """
          user: this.$store.state.user.uuid,
          type: this.type,
          name: this.title,
          item: this.uuid,
          from: 'card'
    """
    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    print('########')
    pprint.pprint(request.json)

    msg = "removed {type} {name}".format(**request.json)
    tic.log_event(request.json.get('user'), msg)

    status = g.run("""
        MATCH (m:Member {uuid:$user})-[]-(item {uuid:$item})
        DETACH DELETE item RETURN item
        """, request.json).evaluate()

    print("*** remove activity ", status)

    reply = {'status': 'ok'}
    return json(reply)


# ------------------------------------------------------------------------
@BP.route('/api/update-activity', methods=['OPTIONS', 'POST'])
async def update_activity(request):
    """create a new activity for the current user
    triggered by /#/builder
    """
    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    print("UPDATE ACTIVITY === json received ")
    pprint.pprint(request.json)
    #print(80*'-')

    a = request.json.get('activity')
    tags = a.get('tags')
    blocks = a.get('bblocks')

    #print('== TAGS')
    #pprint.pprint(tags)

    #print('== BLOCKS')
    #pprint.pprint(blocks)

    act_data = {
        'user_uuid':    request.json.get('user'),
        'uuid':         a.get('uuid'),
        'blockOrder':   a.get('blockOrder'),
        'title':        a.get('title'),
        'description':  a.get('description'),
    }

    #print("ACT_DATA: ", act_data)

    try:
        img_data = {
            'user_uuid':    request.json.get('user'),
            'uuid':         a.get('uuid'),
            'image_uuid':   a.get('image')['uuid'],
            'image_url':    a.get('image')['url']
        }
    except:
        img_data = None

    print('===============')
    pprint.pprint(act_data)
    #print("image: ", a['image'])

    # update the activity attributes
    act = g.run("""
        MATCH (a:MyActivity {uuid:$uuid})-[]-(m:Member {uuid:$user_uuid})
        SET
            a.blockOrder = $blockOrder,
            a.title = $title,
            a.description = $description
        RETURN a
        """, act_data)

    # if there is an image connect it to the activity
    if img_data:
        print("RELATE IMAGE")
        img = g.run("""
            MATCH (a:MyActivity {uuid:$uuid})-[]-(m:Member {uuid:$user_uuid})
            MATCH (i:UserAsset {uuid:$image_uuid})
            MERGE (a)-[r:HAS_IMAGE]->(i)
            RETURN r
            """, img_data)

    for t in tags:
        if t and t['uuid']:
            print('tag: ', t, t['uuid'], "act: ", a.get('uuid'))
            g.run("""
                MATCH (a:MyActivity {uuid:$uuid})
                MATCH (tag {uuid:$tag})
                MERGE (a)-[:MYTAG]->(tag)
                """, {'uuid': a.get('uuid'), 'tag': t['uuid']})

    # for now we zap any existing blocks...
    g.run("""
        MATCH (a:MyActivity {uuid:$uuid})-[]-(m:Member {uuid:$user_uuid})
        MATCH (a)-[r:BLOCK]-(b:MyBlocks)
        DETACH DELETE b
        """, act_data)

    # ... and create the current situation from scratch
    for b in blocks:
        print(60*'=')
        print("CREATE BLOCK: ", json_dumps(blocks[b]))

        typecheck = blocks[b].get('type')
        if not typecheck:
           return api_response(status.ERROR, msg="block has no type", block=b)

        rec = blocks[b]
        rec['activity_uuid'] = act_data['uuid']
        rec['user_uuid'] = request.json.get('user')
        rec['json_record'] = json_dumps(blocks[b]['record'])
        # rec['name'] = blocks[b]['name']
        # rec['type'] = blocks[b]['type']
        # rec['id'] = blocks[b]['id']

        try:
            create_block = g.run("""
                MATCH (a:MyActivity {uuid:$activity_uuid})-[]-(m:Member {uuid:$user_uuid})
                CREATE (b:MyBlocks)-[:BLOCK]->(a)
                SET
                    b.id = $id,
                    b.name = $name,
                    b.type = $type,
                    b.record = $json_record
                RETURN b
            """, rec)
        except Exception as e:
            return api_response(status.ERROR, msg=str(e))

    reply = {'status': 'ok'}

    # uncomment to throw error while testing UX feedback
    # return api_response(status.NOK)

    return json(reply)

# ------------------------------------------------------------------------
@BP.route('/api/share-activity', methods=['OPTIONS', 'POST'])
async def share_activity(request):
    print("### share activity ###")
    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    params = {
        'user_uuid': request.json.get('user'),
        'activity_uuid': request.json.get('activity'),
        'uuid': tic.NEW_UUID(),
        'type': 'EActivity',
        'mode': request.json.get('mode'),
        'tags': request.json.get('tags')
    }

    print( params )

# MATCH (m:Member {uuid:'09a737c1-edba-4cd0-b3e0-616bb482f963'})<-[:IS_OWNER]-(a:MyActivity {uuid:'879fe9c7-fee8-4559-92bd-e49bb8b43ccc'})
# CREATE (s:MyShares)<-[:SHARED]-(a)
#         SET
#         s.created = timestamp(),
#         s.type = 'type',
#         s.mode = 'mode',
#         s.uuid = 'iets'
# RETURN m,a,s

    rec = g.run("""
        MATCH (m:Member {uuid:$user_uuid})<-[:IS_OWNER]-(a:MyActivity {uuid:$activity_uuid})
        CREATE (m)-[:OWNS]->(s:MyShares)<-[:SHARED]-(a)
        SET
            s.created = timestamp(),
            s.type = {type},
            s.mode = {mode},
            s.uuid = {uuid}
        RETURN s
        """, params).data()

    for tag in params['tags']:
        print("TAG: ", tag)
        tparms = {
            'uuid': rec[0]['s']['uuid'],
            'tag_uuid': tag
        }

        """ XXX global uuid (t) MATCH to get any Tag type
        ## might be an performance issue at some time
        ## If so remodel by adding a MyTag to the current MyYears etc """
        x = g.run("""
            MATCH (s:MyShares {uuid:$uuid})
            MATCH (t {uuid:$tag_uuid})
            CREATE (s)<-[r:MYTAG]-(t)
            RETURN r
            """, tparms)

        #print("tparms: ", tparms)


    #print("rec: ", rec)

    if rec == None:
        reply = {'status': 'nok', 'msg': 'Could not create share'}
        return json(reply, headers=HEADERS)

    share = rec[0]['s']
    #share['url'] = CONFIG.SITE_URL + '/#/student/view/' + share['uuid']
    # add MyShare.uuid to the URL because we need to keep track in which Share
    # to correctly store the answers..

    if request.json.get('mode') == 'Others':
        # share['url'] = CONFIG.HOME + '/ea/share/' + request.json.get('activity') #+ '/?s=' + params['uuid']
        share['url'] = f"{CONFIG.HOME}/ea/view/{params['uuid']}"
    else:
        # share['url'] = CONFIG.HOME + '/ea/share/' + request.json.get('activity') #+ '/?s=' + params['uuid']
        share['url'] = f"{CONFIG.HOME}/ea/student/{params['uuid']}"

    reply = {'status': 'ok', 'share':share}
    print("MADE: ", reply)
    return json(reply)



@BP.route('/api/delete-answer', methods=['OPTIONS', 'POST'])
async def delete_answer(request):
    s=g.run("""
        MATCH (a:StudentAnswers {uuid:$answer})
        DETACH DELETE a
        """, request.json)
    print("DELETE STATUS: ",s)
    return api_response(status.OK)

@BP.route('/api/get-answers/<uuid>', methods=['OPTIONS', 'POST', 'GET'])
async def get_answers(request, uuid):
    """get the answers on a shared activity"""
    print("get answers for: ", uuid)
    data = g.run("""
        MATCH (s:MyShares {uuid:$share_uuid})-[]-(a:StudentAnswers)-[]-(m:Member)
        RETURN collect({
            ts: a.created,
            email: m.email,
            name: m.name,
            answers: apoc.json.path(a.answers),
            uuid: a.uuid
        })
    """, share_uuid=uuid).evaluate()
    return json(data)

# ------------------------------------------------------------------------
@BP.route('/api/load-activity', methods=['OPTIONS', 'POST'])
async def load_activity(request):
    """edit an existing activity for the current user
    triggered via /#/ela/uuid
    """

    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    params = {
        'user_uuid': request.json.get('user'),
        'activity_uuid': request.json.get('activity')
    }

    print("\033[1;32;40m")
    print("=======================================")
    print("LOAD ACTIVITY: ", params)
    print("=======================================")
    print("\033[0;37;40m")


    activity = g.run("""
        MATCH (m:Member)<-[:IS_OWNER]-(a:MyActivity {uuid:$activity_uuid})
        OPTIONAL MATCH (a)-[:MYTAG]-(tag)
        OPTIONAL MATCH (ua:UserAsset)-[]-(a)
        OPTIONAL MATCH (a)-[r:BLOCK]-(b:MyBlocks)
        WITH collect(b) as blocks, a,ua,tag
        RETURN
            blocks,
            properties(a) AS activity,
            properties(ua) AS image,
            count(ua) AS has_image,
            count(tag) AS has_tags,
            collect( distinct {
                name:tag.name,
                uuid:tag.uuid,
                tags:labels(tag)[0]
            }) AS tags
        ORDER BY activity.created ASC
    """, params)

    if activity:
        reply = {'status': 'ok', 'data': activity.data()[0]}
        #print('edit activity reply;)
    else:
        reply = {'status': 'nok', 'msg': 'cannot get MyActivity node'}

    return json(reply)


# ------------------------------------------------------------------------
# moved to /ea/student/save


# @BP.route('/api/save-student-answers', methods=['OPTIONS', 'POST'])
# async def save_student_answers(request):
#     """save the answers given on a start-activity"""
#     print("** SAVE STUDENT ANSWERS **")
#     r = request.json
#     uuid = r.get('sa', None)
#     answers = json_dumps(r.get('answers', None))

#     if not all([uuid,answers]):
#         return api_response(status.NO_DATA)

#     g.run("""
#         MATCH (sa:StudentAnswers {uuid:$uuid})
#         SET
#             sa.answers={answers},
#             sa.ts=timestamp()

#     """, uuid=uuid, answers=answers)

#     print(">>>>>>>> ", status.OK)
#     # BUG: ea/student/start adds data to status.OK, wipe it here
#     return api_response(status.OK, result="")

# ------------------------------------------------------------------------

# vervangen door eactivity/student/start

# @BP.route('/api/start-activity', methods=['OPTIONS', 'POST'])
# async def start_activity(request):
#     """
#         Start a studentview activity
#         - check email; create Member if unknown
#         - create a unique id for the session

#     """
#     r = request.json
#     email = r.get('email', None)
#     share = r.get('share', None)
#     activity = r.get('activity', None)

#     print("*** start-activity ", request.json)


#     if not email or not activity or not share:
#         return api_response(status.NO_DATA)

#     print("start-activity for share: ", share)

#     u = g.run("MATCH (m:Member) WHERE m.email={email} RETURN m.uuid", email=email).evaluate()
#     if not u:
#         # mail not found; create user
#         u = g.run("""
#             CREATE (m:Member)
#             SET m.email={email},
#             m.can_create=false,
#             m.is_teacher=false,
#             m.is_staff=false,
#             m.is_confirmed=false,
#             m.joined=timestamp(),
#             m.uuid={uuid}
#             RETURN m.uuid
#         """, email=email, uuid=tic.NEW_UUID()).evaluate()

#         # create a task to confirm this email address
#         tasks.start_new_user_confirmation(request, email, u)

#     else:
#         print("email bekend")

#     print("u>>>>>: ", u)

#     new_uuid = tic.NEW_UUID()
#     sv = g.run("""
#         MATCH (m:Member {uuid:$member_uuid})
#         MATCH (a:MyActivity {uuid:$activity_uuid})
#         MATCH (s:MyShares {uuid:$share_uuid})
#         CREATE (m)<-[r:ANSWERS]-(sa:StudentAnswers)<-[:ANSWERS]-(s)
#         SET
#             sa.uuid = {new_uuid},
#             sa.created = timestamp(),
#             sa.answers = ''
#         RETURN sa
#     """, member_uuid=u, activity_uuid=activity, share_uuid=share, new_uuid=new_uuid).evaluate()
#     print("SV CREATED: ", sv, activity)

#     reply = {
#         'sa_uuid': new_uuid

#     }

#     return api_response(reply)

# ------------------------------------------------------------------------
@BP.route('/api/edit-activity', methods=['OPTIONS', 'POST'])
async def edit_activity(request):
    """edit an existing activity for the current user
    triggered via /#/ela/uuid
    """

    user_uuid = request.json.get('user')
    if not user_uuid:
        reply = {'status': 'nok', 'msg': 'User not logged in.', 'data':[]}
        return json(reply)

    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    print('** EDIT ACTIVITY ', request.json)

    params = {
        'user_uuid': request.json.get('user'),
        'activity_uuid': request.json.get('activity')
    }

    print("\033[1;32;40m")
    print("=======================================")
    print("EDIT ACTIVITY START : ", params)
    print("=======================================")
    print("\033[0;37;40m")

    activity = g.run("""
        MATCH (m:Member {uuid:$user_uuid})<-[:IS_OWNER]-(a:MyActivity {uuid:$activity_uuid})
        OPTIONAL MATCH (a)-[:MYTAG]-(tag)
        OPTIONAL MATCH (ua:UserAsset)-[]-(a)
        OPTIONAL MATCH (a)-[r:BLOCK]-(b:MyBlocks)
        WITH collect(b) as blocks, a,ua,tag
        order by ua.created DESC
        RETURN
            blocks,
            properties(a) AS activity,
            collect(properties(ua))[0] AS image,
            count(ua) AS has_image,
            count(tag) AS has_tags,
            collect( distinct {
                name:tag.name,
                uuid:tag.uuid,
                tags:labels(tag)[0]
            }) AS tags

        ORDER BY activity.created ASC
    """, params)

    if activity:
        data = activity.data()
        if data:
            reply = {'status': 'ok', 'data': data[0]}
        else:
            return api_response(status.NO_DATA)
        #print('edit activity reply: ', reply)
    else:
        reply = {'status': 'nok', 'msg': 'cannot get MyActivity node'}

    return json(reply)

# ------------------------------------------------------------------------
@BP.route('/api/delete-activity', methods=['OPTIONS', 'POST'])
async def delete_activity(request):
    """
    delete a specific activity for the current user
    """

    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    print('DELETE ACT ', request.json)

    params = {
        'user_uuid': request.json.get('user'),
        'activity_uuid': request.json.get('activity')
    }

    activity = g.run("""
        MATCH (m:Member {uuid:$user_uuid})-[:IS_OWNER]-(act:MyActivity {uuid: {activity_uuid}})
        DETACH DELETE act RETURN act""", params).evaluate()

    if activity == None:
        reply = {'status': 'nok', 'msg': 'cannot delete Activity'}
    else:
        reply = {'status': 'ok', 'msg': 'Activity deleted'}
    print("reply: ", reply)
    return json(reply)


# ------------------------------------------------------------------------
@BP.route('/api/create-activity', methods=['OPTIONS', 'POST'])
async def create_activity(request):
    """
    create a new activity for the current user
    triggered in Builder.vue

    returns: new uuid for activity
    """

    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    print('CREATE ACT ', request.json)

    params = {
        'user_uuid': request.json.get('user'),
        'activity_uuid': tic.NEW_UUID()
    }

    activity = g.run("""
        MATCH (m:Member {uuid:$user_uuid})
        CREATE (act:MyActivity {uuid: $activity_uuid, created:timestamp()})-[:IS_OWNER]->(m)
        SET act.created = timestamp()
        RETURN act""", params).evaluate()

    if activity:
        reply = {'status': 'ok', 'uuid': activity['uuid']}
    else:
        reply = {'status': 'nok', 'msg': 'cannot create Activity node'}

    return json(reply)


# ------------------------------------------------------------------------
@BP.route('/api/duplicate-source', methods=['OPTIONS', 'POST'])
async def duplicate_my_source(request):
    """
    user: uuid of member
    source: uuid of source to duplicate
    title: new title for this source
    """

    data = request.json
    data['new_uuid'] = tic.NEW_UUID()

    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    print('**** ', request.json)

    try:
        u = g.run("""
            MATCH (u:Member {uuid:$user})-[:SOURCE]-(s:MySources {uuid:$source})
            CREATE (copy:MySources)<-[:SOURCE]-(u)
            SET copy += s,
            copy.uuid=$new_uuid,
            copy.title=$title
            RETURN copy
        """, **data).evaluate()
        reply = {'status': 'ok'}

    except Exception as e:
        print("ERROR DUPLICATING SOURCE ", e)
        reply = {'status': 'nok', 'msg': str(e)}

    return json(reply)

# ------------------------------------------------------------------------
@BP.route('/api/delete-source', methods=['OPTIONS', 'POST'])
async def delete_my_source(request):
    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    print('** delete-source')
    u = g.run("""
        MATCH (u:Member {uuid:$user})-[]-(s:MySources {uuid:$source})
        DETACH DELETE s
    """,request.json).evaluate()

    reply = {'status': 'ok'}
    return json(reply)


# ------------------------------------------------------------------------
@BP.route('/api/update-source', methods=['OPTIONS', 'POST'])
async def update_my_source(request):
    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    print('** update-source')

    user = request.json.get("user")
    source = request.json.get("source")
    mytags = request.json.get("mytags", [])

    u = g.run("""
        MATCH (u:Member {uuid:$user})-[]-(s:MySources {uuid:$source})
        SET
            s.title = $title,
            s.description = $description,
            s.modified = timestamp()
        RETURN s
    """,request.json).evaluate()

    # set mytags

    # remove old tags
    t = g.run("""
        MATCH (u:Member {uuid:$user})-[]-(s:MySources {uuid:$source})
        MATCH (s)-[rel:MYTAG]-(tag)
        DELETE rel
    """, user=user, source=source ).evaluate()

    for tag in request.json.get("mytags", []):
        t = g.run("""
            MATCH (u:Member {uuid:$user})-[]-(s:MySources {uuid:$source})
            MATCH (t {uuid:$tag})
            CREATE (s)-[rel:MYTAG]-(t)

        """, user=user, source=source, tag=tag ).evaluate()


    #return new_api_response(status.O)

    reply = {'status': 'ok'}
    return json(reply)

# ------------------------------------------------------------------------
@BP.route('/api/my-source', methods=['OPTIONS', 'POST'])
async def my_source(request):
    print("** mysource")
    print("******* ")
    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    uuid = request.json.get('uuid')
    # print('my-sources: ', uuid)
    if uuid:
        reply = {'status': 'ok'}
        source = g.run("""
            MATCH (source:MySources {uuid:$uuid})
            RETURN source
            """, uuid=uuid)
        # source = g.run("""
        #     MATCH (source:MySources {uuid:uuid})-[]-(m:Member {uuid:{uuid}})
        #     RETURN source
        #     """, uuid=uuid)
        reply['data'] = source.data()[0]
    return json(reply)


# ------------------------------------------------------------------------
@BP.route('/api/my-sources', methods=['OPTIONS', 'POST'])
async def my_sources(request):
    """list of sources
    used in /my/sources and /Medialibrary
    """
    from graph import graph
    _gr = graph()

    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    uuid = request.json.get('uuid')
    print("GET SOURCES FOR MEMBER", uuid)

    if not uuid:
        print("** NOT LOGGED IN")
        return api_response(status.NOT_LOGGED_IN)

    searchFor = request.json.get('searchFor')

    if uuid:

        if searchFor:
            print("* SEARCH ON: ", searchFor)
            sources = _gr.run("""
                MATCH (source:MySources)-[]-(m:Member {uuid:$uuid})
                WHERE source.title =~ '(?i).*"""+ searchFor + """.*'
                OPTIONAL MATCH (source)-[:MYTAG]-(tag)
                OPTIONAL MATCH (ua:UserAsset)-[]-(source)
                OPTIONAL MATCH (source)-[meta:HAS_PARTNER]-(partner:Partner)-[:IS_ICON]-(icon:Upload)
                OPTIONAL MATCH (partner)-[:IS_LOGO]-(logo:Upload)
                OPTIONAL MATCH (m)-[:HTAGGED]-(tag:Tag)
                WITH collect(tag) as tags, source, ua, partner, logo, icon

                RETURN source,
                    CASE WHEN partner IS NOT NULL THEN {
                    icon: icon.url,
                    logo: logo.url,
                    name: partner.name,
                    meta: properties(meta),
                    uuid: partner.uuid,
                    tags: tags
                } ELSE NULL END as partner,
                {
                    filename: ua.filename,
                    type: ua.type,
                    url: 'get-teaching-learning/'+ua.path
                } as asset
                ORDER BY source.created DESC
            """, uuid=uuid, searchFor=searchFor)

        else:

            # sources = g.run("""
            #     MATCH (source:MySources)-[]-(m:Member {uuid:{uuid}})
            #     OPTIONAL MATCH (source)-[:HAS_PARTNER]-(partner:Partners)-[IS_ICON]-(u:Upload)
            #     RETURN source, partner, u.url as partner_icon
            #     ORDER BY source.created DESC
            #     """, uuid=uuid)

            #                 OPTIONAL MATCH (thumb:UserAsset)-[:THUMB_FOR]-(ua)

            sources = _gr.run("""
                MATCH (source:MySources)-[]-(m:Member {uuid:$uuid})
                OPTIONAL MATCH (source)-[meta:HAS_PARTNER]-(partner:Partner)
                OPTIONAL MATCH (partner)-[:IS_ICON]-(icon:Upload)
                OPTIONAL MATCH (ua:UserAsset)-[]-(source)
                OPTIONAL MATCH (partner)-[:IS_LOGO]-(logo:Upload)
                OPTIONAL MATCH (source)-[:HAS_COPYRIGHT]-(cr:Copyright)
                OPTIONAL MATCH (source)-[:MYTAG]-(mytags)
                OPTIONAL MATCH (source)-[:HAS_CONTENT_TYPE]-(ct:ContentType)
                OPTIONAL MATCH (source)-[:HTAGGED]-(tag:Tag)

                WITH collect(tag) AS tags, ua, ct, source, partner, cr, icon, logo, meta

                RETURN
                    source {
                        .*,
                        tags: tags,
                        type: ua.type,
                        content_type: ct,
                        thumb_url: case when ua.thumb_url is null then source.url else ua.thumb_url end,
                        playback: ua.playback_url
                    } as source,

                    {
                        icon: icon.url,
                        logo: logo.url,
                        name: partner.name,
                        meta: properties(meta),
                        uuid: partner.uuid
                    } as partner,

                    {
                        name: cr.name,
                        url: cr.url,
                        uuid: cr.uuid
                    } as copyright,

                    {
                        filename: ua.filename,
                        type: ua.type,
                        url: '/ua/'+ua.path,
                        thumb: case when ua.thumb_url is null then source.url else ua.thumb_url end,
                        via: ua.via
                    } as asset

                ORDER BY source.created DESC
                """, uuid=uuid)

        print("FIND SOURCES: QUERY DONE")
        #print("res: ", sources)
        s = sources.data() if sources else []

    cnt = len(s)
    print(f"RETURN {cnt} SOURCES")
    return api_response(status.OK, sources=s)


# ------------------------------------------------------------------------
@BP.route('/api/my-activities', methods=['OPTIONS', 'POST'])
async def my_activities(request):
    """list of activities"""

    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    uuid = request.json.get('uuid')
    searchFor = tic.sanitize(request.json.get('searchFor'))

    try:
        filters = [f['uuid'] for f in request.json.get('filters')]
    except:
        filters = []

    totalActivities = g.run("""
        MATCH (u:MyActivity)-[]-(m:Member {uuid:$uuid})
        RETURN count(u)
    """, uuid=uuid).evaluate()

    reply = {}
    reply['totalActivities'] = totalActivities

    print('** my-activies: ', uuid, searchFor, filters)
    print(reply)

    if uuid:
        reply.update({'status': 'ok', 'activities': []})

        if filters:
            print("***filter")
            records = g.run("""
                MATCH (u:MyActivity)-[]-(m:Member {uuid:$uuid})
                MATCH (u)-[:MYTAG]-(tag) WHERE tag.uuid in $filters
                MATCH (u)-[:MYTAG]-(mytags)
                OPTIONAL MATCH (ua:UserAsset)-[]-(u)
                OPTIONAL MATCH (u)-[:SHARED]-(share:MyShares)
                RETURN
                    properties(u) AS activity,
                    properties(ua) AS image,
                    count(ua) AS has_image,
                    count(tag) AS has_tags,
                    count(share) AS share_count,
                    collect( distinct {
                        name:mytags.name,
                        uuid:mytags.uuid,
                        tags:labels(mytags)[0]
                    }) AS tags
                ORDER BY activity.created DESC

            """, uuid=uuid, searchFor=searchFor, filters=filters)

        elif searchFor:
            records = g.run("""
                MATCH (u:MyActivity)-[]-(m:Member {uuid:$uuid})
                WHERE u.title =~ '(?i).*"""+ searchFor + """.*'
                OPTIONAL MATCH (u)-[:MYTAG]-(tag)
                OPTIONAL MATCH (ua:UserAsset)-[]-(u)
                OPTIONAL MATCH (u)-[:SHARED]-(share:MyShares)

                RETURN
                    count(share) AS share_count,
                    properties(u) AS activity,
                    properties(ua) AS image,
                    count(ua) AS has_image,
                    count(tag) AS has_tags,
                    collect({name:tag.name,uuid:tag.uuid,tag:labels(tag)[0]}) AS tags
                ORDER BY activity.created DESC

            """, uuid=uuid, searchFor=searchFor)
        else:

# MATCH (u:MyActivity)-[]-(m:Member {uuid:'09a737c1-edba-4cd0-b3e0-616bb482f963'}) where u.title='aaaa'
#                 OPTIONAL MATCH (u)-[:MYTAG]-(tag)
#                 OPTIONAL MATCH (ua:UserAsset)-[]-(u)
#                 with ua, u,tag
#                 order by ua.created DESC
#                 return
#                     properties(u) AS activity,
#                     collect(properties(ua)) AS image,
#                     count(ua) as has_image,
#                     count(tag) AS has_tags,
#                     collect({name:tag.name,uuid:tag.uuid,tag:labels(tag)[0]}) AS tags

            # collect all images connected to this activity and select the latest image
            records = g.run("""
                MATCH (u:MyActivity)-[]-(m:Member {uuid:$uuid})
                OPTIONAL MATCH (ua:UserAsset)-[]-(u)
                OPTIONAL MATCH (u)-[:SHARED]-(share:MyShares)
                OPTIONAL MATCH (u)-[:HTAGGED]-(tag:Tag)
                WITH tag, ua, share, u
                order by ua.created DESC
                RETURN
                    collect(tag) as tags,
                    count(share) AS share_count,
                    properties(u) AS activity,
                    collect(properties(ua))[0] AS image,
                    count(ua) AS has_image,
                    count(tag) AS has_tags

                ORDER BY activity.created

            """, uuid=uuid)


        reply['activities'] = []
        for rec in records:
            #print("props: ", x['source'].properties)
            reply['activities'].append(rec.data())

            print("======================")
            print(rec.data())

    # deal with interchange.DateTime transparently
    # we should set this on configuration level
    # see https://sanic.dev/en/guide/basics/response.html#methods
    from tic.hijson import HiJSONdump
    return json(reply, dumps=HiJSONdump)


# ------------------------------------------------------------------------
@BP.route('/api/session/create')
async def bp_session_create(request):
    """create a new anonymous session for the frontend"""
    """note: this is a GET """

    print('Create NEW anonymous session')
    ra = tic.get_remote_addr(request)
    params = {
        'ra': ra,
        'uuid': tic.NEW_UUID()
    }
    sess = g.run("""
        CREATE (
            s:Session {
                remoteAddr: {ra},
                sessionId: {uuid},
                sessionValid: true,
                isAnonymous: true,
                ts:timestamp()
            }
        )
        RETURN s.sessionId
    """, params).evaluate()
    return json({'sessionId': sess})


# ------------------------------------------------------------------------
@BP.route('/api/upload', methods=['OPTIONS', 'POST', 'GET'])
async def handle_upload(request):
    """Handle Upload;
    - get current user or anonymous
    - store on filesystem for user or anon
    - store in UserAsset when not anon
    - return path and uuid of UserAsset
    """

    if request.method == 'GET':
        print("GET REQ")
        return json(dict(status=status.BAD_REQUEST), headers=HEADERS)

    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    #session = request.form.get('session')
    # print("FILES: ", request.files.keys())
    # print("FORM: ", request.form)
    #for k in request.files.keys():
    #     print("k: ", k)
    # print("headers: ", request.headers)

    print(60*'=')
    print("API/upload: ", request.files.keys())

    for (order, k) in enumerate(request.files.keys()):
        session = request.headers.get('X-Session')
        process = request.headers.get('X-Process')
        # deze waarde wordt terug wordt gegeven aan de caller
        item = request.headers.get("item")
        if not session:
            return api_response(status.NO_SESSION)

        file = request.files.get(k)
        digest = hashlib.sha256(file.body).hexdigest()
        (fname, ext) = os.path.splitext(file.name)
        upload_uuid = tic.NEW_UUID()
        upload = {
            'filename': file.name,
            'extension': ext.lower(),
            'digest': digest,
            'type': file.type,
            'uuid': upload_uuid
        }

        print("NEW UPLOAD: ", json_dumps(upload, indent=4))
        print("PROCESS   : ", process)

        # lookup session and match user if possible

        gg = graph()
        s = gg.run("""
            MATCH (s:Session {sessionId:$sess})
            OPTIONAL MATCH (s)-[]-(m:Member)
            RETURN {
                valid:s.sessionValid,
                user: m
            }
        """, sess=session).evaluate()

        user = s.get('user')
        session_valid = s.get('valid')

        # put file into a user-uuid/upload-uuid/filename hierarchy
        relative = []

        if user:
            print("has user ", user.get('uuid'))
            relative.append(user.get('uuid'))

        else:
            """ TODO : validate session otherwise reject upload """
            print("no user")
            relative.append('anonymous')

        #print('rel: ', relative)

        relative.append(upload['uuid'])
        path = "/".join([CONFIG.USER_UPLOADS,]+relative)
        destination = path + '/' + file.name
        upload['path'] = "/".join(relative + [file.name])
        upload['created'] = tic.js_now()

        #print("== ", upload)
        #print("== create : ",  destination)
        print("MAKE: ", path)
        os.makedirs(path, exist_ok=True)
        bla = open(destination, 'wb')
        try:
            os.makedirs(path, exist_ok=True)
            with open(destination, 'wb') as dest:
                dest.write(file.body)
                dest.close()

                if upload['extension']=='.pdf':
                    # print("**** PDF FOUND ***")
                    outfile = os.path.normpath(f"{path}/thumb.jpg")
                    infile = os.path.normpath(path + '/' + file.name)
                    upload['thumb_url'] = os.path.normpath('/ua/'+"/".join(relative)+"/thumb.jpg")
                    #print("upload: ", upload['thumb_url'])

                    # add a white background and flatten to avoid black background
                    # because JPGs can not handle transparency
                    cmd = f"""/usr/bin/convert -thumbnail 560x "{infile}[0]"  -background white -flatten {outfile}"""
                    print(cmd)
                    c = subprocess.run(cmd, shell=True)
                    if c.returncode!=0:
                        print("ERROR CREATING PDF THUMB FOR UPLOAD")

        except:
            tic.log('USER UPLOAD FAILED %s' % sys.exc_info()[0])
            return json({'status': 'nok', 'msg': 'cannot create path'}, headers=HEADERS)

        # create the upload object
        n = Node('UserAsset', **upload)
        g.create(n)

        if user:
            # print('connect to : ', user)
            r = Relationship(n, "UPLOADED_BY", user) #, since=1999)
            g.create(r)

        print("PROCESS: ", process)


        if process=='collection-powerpoint':
            print("** POWERPOINT BATCH ")
            url = "/".join([CONFIG.USER_UPLOADS_URL, n['path']])
            url = os.path.dirname(url)
            return tasks.pptx_to_collection(destination, url=url, user=user['uuid'])

            # print("user: ", user)
            # print("collection: ", request.headers.get('X-Collection'))
            # print("u: ", upload)
            # print("excel:", destination)
            # # spawn as an external task

            # cmd = [sys.executable, f"{CONFIG.SITE['tasks']}/excel_import.py", destination, user['uuid'], request.headers.get('X-Collection')]
            # print("SUBPROCESS: ", cmd)
            # sp = subprocess.Popen(cmd)


        if process=='collection-excel':
            # assume auto-upload==False and the UI gives us a valid collection.uuid
            print("** EXCEL BATCH ")
            collection = request.headers.get('X-Collection', None)

            # spawn as an external task
            cmd = [sys.executable, f"{CONFIG.SITE['tasks']}/excel_import.py", destination, user['uuid'], collection]
            sp = subprocess.Popen(cmd)


        if process=='collection-batch':
            # create a CollectionItem for an entry
            # we could also create a source from it, for now we skip this
            print("** COLLECTION BATCH ")
            data = {
                "asset_uuid": n['uuid'],
                "url": '/ua/' + n['path'],
                "user_uuid": user['uuid'],
                "uuid": tic.NEW_UUID(),
                "collection_uuid": request.headers.get('X-Collection'),
                "order": order
            }

            print("#########")
            collection = request.headers.get('X-Collection')
            print("collection to connect: ", collection)
            if collection=='null':
                print("CREATE COLL !!")


            # create iconic image for this collection
            if order==0:
                g.run(
                    """
                    MATCH (c:Collection {uuid:$collection})
                    MATCH (ua:UserAsset {uuid:$asset})
                    CREATE (ua)-[:THUMB_FOR]->(c)
                    """,
                    dict(collection=data["collection_uuid"], asset=data["asset_uuid"]),
                )



            # create CollectionItem
            # frontend code assumes url on CollectionItem
            # we need to keep track of UserAsset.uuid in order to set the iconic image
            # change frontend to use relation from CI to UA would be an optimalisation
            # and make ci.url redundant
            g.run("""
                MATCH (col:Collection {uuid:$collection_uuid})
                MATCH (ua:UserAsset {uuid:$asset_uuid})
                CREATE (ua)<-[:IS_ASSET]-(ci:CollectionItem)-[:IN_COLLECTION]->(col)
                SET
                    ci.url=$url,
                    ci.uuid=$uuid,
                    ci.title='',
                    ci.order=$order,
                    ci.description=''

                RETURN ci
            """, data).evaluate()


        # process 'Add item' in CollectionEditor
        if process=='collection-item':
            # create iconic image for this collection
            meta = dict(collection=request.headers.get('X-Collection'), asset=upload_uuid, filename=upload['filename'], url=f"/ua/{n['path']}")

            # TODO: also create a UserASSETS-[:IS_ASSET]



            g.run("""
                MATCH (col:Collection {uuid:$collection})
                MATCH (ua:UserAsset {uuid:$asset})
                CREATE (ua)<-[:IS_ASSET]-(ci:CollectionItem)-[:IN_COLLECTION]->(col)
                WITH ci
                SET
                  ci.uuid = apoc.create.uuid(),
                  ci.title = '',
                  ci.filename = $filename,
                  ci.url = $url,
                  ci.description = '',
                  ci.order=0
                RETURN ci

              """, meta)


            print(">> UPLOAD ADDITIONAL ITEM >> ", meta)
            return api_response(status.OK) #, dict(url=f"/ua/{asset['path']}", uuid=asset['path']))

        if process=='collection-iconic':
            # create iconic image for this collection
            meta = dict(collection=request.headers.get('X-Collection'), asset=upload_uuid)
            asset = g.run(
                """
                MATCH (c:Collection {uuid:$collection})
                MATCH (ua:UserAsset {uuid:$asset})
                OPTIONAL MATCH (ua)-[:THUMB_FOR]->(oldthumbs)
                DETACH DELETE oldthumbs
                CREATE (ua)-[:THUMB_FOR]->(c)
                RETURN ua
                """,
                meta
            ).evaluate()

            print(">> ASSET >> ", asset)
            return api_response(status.OK, dict(url=f"/ua/{asset['path']}", uuid=asset['path']))

        if process=='tag-iconic':
            # create iconic image for this collection
            meta = dict(tag=request.headers.get('X-Tag'), asset=upload_uuid)
            asset = g.run(
                """
                MATCH (c:Tag {uuid:$tag})
                MATCH (ua:UserAsset {uuid:$asset})
                OPTIONAL MATCH (ua)-[:THUMB_FOR]->(oldthumbs)
                DETACH DELETE oldthumbs
                CREATE (ua)-[:THUMB_FOR]->(c)
                RETURN ua
                """,
                meta
            ).evaluate()

            print(">> ASSET >> ", asset)
            return api_response(status.OK, dict(url=f"/ua/{asset['path']}", uuid=upload_uuid))


        if process=='createSource':
            print("** upload/createSource")
            data = {
                "asset_uuid": n['uuid'],
                "url": "/".join(["", 'ua', n['path']]),
                "user_uuid": user['uuid'],
                "uuid": tic.NEW_UUID(),
            }

            cr = g.run("""
                MATCH (m:Member {uuid:$user_uuid})
                MATCH (u:UserAsset {uuid:$asset_uuid})
                CREATE (s:MySources)
                SET
                    s.title=u.filename,
                    s.description='',
                    s.url=$url,
                    s.uuid=$uuid,
                    s.created=timestamp()
                CREATE (s)-[:ASSET]->(u)
                CREATE (s)-[:SOURCE]->(m)
                RETURN s

            """, data)

            # process Video uploads further, add to MUX
            print(60*'-')
            print("UPL: ", upload)
            if upload['type'] in ['video/mp4', 'video/x-ms-wmv', 'video/quicktime', 'video/webm']:
                print("=== PROCESS VIDEO ===")

                # preview url for MUX to use for thumbnailing
                if "atom" in os.uname().nodename:
                    url = CONFIG.SITE['remote'] + data['url']
                else:
                    url = CONFIG.SITE['home'] + data['url']

                print("URL FOR MUX: ", url)

                from tasks import mux

                # this blocks exection for now until processing @MUX is completed
                muxmeta = mux.create(url)

                asset = g.run("""
                    MATCH (ua:UserAsset {uuid:$asset_uuid})
                    SET
                        ua.thumb_url=$thumb_url,
                        ua.playback_url=$playback_url,
                        ua.thumb_url=$thumb_url
                    RETURN ua
                """,
                    asset_uuid = n['uuid'],
                    mux_id = muxmeta['mux_id'],
                    thumb_url = muxmeta['thumb'],
                    playback_url = muxmeta['playback']
                ).evaluate()



        if process=='createCollectionfromPPTX':
            print("** upload/createCollectionfromPPTX")
            print("file: ", destination)
            print("user: ", user['uuid'])
            url = "/".join([CONFIG.USER_UPLOADS_URL, n['path']])
            url = os.path.dirname(url)
            return tasks.pptx_to_collection(destination, url=url, user=user['uuid'])

        if process=='thumbForCollection':
            collection = request.query_string
            print("** thumb for collection: ", collection)
            url = "/".join([CONFIG.USER_UPLOADS_URL, n['path']])
            print("url: ", url)
            g.run("""
                MATCH (col:Collection {uuid:$col})
                MATCH (t:UserAsset {uuid:$upload_uuid})
                CREATE (col)<-[:THUMB_FOR]-(t)
            """, col=collection, upload_uuid=upload_uuid)

        # Viewpoints
        if process=='iconicimage':
            #print("XXX >>> ", request.headers)
            print(">>>> ", dict(viewpoint=request.headers.get('item'), asset=n['uuid']))

            # disconnect old image if there is one
            g.run("""
                MATCH (c:ViewpointCollection {uuid:$viewpoint})<-[x:THUMB_FOR]-(asset)
                DETACH DELETE x
            """, viewpoint=request.headers.get('item'))


            # create iconic image for this viewpoint
            g.run(
                """
                MATCH (c:ViewpointCollection {uuid:$viewpoint})
                MATCH (ua:UserAsset {uuid:$asset})
                CREATE (ua)-[:THUMB_FOR]->(c)
                """,
                dict(viewpoint=request.headers.get('item'), asset=n['uuid']),
            )


        # upload iconic image in LearningActivityEditor
        if process=='thumbLA':
            print("LA THUMB---")
            uuid = request.query_string.strip()

            # delete old
            d = g.run("""
                MATCH (la {uuid:$uuid})-[x:ICONIC_IMAGE]->(a)
                DELETE x
            """, uuid=uuid)

            connect = g.run("""

                MATCH (la:LearningActivity {uuid:$uuid})
                MATCH (a:UserAsset {uuid:$asset})
                CREATE (la)-[:ICONIC_IMAGE]->(a)
            """, uuid=uuid, asset=n['uuid'])


        # asset (donwload) for this LA
        # passes uuid of la as ?uuid
        if process=='laAsset':
            print("\nLA ASSET-----", n)
            print("asset: ", n['uuid'])

            uuid = request.query_string.strip()
            connect = g.run("""
                MATCH (la:LearningActivity {uuid:$uuid})
                MATCH (a:UserAsset {uuid:$asset})
                CREATE (la)<-[:ASSET_FOR {new:true}]-(a)
            """, uuid=uuid, asset=n['uuid'])

        tic.log('UPLOAD OK')
        #//upl = Node('UserAssset',


    # print('** UPLOAD **', request.method)
    # print( dir(request))
    # print( request.parsed_files )
    # print( "vals: ", request.values() )
    # print( dir(request.files))
    # print( "rf-items: ", request.files.items())
    # print( "rf-values: ", request.files.values())
    return json({'uuid': upload_uuid, 'url': '/ua/'+upload['path'], 'item': item})

# ------------------------------------------------------------------------
@BP.route('/api/getActivityLog', methods=['OPTIONS', 'POST'])
async def bp_get_activitylog(request):
    """get activity log (eventlog) for uuid"""

    data = g.run("""
        MATCH (m:Member {uuid:$uuid})-[]-(e:EventLog)
        RETURN collect(e) AS events
        """, request.json)

    print(request.json.get('uuid'))
    return json({'status':'ok', 'data': data.data()[0]['events']})

# ------------------------------------------------------------------------
@BP.route('/api/delete-share', methods=['OPTIONS', 'POST'])
async def delete_share(request):
    r = request.json
    s = g.run("""
        MATCH (u:Member {uuid:$uuid})-[:OWNS]-(s:MyShares {uuid:$share})
        DETACH DELETE s
        """,r)
    return api_response(status.OK)


# ------------------------------------------------------------------------
@BP.route('/api/my-shares', methods=['OPTIONS', 'POST'])
async def get_my_shares(request):
    """get shares for member"""

    #### note: we use :TAG for relations between SHARE and TAG
    #### but :MYTAG between MEMBER and MYTAG

    # data = g.run("""
    #     MATCH (m:Member {uuid:{uuid}})-[]-(a:MyActivity)-[]-(s:MyShares)
    #     OPTIONAL MATCH (s)-[:TAG]-(t)
    #     WITH s,a
    #     ORDER BY s.created DESC
    #     RETURN collect({
    #         share: s,
    #         title: a.title,
    #         activity: a.uuid
    #     }) as shares
    #     """, request.json)


    uuid = request.json.get('uuid')
    searchFor = request.json.get('searchFor')
    try:
        filters = [ f['uuid'] for f in request.json.get('filters') ]
    except:
        filters = []

    print('======================================')
    print('my-shares: ', uuid, searchFor, filters)


    if searchFor:
        print("## SEARCHFOR ", searchFor)
        data = g.run("""
            MATCH (m:Member {uuid:$uuid})-[]-(a:MyActivity)-[]-(s:MyShares)
            WHERE a.title =~ '(?i).*"""+ searchFor + """.*'
            OPTIONAL MATCH (s)-[:MYTAG]-(t)
            OPTIONAL MATCH (s)-[short:SHORTLINK_FOR]-(:Redirect)
            OPTIONAL MATCH (ua:UserAsset)-[:HAS_IMAGE]-(a)

            WITH s,a,t,short,ua, count(ua) as has_image
            with collect({
                name: t.name,
                uuid: t.uuid,
                label: labels(t)[0]
            }) as stags,s,a, short, ua, has_image
            ORDER BY s.created DESC
            RETURN collect({
                has_image: has_image,
                image: { url: '/ua/' + ua.path},
                share: s,
                short: short.url,
                title: a.title,
                activity: a.uuid,
                tags: stags
            }) as shares
            """, uuid=uuid)
    elif filters:
        print("## FILTERS ", filters)

        data = g.run("""
            MATCH (m:Member {uuid:$uuid})-[]-(a:MyActivity)-[]-(s:MyShares)
            MATCH (s)-[:MYTAG]-(tag) WHERE tag.uuid in {filters}
            MATCH (s)-[:MYTAG]-(mytags)
            OPTIONAL MATCH (s)-[:MYTAG]-(t)
            OPTIONAL MATCH (s)-[short:SHORTLINK_FOR]-
            (:Redirect)
            OPTIONAL MATCH (ua:UserAsset)-[:HAS_IMAGE]-(a)

            WITH s,a,t,short,ua, count(ua) as has_image
            with collect( DISTINCT {
                name: t.name,
                uuid: t.uuid,
                label: labels(t)[0]
            }) as stags,s,a,short,ua, has_image
            ORDER BY s.created DESC
            RETURN collect({
                has_image: has_image,
                image: { url: '/ua/' + ua.path},
                share: s,
                short: short.url,
                title: a.title,
                activity: a.uuid,
                tags: stags
            }) as shares
            """, uuid=uuid, filters=filters)


    else:
        print('## STANDARD ')
        data = g.run("""
            MATCH (m:Member {uuid:$uuid})-[]-(a:MyActivity)-[]-(s:MyShares)
            OPTIONAL MATCH (s)-[:MYTAG]-(t)
            OPTIONAL MATCH (s)-[short:SHORTLINK_FOR]-(:Redirect)
            OPTIONAL MATCH (ua:UserAsset)-[:HAS_IMAGE]-(a)
            OPTIONAL MATCH (sa:StudentAnswers)-[:ANSWERS]-(s)

            WITH s,a,t,short,ua, count(ua) as has_image, count(sa) as sa
            with collect({
                name: t.name,
                uuid: t.uuid,
                label: labels(t)[0]
            }) as stags,s,a,short,ua, has_image, sa
            ORDER BY s.created DESC
            RETURN collect({
                has_image: has_image,
                image: { url: '/ua/' + ua.path},
                share: s,
                short: short.url,
                title: a.title,
                activity: a.uuid,
                tags: stags,
                answers: sa
            }) as shares
            """, uuid=uuid)

    #print(data.data())
    return json({'status':'ok', 'data': data.data()[0]})


# ------------------------------------------------------------------------
@BP.route('/api/getTags', methods=['OPTIONS', 'POST'])
async def bp_get_tags(request):
    """get tags for uuid
    """
    print("** getProfile ", request.json.get('uuid'))
    # ORDER BY x.ts ASC limit 1
    res = g.run("""
        MATCH (m:Member {uuid:$uuid})
        OPTIONAL MATCH (m)-[:MYTAG]-(tag)
        RETURN {
            uuid: m.uuid,
            has_tags: count(tag),
            tags: collect({name:tag.name,uuid:tag.uuid,tag:labels(tag)[0]}),
            tagcloud: collect( distinct labels(tag))
        }
        """, uuid=request.json.get('uuid')).evaluate()

    #print("getprofile res: ", res)
    return json({'status':'ok', 'data': res})


# ------------------------------------------------------------------------
@BP.route('/api/getProfile', methods=['OPTIONS', 'POST'])
async def bp_get_profile(request):
    """get profile for uuid
    history of countries is available, we limit to 1 and sort to
    get the most recent one.
    all tags are collected via :MYTAG relation
    the UI contains the strings hardcoded but the API will automatically
    send newly created types
    """
    print("** getProfile ", request.json.get('uuid'))
    # ORDER BY x.ts ASC limit 1
    res = g.run("""
        MATCH (m:Member {uuid:$uuid})
        OPTIONAL MATCH (c:Country)-[x:LIVES_IN]-(m)
        WHERE x.ts is not null
        WITH m, c.code AS countrycode
        ORDER BY x.ts DESC limit 1
        CALL {
            MATCH (m:Member {uuid:$uuid})-[:MYTAG]-(tag)
            RETURN
                collect({name:tag.name,uuid:tag.uuid,tag:labels(tag)[0]}) as tags,
                collect( distinct labels(tag)) as tagcloud,
                count(tag) as has_tags
        }
        WITH tags, tagcloud, has_tags, m, countrycode

        RETURN collect({
            email: m.email,
            name: m.name,
            countrycode: countrycode,
            has_tags: has_tags,
            tags: tags,
            tagcloud: tagcloud
        })

        """, uuid=request.json.get('uuid')).evaluate()

    #print("getprofile res: ", res)
    return json({'status':'ok', 'user': res})

# ------------------------------------------------------------------------
@BP.route('/api/MyTags/remove', methods=['OPTIONS', 'POST'])
async def bp_mytags_remove(request):
    """remove a tag"""
    data = request.json
    #print("remove request data: ", data)
    rec = g.run("""
        MATCH (m:Member {uuid:$member})-[:MYTAG]-(t {uuid:$tag_uuid})
        DETACH DELETE t
    """, data).evaluate()
    return json({'status':'ok'})


# ------------------------------------------------------------------------
@BP.route('/api/MyTags/create', methods=['OPTIONS', 'POST'])
async def bp_mytags_create(request):
    """create a new Tag in a certain set"""
    log.debug("MyTags.create")
    data = request.json
    #member = data.get('member')
    #session = data.get('session')
    #tagname = data.get('tagname')
    #print("set: ", tagset)
    #print(json_dumps(data))
    tagset = data.get('tagset')
    data['tag_uuid'] = tic.NEW_UUID()
    #print("data: ", data)

    rec = g.run("""
        MATCH (m:Member {uuid: $member})
        WITH m
        MERGE (t:"""+tagset+""" {name:$tagname, uuid:$tag_uuid})<-[:MYTAG]-(m)
        RETURN t
    """, data).evaluate()

    print("rec: ", rec)

    return api_response(status.OK, rec)
    #return json({'status': 'ok', 'tag': rec.properties})


# ------------------------------------------------------------------------


@BP.route('/api/updateMember', methods=['OPTIONS', 'POST'])
async def bp_updatemember(request):
    """todo: validate session"""
    print("** UPDATE MEMBER")
    data = {
        'uuid': request.json.get('_uuid'),
        'name': request.json.get('name'),
        'email': request.json.get('email',''),
        'login': request.json.get('login',''),
        'avatar': request.json.get('avatar'),
        'show_beta': request.json.get('show_beta', False),
        'countrycode': request.json.get('countrycode', None)
    }
    print(data)
    tic.log_event(data['uuid'], 'Update Profile')

    # use a MERGE with a timestamp creates a history of places
    # where people lived
    #
    # nadeel is dat op dit moment we ALTIJD een merge doen
    # dus ook als het land niet is veranderd krijgen we toch een relatie er bij
    m = g.run("""
        MATCH (m:Member {uuid:$uuid})
            SET
                m.name = $name,
                m.email = $email,
                m.login = $login,
                m.avatar = $avatar,
                m.show_beta = $show_beta
        WITH m
        OPTIONAL MATCH (c:Country {code:$countrycode})
        MERGE (m)-[:LIVES_IN {ts:timestamp()}]->(c)
        RETURN m,c
        """, **data)


    #print("m neo: ", m)
    return json({'status': 'ok'})

# ------------------------------------------------------------------------

@BP.route('/api/update', methods=['OPTIONS', 'POST'])
async def bp_update(request):
    """todo: validate session"""

    print("\n")
    print("** UPDATE ", request.method)
    print("json :", request.json)

    data = request.json

    # make this generic?
    # _rel_ is a relation
    # _name is a lookup
    # others are data

    for k in data.keys():

        if k.startswith('_rel_'):
            print("rel ", k, " ", data[k])
            continue
        elif k.startswith('_'):
            print("var ", k, " ", data[k])
            continue
        else:
            print("val", k, " ", data[k])



    return json({'update': 'ok'})

# ------------------------------------------------------------------------

@BP.route('/api/ac/country', methods=['GET'])
async def ac_country(request):
    """countries for profile registation"""
    term = request.args.get('term')
    rec = g.run("""MATCH (n:Country)
                 RETURN n.name as name, n.code as code
                ORDER BY n.name
            """)
    #data = [1,2,3]
    return json(rec.data())
    #return json(data, headers=HEADERS)

# ------------------------------------------------------------------------

@BP.route('/api/ac/language', methods=['GET'])
async def ac_language(request):
    term = request.args.get('term')
    if term:
        rec = g.run("""MATCH (n:Language)
                 WHERE n.name =~ '(?i)%s.*'
                 RETURN n.name as name, n.code as code
                ORDER BY n.name
            """ % term)

    else:
        rec = g.run("""
            MATCH (n:Language)
            RETURN n.name as name, n.code as code
            ORDER BY n.name
        """)

    return json(rec.data())

# ------------------------------------------------------------------------

@BP.route('/api/get-node-labelxs')
async def bp_get_node_labels(request):
    """ get node labls"""

    rec = g.run("""MATCH (n)
             WITH DISTINCT labels(n) AS labels
             UNWIND labels AS label
             RETURN DISTINCT label
             ORDER BY label
        """)

    return json(rec.data())

# ------------------------------------------------------------------------

@BP.route('/api/get-search-partners')
async def get_search_partners(request):
    """ get the search partners (with logo) """

    result = g.run("""
        MATCH (sp:SearchPartner)-[:HAS_LOGO]->(l)
        RETURN sp, '/uploads/'+l.uuid+'_'+l.filename as logo
        ORDER BY sp.name

    """)

    # join sp and logo into flat one record for json
    sps = []
    for res in result:
        rec = res['sp']
        rec['logo'] =  res['logo']
        #rec['logo'] = res['logo']
        rec['display'] = rec['name']
        sps.append(rec)

    return json(sps, headers={'Access-Control-Allow-Origin': '*'})
# ------------------------------------------------------------------------

@BP.route('/api/oldstartsession', methods=['POST', 'OPTIONS'])
async def oldstartsession(request):
    if request.method == 'OPTIONS':
        # mark all options valid during development
        return json([], headers=HEADERS)

    print("## /api/oldstartsession")
    params = {
        'session': request.json.get('session', '')
    }

    r = g.run("""
        MATCH (s:Session {sessionId:$session})-[]-(u:Member)
        OPTIONAL MATCH (c:Country)-[x:LIVES_IN]-(u)
        WHERE exists(x.ts)
        WITH u, c.code AS countrycode
        ORDER BY x.ts DESC limit 1
        RETURN u,countrycode
        """, params).data()[0]

    result = r['u']
    result['countrycode'] = r['countrycode']

    if result:
        # we return the complete Node minus the password
        del result['password']
        result['isError'] = False
        result['msg'] = 'LOGIN OK'
        result['remote'] = tic.get_remote_addr(request)
        result['session_uuid'] = tic.NEW_UUID()
        # create a session
        sess = g.run("""
            MATCH (m:Member {uuid:$uuid})
            CREATE (
                s:Session {
                    sessionId: {session_uuid},
                    sessionValid: true,
                    ts:timestamp()
                }
            ) <- [:LOGGED_IN] - (m)
            RETURN s.sessionId
        """, result).evaluate()

        result['session'] = sess
        result['authenticated'] = True
        print("1316 created session for: ", sess)
    else:
        result = {'isError': True, 'msg': 'Invalid credentials'}

    print('startsession returns:  ', result)
    return json(result)



# ------------------------------------------------------------------------
@BP.route('/api/validate-password-reset', methods=['POST', 'OPTIONS'])
async def validate_password_reset(request):
    """
       validate setting the new password request by checking challenge + client_ip
       todo: validate the timestamp timeout as well.
    """

    challenge = request.json.get('challenge','').strip()
    newpass = request.json.get('newpass','').strip()
    #print(">>>> validate: ", challenge, request.ip)

    r = g.run("""
        MATCH (r:PasswordResetRequest {challenge:$challenge})-[:REQUEST]-(m:Member)
        WHERE r.client_ip=$client_ip
        RETURN m
    """, challenge=challenge, client_ip=request.ip).evaluate()

    if (r):
        # update Member node with new password
        r['password'] = hashlib.sha1(newpass.encode()).hexdigest()
        g.push(r)

        # log the event
        g.run("""
            MATCH (m:Member {uuid:$uuid})
            CREATE (l:Log)
            SET
                l.action='password-reset-complete',
                l.ts=datetime(),
                l.challenge=$challenge,
                l.client_ip=$client_ip
            WITH l,m
                CREATE (l)-[:PASSWORD_CHANGED]->(m)
        """, challenge=challenge, client_ip=request.ip, uuid=r['uuid'])

        return api_response(status.OK, r)
    else:

        # log the event
        g.run("""
            MATCH (m:Member {uuid:$uuid})
            CREATE (l:Log)
            SET
                l.action='password-reset-error',
                l.ts=datetime(),
                l.challenge=$challenge,
                l.client_ip=$client_ip

        """, challenge=challenge, client_ip=request.ip)

        return api_response({'isError':True})





# ------------------------------------------------------------------------
@BP.route('/api/validate-request', methods=['POST', 'OPTIONS'])
async def validate_password_reset_request(request):
    """
       validate request by checking challenge + client_ip
       todo: validate the timestamp timeout as well.
    """

    challenge = request.json.get('challenge').strip()
    print(">>>> validate: ", challenge, request.ip)

    r = g.run("""
        MATCH (r:PasswordResetRequest {challenge:$challenge})-[:REQUEST]-(m:Member) WHERE r.client_ip=$client_ip
        RETURN m
    """, challenge=challenge, client_ip=request.ip).evaluate()

    print(">>> result: ", r)
    if (r):
        return api_response(status.OK, r)
    else:
        return api_response({'isError':True})




# ------------------------------------------------------------------------
@BP.route('/api/reset-password', methods=['POST', 'OPTIONS'])
#@BP.route('/api/reset-password/<challenge>', methods=['POST', 'OPTIONS'])
async def reset_password(request):
    print("PWRSET** ")
    email = request.json.get('email')
    location = request.json.get('location')

    # validate the request
    user = g.run("""
        MATCH (m:Member {email:$email})
        RETURN {
            name: m.name,
            login: [m.login, m.email]
        }
    """, email=email).evaluate()

    # bail out if not found
    if user==None:
        return api_response(status.USER_NOTFOUND)

    print("user: ", user)
    print("="*60)

    subject = 'Reset your password for Historiana'
    challenge = tic.NEW_UUID()

    # text-only variant, we don't have one at the moment
    # can be supported by either creating a text-only template
    # or strip the html from the output of template.render()
    # to avoid two similar templates
    msg = ""

    # used to create transactional links in the mail back to the API
    values = {
        'subject':      subject,
        'host':         location,
        'resetlink':    f'/reset-password/{challenge}',
        'message':      msg,
        'login':        next(u for u in user['login'] if u),
        'dear':         user['name'],
    }
    print("=== ", request.headers)
    print(f'RESET: /reset-password/{challenge}')

    message = mail.create("reset-password", email, values)

    try:
        resp = mail.send(message)
    except:
        print("api.py CANNOT SEND")
        # TODO instead of ABORT log the error
        raise

    # log status; create an entry
    log_entry = g.run("""
        CREATE (l:Log)
        SET
            l.action='reset-password-mail-sent',
            l.ts=datetime()
        RETURN l
    """, to=email).evaluate()

    # store ip of requesting client
    resp['client_ip'] = request.ip

    # extend it with all properties from the response and store them.
    log_entry.update(resp)
    g.push(log_entry)

    # actually create the Reset Request

    g.run("""
        MATCH (m:Member {email:$email})
        CREATE (r:PasswordResetRequest)< -[:REQUEST]-(m)
        SET
            r.ts = datetime(),
            r.challenge = $challenge,
            r.client_ip = $client_ip
        RETURN r
    """, email=email, challenge=challenge, client_ip=request.ip).evaluate()



    return api_response(resp)



# ------------------------------------------------------------------------
@BP.route('/api/login', methods=['POST', 'OPTIONS'])
async def login(request):
    """
    do the login
    ============
    We accept either email or login name (which is an optional attribute on Member)
    The email is lowercased on both ends before matching, the login is not.
    """
    print("/api/login")
    if request.method == 'OPTIONS':
        # mark all options valid during development
        print("OPTIONS REQUEST")
        return json([], headers=HEADERS)

    #print("*** req ***", request.method, request.json)
    #print("== Process login request")
    #print("json :", request.json)

    if not request.json.get('password'):
        print("api/login: missing password")
        return api_response(status.ERROR, dict(msg="Password is required."))

    params = {
        'email': request.json.get('login', '').lower(),
        'login': request.json.get('login', ''),
        'password':  hashlib.sha1(request.json.get('password').encode()).hexdigest(),
    }


    result = g.run(
        """
        MATCH (m:Member {password:$password})
        WHERE toLower(m.email)=$email OR m.login=$login
        RETURN m LIMIT 1
    """, params).evaluate()

    # print("api/login user lookup: ", json_dumps(result, indent=4))

    if result:
        #print("login found")
        # we return the complete Node minus the password
        del result['password']
        # and add some session specific values
        result['isError'] = False
        result['msg'] = 'LOGIN OK'
        result['remote'] = tic.get_remote_addr(request)
        result['session_uuid'] = tic.NEW_UUID()
        # create a session and fetch user data
        r = g.run("""
            MATCH (u:Member {uuid:$uuid})
            CREATE (
                    s:Session {
                        sessionId: $session_uuid,
                        sessionValid: true,
                        ts:timestamp(),
                        remoteAddr: $remote
                    }
                ) <- [:LOGGED_IN] - (u)

            SET u.last_login = timestamp()
            WITH u,s
            OPTIONAL MATCH (c:Country)-[x:LIVES_IN]-(u)
            WHERE x.ts IS NOT NULL
            WITH u, c.code AS countrycode, s
            ORDER BY x.ts DESC LIMIT 1
            WITH u, countrycode, s
            OPTIONAL MATCH (u)-[:MYTAG]-(tag)
            WITH u,countrycode,tag, s
            ORDER BY toUpper(tag.name)
            WITH u,countrycode,tag, s

            OPTIONAL MATCH (u)-[:PARTNER_ADMIN]-(p:Partner)
            OPTIONAL MATCH (u)-[:PARTNER_MEMBER]-(mp:Partner)

            WITH u,countrycode,tag, s, p, mp,
            count(tag) as has_tags,
            collect({name:tag.name,uuid:tag.uuid,tag:labels(tag)[0]}) as tags,
            collect( distinct labels(tag)) as tagcloud

            RETURN {
                session: s,
                user: u,
                country: countrycode,
                uuid: u.uuid,
                has_tags: has_tags,
                tags: tags,
                tagcloud: tagcloud,
                partner_admin: p,
                partner_member: mp
            }

            """, result).evaluate()

        # see User.setStore
        reply = {}
        reply['authenticated'] = True
        reply['isError'] = False
        reply['msg'] = 'LOGIN OK'
        reply['remote'] = tic.get_remote_addr(request)
        # reply['uuid'] = r['user']['uuid']
        reply['data'] = r


        #print("login created session: ", json_dumps(reply, indent=4))
        return json(reply)

    else:
        result = {'isError': True, 'msg': 'Invalid credentials'}

    return json(result)

# ------------------------------------------------------------------------

@BP.route('/api/register', methods=['POST', 'OPTIONS'])
async def register(request):
    """
    register a new login
    ====================
    """

    if request.method == 'OPTIONS':
        # mark all options valid during development
        return json([], headers=HEADERS)

    #print("*** req ***", request.method, request.json)
    #print("Process register request")
    #print("req: ", request.json)
    #         'password':  hashlib.sha1(request.json.get('password').encode()).hexdigest(),

    params = {
        'login': request.json.get('login', '').strip(),
        'password': hashlib.sha1(request.json.get('password').encode()).hexdigest(),
        'user_uuid': tic.NEW_UUID(),
        'session_uuid': tic.NEW_UUID()
    }

    res = g.run(
        """
        MATCH (m:Member) WHERE toLower(m.login)=$login OR toLower(m.email)=$login
        RETURN m LIMIT 1
        """, params).evaluate()

    # only if there is a None result we can continue
    if res:
        result = {'isError': True, 'msg': 'Login name already exists.'}
        return json(result)

    else:
        res = g.run("""
            CREATE (m:Member {
                email:$login,
                password:$password,
                joined:timestamp(),
                uuid:$user_uuid
            })

            WITH m
            CREATE (s:Session {
                sessionId: $session_uuid,
                sessionValid: true,
                ts:timestamp()
            }) <- [:LOGGED_IN] - (m)

            RETURN {
                user: m,
                session: s.sessionId
            }
            """, params).evaluate()

        if res:
                result = res['user']
                result['session'] = res['session']
                result['isError'] = False
                result['msg'] = 'Member created'
        else:
                result['isError'] = True
                result['msg'] = 'Failed to create member'

    return json(result, headers={'Access-Control-Allow-Origin': '*'})

# ------------------------------------------------------------------------
@BP.route('/api/sas/explore/<slug>', methods=['OPTIONS', 'POST', 'GET'])
async def sas_explore(request, slug=None):
    """show specific collection"""


    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    print("sas-explore-slug: ", slug)
    res = g.run(
        """
          MATCH (col:Collection {slug:$slug})-[:IN_COLLECTION]-(entry:CollectionItem)
          OPTIONAL MATCH (entry)-[:HAS_LICENSE]->(l:License)
          OPTIONAL MATCH (entry)-[:HAS_PARTNER]->(itempartner:Partner)
          OPTIONAL MATCH (col)-[:THUMB_FOR]-(thumb)
          OPTIONAL MATCH (ml:License)<-[:HAS_LICENSE]-(col)

          OPTIONAL MATCH (partner:Partner)-[:HAS_PARTNER]-(col)
          OPTIONAL MATCH (partner)-[:IS_LOGO]-(logo)
          OPTIONAL MATCH (partner)-[:IS_ICON]-(icon)

          OPTIONAL MATCH (itempartner)-[:IS_LOGO]-(itemlogo)
          OPTIONAL MATCH (itempartner)-[:IS_ICON]-(itemicon)


          WITH
            case when thumb.path is null
                then "/uploads/"+thumb.uuid+"_"+thumb.filename
                else "/ua/"+thumb.path
            end as thumb,

            case when itempartner.name is null
                then
                    partner {
                        .*,
                        logo: logo.url,
                        icon: icon.url
                    }
                else
                    itempartner {
                        .*,
                        logo: itemlogo.url,
                        icon: itemicon.url
                    }
                end as partner,


            entry, col, l, ml

          RETURN col,entry,l,ml,partner,thumb
          ORDER BY entry.order
        """, slug=slug)

    # TODO returns ugly data!
    # better change into one object with items[] array
    # SASExploreSlug.vue needs to be changed too.
    data = res.data()
    #print(data)
    reply = {'status': 'ok', 'data': data}
    # print(reply)
    return json(reply)

# ------------------------------------------------------------------------
@BP.route('/api/sas/explore', methods=['OPTIONS', 'POST'])
async def sas_explore_index(request):
    """show index of collections"""
    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    res = g.run(
        """
        MATCH (col:Collection) OPTIONAL MATCH (col)-[:THUMB_FOR]-(thumb:NewUpload)
        RETURN col, thumb
        """)

    # print("res: ", res)

    reply = {'status': 'ok', 'data': res.data()}

    return json(reply)



# ------------------------------------------------------------------------
@BP.route('/api/addSource', methods=['OPTIONS', 'POST'])
async def add_source(request, slug=None):
    """add source to My Sources collection"""
    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    user = request.json.get('user')
    source = request.json.get('source')
    # print('========================')
    # print("user: ", user)
    # print("src: ", source)
    ## url = """<img :src="'http://sas.historiana.eu/objects/'+e.col.object+'/images/'+e.entry.filename"/>"""

    res = g.run(
        """
        MATCH (s {uuid:$source})-[:IN_COLLECTION]-(col)
        MATCH (m:Member {uuid:$user})
        CREATE (s)<-[:ORIGIN]-(ms:MySources)<-[:SOURCE]-(m)
        SET
            ms.uuid = $new_uuid,
            ms.created = timestamp(),
            ms.title = s.title,
            ms.description = s.description,
            ms.url = s.url
        RETURN ms
        """, source=source, user=user, new_uuid=tic.NEW_UUID())

    reply = {'status': 'ok', 'data': res.data()}
    return json(reply)


# ------------------------------------------------------------------------
@BP.route('/api/addSources', methods=['OPTIONS', 'POST'])
async def add_sources(request, slug=None):
    """add sources to My Sources collection"""
    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)

    user = request.json.get('user')
    sources = request.json.get('sources')
    print("user: ", user)
    print("src: ", sources)
    ## url = """<img :src="'http://sas.historiana.eu/objects/'+e.col.object+'/images/'+e.entry.filename"/>"""

    for source in sources:
        res = g.run("""
            MATCH (s {uuid:$source})-[:IN_COLLECTION]-(col)
            MATCH (m:Member {uuid:$user})
            CREATE (s)<-[:ORIGIN]-(ms:MySources)<-[:SOURCE]-(m)
            SET
                ms.uuid = {new_uuid},
                ms.created = timestamp(),
                ms.title = s.title,
                ms.description = s.description,
                ms.url = s.url
            RETURN ms
            """, source=source, user=user, new_uuid=tic.NEW_UUID())

    reply = {'status': 'ok', 'data': res.data()}
    return json(reply)
# ------------------------------------------------------------------------

@BP.route('/api/testdata', methods=['OPTIONS', 'POST','GET'])
async def testdata(request, slug=None):
    """test stuff"""
    from time import sleep
    import random
    n = int(random.random()*100)
    bla = {'id': n, 'name': 'Sven {}'.format(n), 'registered': False}

    cmd = [ sys.executable, f"{tasks.__path__}/excel_import.py"]

    reply = {'status': 'ok', 'data': cmd}
    print(reply)
    return json(reply)

# ------------------------------------------------------------------------
@BP.route('/api/startsession', methods=['POST', 'OPTIONS'])
async def startsession(request):
    """start a new session"""
    print("** startsession ", request.json)
    if request.method == 'OPTIONS':
        # mark all options valid during development
        return json([], headers=HEADERS)

    # params = {
    #     'session': request.json.get('session', '')
    # }

        #     RETURN {
        #     user: u,
        #     session: s,
        #     country: countrycode,
        #     uuid: u.uuid,
        #     has_tags: count(tag),
        #     tags: collect( distinct {name:tag.name,uuid:tag.uuid,tag:labels(tag)[0]}),
        #     activities: collect( distinct {name:a.title,uuid:a.uuid}),
        #     tagcloud: collect( distinct labels(tag)),
        #     partner_admin: p,
        #     partner_member: mp
        # }

    r = g.run("""
        MATCH (s:Session {sessionId:$session})-[]-(u:Member)

        OPTIONAL MATCH (c:Country)-[x:LIVES_IN]-(u)
        WHERE x.ts IS NOT NULL
        WITH u, c.code AS countrycode, s
        ORDER BY x.ts DESC limit 1
        WITH u,countrycode,s
        OPTIONAL MATCH (u)-[]-(a:MyActivity)
        WITH u,countrycode,s,a
        OPTIONAL MATCH (u)-[:PARTNER_ADMIN]-(p:Partner)
        OPTIONAL MATCH (u)-[:PARTNER_MEMBER]-(mp:Partner)

        CALL {
          MATCH (s:Session {sessionId:$session})-[]-(u:Member)
          MATCH (u)-[:MYTAG]-(tag)
          WITH tag
          ORDER BY tag.name
          RETURN
            collect( distinct {name:tag.name,uuid:tag.uuid,tag:labels(tag)[0]}) as tags,
            collect( distinct labels(tag)) as tagcloud
        }

        WITH tags, tagcloud,
             collect( distinct {name:a.title,uuid:a.uuid}) as activities,
             u,countrycode,s,a,p,mp

        RETURN {
            user: u,
            session: s,
            country: countrycode,
            uuid: u.uuid,
            has_tags: size(tags),
            tags: tags,
            activities: activities,
            tagcloud: tagcloud,
            partner_admin: p,
            partner_member: mp
        }

        """, session=request.json.get('session')).evaluate()

    #result['countrycode'] = r['countrycode']

    if r:
        # dont print r, it can be huge!
        result = {}
        # we return the complete Node minus the password
        # del r['password']
        result['isError'] = False
        result['msg'] = 'LOGIN OK'
        result['remote'] = tic.get_remote_addr(request)
        result['session_uuid'] = tic.NEW_UUID()
        result['uuid'] = r['user']['uuid']
        result['data'] = r

        # create a session
        sess = g.run("""
            MATCH (m:Member {uuid:$uuid})
            CREATE (
                s:Session {
                    sessionId: $session_uuid,
                    sessionValid: true,
                    ts:timestamp()
                }
            ) <- [:LOGGED_IN] - (m)
            RETURN s.sessionId
        """, result).evaluate()

        result['session'] = sess
        result['authenticated'] = True
    else:
        result = {'isError': True, 'msg': 'Invalid credentials'}

    return json(result)

# ------------------------------------------------------------------------
@BP.route('/api/create-anonymous-session', methods=['POST', 'OPTIONS'])
async def create_anonymous_session(request):
    """create a new session"""
    print("create_anonymous_session")
    if request.method == 'OPTIONS':
        # mark all options valid during development
        return json([], headers=HEADERS)

    params = {
        'remote': tic.get_remote_addr(request),
        'session_uuid': tic.NEW_UUID(),
    }

    r = g.run("""
        CREATE (
        s:Session {
            remoteAddr: $remote,
            sessionId: $session_uuid,
            sessionValid: true,
            ts:timestamp()
        }) return s
    """, params).evaluate()

    if r:
        result = {}
        result['isError'] = False
        result['msg'] = 'REQUEST OK'
        result['remote'] = params['remote']
        result['session_uuid'] = params['session_uuid']
        result['authenticated'] = False
        result['anonymous'] = True

    else:
        result = {'isError': True, 'msg': 'Invalid request'}

    return json(result)


# ------------------------------------------------------------------------
@BP.route('/api/tags/<object>', methods=['GET','OPTIONS', 'DELETE', 'PUT'])
async def tags_create(request, object):
    """tags"""
    if request.method == 'OPTIONS':
        # mark all options valid during development
        return json([], headers=HEADERS)

    if request.method=='GET':
        print("GET: ", object)
        result = { 'isError': False, 'msg': 'ok' }

    else:

        params = {
            'user': request.json.get('user'),
            'target': request.json.get('target'),
            'tag': request.json.get('tag')
        }

        if request.method=='PUT':
            print("CREATE TAG: ", params)
            r = g.run("""
                MATCH (m:Member {uuid:$user})-[:SOURCE]->(target {uuid:$target})
                MATCH (m:Member {uuid:$user})-[:MYTAG]->(tag {uuid:$tag})
                MERGE (target)-[r:MYTAG]-(tag)
                RETURN r
            """, params).evaluate()

            result = { 'isError': False, 'msg': 'ok' }

        if request.method=='DELETE':
            print("DELETE TAG: ", params)
            r = g.run("""
                MATCH (m:Member {uuid:$user})-[:SOURCE]->(target {uuid:$target})
                MATCH (m:Member {uuid:$user})-[:MYTAG]->(tag {uuid:$tag})
                MATCH (target)-[r:MYTAG]-(tag)
                DELETE r
                RETURN r
            """, params).evaluate()

            result = { 'isError': False, 'msg': 'ok' }


    return json(result)

# ------------------------------------------------------------------------
@BP.route('/api/publish-narrative', methods=['POST'])
async def publish_narrative(request):

    params = {
        'uuid': request.json.get('uuid'),
        'user': request.json.get('user')
    }
    # TODO: validate that user OWNS this Activity
    r = g.run("MATCH (a:Narrative {uuid:$uuid}) SET a.is_published=true RETURN a", params)
    reply = {'status': 'ok'}
    return json(reply)


# ------------------------------------------------------------------------
@BP.route('/api/unpublish-narrative', methods=['OPTIONS', 'POST'])
async def unpublish_narrative(request):

    params = {
        'uuid': request.json.get('uuid'),
        'user': request.json.get('user')
    }

    # TODO: validate that user OWNS this Activity
    r = g.run("MATCH (a:Narrative {uuid:$uuid}) SET a.is_published=false RETURN a", params)
    print("unpub: ", r)
    reply = {'status': 'ok'}
    return json(reply)



# ------------------------------------------------------------------------
@BP.route('/api/publish-activity', methods=['OPTIONS', 'POST'])
async def publish_activity(request):
    if request.method == 'OPTIONS':
        # mark all options valid during development
        return json([], headers=HEADERS)

    params = {
        'uuid': request.json.get('uuid'),
        'user': request.json.get('user')
    }

    # TODO: validate that user OWNS this Activity
    r = g.run("MATCH (a:MyActivity {uuid:$uuid}) SET a.is_published=true RETURN a", params)
    reply = {'status': 'ok'}
    return json(reply)

# ------------------------------------------------------------------------
@BP.route('/api/unpublish-activity', methods=['OPTIONS', 'POST'])
async def unpublish_activity(request):
    if request.method == 'OPTIONS':
        # mark all options valid during development
        return json([], headers=HEADERS)

    params = {
        'uuid': request.json.get('uuid'),
        'user': request.json.get('user')
    }

    # TODO: validate that user OWNS this Activity
    r = g.run("MATCH (a:MyActivity {uuid:$uuid}) SET a.is_published=false RETURN a", params)
    reply = {'status': 'ok'}
    return json(reply)

# ------------------------------------------------------------------------
@BP.route('/api/get-teaching-learning', methods=['OPTIONS', 'POST', 'GET'])
async def get_teaching_learning(request):
    if request.method == 'OPTIONS':
        return json([], headers=HEADERS)


    """
    Through the introduciton of neo4j 5 the exists() function for a property
    ceased to exists. We boldy assume that the syntax used below :

    image: CASE img.upload_uuid WHEN true

    works and gives the wanted results.

    It used to be image: CASE exists(img.upload_uuid) WHEN true

    This code:

    match (a:Member) return CASE a.is_admin WHEN true
                   THEN "ja"
                   ELSE "nee"
                   END
    limit 10

    works as expected, so assuming this code works as well.

    however not sure if we need the image and image_url for the learning activities in the reply.
    """

    ela = g.run("""
        MATCH (e:MyActivity {is_published:true})
        OPTIONAL MATCH (ua:UserAsset)-[]-(e)
        OPTIONAL MATCH (e)-[:IS_OWNER]-(m:Member)
        OPTIONAL MATCH (e)-[:HTAGGED]-(tag:Tag)
        WITH collect(tag) as tags, count(ua) AS has_image, e, m, ua
        RETURN collect({
            type: 'e-Learning Activity',
            typecode: 'ela',
            activity: properties(e),
            image: properties(ua),
            has_image: has_image,
            icon: apoc.text.join(['/ua',ua.path],'/'),
            image_url: apoc.text.join(['','ua',ua.path],'/'),
            owner: { name: m.name, uuid: m.uuid, login: m.login },
            tags: tags
        }) AS r

        """).evaluate()

    # dont match the image optionally, breaks in 4.0
    la = g.run("""
        MATCH (e:LearningActivity)
        MATCH (e)-[:ICONIC_IMAGE]-(img)
        OPTIONAL MATCH (e)-[:HTAGGED]-(tag:Tag)
        WITH collect(tag) as tags, collect(img)[0] as img, e, count(img) as has_image
        OPTIONAL MATCH (e)-[:IS_OWNER]-(m:Member)
        RETURN collect( DISTINCT {
            type: 'Learning Activity',
            typecode: 'la',
            url: '/learning-activity/'+e.slug,
            uuid: e.uuid,
            activity: properties(e),
            upload_uuid: img.upload_uuid,
            gewone_uuid: img.uuid,
            icon: apoc.text.join(['/ua',img.path],'/'),
            image: CASE img.upload_uuid WHEN true
                   THEN apoc.text.join(['objects',img.upload_uuid,img.thumb],'/')
                   ELSE apoc.text.join(['objects',img.uuid,img.thumb],'/')
                   END,
            x: img,
            has_image: has_image,
            image_url: CASE img.upload_uuid WHEN true
                       THEN apoc.text.join(['objects',img.upload_uuid,img.thumb],'/')
                       ELSE apoc.text.join(['objects',img.uuid,img.thumb],'/')
                       END,
            owner: { name: m.name, uuid: m.uuid, login: m.login },
            tags: tags
        })

        """).evaluate()

    reply = {'status': 'ok', 'data':  la + ela}
    # there is interchange.DateTime crap in the data, not sure which fields
    # use HiJSONdump to deal with these for now
    from tic.hijson import HiJSONdump
    return json(reply, dumps=HiJSONdump)
    #return json_dumps(reply)

# ------------------------------------------------------------------------

# ------------------
@BP.route('/api/checkRegisterEmail', methods=['OPTIONS', 'POST'])
async def register_check_email(request):
    print("check email ", request.json)
    email = request.json.get('email')
    if not email:
        reply = dict(isError=True, msg="Bad request")
        return json(reply)

    if not '@' in email:
        reply = dict(isError=True, msg="Invalid email")
        return json(reply)

    e = g.run("MATCH (m:Member {email:$email}) RETURN m.email", email=email).evaluate()
    print("e: ", e)
    if e:
        reply = dict(isError=True, msg="Email already exists")
        return json(reply)

    reply = dict(isError=False)
    return json(reply)



@BP.route('/api/avatar/<uuid>', methods=['OPTIONS', 'POST'])
async def avatar_upload(request, uuid):
    if request.method == 'OPTIONS':
        return json([]) #, headers=HEADERS)

    for k in request.files.keys():
        file = request.files.get(k)
        path = "/".join([CONFIG.USER_UPLOADS,'avatars'])
        destination = '{}/{}.jpg'.format(path,uuid)
        os.makedirs(path, exist_ok=True)
        try:
            os.makedirs(path, exist_ok=True)
            with open(destination, 'wb') as dest:
                dest.write(file.body)
                dest.close()
        except:
            pass
    return api_response(status.OK)


@BP.route('/api/doUpload', methods=['OPTIONS', 'POST'])
async def do_upload(request):
    """
    used for Partner Logo/Icon upload
    see admin/Partners.vue
    """

    if request.method == 'OPTIONS':
        return json([]) #, headers=HEADERS)

    print("** UPLOAD ")
    print("form: ", request.form)
    #print("file: ", request.files)
    print("addr: ", request.headers.get('remote_addr'))

    path = "/".join([CONFIG.USER_UPLOADS,request.form.get('context')])
    for k in request.files.keys():
        file = request.files.get(k)
        #print("file: ", file.keys())
        uuid = tic.NEW_UUID()
        destination = '{}/{}.jpg'.format(path,uuid)
        base_url = "/".join([CONFIG.USER_UPLOADS_URL,request.form.get('context')])
        url = '{}/{}.jpg'.format(base_url,uuid)
        print("dest: ", destination)

        os.makedirs(path, exist_ok=True)
        errors = False
        try:
            os.makedirs(path, exist_ok=True)
            with open(destination, 'wb') as dest:
                dest.write(file.body)
                dest.close()
        except Exception as e:
            errors = True
            return api_response(status.ERROR, msg=str(e))

        # store an Upload() node
        upload = {
            'user': request.form.get('user'),
            'filename': file.name,
            'remote_addr': request.headers.get('remote_addr'),
            'url': url,
            'uuid': uuid
        }

        # TODO: add digest?
        g.run("""
            CREATE (u:Upload)
            SET
                u.filename=$filename,
                u.url=$url,
                u.uuid=$uuid
            WITH u
            MATCH (m:Member {uuid:$user})
            CREATE (m)-[r:UPLOADED]->(u)
            SET
                r.remote_addr = {remote_addr},
                r.ts = timestamp()
            RETURN u
        """, upload)

        # connect as logo / icon

        partner = request.form.get('partner')
        context = request.form.get('context').upper()

        if partner and context:

            context = "IS_{}".format(context)
            print("c: ", context)
            print("p: ", partner)
            q = """
                MATCH (p:Partner {{uuid:'{partner}'}})
                OPTIONAL MATCH (p)-[old:$context]-(u:Upload)
                DETACH DELETE old
                WITH p
                MATCH (asset:Upload {{uuid:'{uuid}'}})
                CREATE (p)-[:$context]->(asset)
            """.format(partner=partner,context=context,uuid=uuid)
            r = g.run(q)
            print("q: ", q)
            print("res:", r)



        reply = {
            'name': 'Upload',
            'url': url,
            'uuid': uuid,
            'context': request.form.get('context')
        }
        return api_response(status.OK, reply)
        print("*** ", upload)

    # for k in request.files.keys():
    #     file = request.files.get(k)
    #     path = "/".join([CONFIG.USER_UPLOADS,'avatars'])
    #     destination = '{}/{}.jpg'.format(path,uuid)
    #     os.makedirs(path, exist_ok=True)
    #     try:
    #         os.makedirs(path, exist_ok=True)
    #         with open(destination, 'wb') as dest:
    #             dest.write(file.body)
    #             dest.close()
    #     except:
    #         pass
    return api_response(status.OK)


# ==================================================================
def get_id_field(form):
    # return the unique ID field for a form
    for f in form['fields']:
        if f['type']=='id':
            return f['name']
    return None


# ==================================================================
def get_node(value, form):
    # get the "node" information for a field
    print("getvalue: {}".format(value))
    for f in form['fields']:
        #print("f: ", f)
        if f['name']==value:
            print("GETNODE: ", f['node'])
            return f['node']
    return None

# ==================================================================
@BP.route('/api/generic', methods=['OPTIONS', 'POST'])
async def do_generic(request):
    if request.method == 'OPTIONS':
        return json([]) #, headers=HEADERS)

    form = request.json

    print("store: ", form.get('store'))
    print("id   : ", get_id_field(form))
    print("keys : ", form.get('data').keys())
    for (k,v) in form.get('data').items():
        print("{:12} : {}".format(k,v))

    # if there is a uuid in the form it is not new
    is_new_record =  get_id_field(form) not in form.get('data').keys()
    print("new: ", is_new_record)

    if is_new_record:
        uuid = tic.NEW_UUID()
        q = "CREATE (r:{}) SET r.uuid='{}' RETURN r".format(form.get('store'), uuid)
        record = g.run(q).evaluate()
    else:
        uuid = form.get('data').get('uuid')
        q = "MATCH (r:{} {{uuid:'{}'}}) RETURN r".format(form.get('store'), uuid)
        #print("q: ", q)
        record = g.run(q).evaluate()

    record.update(form.get('data'))
    g.push(record)

    for r in form.get('relations'):

        #m = NodeMatcher(g)

        # get relationship
        meta = get_node(r['context'], form)
        print("xxx: ", meta['name'], "uuid",  r['uuid'])
        # related = g.find_one(meta['name'], 'uuid', r['uuid'])

        related = g.match(meta['name'], uuid=r['uuid'])
        rel = Relationship(record, meta['relation'], related)
        g.create(rel)
        #print("=======================================================")

    return api_response(status.OK, uuid=uuid)

# ==================================================================
@BP.route('/api/generic-delete', methods=['OPTIONS', 'POST'])
async def do_generic_delete(request):
    if request.method == 'OPTIONS':
        return json([]) #, headers=HEADERS)
    print("===DELETE===")
    print("r: ", request.json)
    uuid = request.json.get('uuid')
    if not uuid:
        return api_response(status.NO_DATA)

    g.run("MATCH (n {uuid: {uuid}}) DETACH DELETE n", uuid=uuid)
    return api_response(status.OK)

# ==================================================================
@BP.route('/api/generic-edit', methods=['OPTIONS', 'POST'])
async def do_generic_edit(request):
    print("## generic edit ##")
    if request.method == 'OPTIONS':
        return json([]) #, headers=HEADERS)

    # print("## request: ", request.json)

    uuid = request.json.get('uuid')
    if not uuid:
        return api_response(status.NO_DATA)

    recs = g.run("""
        MATCH (n {uuid: {uuid}})
        OPTIONAL MATCH (n)-[r]-(rn)
        RETURN n,r,rn,
            replace(toLower(type(r)),'is_','') as context,
            labels(rn)[0] as relationType
    """, uuid=uuid)

    relations = []

    for i in recs:
        node = i.get('n')
        related = i.get('rn')
        reply = {
            'name': i['relationType'],
            'url': related['url'] if related else None,
            'uuid': related['uuid'] if related else None,
            'context': i['context']
        }

        if i.get('r'): relations.append(reply)

    return api_response(status.OK, record=node, relations=relations)

# ==================================================================
@BP.route('/api/generic-list', methods=['OPTIONS', 'POST'])
async def do_generic_list(request):
    """make a list"""

    if request.method == 'OPTIONS':
        return json([]) #, headers=HEADERS)

    r = request.json

    ## XXX TODO CHECK 4.0 behaviour
    q = "MATCH (e:{}) RETURN COLLECT(e)".format(r['store'])
    data = g.run(q).evaluate()
    print("q: ", q)
    return api_response(status.OK, records=data)


# ==================================================================
@BP.route('/api/get-copyright-list', methods=['GET'])
async def get_copyright_list(request):
    data = g.run("""
        MATCH (c:Copyright)
        WITH c
        ORDER BY c.name
        RETURN collect({
            name: c.name,
            uuid: c.uuid
        })
    """).evaluate()
    return api_response(status.OK, data)


# ==================================================================
@BP.route('/api/get-partner-list', methods=['GET'])
async def get_partner_list(request):
    partners = g.run("""
        MATCH (p:Partner)
        WITH p
        ORDER BY p.name
        RETURN collect({
            name: p.name,
            uuid: p.uuid
        })

    """).evaluate()
    return api_response(status.OK, partners)


# ==================================================================
@BP.route('/api/store-meta', methods=['OPTIONS', 'POST'])
async def store_meta(request):
    print("### STORE META ###")
    if request.method == 'OPTIONS':
        return json([])

    r = request.json
    if not r.get('target'):
        return api_response(status.NO_DATA)

    # lookup source (==target) and Partner ; connect them and add meta
    # the relation

    print("store meta ", json_dumps(r, indent=4))

    # delete existing connection if is there
    d = g.run("""
        MATCH (t {uuid:$target})-[rel:HAS_PARTNER]->(p:Partner)
        DELETE rel
    """, target=r.get('target'))

    rel = g.run("""
        MATCH (t {uuid: {target}})
        MATCH (p:Partner {uuid:$partner})
        MERGE (t)-[rel:HAS_PARTNER]->(p)
        SET
        rel.meta = {meta},
        rel.text = {text}
        RETURN rel
    """, **r)


    return api_response(status.OK)



# ==================================================================
# was used in hiCopyright.vue; which is replaced by now.
# keeo it for reference due to the weird $rel syntax
# @BP.route('/api/set-relation', methods=['OPTIONS', 'POST'])
# async def set_relation(request):
#     if request.method == 'OPTIONS':
#         return json([])

#     r = request.json

#     if r.get('delete'):
#         # delete existing relation
#         q = """
#             MATCH (s {{uuid:'{source}'}})
#             MATCH (t {{uuid:'{old}'}})
#             MATCH (s)-[r:$rel]->(t)
#             DELETE r
#         """.format(**r)
#         print("DELETE RELATION: ", q)
#         g.run(q)

#     # add relation
#     q = """
#         MATCH (s {{uuid:'{source}'}})
#         MATCH (t {{uuid:'{target}'}})
#         MERGE (s)-[:$rel]->(t)
#         RETURN s
#     """.format(**r)

#     print ("ADD RELATION: ", q)

#     g.run(q)

#     return api_response(status.OK)


# ------------------------------------------------------------------------
@BP.route('/api/get-learning-activity/<slug>', methods=['GET'])
async def get_learning_activity(request, slug):
    print("--> LA GET: ", slug)
    # adapt old path for icon

    # TODO; set related to NULL instead an empty object
    # rework this:
    # MATCH (p) WHERE id(p) = 11
    # OPTIONAL MATCH (p) -[:car]- (c)
    # OPTIONAL MATCH (p) -[:driver]- (u)
    # RETURN {
    #   _id: id(p), name: p.name, type: p.type,
    #   cars: CASE WHEN c IS NOT NULL THEN collect({_id: id(c), name: c.name}) ELSE NULL END,
    #   drivers: CASE WHEN u IS NOT NULL THEN collect({_id: id(u), name: u.email}) ELSE NULL END
    # } AS place
    #
    # https://stackoverflow.com/questions/30044581/neo4j-optional-match-and-null


    la = g.run("""
        MATCH (lt:LinkType)
        WITH COLLECT (lt) AS lt

        MATCH (la:LearningActivity {slug:$slug})

        CALL {
            MATCH (la)-[:LA_RELATED]-(related:LearningActivity)
            RETURN collect({
                title: related.title,
                slug: related.slug
            }) as related
        }

        WITH la, lt, related
        OPTIONAL MATCH (la)-[:TAG]-(tags:Tag)
        WITH la, lt, related, collect(distinct tags) as tags

        // we do not specify Asset or UserAsset as it can be either
        OPTIONAL MATCH (la)-[:INTEREST_TEACHER]-(teacher_links)-[:LINK_ASSET]-(asset)
        OPTIONAL MATCH (asset)-[:LINK_ASSET]-(l:Link)-[:LINK_TYPE]-(linktype:LinkType)
        WITH
        COLLECT ({
            title: asset.title,
            filename: asset.filename,
            uuid: asset.filename,
            xurl: '/objects/'+asset.uuid+'/'+asset.filename,
            burp: teacher_links,
            prub: asset,
            url: CASE asset.upload_uuid
                 WHEN true THEN apoc.text.join(['objects',asset.upload_uuid,asset.filename],'/')
                 ELSE apoc.text.join(['objects',asset.uuid,asset.filename],'/')
                 END,
            type: linktype.name,
            icon: replace(linktype.icon, '/assets/images/icons/', '/static/la/')

        }) as teacher_links, la, lt, related, tags

        OPTIONAL MATCH (la)-[:INTEREST_STUDENT]-(student_links)-[:LINK_ASSET]-(asset)
        OPTIONAL MATCH (asset)-[:LINK_ASSET]-(l:Link)-[:LINK_TYPE]-(linktype:LinkType)

        WITH
        COLLECT ({
            title: asset.title,
            filename: asset.filename,
            uuid: asset.filename,
            xurl: '/objects/'+asset.uuid+'/'+asset.filename,
            prub: asset,
            url: CASE asset.upload_uuid WHEN true
                 THEN apoc.text.join(['objects',asset.upload_uuid,asset.filename],'/')
                 ELSE apoc.text.join(['objects',asset.uuid,asset.filename],'/')
                 END,
            type: linktype.name,
            icon: replace(linktype.icon, '/assets/images/icons/', '/static/la/')

        }) as student_links, teacher_links, la, lt, related, tags

        OPTIONAL MATCH (la)-[:IN_AGE]-(age)
        WITH collect(distinct age) AS age, student_links, teacher_links, la, lt, related, tags

        OPTIONAL MATCH (la)-[:HAS_DURATION]-(duration)
        WITH collect(distinct duration) AS duration, age, student_links, teacher_links, la, lt, related, tags

        CALL {
            OPTIONAL MATCH (la)-[:ICONIC_IMAGE]-(thumb)
            RETURN collect(thumb) AS thumb
        }

        OPTIONAL MATCH (la)-[lathink:LA_THINKING]-(think)
        OPTIONAL MATCH (la)-[lamethod:LA_METHODS]-(method)
        OPTIONAL MATCH (la)-[lachallenge:LA_CHALLENGES]-(challenge)

        CALL {
            OPTIONAL MATCH (la)-[:LA_UPLOAD]-(upload)
            RETURN collect(upload) AS upload
        }

        RETURN collect({
            linktype: lt,
            la: properties(la),
            outcomes: split(la.outcomes,'*'),
            thumb: thumb,
            age: age,
            duration: duration,
            related: related,
            contexts: [
                {label: think.label,
                info: replace(think.intro,'\n','<br/>'),
                why: lathink.why},
                {label: method.label, info: method.intro, why: lamethod.why},
                {label: challenge.label, info: challenge.intro, why: lachallenge.why}
            ],
            upload: upload,
            teacher_links: teacher_links,
            student_links: student_links,
            tags: tags

        }) as data

    """, slug=slug).evaluate()

    if not la:
        return api_response(status.NOT_FOUND)

    la = la[0]

    thumb = la['thumb'].pop() if 'thumb' in la else None

    del la['thumb']

    if 'upload_uuid'in thumb:
        la['la']['thumb'] = "/objects/{}/{}".format(thumb['upload_uuid'], thumb['thumb'])

    elif 'path' in thumb:
        la['la']['thumb'] = "/ua/{}".format(thumb['path'])

    else:
        la['la']['thumb'] = "/objects/{}/{}".format(thumb['uuid'], thumb['thumb'])




    return api_response(status.OK, la)



# ------------------------------------------------------------------------
@BP.route('/api/index-historical-content', methods=['GET','POST'])
async def historical_content_index(request):
    print("--> HC GET INDEX --")
    # return items and array of types

    # ptemp is a hack to limit to 1 partner
    # we used to optional match on the  iconic_image
    # but in 4.0 this gives an :
    # py2neo.database.ClientError: TypeError: Expected a String, Number, Boolean, Temporal or Duration, got: NO_VALUE
    # when an image is actually missing
    r = g.run("""
        MATCH (m:Module)
        MATCH (m)-[:ICONIC_IMAGE]->(a:Asset)
        OPTIONAL MATCH (m)-[]-(mt:ModuleType)
        OPTIONAL MATCH (m)-[:HTAGGED]-(tag:Tag)
        WITH collect(tag) as tags, mt.name_plural AS type, m, a
        WITH collect({
            name: m.name,
            intro: m.short_intro,
            uuid: m.uuid,
            slug: m.slug,
            thumb: "/objects/"+a.upload_uuid+"/"+a.thumb,
            type: type,
            csstype: apoc.text.slug(toLower(toString(type))),
            tags: tags
        }) as modules

        MATCH (c:Collection)-[*1..2]-(owner:Member {is_admin:true}) WHERE c.is_published=true
        OPTIONAL MATCH (c)-[:HTAGGED]-(tag:Tag)
        WITH collect(DISTINCT tag) as tags, c, modules

        OPTIONAL MATCH (c)-[:THUMB_FOR]-(a)
        OPTIONAL MATCH (c)-[:HAS_PARTNER]-(partner:Partner)

        WITH modules, c, a, collect(partner)[0] AS partner, tags

        ORDER BY c.ts DESC, c.name ASC
        WITH distinct c, modules,a, partner, tags
        WITH modules,

        COLLECT(distinct {

            name: c.name,
            is_published: c.is_published,
            intro: c.subtitle,
            uuid: c.uuid,
            slug: c.slug,
            type: 'Source collections',
            csstype: 'sas collection',
            partner: partner,
            tags: tags,
            thumb: case when a.path is null
                then "/uploads/"+a.uuid+"_"+a.filename
                else "/ua/"+a.path
                end

        }) as sources


        WITH apoc.coll.flatten([modules,sources]) as data

        MATCH (x:Module)-[]-(xmt:ModuleType)
        WITH data, collect(distinct xmt.name_plural) as types


        RETURN {
            types: types+['Source collections','Viewpoints', 'Narratives'],
            items: data
        }


    """).evaluate()

    if not r or not r['items']:
      return api_response(status.NO_DATA)

    # remove duplicates which are there due to more than 1 partner connection
    # hard to do in cypher, so we do it here;
    # the collect distinct keeps them in :(

    viewpoints = g.run("""
        MATCH (vp:ViewpointCollection {is_visible:true})
        MATCH (m:Member {is_admin:true})
        MATCH (m)-[:OWNS]->(n)
        OPTIONAL MATCH (vp)-[:THUMB_FOR]-(icon)
        OPTIONAL MATCH (vp)-[:HTAGGED]-(tag:Tag)
        WITH vp, icon, collect(DISTINCT tag) as tags

        RETURN collect({
            name: vp.title,
            intro: vp.introduction,
            uuid: vp.uuid,
            slug: vp.slug,
            thumb: '/ua/'+icon.path,
            type: "Viewpoints",
            csstype: "ViewpointCollection",
            tags: tags

        })

    """).evaluate()

    if viewpoints:
      r['items'] = r['items'] + viewpoints




    # ADD: check only is_admin owners..
    narratives = g.run("""
        MATCH (n:Narrative {is_published:true})
        MATCH (m:Member {is_admin:true})
        MATCH (m)-[:OWNS]->(n)
        OPTIONAL MATCH (n)-[:ICONIC_IMAGE]-(icon)
        OPTIONAL MATCH (n)-[:HTAGGED]-(tag:Tag)
        WITH n, icon, collect(DISTINCT tag) as tags



        RETURN collect({
            name: n.title,
            teaser: n.teaser,
            uuid: n.uuid,
            slug: n.slug,
            thumb: icon.url,
            icon_url: icon.url,
            type: "Narratives",
            csstype: "narrative",
            tags: tags
        })

    """).evaluate()

    r['items'] = r['items'] + narratives
    r['types'] = sorted(r['types'])

    seen = []
    unique_items = []
    for offset,item in enumerate(r['items']):

        if item['uuid'] in seen:
            continue
        else:
            seen.append(item['uuid'])
            unique_items.append(item)

    r['items'] = unique_items

    return api_response(status.OK, r)


# ------------------------------------------------------------------------
@BP.route('/api/get-historical-content-org', methods=['OPTIONS', 'POST'])
async def org_historical_content(request):
    """
    handle the Modules
    """
    if request.method == 'OPTIONS':
        return api_response(status.OK, None)

    slug = request.json.get('slug')
    if not slug:
        return api_response(status.NO_DATA)

    print("--> HC GET BY SLUG: ", slug)

    c = g.run("""
        MATCH (m:Module {slug:$slug})
        OPTIONAL MATCH (m)-[]-(cr:Copyright)
        OPTIONAL MATCH (m)-[:ICONIC_IMAGE]-(a:Asset)

        RETURN {
            module: m,
            copyright: cr,
            thumb: "/objects/"+a.uuid+"/"+a.thumb
        }

    """, slug=slug).evaluate()


    # the old ORM processing starts here...
    # for compatibility we need to do it like this for now
    # new stuff needs to be remodeled

    entries = []

    # get the ModuleItem to start on
    start = g.run("""
        MATCH (m:Module {slug:$slug})-[:NEXT]-(mi:ModuleItem)-[:CURRENT]-(c:ContentItem)
        RETURN {
            next: mi,
            chapter: {
                name: c.title,
                slug: c.slug
            }
        }
    """, slug=slug).evaluate()

    # get all the next chapters via the NEXT relation on a ModuleItem
    entries.append(start['chapter'])
    next = start['next']
    while next:
        #print("next: ", next)
        done = True
        for x in next.match_outgoing(rel_type='NEXT'):
            done = False
            #print("x: ", x, x.labels())
            nieuwe = x.end_node()
            #print("nieuwenext: ", nieuwe, nieuwe.labels())
            for iets in x.end_node().match('CURRENT'):
                bla = iets.start_node()
                #print(bla['title'])
                entries.append({'name': bla['title'], 'slug': bla['slug']})

        next = False if done else nieuwe

    # these are the main chapters for a Module
    c['entries'] = entries

    return api_response(status.OK, c)


# ------------------------------------------------------------------------
@BP.route('/api/get-historical-content/<module>/<slug>', methods=['GET'], name="hc-by-m-slug")
@BP.route('/api/get-historical-content/<slug>', methods=['GET'], name="hc-by-slug")
@BP.route('/api/get-historical-content', methods=['GET'], name="hc-index")
async def historical_content(request, slug=None, module=None):
    """modules/keymoments view"""

    if not slug:
        return api_response(status.NO_DATA)

    print("--> HC GET BY SLUG: ", slug)

    # data = g.run("""
    #     MATCH (m:Module {slug:{slug}})
    #     OPTIONAL MATCH (m)-[]-(cr:Copyright)
    #     OPTIONAL MATCH (m)-[:ICONIC_IMAGE]-(a)
    #     OPTIONAL MATCH ()

    #     RETURN m {
    #         .*,
    #         copyright: cr,
    #         image: "/objects/"+a.upload_uuid+"/"+a.thumb

    #     }
    # """, slug=slug).evaluate()


    data = g.run("""
        MATCH (m:Module {slug:$slug})
        OPTIONAL MATCH (m)-[]-(cr:Copyright)
        OPTIONAL MATCH (m)-[:ICONIC_IMAGE]-(a:Asset)

        WITH m,cr,a

        MATCH (m)-[:NEXT*]-(mi:ModuleItem)-[:CURRENT]-(c:ContentItem)
        OPTIONAL MATCH (m)-[:ICONIC_IMAGE]-(chapterthumb)

        WITH m,cr,a,
            collect({
                uuid: c.uuid,
                name: c.title,
                slug: c.slug,
                intro: replace(c.intro,'.\n','<br/>'),
                thumb: CASE chapterthumb.upload_uuid
                       WHEN true THEN apoc.text.join(['/objects',chapterthumb.upload_uuid,chapterthumb.thumb],'/')
                       ELSE apoc.text.join(['/objects',chapterthumb.uuid,chapterthumb.thumb],'/')
                       END
                }) as chapters


        RETURN m {
            .*,
            copyright: cr,
            image: "/objects/"+a.upload_uuid+"/"+a.thumb,
            thumb: CASE a.upload_uuid WHEN true
                   THEN apoc.text.join(['/objects',a.upload_uuid,a.thumb],'/')
                   ELSE apoc.text.join(['/objects',a.uuid,a.thumb],'/')
                   END,
            chapters: chapters
        }

    """, slug=slug).evaluate()


    return api_response(status.OK, data)


# ------------------------------------------------------------------------
@BP.route('/api/get-hc-items', methods=['GET'], name="hc-items-index")
@BP.route('/api/get-hc-items/<slug>', methods=['GET'], name="hc-items-slug")
async def get_hc_items_for_slug(request, slug=None):

    """
    structuur in de graph is een complexe puinbak;
    de content laten we nu even voor wat het is, migreren later
    naar universele oplossing is nodig..

    deze Q matched de items in een hoofdstuk:
    match (c {slug:'the-world-before-the-war'})-[r]-(mi:ModuleItem)-[x:ITEMS]-(ci)-[]-(o:ContentItem) return distinct mi,type(r),type(x),ci,labels(ci), o

    """

    print("--> get items for slug: ", slug)
    if not slug:
        return api_response(status.NO_DATA)

    # the items for the current slug
    items = g.run("""
        MATCH (c {slug:$slug})-[r]-(mi:ModuleItem)-[x:ITEMS]-(ci)-[]-(items:ContentItem)
        OPTIONAL MATCH (items)-[:RELATED_DOWNLOADS]-(dl)-[:LINK_ASSET]-(downloads)
        OPTIONAL MATCH (items)-[:ICONIC_IMAGE]-(icon)
        OPTIONAL MATCH (items)-[:RELATED_LA]-(la)
        WITH items,
            collect(distinct downloads {
                title: downloads.title,
                url: apoc.text.join(['/objects',downloads.upload_uuid,downloads.filename],'/')
            }) as downs,

            collect(distinct {
                url: apoc.text.join(['/objects',icon.uuid,icon.thumb],'/'),
                title: apoc.text.replace(icon.filename, icon.extension, '')
            }) as icon,

            collect(distinct la) as la

        RETURN collect(items {
            .*,
            downloads: downs,
            icon: head(icon),
            la: la
        })

    """, slug=slug).evaluate()

#match (x:ContentItem {slug:'the-world-before-the-war'})
# -[:CURRENT]-(mi)-[:NEXT]->(c)-[:CURRENT]-(y) return collect(y)

    return api_response(status.OK, items)



# ------------------------------------------------------------------------
@BP.route('/api/get-historical-content-old', methods=['OPTIONS', 'POST'])
async def OLD_historical_content(request):
    """
    handle the Modules
    """
    if request.method == 'OPTIONS':
        return api_response(status.OK, None)

    slug = request.json.get('slug')
    if not slug:
        return api_response(status.NO_DATA)

    print("--> HC GET BY SLUG: ", slug)

    c = g.run("""
        MATCH (m:Module {slug:$slug})
        OPTIONAL MATCH (m)-[]-(cr:Copyright)
        OPTIONAL MATCH (m)-[:ICONIC_IMAGE]-(a:Asset)

        RETURN {
            module: m,
            copyright: cr,
            image: "/objects/"+a.upload_uuid+"/"+a.thumb,
            thumb: CASE EXISTS(a.upload_uuid) WHEN true THEN apoc.text.join(['/objects',a.upload_uuid,a.thumb],'/') ELSE apoc.text.join(['/objects',a.uuid,a.thumb],'/') END,
            aa: a
        }

    """, slug=slug).evaluate()


    # the old ORM processing starts here...
    # for compatibility we need to do it like this for now
    # new stuff needs to be remodeled

    entries = []
    items = {}

    # get the ModuleItem to start on
    start = g.run("""
        MATCH (m:Module {slug:$slug})-[:NEXT]-(mi:ModuleItem)-[:CURRENT]-(c:ContentItem)
        OPTIONAL MATCH (m)-[:ICONIC_IMAGE]-(a)
        RETURN {
            next: mi,
            chapter: {
                name: c.title,
                slug: c.slug,
                intro: replace(c.intro,'.\n','<br/>'),
                thumb: CASE EXISTS(a.upload_uuid) WHEN true THEN apoc.text.join(['/objects',a.upload_uuid,a.thumb],'/') ELSE apoc.text.join(['/objects',a.uuid,a.thumb],'/') END
            }
        }
    """, slug=slug).evaluate()


    # get all the next chapters via the NEXT relation on a ModuleItem
    entries.append(start['chapter'])
    next = start['next']
    items[start['chapter']['slug']] = []
    current = start['chapter']['slug']

    # process the current Module/Chapter
    for iets in next.end_node().match('ITEMS'):
        # print("NEXT: ", iets, iets.labels())
        # print("IETSS: ", iets.start_node().labels())
        # print("IETSE: ", iets.end_node().labels())

        print("INTRO: ", iets['intro'])
        dieper = iets.end_node()

        for bla in dieper.match('CURRENT'):

            print("bla: ", bla.end_node(), bla.labels())
            #print("x: ", bla.start_node()['title'])
            #rec = dict(
            #    title = bla.start_node()['title'],
            #    slug = bla.start_node()['slug']
            #)
            # items[current].append(bla.start_node())
            data = dict(item = bla.start_node())
            data['downloads'] = []
            data['learning_activities'] = []
            for foobar in bla.start_node().match('RELATED_DOWNLOADS'):
                link = foobar.start_node()
                for l in link.match('LINK_ASSET'):
                    doc = l.end_node()

                #print("LINKTID", doc)
                #print(80*'-')
                r = {
                    'title': foobar.end_node()['title'],
                    'is_extern': foobar.end_node()['is_extern'],
                    'filename': doc['filename'],
                    'uuid': doc['uuid'],
                    'upload_uuid': doc['upload_uuid'],
                    'url': '/objects/'+doc['upload_uuid']+'/'+doc['filename']
                }
                #print(json_dumps(r, indent=4))
                # data['downloads'].append(foobar.end_node())
                #print("DOWNLOAD: ", r)
                data['downloads'].append(r)

            for foobar in bla.start_node().match('ICONIC_IMAGE'):
                data['icon'] = foobar.start_node()

            for foobar in bla.start_node().match('RELATED_LA'):
                data['learning_activities'].append(foobar.end_node())

            items[current].append(data)


    # process all the underlaying too
    while next:

        thumb=None

        for bla in next.start_node().match('CURRENT'):
            #print("xx: ", bla, bla.labels())
            intro = bla.start_node()['intro']

        #print("ICOn CHECKL: ", next)
        #print("RELS: ", [r for r in next.match()])
        for bla in next.start_node().match('CURRENT'):
            x = bla.start_node().match('ICONIC_IMAGE')
            #print("xxxx: ", x.__next__().end_node())
            icon = [r for r in x][0]
            #print("xxxRELS: ", [r for r in bla.match()])
            thumb_props = icon.start_node()
            if 'upload_uuid' in thumb_props:
                thumb = "/objects/{upload_uuid}/{thumb}".format(**thumb_props)
            else:
                thumb = "broken" #/objects/{uuid}/{thumb}".format(**thumb_props)



        #print("next: ", next)
        done = True
        for module_item in next.match_outgoing(rel_type='NEXT'):
            done = False
            #print("module_item: ", module_item, module_item.labels())
            nieuwe = module_item.end_node()
            #print("nieuwenext: ", nieuwe, nieuwe.labels())
            for iets in module_item.end_node().match('CURRENT'):
                bla = iets.start_node()
                entries.append({
                    'name': bla['title'],
                    'slug': bla['slug'],
                    'intro': remove_tags(bla['intro']),
                    'thumb': thumb
                })
                #print("----> ", bla.labels(), bla['title'])
                current = bla['slug']
                items[current] = []

            #print("")
            for iets in nieuwe.end_node().match('ITEMS'):
                #print("current: ", current)
                #print("NEXT: ", iets, iets.labels())
                #print("IETSS: ", iets.start_node().labels())
                #print("IETSE: ", iets.end_node().labels())

                dieper = iets.end_node()

                for bla in dieper.match('CURRENT'):
                    #print("bla: ", bla, bla.labels())
                    #print("x: ", bla.start_node()['title'])
                    rec = dict(
                        title = bla.start_node()['title'],
                        xtitle = bla.start_node()['title'],
                        slug = bla.start_node()['slug']
                    )
                    # items[current].append(bla.start_node())
                    data = dict(item = bla.start_node())
                    data['downloads'] = []
                    data['learning_activities'] = []

                    for foobar in bla.start_node().match('RELATED_DOWNLOADS'):
                        link = foobar.start_node()
                        for l in link.match('LINK_ASSET'):
                            doc = l.end_node()

                        #print("LINKTID", doc)
                        r = {
                            'title': foobar.end_node()['title'],
                            'is_extern': foobar.end_node()['is_extern'],
                            'filename': doc['filename'],
                            'url': '/objects/' + doc['upload_uuid'] + '/' + doc['filename'],
                            'uuid': doc['uuid'],
                            'upload_uuid': doc['upload_uuid']
                        }
                        #data['downloads'].append(foobar.end_node())
                        #print("DOWNLOAD: ", r)
                        data['downloads'].append(r)

                    for foobar in bla.start_node().match('ICONIC_IMAGE'):
                        data['icon'] = foobar.start_node()

                    for foobar in bla.start_node().match('RELATED_LA'):
                        data['learning_activities'].append(foobar.end_node())

                    items[current].append(data)


        next = False if done else nieuwe

    # these are the main chapters for a Module
    c['entries'] = entries
    c['items'] = items

    return api_response(status.OK, c)


# ------------------------------------------------------------------------
@BP.route('/api/add-image-from-url', methods=['OPTIONS', 'POST'])
async def add_image(request):
    """users can add an image via an url to MySources"""

    print("--> ADD IMAGE: ")
    # adapt old path for icon

    if request.method == 'OPTIONS':
        return api_response(status.OK, None)

    url = request.json.get('url')
    user = request.json.get('user')
    info = request.json.get('info')
    partner = request.json.get('partner')

    t = urlparse(url)
    filename = t.path.replace("/","_")
    target = tic.NEW_UUID() + '_' + filename

    url_path = "/".join(['/ua', user, target])
    file_path = "/".join([CONFIG.USER_UPLOADS, user])
    target ="/".join([CONFIG.USER_UPLOADS, user, target])

    try:
        r = requests.get(url)
    except requests.exceptions.SSLError:
        # retry https:// urls with invalid cert by ignoring the cert
        r = requests.get(url, verify=False)
    except Exception as e:
        print(f"ERROR add-image-from-url ON {url}")
        pass

    if not r.ok:
        return api_response(status.CANNOT_FETCH)

    try:
        os.makedirs(file_path, exist_ok=True)
        with open(target, 'wb') as dest:
            dest.write(r.content)
            dest.close()
    except Exception as e:
        print("err: ", str(e))
        tic.log('USER FETCH SOURCE FAILED %s' % sys.exc_info()[0])
        return json({'status': 'nok', 'msg': 'cannot create path'}, headers=HEADERS)


    # we zouden eventueel ook nog een ASset entry kunnen maken...
    # voor tonen in MySources is dat echter niet nodig..

    rec = g.run("""
        MATCH (u:Member {uuid:$uuid})
        CREATE (s:MySources) SET
            s.created = timestamp(),
            s.imported_from = $url,
            s.url = $url_path,
            s.uuid = apoc.create.uuid()
        CREATE (u)<-[:SOURCE]-(s)
        RETURN s

    """, uuid=user, url=url, url_path=url_path).evaluate()

    if partner:
        c = g.run("""
            MATCH (p:Partner {code:$partner})
            MATCH (s:MySources {uuid:$uuid})
            CREATE (p)<-[:HAS_PARTNER]-(s)
        """, partner=partner, uuid=rec['uuid']).evaluate()


    print("file: ", file_path)
    print("url: ", url_path)
    print("info: ", info, type(info))

    for (k,v) in info.items():
        rec[k] = v

    g.push(rec)

    return api_response(status.OK)



# ------------------------------------------------------------------------
@BP.route('/api/send-a-mail', methods=['OPTIONS', 'POST'])
async def send_a_mail(request):
    if request.method == 'OPTIONS':
        return api_response(status.OK, None)

    print("--> SEND A MAIL")

    user = request.json.get('user')
    print("user: ", user)
    email = g.run("MATCH (m:Member {uuid:$uuid}) RETURN m.email", uuid=user).evaluate()
    print("em: ", email)

    data = {
        'subject': 'onderwerp',
        'to': email

    }
    mail.send_mail('dc-contact', data)


    return api_response(status.OK)


# ------------------------------------------------------------------------
@BP.route('/api/check-username', methods=['OPTIONS', 'POST'])
async def check_username(request):
    if request.method == 'OPTIONS':
        return api_response(status.OK, None)

    username = request.json.get('username')
    login_exists = g.run("MATCH (m:Member {login:$username}) RETURN count(m)", username=username).evaluate()
    print("check username: ", username, login_exists)

    return api_response(status.OK, dict(username=username, exists=bool(login_exists)))


# ------------------------------------------------------------------------
@BP.route('/api/search-insights', methods=['GET'])
async def _get_search_insignts(request):
    import datetime

    # data = [
    #     dict(section="partner-pages", timestamp=datetime.datetime.now(), searchstring="string", results=42),
    #     dict(section="partner-pages", timestamp="now", searchstring="string", results=42),
    #     dict(section="partner-pages", timestamp="now", searchstring="string", results=42),
    #     dict(section="partner-pages", timestamp="now", searchstring="string", results=42),
    #     dict(section="partner-pages", timestamp="now", searchstring="string", results=42),
    #     dict(section="partner-pages", timestamp="now", searchstring="string", results=42),
    #     dict(section="partner-pages", timestamp="now", searchstring="string", results=42),
    #     dict(section="partner-pages", timestamp="now", searchstring="string", results=42),
    # ]

    data = g.run("MATCH (s:SearchInsight) RETURN collect(s)").evaluate()
    return api_response(status.OK, data)


# ------------------------------------------------------------------------
@BP.route('/api/add-search-insight', methods=['POST'])
async def _add_search_insignt(request):
    import datetime
    entry = dict(
        results = request.json.get('results'),
        searchstring = request.json.get('searchstring'),
        section = request.json.get('section'),
        context = request.json.get('context'),
        timestamp = datetime.datetime.now()
    )

    # create the object
    _gr = graph()
    n = Node('SearchInsight', **entry)
    _gr.create(n)
    return api_response(status.OK)


# ------------------------------------------------------------------------



@BP.route('/api/admin/meta', methods=['OPTIONS', 'POST', 'GET'])
async def get_admin_meta(request):

    data = g.run("""
        MATCH (ad:ActivityDuration)
        MATCH (ht:HistoricalThinking)
        MATCH (tc:TeachingChallenges)
        MATCH (age:Age)

        RETURN {
            ActivityDuration: collect(distinct ad),
            HistoricalThinking: collect(distinct ht),
            TeachingChallenges: collect(distinct tc),
            Age: collect(distinct age)
        }

    """).evaluate()
    return api_response(status.OK, data)

# ------------------------------------------------------------------------

@BP.route('/api/la/<uuid>', methods=['OPTIONS', 'POST', 'GET'])
async def edit_learning_activity(request, uuid):
    print("=====================")
    print("--> /api/la/<uuid>/ EDIT: ", uuid)


    # WITH icon, la, rel, collect(age) as age, {
    #     type: labels(rel),
    #     why: w.why,
    #     data: collect(rel)
    # } as rels,

    data = g.run("""
        MATCH (all:LearningActivity) WHERE all.title is not null
        WITH all ORDER BY all.title
        WITH collect({
            uuid: all.uuid,
            slug: all.slug,
            title: all.title
        }) as all

        WITH all
        MATCH (la:LearningActivity {uuid:$uuid})
        OPTIONAL MATCH (la)-[:LA_RELATED]-(related:LearningActivity)
        OPTIONAL MATCH (la)-[:ICONIC_IMAGE]-(icon)
        OPTIONAL MATCH (la)-[:IN_AGE]-(age:Age)
        OPTIONAL MATCH (la)-[:HAS_DURATION]-(d:ActivityDuration)
        OPTIONAL MATCH (la)-[:TAG]-(tag:Tag)
        OPTIONAL MATCH (la)-[:HAS_LICENSE]-(copyright:License)

        WITH icon, la, copyright, all, related,
            collect(distinct age.label) as age,
            collect(distinct tag.name) as tags,
            collect(distinct d.label) as duration

        RETURN la {
            .*,
            duration: duration,
            tags: tags,
            age: age,
            methods: [],
            challenges: [],
            thinking: [],
            icon: collect(icon),
            license: copyright,
            las: all,
            related: collect(related.uuid)
        }


    """, uuid=uuid).evaluate()

    thumb = data['icon'].pop() if 'icon' in data and data['icon'] else None
    #del data['icon']
    #print("thumb: ", thumb)

    if thumb:

        if 'upload_uuid'in thumb:
            data['thumb'] = "/objects/{}/{}".format(thumb['upload_uuid'], thumb['thumb'])
        elif 'path' in thumb:
            data['thumb'] = '/ua/' + thumb['path']

        else:
            data['thumb'] = "/objects/{}/{}".format(thumb['uuid'], thumb['thumb'])

    else:
        data['thumb'] = ''


    return api_response(status.OK, data)

# ------------------------------------------------------------------------

@BP.route('/api/la', methods=['OPTIONS', 'POST', 'GET'])
async def get_learning_activity_index(request):
    data = g.run("""
        MATCH (la:LearningActivity)
        OPTIONAL MATCH (la)-[:HAS_STATUS]->(status:Status)
        WITH la, status
        ORDER BY la.title
        RETURN collect({
            title: la.title,
            author: la.author,
            slug: la.slug,
            license: la.copyright,
            uuid: la.uuid,
            status: status.uuid
        })
    """).evaluate()

    return api_response(status.OK, data)



# the /meta call gives too little useable meta
@BP.route('/api/getcollectionmeta/<uuid>', methods=['GET'])
async def get_collectionmeta_for_uuid_enduser(request, uuid):

    """prototype for specific Collection meta info, used in (i) button on Collection Card"""
    rec = g.run("""
        MATCH (col:Collection {uuid:$uuid})-[]-(ci:CollectionItem)
        RETURN {
                name: col.name,
                type: 'Collection',
                created: col.created,
                modified: col.modified,
                items: count(ci)
            }

    """, uuid=uuid).evaluate()

    return api_response(status.OK, rec)

# the /meta call gives too little useable meta
@BP.route('/api/getelameta/<uuid>', methods=['GET'])
async def get_elameta_for_uuid_enduser(request, uuid):

    """prototype for specific e-Activity meta info, used in (i) button on Partner Cards"""
    rec = g.run("""
        MATCH (e:MyActivity {uuid:$uuid})-[]-(b:MyBlocks)
        OPTIONAL MATCH (e)-[]-(m:Member)
        RETURN {
                name: e.title,
                type: 'e-Activity',
                created: apoc.date.format(e.created,'ms','dd MMMM yyyy'),
                by: m.name,
                blocks: count(b)
            }

    """, uuid=uuid).evaluate()

    return api_response(status.OK, rec)




# the /meta call gives too little useable meta
@BP.route('/api/getcimeta/<uuid>', methods=['GET'])
async def get_meta_for_uuid_enduser(request, uuid):

    """prototype for specific CollectionItem meta info, used in (i) button on CollectionItem Card"""

    #print("\n** CI META FOR ", uuid)
    # rec = g.run("""
    #     MATCH (n {uuid:$uuid})
    #     OPTIONAL MATCH (n)-[r]-(n1)
    #     RETURN {
    #         type: head(labels(n)),
    #         record: n,
    #         rels: collect(r)
    #     }
    # """, uuid=uuid).evaluate()

    rec = g.run("""
        MATCH (n {uuid:$uuid})
        OPTIONAL MATCH (col:Collection)-[]-(n)
        OPTIONAL MATCH (col)-[]-(m:Member)-[]-(p:Partner)
        RETURN {
                type: labels(n)[0],
                title: n.title,
                description: n.description,
                permalink: n.url,
                collection: col.name,
                partner_name: p.name,
                partner_logo: p.logo
            }

    """, uuid=uuid).evaluate()

    #print(">> rec: ", rec)

    return api_response(status.OK, rec)


from api.models import *

@BP.route('/api/meta/<uuid>', methods=['GET'])
async def get_meta_for_uuid(request, uuid):


    print(uuid)
    d = g.run("""
        MATCH (n {uuid:$uuid})
        OPTIONAL MATCH (n)-[r]-(n1)
        RETURN {
            type: head(labels(n)),
            record: n
        }
    """, uuid=uuid).evaluate()

    print("GET : ", uuid)
    print("type: ", d['type'])

    try:
        # get class for this label name
        current_class = globals()[d['type']]
        # create an instance
        node = current_class()
        # get query string and run it
        meta = g.run(node.getMetaQuery(), uuid=uuid).evaluate()
    except Exception as e:

        print("ERROR: ", e )
        print("ERROR: ", type(e) )
        print("ERROR: ", e.args)

        return api_response(status.ERROR, dict(msg=f"No definitions for {d['type']}"))

    data = {
        'record': d['record'],
        'type': d['type'],
        #'meta': models.models.get(d['type']).meta if models.models.get(d['type']) else {}
        'meta': meta
    }

    return api_response(status.OK, data)



# the /meta call gives too little useable meta
@BP.route('/api/sendamail', methods=['GET'])
async def sendamail(request):



    print("== SEND A MAIL ==")
    print(CONFIG.SITE_URL)
    print("mail: ", mail.__file__)
    msg = mail.create("excel_process", "paulj@webtic.nl", dict(
        dear="Hallo Daar", host=CONFIG.SITE_URL, subject="Upload Collection: Excel processing finished"))
    mail.send(msg)
    print("msg: ", msg)
    return api_response(status.OK)



# ------------------------------------------------------------------------
@BP.route('/api/import', methods=['POST'])
async def import_asset(request):
    print("** IMPORT ASSET FROM URL")

    _user = request.json.get('user')
    _url = request.json.get('url')

    print (f"  user {_user}")
    print (f"  url  {_url}")
    print(40*'-')

    if not _user:
        return api_response(status.USER_NOTFOUND)

    if not _url:
        return api_response(status.NO_DATA)


    headers = {
        "User-Agent": "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
    }

    # put file into a user-uuid/upload-uuid/filename hierarchy
    relative = []
    relative.append(_user)
    path = "/".join([CONFIG.USER_UPLOADS,]+relative)
    #destination = path + '/' + file.name


    r = requests.get(_url, headers=headers)
    if r.ok:

        # get mimetype
        mimetype = r.headers.get('Content-Type','')
        print(">> TYPE ", mimetype)
        print(">> CD: ", r.headers.get("Content-Disposition"))

        # get filename from request, this fails on non-download links which we will not parse
        reqfile = r.headers.get("Content-Disposition")
        if not reqfile:
            print("** RETURN NO_DOWNLOAD")
            return api_response(status.NO_DOWNLOAD)


        # process the download
        # 1. pass import to MUX and import the same _url via MUX API
        from tasks import mux
        muxmeta = mux.create(_url)

        # 2. download and store the Asset
        filename = re.findall("filename=\"(.+)\"", reqfile).pop(0)
        _, extension = os.path.splitext(filename)


        print("fname: ", filename)
        print("ext: ", extension)
        print("fname from url: ", _url.split("/")[-1])

        dest = path + '/' + filename
        relative = tic.get_relative_path(dest)
        print(">>>>> ", relative)
        offset = 0
        my_dest = dest

        # check for file existance.
        # prefix with number until file doe not exist.
        while True:

            offset = offset+1
            if not os.path.exists(my_dest):
                break

            else:
                my_dest = f"{path}/{offset}_{filename}"
                continue


        with open(my_dest,"wb") as out:
            out.write(r.content)
            digest = tic.get_hash(r.content)

            # store it on the graph
            rec = g.run(
                """
                MATCH (m:Member {uuid:$user})
                CREATE (ua:UserAsset)-[:UPLOADED_BY]->(m)
                SET ua.created = timestamp(),
                    ua.type = $mimetype,
                    ua.extension = $extension,
                    ua.filename = $filename,
                    ua.path = $relpath,
                    ua.uuid = apoc.create.uuid(),
                    ua.digest = $digest,
                    ua.origin = $origin,
                    ua.thumb_url = $thumb_url,
                    ua.playback_url = $playback_url,
                    ua.mux_id = $mux_id
                RETURN ua
                """,
                user = _user,
                mimetype = mimetype,
                extension = extension,
                filename = filename,
                relpath = relative,
                digest = digest,
                origin = _url,
                mux_id = muxmeta['mux_id'],
                thumb_url = muxmeta['thumb'],
                playback_url = muxmeta['playback']
            ).evaluate()

            # create a MySource too

            source = g.run("""
                MATCH (m:Member {uuid:$user})
                CREATE (s:MySources)<-[:SOURCE]-(m)
                SET s.created = timestamp(),
                    s.url = $url,
                    s.uuid = apoc.create.uuid(),
                    s.description = 'Description for this video',
                    s.title = 'Title for this video'
                RETURN s
            """,

                user = _user,
                url = muxmeta['playback']
            ).evaluate()

            class IsAsset(Relationship): pass
            ac = IsAsset(source, rec)
            g.create(ac)



    return api_response(status.OK)
