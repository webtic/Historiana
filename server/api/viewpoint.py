from graph import g
from sanic import Blueprint, response
from tic import api_response, status, js_now, NOW
from slugify import slugify
from os.path import dirname

VIEWPOINT = Blueprint("viewpoint", url_prefix="/api/viewpoint")

# ------------------------------------------------------------------------
@VIEWPOINT.route("/collections", methods=["POST"])
async def get_collections(request):

    user = request.json.get('user')
    if not user:
        return api_response(status.NO_DATA)

    data = g.run("""
        MATCH (m:Member {uuid:$user})-[:OWNS]-(vp:ViewpointCollection)
        OPTIONAL MATCH (vp)-[:THUMB_FOR]-(a)

        RETURN COLLECT(vp {
            .*,
            thumb: '/ua/'+a.path,
            thumb_uuid: a.uuid
        }) as data
    """, user=user).evaluate()

    return api_response(status.OK, data)

# ------------------------------------------------------------------------
@VIEWPOINT.route("/delete", methods=["POST"])
async def delete_viewpoint_collection(request):
    print("CREATE VIEWPOINT--")
    if not request.json:
        print("no json bialout")
        return api_response(status.NO_DATA)

    uuid = request.json.get("uuid")
    user = request.json.get("user")
    g.run("""
        MATCH (m:Member {uuid:$user})
        MATCH (m)-[:OWNS]-(vp:ViewpointCollection {uuid:$uuid})
        DETACH
        DELETE vp
    """, user=user, uuid=uuid)

    return api_response(status.OK)

# ------------------------------------------------------------------------
@VIEWPOINT.route("/edit", methods=["POST"])
async def edit_viewpoint_collection(request):

    if not request.json:
        print("no json bialout")
        return api_response(status.NO_DATA)

    record = request.json.get("rec")
    user = request.json.get("user")

    # TODO: match user too
    rec = g.run("MATCH (vp:ViewpointCollection {uuid:{uuid}}) RETURN vp", uuid=record['uuid']).evaluate()

    print("rec: ", dir(rec))
    print(rec.items())

    # update all properties if applicable
    skip = ['uuid', 'created']
    for (k, v) in record.items():
        if k not in skip:
            rec[k] = v

    rec['modified'] = NOW()
    g.push(rec)


    return api_response(status.OK, rec)


# ------------------------------------------------------------------------
@VIEWPOINT.route("/create", methods=["POST"])
async def create_viewpoint_collection(request):
    print("CREATE VIEWPOINT--")
    if not request.json:
        print("no json bialout")
        return api_response(status.NO_DATA)

    record = request.json.get("rec")
    user = request.json.get("user")

    if not all([user, record]):
        print("no data bialout")
        return api_response(status.NO_DATA)

    vp = g.run("""
        MATCH (m {uuid: $user})
        CREATE (vp:ViewpointCollection)<-[:OWNS]-(m)
        SET
            vp.title = $rec.title,
            vp.subtitle = $rec.subtitle,
            vp.introduction = $rec.introduction,
            vp.acknowledgements = $rec.acknowledgements,
            vp.created = timestamp(),
            vp.uuid = apoc.create.uuid()
        RETURN vp
    """, rec=record, user=user).evaluate()

    print("ccreated: ", vp)
    return api_response(status.OK, vp)


#     print(f"GET COLLECTION {collection} OF {partner}")

#     if not all([partner, collection]):
#         return api_response(status.NO_DATA)

#     data = g.run(
#         """
#         MATCH (p:Partner {slug:$partner})-[]-(m:Member)-[]-(col:Collection {slug:$collection})
#         OPTIONAL MATCH (items:CollectionItem)-[]-(col)
#         OPTIONAL MATCH (col)-[:THUMB_FOR]-(thumb)

#         WITH COLLECT(items) as items, p, col, m, thumb
#         RETURN {
#             owner: m.uuid,
#             partner: p,
#             collection: col,
#             items: items,
#             thumb: '/ua/'+thumb.path
#         }
#         """,
#         partner=partner,
#         collection=collection,
#     ).evaluate()

#     if data:
#         return api_response(status.OK, **data)

#     print("** ABORT NO DATA")
#     return api_response(status.NO_DATA)


# # ------------------------------------------------------------------------
# @COLLECTION.route("/set-iconic-image", methods=["POST", "GET"])
# async def set_iconic_image(request):
#     """
#     Interestly enough a collectionitem cannot be directly an iconic image.
#     An iconic image is always a UserAsset, luckily a CollectionItem is a UserAsset in disguise.
#     For now we hack around it by getting the UserAsset.uuid from the url of the image
#     """

#     collection = request.json.get("collection")
#     image = request.json.get("image")

#     if collection and image:
#         # print("SET ICONIC IMAGE === ", collection)
#         # print("image: ", image)

#         # remove the current iconic image
#         g.run(
#             """
#             MATCH (c:Collection {uuid:$collection})<-[r:THUMB_FOR]-(image)
#             DETACH DELETE r
#             """,
#             dict(collection=collection),
#         )

#         # old hack; we got the asset uuid from the path because CollectionItem
#         # had no relation to UserAsset, therefore it could not match it directly.
#         # Now there is (CollectionItem)-[:IS_ASSET]-(UserAsset)
#         asset = image['ua']['uuid']

#         # the url format is /ua/Member.uuid/UserAsset.uuid/filename.jpg
#         # get the UserAsset uuid from the url
#         # asset = dirname(image["url"]).split("/").pop()
#         # print("ASSET UUID FROM url: ", asset)

#         # set the new one image
#         g.run(
#             """
#             MATCH (c:Collection {uuid:$collection})
#             MATCH (ua:UserAsset {uuid:$asset})
#             CREATE (ua)-[:THUMB_FOR]->(c)
#             """,
#             dict(collection=collection, asset=asset),
#         )

#         return api_response(status.OK)

#     return api_response(status.NO_DATA)


# # ------------------------------------------------------------------------
# @COLLECTION.route("/delete-source", methods=["POST", "GET"])
# async def delete_source(request):
#     """remove a source (CollectionItem) from a collection, called from CollectionEditor"""
#     print("** DELETE SOURCE **")

#     collection = request.json.get("collection")
#     item = request.json.get("item")

#     try:
#         g.run(
#             """
#                 MATCH (c:Collection {uuid:$collection})-[r:IN_COLLECTION]-(i:CollectionItem {uuid:$item})
#                 DETACH
#                 DELETE r
#             """,
#             dict(collection=collection, item=item),
#         )

#         return api_response(status.OK)

#     except Exception as e:

#         return api_response(status.ERROR, dict(msg=str(e)))


# # ------------------------------------------------------------------------
# # @COLLECTION.route("/test", methods=["POST", "GET"])
# # async def test(request):
# #     print("** TEST **")
# #     return api_response(status.OK)


# # ------------------------------------------------------------------------


# @COLLECTION.route("/<slug>", methods=["OPTIONS", "POST", "GET"])
# async def get_collection_by_slug(request, slug):
#     print("## get collection by slug")

#     collection = g.run(
#         """
#         MATCH (col:Collection {slug:$slug})-[:IN_COLLECTION]-(entry:CollectionItem)
#         OPTIONAL MATCH (entry)-[:HAS_LICENSE]->(l:License)
#         OPTIONAL MATCH (entry)-[:IS_ASSET]->(ua:UserAsset)

#         OPTIONAL MATCH (col)-[:THUMB_FOR]-(thumb:NewUpload)
#         OPTIONAL MATCH (ml:License)<-[:HAS_LICENSE]-(col)
#         OPTIONAL MATCH (el:License)<-[:HAS_LICENSE]-(entry)
#         WITH entry, col, thumb, l, ml, el, ua
#         ORDER BY entry.order ASC
#         RETURN col {
#             items: collect (entry { ua: ua, license: el, copyright: l.uuid, .*}),
#             thumb: '/uploads/'+thumb.uuid+'_'+thumb.filename,
#             .*

#         }

#     """,
#         slug=slug,
#     ).evaluate()

#     print("col: ", collection)
#     if not collection:
#         return api_response(status.NO_DATA)

#     return api_response(status.OK, collection)


# # ------------------------------------------------------------------------
# @COLLECTION.route("/delete", methods=["POST"])
# async def delete(request):

#     params = request.json

#     # delete specified record, retrieve and return new set
#     data = g.run(
#         """
#         MATCH (m:Member {uuid:$user})-[:OWNS]-(c:Collection {uuid:$collection})
#         DETACH DELETE c
#        """,
#         params,
#     )

#     data = g.run(
#         """
#         MATCH (m:Member {uuid:$user})-[:OWNS]-(c:Collection)
#         RETURN collect(c)
#     """,
#         params,
#     ).evaluate()
#     return api_response(status.OK, data)




# # ------------------------------------------------------------------------
# @COLLECTION.route("/", methods=["POST"])
# async def index(request):
#     print("DATA: ", request.json)
#     user = request.json.get("user")
#     if not user:
#         return api_response(status.USER_NOTFOUND)

#     data = g.run(
#         """
#         MATCH (m:Member {uuid:$user})-[:OWNS]-(col:Collection)
#         WITH col
#         OPTIONAL MATCH (col)-[:THUMB_FOR]-(a)
#         OPTIONAL MATCH (col)-[:HAS_LICENSE]-(license:License)
#         OPTIONAL MATCH (col)-[:HAS_LANGUAGE]-(language:Language)
#         WITH collect( col {
#             .*,
#             license: license,
#             language: language,
#             thumb: '/ua/'+a.path,
#             thumb_uuid: a.uuid
#             }) as collections
#         RETURN collections
#     """,
#         request.json,
#     ).evaluate()

#     return api_response(status.OK, data)


# # ------------------------------------------------------------------------
# @COLLECTION.route("/add", methods=["POST"])
# async def add(request):

#     print("ADD NEW COLLECTION: ", request.json)
#     record = request.json
#     record["slug"] = slugify(record["name"])
#     print("RECORD: ", record)

#     # duplicate subtitle and introduction until we can rename all subtitle to introduction
#     try:
#         print("TRY CREATE")
#         data = g.run(
#             """
#             MATCH (m:Member {uuid:$user})
#             CREATE (c:Collection)<-[:OWNS]-(m)
#             SET
#                 c.name=$name,
#                 c.introduction=$introduction,
#                 c.subtitle=$introduction,
#                 c.description=$description,
#                 c.acknowledgements=$acknowledgements,
#                 c.uuid=apoc.create.uuid(),
#                 c.created=datetime(),
#                 c.status='uploaded',
#                 c.slug=$slug
#             RETURN c

#         """,
#             record,
#         ).evaluate()
#         return api_response(status.OK, data)

#     except Exception as e:
#         print("Error on CREATE: ", str(e))
#         return api_response(status.ERROR, str(e))


# # ------------------------------------------------------------------------
# @COLLECTION.route("/removeCopyright", methods=["OPTIONS", "POST"])
# async def collection_removeCopyright(request):
#     """remove partner from item"""

#     item = request.json.get("item")
#     copyright = request.json.get("copyright")

#     g.run(
#         """
#         MATCH (i:CollectionItem {uuid:$item})
#         MATCH (l:License {uuid:$copyright})
#         MATCH (i)-[r:HAS_LICENSE]-(l)
#         DELETE r
#     """,
#         item=item,
#         copyright=copyright,
#     )

#     return api_response(status.OK)


# # ------------------------------------------------------------------------
# @COLLECTION.route("/changeCopyright", methods=["OPTIONS", "POST"])
# async def collection_changeCopyright(request):
#     g.run(
#         """
#         MATCH (c:CollectionItem {uuid:$c})-[x]-(l:License)
#         DELETE x
#     """,
#         c=request.json.get("item"),
#     ).evaluate()

#     g.run(
#         """
#         MATCH (i:CollectionItem {uuid:$c})
#         MATCH (l:License {uuid:$l})
#         CREATE (i)-[:HAS_LICENSE]->(l)

#         """,
#         c=request.json.get("item"),
#         l=request.json.get("copyright"),
#     ).evaluate()

#     return api_response(status.OK)


# # ------------------------------------------------------------------------
# @COLLECTION.route("/removePartner", methods=["OPTIONS", "POST"])
# async def collection_removePartner(request):
#     """remove partner from item"""

#     item = request.json.get("item")
#     partner = request.json.get("partner")
#     # print("item: ",item)
#     # print("p: ", partner)

#     # could be either Colllection or CollectionItem
#     # we can't specify that on the match
#     g.run(
#         """
#         MATCH (i {uuid:$item})
#         MATCH (p:Partner {uuid:$partner})
#         MATCH (i)-[r:HAS_PARTNER]-(p)
#         DELETE r
#     """,
#         item=item,
#         partner=partner,
#     )

#     return api_response(status.OK)


# # ------------------------------------------------------------------------
# @COLLECTION.route("/changePartner", methods=["OPTIONS", "POST"])
# async def collection_changePartner(request):
#     print("--> CHANGE PARTNER ")
#     g.run(
#         """
#         MATCH (c:CollectionItem {uuid:$c})-[x]-(l:Partner)
#         DELETE x
#     """,
#         c=request.json.get("item"),
#     ).evaluate()

#     g.run(
#         """
#         MATCH (i:CollectionItem {uuid:$c})
#         MATCH (l:Partner {uuid:$l})
#         CREATE (i)-[:HAS_PARTNER]->(l)

#         """,
#         c=request.json.get("item"),
#         l=request.json.get("partner"),
#     ).evaluate()

#     return api_response(status.OK)


# # ------------------------------------------------------------------------
# @COLLECTION.route("/deleteItem", methods=["OPTIONS", "POST"])
# async def collection_deleteItem(request):
#     print("DELETE ITEM")
#     print("d: ", request.json)
#     g.run(
#         """
#         MATCH (c:Collection {uuid:$c})<-[x:IN_COLLECTION]-(ci:CollectionItem {uuid:$item})
#         DELETE x
#         """,
#         c=request.json.get("collection"),
#         item=request.json.get("item"),
#     ).evaluate()

#     return api_response(status.OK)


# # ------------------------------------------------------------------------

# # deze wordt/werd gebruikt in de oude admin?
# @COLLECTION.route("/update", methods=["POST"])
# async def edit_collection(request):

#     collection = request.json.get("uuid")
#     if not collection:
#         return api_response(status.NO_DATA)

#     print("Edit collection: ", collection)
#     col = g.run(
#         """
#         MATCH (c:Collection {uuid:$col})
#         RETURN c
#     """,
#         col=collection,
#     ).evaluate()

#     # import json
#     # print(json.dumps(request.json))

#     col["name"] = request.json.get("name")
#     col["subtitle"] = request.json.get("subtitle")
#     col["description"] = request.json.get("description")
#     col["acknowledgements"] = request.json.get("acknowledgements")
#     col["introduction"] = request.json.get("introduction")
#     col["slug"] = slugify(request.json.get("name"))
#     col["status"] = request.json.get("status")
#     col["modified"] = js_now()

#     # deal with License
#     lic = request.json.get("license")
#     license_uuid = lic.get('uuid')
#     print("lic: ", license_uuid)
#     if lic:
#         # remove old
#         g.run("""
#             MATCH (c:Collection {uuid:$collection})-[rel:HAS_LICENSE]-(l:License)
#             DELETE rel
#         """, collection=collection)

#         g.run("""
#             MATCH (c:Collection {uuid:$collection})
#             MATCH (l:License {uuid:$license})
#             MERGE (c)-[:HAS_LICENSE]-(l)
#         """, collection=collection, license=license_uuid)

#     # deal with Language
#     lang = request.json.get("language")
#     if lang:
#         language_uuid = lang.get('uuid')
#         print("lang: ", language_uuid)
#         if lang:
#             # remove old
#             g.run("""
#                 MATCH (c:Collection {uuid:$collection})-[rel:HAS_LANGUAGE]-(l:Language)
#                 DELETE rel
#             """, collection=collection)

#             g.run("""
#                 MATCH (c:Collection {uuid:$collection})
#                 MATCH (l:Language {uuid:$language})
#                 MERGE (c)-[:HAS_LANGUAGE]-(l)
#             """, collection=collection, language=language_uuid)


#     try:
#         s = g.push(col)
#     except Exception as e:
#         print("ERROR: ", e)
#         return api_response(status.ERROR, dict(msg=str(e)))
#     return api_response(status.OK, s)


# # ------------------------------------------------------------------------
# # deze wordt/werd gebruikt in de oude admin?
# @COLLECTION.route("/edit/<uuid>", methods=["OPTIONS", "POST", "GET"])
# async def old_edit_collection(request, uuid):

#     if request.method == "OPTIONS":
#         return api_response(status.OK, None)

#     if request.method == "POST":

#         collection = request.json.get("uuid")
#         print("Edit collection: ", collection)

#         col = g.run(
#             """
#             MATCH (c:Collection {uuid:$col})
#             RETURN c
#         """,
#             col=collection,
#         ).evaluate()

#         col["name"] = request.json.get("name")
#         col["subtitle"] = request.json.get("subtitle")
#         col["description"] = request.json.get("description")
#         col["acknowledgements"] = request.json.get("acknowledgements")
#         col["slug"] = request.json.get("slug")
#         col["status"] = request.json.get("status")
#         col["modified"] = js_now()
#         partner = request.json.get("partner")

#         print("partner: ", partner)
#         # col['thumb'] = t

#         # print("-> ", type(col))
#         g.push(col)
#         # print("SAVED: ", col)
#         # print(json_dumps(request.json))

#         items = request.json.get("items")

#         # remove old items
#         g.run(
#             """
#             MATCH (c:Collection {uuid:$collection})
#             MATCH (c)<-[:IN_COLLECTION]-(ci:CollectionItem)
#             DETACH DELETE ci
#         """,
#             collection=collection,
#         ).evaluate()

#         # connect collection to a partner if specified
#         if partner:
#             print("** deelte old partner")
#             # delete old partner

#             g.run(
#                 """
#                 MATCH (c:Collection {uuid:$collection})
#                 MATCH (c)-[old:HAS_PARTNER]->(oldp:Partner)
#                 DELETE old
#             """,
#                 collection=collection,
#             ).evaluate()

#             # connect new
#             g.run(
#                 """
#                 MATCH (c:Collection {uuid:$collection})
#                 MATCH (p:Partner {uuid:$partner})
#                 CREATE (c)-[:HAS_PARTNER]->(p)
#             """,
#                 collection=collection,
#                 partner=partner,
#             ).evaluate()

#         # create the new nodes
#         # print('***********************')
#         # print('**** items')

#         for item in items:

#             # create attrib if its not there otherwise Cypher create fails
#             if not "description" in item:
#                 item["description"] = ""

#             item["collection"] = collection
#             # print(json_dumps(item))
#             g.run(
#                 """
#               MATCH (c:Collection {uuid:$collection})
#               CREATE (ci:CollectionItem)-[:IN_COLLECTION]->(c)
#               SET


#                 ci.uuid = {uuid},
#                 ci.order = {order},
#                 ci.title = {title},
#                 ci.filename = {filename},
#                 ci.url = {url},
#                 ci.description = {description}
#               RETURN ci

#             """,
#                 item,
#             )

#             # handle copyright...
#             m = dict(ci=item["uuid"], license=item["copyright"])
#             if item["copyright"]:
#                 g.run(
#                     """
#                     MATCH (ci:CollectionItem {uuid:$ci})
#                     MATCH (l:License {uuid:$license})
#                     CREATE (ci)-[:HAS_LICENSE]->(l)
#                 """,
#                     m,
#                 )

#             # i = g.run("MATCH (ci {uuid:{uuid}}) RETURN ci", uuid=item['uuid']).evaluate()
#             # print(i['title'])

#     collection = g.run(
#         """
#         MATCH (col:Collection {uuid:$uuid})-[:IN_COLLECTION]-(entry:CollectionItem)
#         OPTIONAL MATCH (entry)-[:HAS_LICENSE]->(l:License)
#         OPTIONAL MATCH (col)-[:THUMB_FOR]-(asset)
#         OPTIONAL MATCH (ml:License)<-[:HAS_LICENSE]-(col)
#         OPTIONAL MATCH (col)-[:HAS_PARTNER]->(p:Partner)
#         WITH entry, col, asset, l, ml, p
#         ORDER BY entry.order ASC
#         RETURN col {
#             items: collect(entry { copyright: l.uuid, .*}),
#             partner: p.uuid,
#             thumb: case when asset.path is null
#                     then "/uploads/"+asset.uuid+"_"+asset.filename
#                     else "/ua/"+asset.path
#                     end,
#             .*
#         }

#     """,
#         uuid=uuid,
#     ).evaluate()

#     # print("col: ", collection.keys())
#     if not collection:
#         return api_response(status.NO_DATA)

#     return api_response(status.OK, collection)


# # ------------------------------------------------------------------------


# @COLLECTION.route("/publish", methods=["POST"])
# async def publish_collection(request):

#     collection = request.json.get("collection")
#     partner = request.json.get("partner")

#     if not (all([collection, partner])):
#         return api_response(status.NO_DATA)

#     g.run(
#         """
#         MATCH (p:Member {uuid:$partner})-[]-(c:Collection {uuid:$collection})
#         SET
#             c.is_published=true,
#             c.updated=timestamp()

#         RETURN c
#         """,
#         collection=collection,
#         partner=partner,
#     ).evaluate()

#     return api_response(status.OK)


# # ------------------------------------------------------------------------
# @COLLECTION.route("/private", methods=["POST"])
# async def private_collection(request):
#     """set a collection to private"""

#     collection = request.json.get("collection")
#     partner = request.json.get("partner")

#     if not (all([collection, partner])):
#         return api_response(status.NO_DATA)

#     g.run(
#         """
#         MATCH (p:Member {uuid:$partner})-[]-(c:Collection {uuid:$collection})
#         SET
#             c.is_published=false,
#             c.updated=timestamp()

#         RETURN c
#         """,
#         collection=collection,
#         partner=partner,
#     ).evaluate()

#     return api_response(status.OK)


# # ------------------------------------------------------------------------

# # ------------------------------------------------------------------------
# @COLLECTION.route("/update-item", methods=["POST"])
# async def update_item(request):
#     """update a CollectionItem"""

#     print("-- UPDATE ITEM")
#     item = request.json.get("item")
#     item_license = item.get("license")

#     if not item:
#         print("NO DATA")
#         return api_response(status.NO_DATA)

#     print("item: ", item)

#     g.run(
#         """
#         MATCH (item:CollectionItem {uuid:$uuid})
#         SET
#             item.title=$title,
#             item.description=$description,
#             item.order=$order

#     """,
#         uuid=item["uuid"],
#         title=item["title"],
#         description=item["description"],
#         order=item["order"],
#     )

#     if item_license:
#         print("-> ", item["uuid"])

#         # delete existing connections
#         g.run(
#             """
#             MATCH (ci:CollectionItem {uuid:$item})-[x:HAS_LICENSE]->(l:License)
#             DETACH DELETE x
#         """,
#             item=item["uuid"],
#         ).evaluate()

#         # create new license connection
#         d = g.run(
#             """
#             MATCH (ci:CollectionItem {uuid:$item})
#             MATCH (l:License {uuid:$item_license})
#             MERGE (ci)-[:HAS_LICENSE]->(l)
#             RETURN l,ci
#         """,
#             item=item["uuid"],
#             item_license=item_license["uuid"],
#         ).evaluate()

#         print("UPDATE LICENSE: ", d)
#     return api_response(status.OK)
