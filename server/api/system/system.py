#
# Historiana System
#

from .archetypes import ROLES
from .archetypes import ARCHETYPES
from .archetypes import ACTIONS
import json
from collections import Counter
from sanic import Blueprint, response
from tic import api_response, status, js_now, NOW
from slugify import slugify
from os.path import dirname
from graph import g

SYSTEM = Blueprint("system", url_prefix="/api/system")

# ------------------------------------------------------------------------
@SYSTEM.route("/member-select", methods=["GET"])
async def get_member_select(request):

    data = g.run("""
        MATCH (m:Member) WITH m, apoc.text.join([m.name, m.email],  ' • ') as value
        RETURN collect({uuid:m.uuid, label: value})
    """).evaluate()

    return api_response(status.OK, data)

# ------------------------------------------------------------------------
@SYSTEM.route("/partner-add-member", methods=["POST"])
async def partner_add_member(request):

    partner = request.json.get('partner')
    member = request.json.get('member')
    if not all([member, partner]):
        return api_response(status.NO_DATA)

    data = g.run("""
        MATCH (m:Member {uuid:$member})
        MATCH (p:Partner {uuid:$partner})
        CREATE (m)-[rel:PARTNER_ADMIN]->(p)
        RETURN 0
    """, partner=partner, member=member).evaluate()


    return api_response(status.OK, data)

# ------------------------------------------------------------------------
@SYSTEM.route("/partner-revoke-member", methods=["POST"])
async def partner_revoke_member(request):

    partner = request.json.get('partner')
    member = request.json.get('member')
    if not all([member, partner]):
        return api_response(status.NO_DATA)


    data = g.run("""
        MATCH (m:Member {uuid:$member})
        MATCH (p:Partner {uuid:$partner})
        WITH m,p
        MATCH (m)-[rel:PARTNER_ADMIN]-(p)
        DETACH
        DELETE rel
        RETURN 0
    """, partner=partner, member=member).evaluate()

    return api_response(status.OK, data)

# ------------------------------------------------------------------------
@SYSTEM.route("/eu-providers", methods=["GET"])
def get_eu_providers(request):
    print("** GET EU PROVIDERS")
    # from api.europeana import get_providers
    # data = get_providers()
    data = dict(dataproviders=[], providers=[])
    return api_response(status.OK, data)


# ------------------------------------------------------------------------
@SYSTEM.route("/partners", methods=["GET"])
async def partners_index(request):

    # use apoc.agg to avoid multiple entries of the same Partner
    # due to multiple connections to either logo/icon
    data = g.run("""
        MATCH (p:Partner)
        OPTIONAL MATCH (p)-[:LOGO]-(logo)
        OPTIONAL MATCH (p)-[:ICON]-(icon)
        OPTIONAL MATCH (p)-[:PARTNER_ADMIN]-(member:Member)

        WITH
            p,
            apoc.agg.first(logo) as logo,
            apoc.agg.first(icon) as icon,
            collect(distinct member) as admins

        ORDER BY p.name
        RETURN collect(distinct p {
            .*,
            logo: { url: '/ua/'+logo.path, uuid: logo.uuid},
            icon: { url: '/ua/'+icon.url, uuid: icon.uuid},
            admins: admins
        })

    """).evaluate()

    # Neo4j version 4 syntax to test:
    # data = g.run("""
    #     MATCH (p:Partner)
    #     CALL {
    #         WITH p
    #         OPTIONAL MATCH (p)-[:LOGO]-(logo)
    #         OPTIONAL MATCH (p)-[:ICON]-(icon)
    #         RETURN logo,icon
    #         LIMIT 1
    #     }

    #     ORDER BY p.name
    #     RETURN collect(distinct p {
    #         .*,
    #         logo: { url: '/ua/'+logo.path, uuid: logo.uuid},
    #         icon: { url: '/ua/'+icon.url, uuid: icon.uuid}
    #     })
    #""").evaluate()


    return api_response(status.OK, data)


# ------------------------------------------------------------------------
@SYSTEM.route("/member-search", methods=["GET"])
async def member_search(request):
    search = request.args['search'].pop()
    print("search member for: ", search)
    data = g.run("MATCH (m:Member) WHERE m.email CONTAINS $search RETURN collect(distinct m)",
                 search=search).evaluate()
    return api_response(status.OK, data)


# roles
# ------------------------------------------------------------------------
@SYSTEM.route("/roles", methods=["GET"])
async def get_roles(request):
    return api_response(status.OK, dict(roles=ROLES, actions=ACTIONS))

# ------------------------------------------------------------------------


@SYSTEM.route("/archetypes", methods=["GET"])
async def archetypes(request):
    return api_response(status.OK, ARCHETYPES)

# ------------------------------------------------------------------------


@SYSTEM.route("/group-permissions", methods=["GET"])
async def group_permissions(request):
    return api_response(status.OK, dict(archetypes=ARCHETYPES, actions=ACTIONS))

# ------------------------------------------------------------------------


@SYSTEM.route("/groups", methods=["GET"])
async def groups(request):
    data = g.run(
        "MATCH (g:Group) WITH g ORDER BY g.name RETURN collect(g)").evaluate()
    return api_response(status.OK, data)

# ------------------------------------------------------------------------


@SYSTEM.route("/groups/<uuid>", methods=["DELETE"])
async def group_delete(request, uuid):
    data = g.run(
        "MATCH (g:Group {uuid:$uuid}) DETACH DELETE g", uuid=uuid).evaluate()
    data = g.run("MATCH (g:Group) RETURN collect(g)").evaluate()
    return api_response(status.OK, data)


# ------------------------------------------------------------------------
@SYSTEM.route("/group", methods=["POST"])
async def add_update_group(request):

    code = request.json.get('code')
    name = request.json.get('name')
    perms = request.json.get('perms')
    uuid = request.json.get('uuid')

    # update an existing or create a new one if no uuid
    if uuid:
        g.run("""
			MATCH (g:Group {uuid:$uuid})
			SET
				g.name=$name,
				g.code=$code,
				g.perms=$perms

			RETURN g

		""", dict(uuid=uuid, name=name, code=code, perms=json.dumps(perms)))

    else:
        g.run("""
			CREATE (g:Group)
			SET
				g.uuid=apoc.create.uuid(),
				g.name=$name,
				g.code=$code,
				g.perms=$perms
			RETURN g

		""", dict(name=name, code=code, perms=json.dumps(perms)))

    return api_response(status.OK)


# members

# ------------------------------------------------------------------------
@SYSTEM.route("/members", methods=["GET"])
async def members(request):
    data = g.run(
        "MATCH (m:Member) WITH m ORDER BY m.name RETURN collect(m)").evaluate()
    return api_response(status.OK, data)

# ------------------------------------------------------------------------


@SYSTEM.route("/member/<uuid>", methods=["DELETE"])
async def member_delete(request, uuid):
    data = g.run(
        "MATCH (m:Member {uuid:$uuid}) DETACH DELETE m", uuid=uuid).evaluate()
    return api_response(status.OK, data)


# ------------------------------------------------------------------------
@SYSTEM.route("/keys/<label>", methods=["GET"])
async def node_keys(request, label):
    s = f"MATCH (n:{label}) RETURN count(n) AS cnt"
    cnt = g.run(s).evaluate()

    is_timestamp = ['joined', 'created']

    s = f"MATCH (n:{label}) WITH n, count(n) as cnt RETURN keys(n) as keys, cnt"
    props = set()
    props_counter = Counter()
    types = {}

    data = g.run(s).data()
    for row in data:
        for p in row['keys']:
            props_counter[p] = props_counter[p] + 1
            props.add(p)

    # get all properties and number of records it is used
    properties = {k: v for (k, v) in props_counter.most_common(None)}

    m = []
    m.append("@dataclass")
    m.append(f"class {label}:")
    for p in props:

        _type = 'str'

        if p.startswith('is_') or p.startswith('can_'):
            _type = 'bool'

        if p in is_timestamp:
            _type = 'datetime.datetime'

        m.append(f"\t{p}: {_type}")
        types[p] = _type

    stats = dict(nodes=cnt, properties=properties,
                 types=types, model="\n".join(m))

    return api_response(status.OK, stats)
