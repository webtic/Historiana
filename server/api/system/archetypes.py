
ARCHETYPES = sorted([
	dict(code='KeyMoment', label="Key Moment", type='content'),
	dict(code='Unit', label='Unit', type='content'),
	dict(code='SourceCollection', label='Source Collection', type='content'),
    dict(code='Viewpoint', label='Viewpoint', type='content'),
	dict(code='LearningActivity', label='Learning Activity', type='content'),
	dict(code='eActivity', label='eLearning Activity', type='content'),
	dict(code='Source', label='Source', type='content'),
	dict(code='Member', label='Member', type='admin'),
	dict(code='Partner', label='Partner', type='admin')
], key=lambda k: k['label'].lower())

ACTIONS = [
	dict(code='admin', label="Admin"),
	dict(code='view', label="View"),
	dict(code='create', label="Create"),
	dict(code='edit', label='Edit'),
	dict(code='delete', label='Delete'),
	# publish right implies unpublish
    dict(code='publish', label='Publish'),
	dict(code='share', label='Share'),
]
#, key=lambda k: k['label'].lower())


# edit is used in front-end to toggle editing
ROLES = [
	dict(
		code="super",
		label='SuperAdmin',
		context='site',
		edit=False,
		perms=dict(admin=True, view=True, create=True, edit=True, delete=True, publish=True, share=True),
		system=True,
		teams=False
	),

	dict(
		code="admin",
		label='Administrator',
		context='context',
		edit=False,
		perms=dict(admin=True, view=True, create=True, edit=True, delete=True, publish=True, share=True),
		system=True,
		teams=True
	),

	dict(
		code="manager",
		label='Manager',
		context='context',
		edit=False,
		perms=dict(admin=False, view=True, create=True, edit=True, delete=True, publish=True, share=True),
		system=True,
		teams=True
	),

	dict(
		code="editor",
		label='Editor',
		context='context',
		edit=False,
		perms=dict(admin=False, view=True, create=True, edit=True, delete=True, publish=True, share=True),
		system=True,
		teams=True

	),

	dict(
		code="contributor",
		label='Contributor',
		context='context',
		edit=False,
		perms=dict(admin=False, view=True, create=True, edit=True, delete=True, publish=False, share=True),
		system=True,
		teams=True
	),

	dict(
		code="authenticated",
		label='Authenticated',
		context='site',
		edit=False,
		perms=dict(admin=False, view=True, create=True, edit=True, delete=True, publish=True, share=True),
		system=True,
		teams=False

	),

	dict(
		code="anonymous",
		label='Anonymous',
		context='site',
		edit=False,
		perms=dict(admin=False, view=True, create=False, edit=False, delete=False, publish=False, share=False),
		system=True,
		teams=False
	)
]