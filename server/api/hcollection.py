from graph import g
from sanic import Blueprint, response
from tic import new_api_response, api_response, status, js_now
from slugify import slugify
from os.path import dirname

COLLECTION = Blueprint("collection", url_prefix="/api/collection")
# ------------------------------------------------------------------------
@COLLECTION.route("/by-partner", methods=["POST"])
async def collection_by_partner(request):
    print("** COL BY PARTNER **")

    if not request.json:
        return api_response(status.NO_DATA)

    partner = request.json.get("partner")
    collection = request.json.get("collection")

    print(f"GET COLLECTION {collection} OF {partner}")

    if not all([partner, collection]):
        return api_response(status.NO_DATA)

    data = g.run(
        """
        MATCH (p:Partner {slug:$partner})-[]-(m:Member)-[]-(col:Collection {slug:$collection})
        OPTIONAL MATCH (items:CollectionItem)-[]-(col)
        OPTIONAL MATCH (col)-[:THUMB_FOR]-(thumb)
        WITH items, p, col, m, thumb
        ORDER BY items.order
        WITH COLLECT(items) as items, p, col, m, thumb
        RETURN {
            owner: m.uuid,
            partner: p,
            collection: col,
            items: items,
            thumb: '/ua/'+thumb.path
        }
        """,
        partner=partner,
        collection=collection,
    ).evaluate()

    if data:
        return api_response(status.OK, **data)

    print("** ABORT NO DATA")
    return api_response(status.NO_DATA)


# ------------------------------------------------------------------------
@COLLECTION.route("/set-iconic-image", methods=["POST", "GET"])
async def set_iconic_image(request):
    """
    Interestly enough a collectionitem cannot be directly an iconic image.
    An iconic image is always a UserAsset, luckily a CollectionItem is a UserAsset in disguise.
    For now we hack around it by getting the UserAsset.uuid from the url of the image
    """

    collection = request.json.get("collection")
    image = request.json.get("image")

    if collection and image:
        print("SET ICONIC IMAGE === ", collection)
        print("image: ", image)

        # remove the current iconic image
        g.run(
            """
            MATCH (c:Collection {uuid:$collection})<-[r:THUMB_FOR]-(image)
            DETACH DELETE r
            """,
            dict(collection=collection),
        )

        # old hack; we got the asset uuid from the path because CollectionItem
        # had no relation to UserAsset, therefore it could not match it directly.
        # Now there is (CollectionItem)-[:IS_ASSET]-(UserAsset)
        asset = image["ua"]["uuid"]

        # the url format is /ua/Member.uuid/UserAsset.uuid/filename.jpg
        # get the UserAsset uuid from the url
        # asset = dirname(image["url"]).split("/").pop()
        # print("ASSET UUID FROM url: ", asset)

        # set the new one image
        g.run(
            """
            MATCH (c:Collection {uuid:$collection})
            MATCH (ua:UserAsset {uuid:$asset})
            CREATE (ua)-[:THUMB_FOR]->(c)
            """,
            dict(collection=collection, asset=asset),
        )

        return api_response(status.OK)

    return api_response(status.NO_DATA)


# ------------------------------------------------------------------------
@COLLECTION.route("/delete-iconic", methods=["POST", "GET"])
async def delete_iconic_image(request):

    collection = request.json.get("collection")
    if collection:
        print("col:", collection)
        g.run(
            """
            MATCH (col {uuid:$collection})-[r:THUMB_FOR]-(asset)
            DELETE r
        """,
            collection=collection,
        )

        return api_response(status.OK)

    return api_response(status.NO_DATA)


# ------------------------------------------------------------------------
@COLLECTION.route("/delete-source", methods=["POST", "GET"])
async def delete_source(request):
    """remove a source (CollectionItem) from a collection, called from CollectionEditor"""
    print("** DELETE SOURCE **")

    collection = request.json.get("collection")
    items = request.json.get("item")

    try:
        for item in items:
            g.run(
                """
                    MATCH (c:Collection {uuid:$collection})-[r:IN_COLLECTION]-(i:CollectionItem {uuid:$item})
                    DETACH
                    DELETE r
                """,
                dict(collection=collection, item=item),
            )

        return api_response(status.OK)

    except Exception as e:

        return api_response(status.ERROR, dict(msg=str(e)))


# ------------------------------------------------------------------------
# @COLLECTION.route("/test", methods=["POST", "GET"])
# async def test(request):
#     print("** TEST **")
#     return api_response(status.OK)


# ------------------------------------------------------------------------


@COLLECTION.route("/<slug>", methods=["OPTIONS", "POST", "GET"])
async def get_collection_by_slug(request, slug):
    print("## get collection by slug")

    # sent all tags for sourceEditor

    collection = g.run(
        """
        MATCH (col:Collection {slug:$slug})-[:IN_COLLECTION]-(entry:CollectionItem)
        OPTIONAL MATCH (entry)-[:IS_ASSET]->(ua:UserAsset)
        OPTIONAL MATCH (ua)-[:HAS_LICENSE]->(l:License)
        OPTIONAL MATCH (ua)-[:HAS_LANGUAGE]->(la:Language)
        OPTIONAL MATCH (col)-[:THUMB_FOR]-(thumb)
        OPTIONAL MATCH (ml:License)<-[:HAS_LICENSE]-(col)
        OPTIONAL MATCH (el:License)<-[:HAS_LICENSE]-(entry)
        OPTIONAL MATCH (tags:Tag)<-[:HAS_TAG]-(ua)
        optional match (myTags1:MyKeywords)<-[:HAS_TAG]-(a)
        optional match (myTags2:MyYears)<-[:HAS_TAG]-(a)
        optional match (myTags3:MyClasses)<-[:HAS_TAG]-(a)
        optional match (myTags4:MyLanguages)<-[:HAS_TAG]-(a)

        WITH entry, col, thumb, l, la, ml, el, ua,
            collect({label:tags.name, value:tags.uuid}) as tags,
            collect(distinct [
                {label:myTags1.name, value:myTags1.uuid},
                {label:myTags2.name, value:myTags2.uuid},
                {label:myTags3.name, value:myTags3.uuid},
                {label:myTags4.name, value:myTags4.uuid}
            ]) as myTags


        ORDER BY entry.order ASC

        WITH collect(entry {
            license:l,
            ua: ua,
            language: la,
            tags: [tag in tags WHERE tag.value is not null],
            myTags: myTags,
            .*
        }) as items, thumb, col

        RETURN col {
            items: items,
            thumb: '/uploads/'+thumb.uuid+'_'+thumb.filename,
            .*

        }

    """,
        slug=slug,
    ).evaluate()

    if not collection:
        return api_response(status.NO_DATA)

    return api_response(status.OK, collection)


# ------------------------------------------------------------------------
@COLLECTION.route("/delete", methods=["POST"])
async def delete(request):

    print("DELETE COLLECTION")

    collection = request.json.get("collection")  # single collection
    collections = request.json.get("collections")  # array of collections
    user = request.json.get("user")  # user

    # print("c1: ", collection)
    # print("c2: ", collections)
    # print("c3: ", user)

    if collection:
        # delete specified record, retrieve and return new set
        data = g.run(
            """
            MATCH (m:Member {uuid:$user})-[:OWNS]-(c:Collection {uuid:$collection})
            DETACH DELETE c
           """,
            collection=collection,
            user=user,
        )

    if collections:
        # delete all collections in array
        data = g.run(
            """
            MATCH (m:Member {uuid:$user})-[:OWNS]-(c:Collection)
            WHERE c.uuid in $collections
            DETACH DELETE c
           """,
            collections=collections,
            user=user,
        )

    # not needed for MyCollections if deleted via an multi-selection
    # double check with single uuid
    data = g.run(
        """
        MATCH (m:Member {uuid:$user})-[:OWNS]-(c:Collection)
        RETURN collect(c)
    """,
        user=user,
    ).evaluate()

    return api_response(status.OK, data)


# ------------------------------------------------------------------------
@COLLECTION.route("/", methods=["POST"])
async def index(request):
    print("** GET COLLECTIONS FOR USER: ", request.json)
    user = request.json.get("user")
    if not user:
        return api_response(status.USER_NOTFOUND)

    data = g.run(
        """
        MATCH (m:Member {uuid:$user})-[:OWNS]-(col:Collection)
        WITH col
        OPTIONAL MATCH (col)-[:THUMB_FOR]-(a)
        OPTIONAL MATCH (col)-[:HAS_LICENSE]-(license:License)
        OPTIONAL MATCH (col)-[:HAS_LANGUAGE]-(language:Language)
        OPTIONAL MATCH (col)-[:HTAGGED]-(tag:Tag)
        WITH  collect(tag) AS tags, col, license, language, a
        RETURN collect( col {
            .*,
            license: license,
            language: language,
            thumb: {url: '/ua/'+a.path, uuid: a.uuid},
            tags: tags
        })

    """,
        request.json,
    ).evaluate()

    print("dda: ", data)
    print(type(data))

    return new_api_response(status.OK, dict(data=data))


# ------------------------------------------------------------------------
@COLLECTION.route("/add", methods=["POST"])
async def add(request):
    print("=====================================================================")
    print("ADD NEW COLLECTION (hcollection): ", request.json)
    record = request.json
    record["slug"] = slugify(record["name"])
    print("RECORD: ", record)

    # duplicate subtitle and introduction until we can rename all subtitle to introduction
    try:
        print("TRY CREATE")
        data = g.run(
            """
            MATCH (m:Member {uuid:$user})
            CREATE (c:Collection)<-[:OWNS]-(m)
            SET
                c.name=$name,
                c.introduction=$introduction,
                c.subtitle=$introduction,
                c.description=$description,
                c.acknowledgements=$acknowledgements,
                c.uuid=apoc.create.uuid(),
                c.created=datetime(),
                c.ts=timestamp(),
                c.status='uploaded',
                c.slug=$slug
            RETURN c

        """,
            record,
        ).evaluate()
        return api_response(status.OK, data)

    except Exception as e:
        print("Error on CREATE: ", str(e))
        return api_response(status.ERROR, str(e))


# ------------------------------------------------------------------------
@COLLECTION.route("/removeCopyright", methods=["OPTIONS", "POST"])
async def collection_removeCopyright(request):
    """remove partner from item"""

    item = request.json.get("item")
    copyright = request.json.get("copyright")

    g.run(
        """
        MATCH (i:CollectionItem {uuid:$item})
        MATCH (l:License {uuid:$copyright})
        MATCH (i)-[r:HAS_LICENSE]-(l)
        DELETE r
    """,
        item=item,
        copyright=copyright,
    )

    return api_response(status.OK)


# ------------------------------------------------------------------------
@COLLECTION.route("/changeCopyright", methods=["OPTIONS", "POST"])
async def collection_changeCopyright(request):
    g.run(
        """
        MATCH (c:CollectionItem {uuid:$c})-[x]-(l:License)
        DELETE x
    """,
        c=request.json.get("item"),
    ).evaluate()

    g.run(
        """
        MATCH (i:CollectionItem {uuid:$c})
        MATCH (l:License {uuid:$l})
        CREATE (i)-[:HAS_LICENSE]->(l)

        """,
        c=request.json.get("item"),
        l=request.json.get("copyright"),
    ).evaluate()

    return api_response(status.OK)


# ------------------------------------------------------------------------
@COLLECTION.route("/removePartner", methods=["OPTIONS", "POST"])
async def collection_removePartner(request):
    """remove partner from item"""

    item = request.json.get("item")
    partner = request.json.get("partner")
    # print("item: ",item)
    # print("p: ", partner)

    # could be either Colllection or CollectionItem
    # we can't specify that on the match
    g.run(
        """
        MATCH (i {uuid:$item})
        MATCH (p:Partner {uuid:$partner})
        MATCH (i)-[r:HAS_PARTNER]-(p)
        DELETE r
    """,
        item=item,
        partner=partner,
    )

    return api_response(status.OK)


# ------------------------------------------------------------------------
@COLLECTION.route("/changePartner", methods=["OPTIONS", "POST"])
async def collection_changePartner(request):
    print("--> CHANGE PARTNER ")
    g.run(
        """
        MATCH (c:CollectionItem {uuid:$c})-[x]-(l:Partner)
        DELETE x
    """,
        c=request.json.get("item"),
    ).evaluate()

    g.run(
        """
        MATCH (i:CollectionItem {uuid:$c})
        MATCH (l:Partner {uuid:$l})
        CREATE (i)-[:HAS_PARTNER]->(l)

        """,
        c=request.json.get("item"),
        l=request.json.get("partner"),
    ).evaluate()

    return api_response(status.OK)


# ------------------------------------------------------------------------
@COLLECTION.route("/deleteItem", methods=["OPTIONS", "POST"])
async def collection_deleteItem(request):
    g.run(
        """
        MATCH (c:Collection {uuid:$c})<-[x:IN_COLLECTION]-(ci:CollectionItem {uuid:$item})
        DELETE x
        """,
        c=request.json.get("collection"),
        item=request.json.get("item"),
    ).evaluate()

    return api_response(status.OK)


# ------------------------------------------------------------------------

# voor nieuwe partner pagina collections
@COLLECTION.route("/update", methods=["POST"])
async def edit_collection(request):

    collection = request.json.get("uuid")
    order = request.json.get("order")

    if not collection:
        return api_response(status.NO_DATA)

    print("Edit collection: ", collection)
    col = g.run(
        """
        MATCH (c:Collection {uuid:$col})
        RETURN c
    """,
        col=collection,
    ).evaluate()

    # import json
    # print(json.dumps(request.json))

    col["name"] = request.json.get("name")
    col["subtitle"] = request.json.get("subtitle")
    col["description"] = request.json.get("description")
    col["acknowledgements"] = request.json.get("acknowledgements")
    col["introduction"] = request.json.get("introduction")
    col["slug"] = slugify(request.json.get("name"))
    col["status"] = request.json.get("status")
    col["modified"] = js_now()

    # set order
    if order:
        for e in order:
            g.run(
                "MATCH (ci:CollectionItem {uuid:$uuid}) SET ci.order=$order",
                uuid=e["uuid"],
                order=e["order"],
            ).evaluate()

    # deal with License
    lic = request.json.get("license")
    print("lic: ", lic)
    if lic:
        license_uuid = lic.get("uuid")

        if license_uuid:
            # remove old
            g.run(
                """
                MATCH (c:Collection {uuid:$collection})-[rel:HAS_LICENSE]-(l:License)
                DELETE rel
            """,
                collection=collection,
            )

            g.run(
                """
                MATCH (c:Collection {uuid:$collection})
                MATCH (l:License {uuid:$license})
                MERGE (c)-[:HAS_LICENSE]-(l)
            """,
                collection=collection,
                license=license_uuid,
            )

    # deal with Language
    lang = request.json.get("language")
    if lang:
        language_uuid = lang.get("uuid")
        print("lang: ", language_uuid)
        if lang:
            # remove old
            g.run(
                """
                MATCH (c:Collection {uuid:$collection})-[rel:HAS_LANGUAGE]-(l:Language)
                DELETE rel
            """,
                collection=collection,
            )

            g.run(
                """
                MATCH (c:Collection {uuid:$collection})
                MATCH (l:Language {uuid:$language})
                MERGE (c)-[:HAS_LANGUAGE]-(l)
            """,
                collection=collection,
                language=language_uuid,
            )

    try:
        s = g.push(col)
    except Exception as e:
        print("ERROR: ", e)
        return api_response(status.ERROR, dict(msg=str(e)))
    return api_response(status.OK, s)


# ------------------------------------------------------------------------
# deze wordt/werd gebruikt in de oude admin
#
@COLLECTION.route("/edit/<uuid>", methods=["OPTIONS", "POST", "GET"])
async def old_edit_collection(request, uuid):
    print("**** OLD EDIT COLLECTION")

    if request.method == "OPTIONS":
        return api_response(status.OK, None)

    if request.method == "POST":

        collection = request.json.get("uuid")
        print("Edit collection: ", collection)

        col = g.run(
            """
            MATCH (c:Collection {uuid:$col})
            RETURN c
        """,
            col=collection,
        ).evaluate()

        col["name"] = request.json.get("name")
        col["subtitle"] = request.json.get("subtitle")
        col["description"] = request.json.get("description")
        col["acknowledgements"] = request.json.get("acknowledgements")
        col["slug"] = request.json.get("slug")
        col["status"] = request.json.get("status")
        col["modified"] = js_now()
        partner = request.json.get("partner")

        print("partner: ", partner)
        # col['thumb'] = t

        # print("-> ", type(col))
        g.push(col)
        # print("SAVED: ", col)
        # print(json_dumps(request.json))

        items = request.json.get("items")

        # remove old items
        g.run(
            """
            MATCH (c:Collection {uuid:$collection})
            MATCH (c)<-[:IN_COLLECTION]-(ci:CollectionItem)
            DETACH DELETE ci
        """,
            collection=collection,
        ).evaluate()

        # connect collection to a partner if specified
        if partner:
            print("** delete old partner")
            # delete old partner

            g.run(
                """
                MATCH (c:Collection {uuid:$collection})
                MATCH (c)-[old:HAS_PARTNER]->(oldp:Partner)
                DELETE old
            """,
                collection=collection,
            ).evaluate()

            # connect new
            g.run(
                """
                MATCH (c:Collection {uuid:$collection})
                MATCH (p:Partner {uuid:$partner})
                CREATE (c)-[:HAS_PARTNER]->(p)
            """,
                collection=collection,
                partner=partner,
            ).evaluate()

        # create the new nodes
        # print('***********************')
        # print('**** items')

        for item in items:
            print("item: ", item)
            # create attrib if its not there otherwise Cypher create fails
            if not "description" in item:
                item["description"] = ""

            item["collection"] = collection
            # print(json_dumps(item))
            g.run(
                """
              MATCH (c:Collection {uuid:$collection})
              CREATE (ci:CollectionItem)-[:IN_COLLECTION]->(c)
              SET
                ci.uuid = $uuid,
                ci.order = $order,
                ci.title = $title,
                ci.filename = $filename,
                ci.url = $url,
                ci.description = $description
              RETURN ci

            """,
                item,
            )

            # handle copyright...
            m = dict(ci=item["uuid"], license=item["copyright"])
            if item["copyright"]:
                g.run(
                    """
                    MATCH (ci:CollectionItem {uuid:$ci})
                    MATCH (l:License {uuid:$license})
                    CREATE (ci)-[:HAS_LICENSE]->(l)
                """,
                    m,
                )

            # i = g.run("MATCH (ci {uuid:{uuid}}) RETURN ci", uuid=item['uuid']).evaluate()
            # print(i['title'])

    collection = g.run(
        """
        MATCH (col:Collection {uuid:$uuid})
        OPTIONAL MATCH (col)-[:IN_COLLECTION]-(entry:CollectionItem)
        OPTIONAL MATCH (entry)-[:HAS_LICENSE]->(l:License)
        OPTIONAL MATCH (col)-[:THUMB_FOR]-(asset)
        OPTIONAL MATCH (ml:License)<-[:HAS_LICENSE]-(col)
        OPTIONAL MATCH (col)-[:HAS_PARTNER]->(p:Partner)
        WITH entry, col, asset, l, ml, p
        ORDER BY entry.order ASC
        RETURN col {
            items: collect(entry { copyright: l.uuid, .*}),
            partner: p.uuid,
            thumb: case when asset.path is null
                    then "/uploads/"+asset.uuid+"_"+asset.filename
                    else "/ua/"+asset.path
                    end,
            .*
        }

    """,
        uuid=uuid,
    ).evaluate()

    # print("col: ", collection.keys())
    if not collection:
        return api_response(status.NO_DATA)

    return api_response(status.OK, collection)


# ------------------------------------------------------------------------


@COLLECTION.route("/publish", methods=["POST"])
async def publish_collection(request):

    collection = request.json.get("collection")
    partner = request.json.get("partner")

    if not (all([collection, partner])):
        return api_response(status.NO_DATA)

    g.run(
        """
        MATCH (p:Member {uuid:$partner})-[]-(c:Collection {uuid:$collection})
        SET
            c.is_published=true,
            c.updated=timestamp()

        RETURN c
        """,
        collection=collection,
        partner=partner,
    ).evaluate()

    return api_response(status.OK)


# ------------------------------------------------------------------------
@COLLECTION.route("/private", methods=["POST"])
async def private_collection(request):
    """set a collection to private"""

    collection = request.json.get("collection")
    partner = request.json.get("partner")

    if not (all([collection, partner])):
        return api_response(status.NO_DATA)

    g.run(
        """
        MATCH (p:Member {uuid:$partner})-[]-(c:Collection {uuid:$collection})
        SET
            c.is_published=false,
            c.updated=timestamp()

        RETURN c
        """,
        collection=collection,
        partner=partner,
    ).evaluate()

    return api_response(status.OK)


# ------------------------------------------------------------------------

# ------------------------------------------------------------------------
@COLLECTION.route("/update-item", methods=["POST"])
async def update_item(request):
    """update a CollectionItem"""

    print("-- UPDATE ITEM")
    item = request.json.get("item")
    item_license = item.get("license")

    if not item:
        print("NO DATA")
        return api_response(status.NO_DATA)

    print("item: ", item)

    g.run(
        """
        MATCH (item:CollectionItem {uuid:$uuid})
        SET
            item.title=$title,
            item.description=$description,
            item.order=$order

    """,
        uuid=item["uuid"],
        title=item["title"],
        description=item["description"],
        order=item["order"],
    )

    if item_license:
        print("-> ", item["uuid"])

        # delete existing connections
        g.run(
            """
            MATCH (ci:CollectionItem {uuid:$item})-[x:HAS_LICENSE]->(l:License)
            DETACH DELETE x
        """,
            item=item["uuid"],
        ).evaluate()

        # create new license connection
        d = g.run(
            """
            MATCH (ci:CollectionItem {uuid:$item})
            MATCH (l:License {uuid:$item_license})
            MERGE (ci)-[:HAS_LICENSE]->(l)
            RETURN l,ci
        """,
            item=item["uuid"],
            item_license=item_license["uuid"],
        ).evaluate()

        print("UPDATE LICENSE: ", d)
    return api_response(status.OK)
