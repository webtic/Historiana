from graph import g, graph
from sanic import Blueprint, response
from tic import api_response, status, NEW_UUID, js_now, url_to_filename
from slugify import slugify
from .europeana import partner_search, get_eu
from py2neo import Node, Relationship
from tasks import import_video_from_url
import os
import hashlib
import datetime
import requests

from config import Config

CONFIG = Config()

PARTNER = Blueprint("partners", url_prefix="/api/partners")

@PARTNER.route("/dashboard", methods=["POST"])
async def dashboard(request):
  user = request.json.get("uuid")
  print("DASHBOARD: ", user)

  n = g.run("MATCH (n:Narrative) RETURN count(n)").evaluate()
  c = g.run("MATCH (n:Collection) RETURN count(n)").evaluate()
  s = g.run("MATCH (n:MySources) RETURN count(n)").evaluate()
  sh = g.run("MATCH (n:MyShares) RETURN count(n)").evaluate()
  t = g.run("MATCH (n:Tag) RETURN count(n)").evaluate()

  recents = g.run("MATCH (s:MySources) WITH s ORDER BY s.created ASC  LIMIT 10 RETURN COLLECT({title:s.title, image:s.url})").evaluate()
  used = g.run("""
    MATCH (s:Collection)
    OPTIONAL MATCH (s)-[:THUMB_FOR]-(asset)
    WITH s,asset LIMIT 10 RETURN COLLECT({title:s.name, image:'/ua/'+asset.path})
    """).evaluate()


  data = dict(
    narratives = dict(total=n),
    sources = dict(total=s),
    collections = dict(total=c),
    tags = dict(total=t),
    shares = dict(total=sh),
    activities = dict(total=0),
    narrativesources = dict(total=0),
    members = dict(total=0),

    # value should be defined as property on the data object so the dashboard can render
    tops = [
        dict(label="Most recent items", value="recents"),
        dict(label="Most used items", value="used")
    ],

    topdata = dict(
        recents = recents,
        used = used
    ),


  )

  return api_response(status.OK, dash=data)


# ------------------------------------------------------------------------
@PARTNER.route("/toggle/visibility", methods=["POST"])
async def toggle_visibility(request):

    session = request.headers.get("X-Session")
    # print("****** ", session)

    session = request.json.get("session")
    partner = request.json.get("partner")

    if not any([session, partner]):
        return api_response(status.NO_DATA)

    s = g.run(
        """
        MATCH (s:Session {sessionId:$session})
        OPTIONAL MATCH (s)-[]-(m:Member)
        RETURN {
            valid:s.sessionValid,
            user: m
        }
    """,
        session=session,
    ).evaluate()

    user = s.get("user")

    if not user:
        return api_response(status.USER_NOTFOUND)

    if not user['is_admin']:
        return api_response(status.NOT_ALLOWED)

    # toggle the is_visible field on the partner
    # print("partner: ", partner)
    # print(f"XXXX: _{partner}_")

    p = g.run(
        """MATCH (p:Partner {uuid:$partner}) RETURN p""", partner=partner).evaluate()

    if 'is_visible' in p.keys():
        p['is_visible'] = not p['is_visible']
    else:
        p['is_visible'] = True

    g.push(p)

    return api_response(status.OK)


# ------------------------------------------------------------------------
@PARTNER.route("/uploadBatch", methods=["POST"])
async def upload_batch(request):
    print("********* UPLOAD EXCEL ************")
    session = request.headers.get("X-Session")
    process = request.headers.get("X-Process")
    print("s: ", session)
    print("p: ", process)

    if process != "ExcelBatch":
        return api_response(status.ERROR)

    if session:
        # lookup session and match user if possible
        s = g.run(
            """
            MATCH (s:Session {sessionId:$sess})
            OPTIONAL MATCH (s)-[]-(m:Member)
            RETURN {
                valid:s.sessionValid,
                user: m
            }
        """,
            sess=session,
        ).evaluate()

        user = s.get("user")

    if not user:
        return api_response(status.ERROR)

    print("PROCESS UPLOAD FOR ", user)

    for (order, k) in enumerate(request.files.keys()):
        file = request.files.get(k)
        digest = hashlib.sha256(file.body).hexdigest()
        (fname, ext) = os.path.splitext(file.name)
        upload_uuid = NEW_UUID()
        upload = {
            "filename": file.name,
            "extension": ext.lower(),
            "digest": digest,
            "type": file.type,
            "uuid": upload_uuid,
        }

        relative = ["ExcelBatch"]
        relative.append(upload["uuid"])
        path = "/".join([CONFIG.USER_UPLOADS, ] + relative)
        destination = path + "/" + file.name
        upload["path"] = "/".join(relative + [file.name])
        upload["created"] = js_now()
        os.makedirs(path, exist_ok=True)
        try:
            os.makedirs(path, exist_ok=True)
            with open(destination, "wb") as dest:
                dest.write(file.body)
                dest.close()

            print("PROCESS EXCEL: ", path)
            print("NAME: ", file.name)
            print("FOR: ", user)
        except Exception as e:
            print("ERROR: ", e)
            return api_response(status.ERROR, str(e))

    return api_response(status.OK)


# ------------------------------------------------------------------------
@PARTNER.route("/get-members", methods=["POST"])
async def getlist(request):

    search = request.json.get("search")

    data = g.run(
        """
        MATCH (m:Member) WHERE m.email CONTAINS $search
        RETURN collect(m.email)
        """,
        search=search,
    ).evaluate()

    return api_response(status.OK, data)


# ------------------------------------------------------------------------
@PARTNER.route("/", methods=["GET"])
async def index(request):
    """list of partners"""
    data = g.run(
        """
        MATCH (p:Partner)
        OPTIONAL MATCH (p)-[:LOGO]-(logo)
        OPTIONAL MATCH (p)-[:ICON]-(icon)
        OPTIONAL MATCH (p)-[:PARTNER_ADMIN]-(member)
        WITH DISTINCT p,
            collect({url: '/ua/'+logo.path, uuid: logo.uuid})[0] as logo,
            collect({url: '/ua/'+icon.path, uuid: icon.uuid})[0] as icon,
            collect(member.email)[0] as member
        ORDER BY lower(p.name)
        RETURN collect( DISTINCT p {
            .*,
            logo,
            icon,
            member
        })

    """
    ).evaluate()

    # data = g.run(
    #     """
    #     MATCH (p:Partner)
    #     WITH p
    #     ORDER BY p.name
    #     RETURN collect(p)
    # """
    # ).evaluate()

    return api_response(status.OK, data)


# ------------------------------------------------------------------------
@PARTNER.route("/delete", methods=["POST"])
async def delete(request):
    """delete partner by uuid"""
    uuid = request.json.get("uuid")
    g.run(
        """
            MATCH (p:Partner {uuid:$uuid})
            DETACH DELETE p
        """,
        uuid=uuid,
    )
    return api_response(status.OK)


# ------------------------------------------------------------------------


@PARTNER.route("/add", methods=["POST"])
@PARTNER.route("/edit", methods=["POST"])
async def partner_add_edit(request):
    print(50 * "=")
    print("add/edit partner!")
    # split current url, last non-empty element is either edit or add
    mode = [e for e in request.path.split("/") if e].pop()

    record = request.json

    # sanitize data where needed
    record["slug"] = slugify(record["name"]) if not record.get("slug") else record["slug"]
    record["providers"] = record.get('providers','')
    record["dataproviders"] = record.get('dataproviders','')

    # make sure adding without logo/icon do not fail
    if not record.get("logo"):
        record["logo"]  = dict(url="", uuid="")

    if not record.get("icon"):
        record["icon"]  = dict(url="", uuid="")

    if mode == "add":
        partner = g.run(
            """
                CREATE (p:Partner)
                SET p.name = $name,
                p.slug = $slug,
                p.uuid = apoc.create.uuid(),
                p.logo = $logo['url'],
                p.icon = $icon['url'],
                p.introduction = $introduction,
                p.providers = $providers,
                p.dataproviders  = $dataproviders
                RETURN p
        """,
            record,
        ).evaluate()

    elif mode == "edit":
        partner = g.run(
            """
                MATCH (p:Partner {uuid:$uuid})
                OPTIONAL MATCH (p)-[l:LOGO]-(a)
                OPTIONAL MATCH (p)-[i:ICON]-(a)
                DELETE l,i
                WITH p
                SET p.name = $name,
                p.slug = $slug,
                p.logo = $logo['url'],
                p.icon = $icon['url'],
                p.introduction = $introduction,
                p.providers = $providers,
                p.dataproviders = $dataproviders
                RETURN p
        """,
            record,
        ).evaluate()

    else:
        print("INVALID MODE")
        return api_response(status.NOT_FOUND)

    # connect logo to new partner
    val = g.run(
        """
            MATCH (p:Partner {uuid:$partner_uuid})
            MATCH (l {uuid:$logo_uuid})
            CREATE (p)-[x:LOGO]->(l)
            RETURN x
        """,
        partner_uuid=partner["uuid"],
        logo_uuid=request.json.get(
            "logo")["uuid"] if request.json.get("logo") else "",
    ).evaluate()

    # connect icon to new partner
    g.run(
        """
            MATCH (p:Partner {uuid:$partner_uuid})
            MATCH (l {uuid:$icon_uuid})
            CREATE (p)-[x:ICON]->(l)
            RETURN x
        """,
        partner_uuid=partner["uuid"],
        icon_uuid=request.json.get(
            "icon")["uuid"] if request.json.get("icon") else "",
    )

    # delete existing member
    g.run(
        """
            MATCH (p:Partner {uuid:$partner_uuid})
            MATCH (m:Member {email:$member})
            OPTIONAL MATCH (p)-[pa:PARTNER_ADMIN]-(m)
            DELETE pa
    """,
        partner_uuid=partner["uuid"],
        member=request.json.get("member"),
    ).evaluate()

    # connect member to this partner
    g.run(
        """
            MATCH (p:Partner {uuid:$partner_uuid})
            MATCH (m:Member {email:$member})
            OPTIONAL MATCH (p)-[pa:PARTNER_ADMIN]-(m)
            DELETE pa
            CREATE (p)<-[x:PARTNER_ADMIN]-(m)
            WITH m
            SET m.is_partner=TRUE
            RETURN m
        """,
        partner_uuid=partner["uuid"],
        member=request.json.get("member"),
    ).evaluate()

    return api_response(status.OK, partner)


# ------------------------------------------------------------------------
@PARTNER.route("/<slug>", methods=["GET"])
async def get_partner(request, slug):

    if not slug:
        return api_response(status.NO_DATA)

    """
    tricky query alert: .* normally returns props of first parameter
    but in order for the thumb to retrieve correctly we need to swap it for the 'item'
    NOT SURE WHY - Neo 3.5.12
    - alternative writing:

        WITH collect( col {
            .*,
            thumb: '/ua/'+a.path
            }) as collections
        RETURN collections

    see mycollections.py index method

    """
    data = g.run(
        """
        MATCH (p:Partner {slug:$slug})
        OPTIONAL match (p)-[]-(m:Member)-[]-(col:Collection {is_published:true})
        OPTIONAL MATCH (p)-[:LOGO]-(logo)
        OPTIONAL MATCH (p)-[:ICON]-(icon)
        OPTIONAL MATCH (col)-[:THUMB_FOR]-(thumb)
        WITH p, logo, icon {
            .*,
            logo: logo.url,
            icon: icon.url
        },

        thumb, col {
            .*,
            thumb_url: '/ua/'+thumb.path,
            csstype:'collection',
            type: 'Collection'
        } as item, m

        RETURN {
            partner: p,
            owner: m.uuid,
            collections: collect(distinct item)
        }
    """,
        slug=slug,
    ).evaluate()

    activities = g.run(
        """
        MATCH (p:Partner {slug:$slug})
        OPTIONAL match (p)-[]-(m:Member)-[]-(col:MyActivity {is_published:true})
        OPTIONAL MATCH (p)-[:LOGO]-(logo)
        OPTIONAL MATCH (p)-[:ICON]-(icon)
        OPTIONAL MATCH (col)-[:HAS_IMAGE]-(thumb)
        WITH p, logo, icon {
            .*,
            logo: logo.url,
            icon: icon.url
        },

        thumb, col {
            .*,
            thumb_url: '/ua/'+thumb.path,
            type: 'e-Learning Activity',
            csstype:'ela'


        } as item, m

        RETURN {
            partner: p,
            owner: m.uuid,
            activities: collect(distinct item)
        }
    """,
        slug=slug,
    ).evaluate()

    if data:
        data["activities"] = activities['activities']
        return api_response(status.OK, **data)

    elif activities:
        return api_response(status.OK, **activities)

    return api_response(status.NO_DATA)


# ------------------------------------------------------------------------
@PARTNER.route("/check_email", methods=["POST"])
async def check_new_partner(request):
    email = request.json.get("email")
    print("\n\n** CHKECK **: ", email)

    if email:
        print("GO")
        data = g.run(
            """MATCH (m:Member {email:$email}) RETURN m""", email=str(email),
        ).evaluate()
        print("DATA: ", data)

        if data:
            print("FOUND")
            return api_response(
                status.OK, uuid=data["uuid"], isMember=True, record=data
            )
        else:
            print("NOT FOUND")
            return api_response(status.OK, isMember=False)

    return api_response(status.ERROR)


# ------------------------------------------------------------------------
@PARTNER.route("/import", methods=["POST"])
async def import_dataset_from_europeana(request):
    """import a dataset found via searchEuropeana.vue"""

    session = request.json.get('session')
    partner = request.json.get('partner')
    dataset = request.json.get('dataset')
    importtype = request.json.get('type')
    print("** import_dataset_from_europeana")
    print(">> SESSION: ", session)
    print(">> PARTNER: ", partner)
    print(">> DATASET: ", dataset)
    print(">> IMPORT-TYPE: ", importtype)

    g = graph()

    name = request.json.get('name')

    # check if this session (user) is a Partner
    check = g.run("""
        MATCH (s:Session {sessionId:$session})-[:LOGGED_IN]-(m:Member)-[:PARTNER_ADMIN]-(p:Partner)
        RETURN {
            partner: p,
            member: m
        }
    """, session=session).evaluate()

    # retry as admin
    if not check:
        check = g.run("""
            MATCH (s:Session {sessionId:$session})-[:LOGGED_IN]-(m:Member {is_admin:true}) RETURN { member: m }
        """, session=session).evaluate()

    # nor partner nor admin, fail hard
    if not check:
        return api_response(status.NOT_ALLOWED)


    if importtype=='collection':
        print("* IMPORT AS COLLECTION ")

        # check for (global) name-collision, adept if needed
        titles = g.run("MATCH (c:Collection) where c.name contains {name} RETURN count(c)", name=name).evaluate()

        if titles>0:
            name = f"{name} ({titles+1})"

        slug = slugify(name)
        uuid = NEW_UUID()

        col = Node('Collection', **dict(name=name, slug=slug, uuid=uuid, status='eu_import'))
        g.create(col)

        # admin is not a partner
        if check.get('partner'):
            has_partner = Relationship(col,"HAS_PARTNER",check['partner'])
            g.create(has_partner)

        owns = Relationship(check["member"],"OWNS",col)
        g.create(owns)


    import_status = []

    # process all images regardless import-type
    # we ontvangen hier een "eulink" die we eerder ontvangen van een zoekresultaat
    # dit is een link zoals:
    # https://api.europeana.eu/record/2051943/data_euscreenXL_EUS_1012A80140E44AE891D2AA241A1CC644.json?wskey=VBCZAGGDZI
    # deze bevat dus onze key dus niet lekken naar de frontend.
    # via deze link kunnen we detail info over het item opvragen.
    #
    # soms is dat een link naar een direct object (jpg/mp4) maar vaker niet.
    # zie de velden:
    #
    # edmIsShownBy: "http://www.euscreen.eu/item.html?id=EUS_1012A80140E44AE891D2AA241A1CC644",
    # edmIsShownAt: "http://www.euscreen.eu/item.html?id=EUS_1012A80140E44AE891D2AA241A1CC644",
    # edmObject: "http://images1.noterik.com/edna/domain/euscreen/user/eu_vrt/video/583/shots/1/h/0/m/0/sec5.jpg",
    #
    #


    for offset, e in enumerate(dataset):
        print(f">> IMPORT >> {e['eulink']}")

        # de oorspronkelijke url is de thumb die europeana standaard gebruikt
        # we gebruiken deze om bij sound en andere media-types die geen plaatje zijn.
        thumb = e.get('url')


        # get details for this object
        obj = await get_eu(e['eulink'])

        print(">> by", obj.get('edmIsShownBy'))
        print(">> at", obj.get('edmIsShownAt'))

        if importtype=='collection':
            # add collection-uuid to path
            path = "/".join([CONFIG.USER_UPLOADS, check['member']['uuid'], uuid])
            base_url = "/".join([check['member']['uuid'], uuid])

        else:
            path = "/".join([CONFIG.USER_UPLOADS, check['member']['uuid']])
            base_url = "/".join([check['member']['uuid']])


        print("XXX: ", obj['edmIsShownBy'])
        asset = requests.get(obj['edmIsShownBy'])
        import_status.append(dict(url=obj['edmIsShownBy'],resp=asset.status_code))


        mimetype = asset.headers.get('Content-Type')
        filesize = asset.headers.get('Content-Length')


        filename = NEW_UUID()
        _, ext = url_to_filename(obj['edmIsShownBy'])


        if ext=='.mp4':
            """post process video"""
            print("** PROCESS AS VIDEO")
            res = import_video_from_url(check['member']['uuid'], obj['edmIsShownBy'],e.get("title"), e.get("description"))
            import_status.append(dict(url=obj['edmIsShownBy'],resp=res))

        else:

            # assume jpg if there is no extension found
            if ext.strip()=='':
                ext = ".jpg"

            # verwerk de eigenlijke asset
            os.makedirs(path, exist_ok=True)
            dest = f"{path}/{filename}{ext}"
            upload_url = f"{base_url}/{filename}{ext}"

            print("CREATE  FILE ", dest)
            print("URL FOR FILE ", upload_url)
            hashfile = hashlib.sha256()
            with open(dest, "wb") as f:
                for chunk in asset.iter_content(1024):
                    hashfile.update(chunk)
                    f.write(chunk)

            upload_uuid = NEW_UUID()
            upload = {
                "filename": filename,
                "extension": ext.lower(),
                "digest": hashfile.hexdigest(),
                "type": asset.headers.get("content-type"),
                "uuid": upload_uuid,
                "path": upload_url,
                "created": datetime.datetime.now(),
                "via": "EU_IMPORT",
            }

            ua = Node("UserAsset", **upload)
            g.create(ua)

            #####
            # doe hetzelfde maar nu voor de externe thumbnail, ook deze nemen we over als asset
            dest = f"{path}/{filename}_thumb.jpg"
            thumb_upload_url = f"{base_url}/{filename}_thumb.jpg"

            print("CREATE  THUMB ", dest)
            print("URL FOR THUMB ", thumb_upload_url)
            hashfile = hashlib.sha256()
            _thumb = requests.get(thumb)
            with open(dest, "wb") as f:
                for chunk in _thumb.iter_content(1024):
                    hashfile.update(chunk)
                    f.write(chunk)



            thumb_uuid = NEW_UUID()
            print("THUMB ", thumb_uuid, " FOR ", filename)
            thumb_upload = {
                "filename": filename,
                "extension": ext.lower(),
                "digest": hashfile.hexdigest(),
                "type": _thumb.headers.get("content-type"),
                "uuid": thumb_uuid,
                "path": thumb_upload_url,
                "created": datetime.datetime.now(),
                "via": "EU_IMPORT",
            }

            # create (UserAsset)-[:THUMB_FOR]-(UserAsset)
            ua_thumb = Node("UserAsset", **thumb_upload)
            g.create(ua_thumb)

            # update UserAsset with url for this thumbnail
            ua['thumb_url'] = f'/ua/{thumb_upload_url}'
            g.push(ua)

            print("CREATE THUMB FOR_RELATION")
            new_thumb = Relationship(ua_thumb, "THUMB_FOR", ua)
            g.create(new_thumb)


            if offset==0 and importtype=='collection':
                # create thumbnail from first image
                thumb = Relationship(ua, "THUMB_FOR", col)
                g.create(thumb)

            else:
                # just a source
                thumb_dest = f"thumb_{filename}"


            # set ownership
            owns = Relationship(check["member"], "OWNS", ua)
            g.create(owns)

            if importtype=='collection':

                # create CollectionItem
                g.run("""
                    MATCH (c:Collection {uuid:$collection})
                    MATCH (ua:UserAsset {uuid:$ua})
                    CREATE (ua)<-[:IS_ASSET]-(ci:CollectionItem)-[:IN_COLLECTION]->(c)
                    SET
                        ci.uuid = apoc.create.uuid(),
                        ci.order = {offset},
                        ci.title = {title},
                        ci.filename = {filename},
                        ci.url = {url},
                        ci.description = {description}
                    RETURN ci
              """, dict(
                ua = upload_uuid,
                collection = uuid,
                offset = offset,
                title = e.get("title"),
                description =e.get("description"),
                filename = filename,
                url = f"/ua/{upload_url}"
              ))


            if importtype=='sources':


                data = dict(
                    asset_uuid = ua['uuid'],
                    url = f"/ua/{upload_url}",
                    user_uuid = check["member"]['uuid'],
                    title = e.get("title"),
                    description =e.get("description"),
                )

                print("CREATE SOURCE WITH: ", data)


                cr = g.run("""
                    MATCH (m:Member {uuid:$user_uuid})
                    MATCH (u:UserAsset {uuid:$asset_uuid})
                    CREATE (s:MySources)
                    SET
                        s.title={title},
                        s.description={description},
                        s.url={url},
                        s.uuid=apoc.create.uuid(),
                        s.created=timestamp()
                    CREATE (s)-[:ASSET]->(u)
                    CREATE (s)-[:SOURCE]->(m)
                    RETURN s

                """, data)


        import json
        print(json.dumps(import_status, indent=4))

    return api_response(status.OK)

# ------------------------------------------------------------------------
@PARTNER.route("/eulinktest", methods=["GET"])
async def eulinktest(request):

    #await get_eu("https://api.europeana.eu/record/90402/SK_A_296.json?wskey=VBCZAGGDZI")

    return api_response(status.OK,
        await get_eu("https://api.europeana.eu/record/90402/SK_A_296.json?wskey=VBCZAGGDZI")
    )



# ------------------------------------------------------------------------
@PARTNER.route("/start-eusearch", methods=["POST"])
async def check_new_partner(request):
    """return the current partner, used in mounted() to get data|provider settings in searchEuropeana.vue"""
    session = request.json.get('session')
    print(f"START EUSSEARCH SESSION {session}")

    # get own instance as this function can be called multithreaded
    gr = graph()

    print("************************************************")
    print("** check_new_partner (START EU_SEARCH) for session: ", session)

    #partner = gr.run("""MATCH (s:Session {sessionId:$session})-[:LOGGED_IN]-(m:Member)-[:PARTNER_ADMIN]-(p:Partner) RETURN collect(p)""", session=session).evaluate()
    partner = gr.run("""
      MATCH (s:Session {sessionId:$session})-[:LOGGED_IN]-(m:Member)-[:PARTNER_ADMIN]-(p:Partner) RETURN collect(p)
    """, session=session).evaluate()

    admin = gr.run("""
        MATCH (s:Session {sessionId:$session})-[:LOGGED_IN]-(m:Member {is_admin:true}) RETURN collect(m)
    """, session=session).evaluate()

    # print("=====")
    print("m_check_new_partner: 1", admin)
    print("m_check_new_partner: 2", partner)
    # print("=====")

    if not partner and not admin:
        #print("NOT PARTNER/ADMIN")
        return api_response(status.NO_PARTNER_CONNECTION)

    return api_response(status.OK, partner if partner else admin)

# ------------------------------------------------------------------------
@PARTNER.route("/eusearch", methods=["POST"])
async def europeana_search(request):

    session = request.json.get('session')
    search = request.json.get('search')
    print(f"### HI EUSEARCH session: {session} / string: {search}")

    gr = graph()

    partner = gr.run("""
        MATCH (s:Session {sessionId:$session})-[:LOGGED_IN]-(m:Member)-[:PARTNER_ADMIN]-(p:Partner) RETURN p""",
        session=session).evaluate()

    admin = gr.run("""
        MATCH (s:Session {sessionId:$session})-[:LOGGED_IN]-(m:Member {is_admin:true}) RETURN m
    """, session=session).evaluate()

    if not partner and not admin:
        print("** NOT ALLOWED")
        return api_response(status.NOT_ALLOWED)


    # print("DF: ", partner['dataproviders'])
    # print("P : ", partner['providers'])

    df = partner['dataproviders'] if partner else ''
    p = partner['providers'] if partner else ''


    data = await partner_search(search, df, p)
    print("GOT: ", data)
    return api_response(status.OK, data)


