from graph import g
from sanic import Blueprint, response
from tic import api_response, status, js_now
from slugify import slugify
from os.path import dirname

DATA = Blueprint("data", url_prefix="/api/data")


# ------------------------------------------------------------------------
@DATA.route("/linktypes", methods=["GET"])
async def get_linktypes(request):
    print("** GET LINK TYPES **")

    data = g.run("""
        MATCH (l:LinkType) WITH l ORDER BY l.name
        RETURN collect({
            uuid: l.uuid,
            name: trim(l.name)
        })
        """).evaluate()

    return api_response(status.OK, data)


# ------------------------------------------------------------------------
@DATA.route("/licenses", methods=["GET"])
async def get_licenses(request):
    print("** GET LICENSE TYPES **")

    data = g.run("""
        MATCH (l:License) WITH l ORDER BY l.name
        RETURN collect({
            uuid: l.uuid,
            name: l.name,
            code: l.code
        })
        """).evaluate()

    return api_response(status.OK, data)

# ------------------------------------------------------------------------
@DATA.route("/durations", methods=["GET"])
async def get_durations(request):
    data = g.run("""
        MATCH (l:ActivityDuration) WITH l ORDER BY l.label
        WITH collect({label:l.label}) as options
        RETURN {
            title: 'Duration',
            options: options
        }
        """).evaluate()

    return api_response(status.OK, data)
