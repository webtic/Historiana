"""
hi.st services
"""

from sanic import Blueprint
from sanic.response import redirect
from tic import base66, api_response, status, js_now
from config import Config

CONFIG = Config()

HIST = Blueprint("hist")
VERSION = 1.0

try:
    from graph import g
except:
    print("Error connecting to NEO4J Graph")
    sys.exit(1)


# ------------------------------------------------------------------------
@HIST.route("/create", methods=["OPTIONS", "POST"])
async def hist_create_shortcode(request):
    """create a hi.st shortcode
    input:
        url - url to shorten
        name - optional name to use as shortcode
    """
    print("** HIST CREATE **")
    print(request.json)
    url = request.json.get("url")
    name = request.json.get("name")
    shortlinkfor = request.json.get("shortlinkfor")

    print("url: ", url)

    if name:
        # print("hist create name ", name, " for ", url)
        reply = {"status": "nok", "msg": "Create link by name not yet implemented"}

    else:
        # print("hi.st create link ", url)
        # get max num and create new link in one transaction
        # is probably thread- and lock safe
        r = g.run(
            """
            MATCH (m:Redirect)
            WITH max(m.num) AS next
            CREATE (r:Redirect) SET
                r.num = next+1,
                r.to = $url,
                r.created = timestamp(),
                r.last_redirect = 0,
                r.redirects = 0
            RETURN r.num as num
        """,
            url=url,
        ).evaluate()

        # shortcode = CONFIG.SHORT_BASE + tic.base66.encode(r)

        shortcode = "{}/{}".format(CONFIG.SITE.get("shortener"), base66.encode(r))
        print("shortcode: ", shortcode)

        if shortlinkfor:
            x = g.run(
                """
                MATCH (r:Redirect {num:$num})
                MATCH (n {uuid:$node_uuid})
                CREATE (r)-[x:SHORTLINK_FOR]->(n)
                SET x.url = $shortcode
                RETURN x
                """,
                num=r,
                node_uuid=shortlinkfor,
                shortcode=shortcode,
            )

        reply = {"status": "ok", "shortUrl": shortcode}

    return api_response(status.OK, shortUrl=shortcode)


# ------------------------------------------------------------------------
@HIST.route("/get/<code>", methods=["GET"], name="get-code")  # used to wrap hi.st service in local development
@HIST.route("/<code>", methods=["GET"], name="no-get-code")
async def hist_get_redirect(request, code):
    """check Redirect nodes for a matching redirect"""

    # when in development we enter via /hist/get/code and we need a client-side redirect
    # hence we do not return a 302 here unless accessed directly via /hist/SOMECODE
    do_redirect = False if "/api/hist/get/" in request.url else True
    # print("code: ", code)
    # print("AA: ",  base66.decode(code))
    try:
        r = g.run(
            """
                MATCH (r:Redirect)
                WHERE r.code=$code
                OR r.num=$num
                RETURN collect(r) AS r
            """,
            {"code": code, "num": base66.decode(code)},
        ).evaluate()[0]

        if r:
            r["last_redirect"] = js_now()
            r["redirects"] = r["redirects"] + 1 if r["redirects"] else 1
            g.push(r)
            # log_request(request, code, 302, r['to'] )

        if do_redirect:
            # print("redirect")
            return redirect(r["to"], status=301)
        else:
            # print("return data")
            return api_response(dict(isError=False, url=r["to"]))

    except Exception as e:
        print("exception: ", e)
        pass

    return api_response(status.NO_DATA)


# ------------------------------------------------------------------------
@HIST.route("/", methods=["GET"])
async def hist_redirect_home(request):
    return redirect("https://live.historiana.eu/")
