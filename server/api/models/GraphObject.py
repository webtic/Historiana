import json
import hjson
import os
import os.path

# global connect to Neo4j
#from py2neo import Graph
#g = Graph(user='neo4j', password='geen')


class GraphObject(object):

	@classmethod
	def __init__(self):

		# get path of this file and use it as the default path for the .hjson
		path = os.path.dirname(__file__)

		# read the definitions from the .hjson file
		with open(f"{path}/{self.__name__}.hjson", "r") as node:
			defs =hjson.load(node)

		# set all as properties of the object
		for d in defs:
			setattr(self, d, defs[d])
		
		# read and store node properties in the properties prop
		with open(f"{path}/{self.get_properties_from}", "r") as props:
			self.properties = json.load(props)

	def __repr__(self):
		return 'GraphObject: ' + str(dict(name=self.name))


	def getMetaQuery(self):
		for e in self.views:
			if e['name'] == 'Meta':
				return e['query']


