

from graph import g
from sanic import Blueprint, response
import datetime

SITEMAP = Blueprint("sitemap", url_prefix="/api/sitemap")


def xml(entries):
    entries = "\n".join([entry(e) for e in entries])
    return f"""<?xml version="1.0" encoding="utf-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
{entries}
</urlset>
"""

def entry(e):
    lastmod = e.get('lastmod', datetime.date.isoformat(datetime.datetime.now()))
    loc = e.get('loc')
    try:
        lastmod = datetime.datetime.fromtimestamp(float(lastmod / 1000.0)).isoformat()

    except:
        pass

    return f"""<url>
<loc>https://historiana.eu{loc}</loc>
<lastmod>{lastmod}</lastmod>
<changefreq>monthly</changefreq>
<priority>0.6</priority>
</url>
"""

urls = [
    dict(loc='/historical-content'),
    dict(loc='/teaching-learning'),
    dict(loc='/sas/partners'),
    dict(loc='/sas/partners')
]

@SITEMAP.route("/", methods=["GET"])
async def generate(request):

    global urls

    modules = g.run("""
        MATCH (m:Module)
        WITH m OPTIONAL MATCH (m)-[]-(mt:ModuleType)
        WITH m,apoc.text.slug(toLower(mt.name_plural)) AS type
        RETURN collect({loc:'/historical-content/'+type+'/'+m.slug})
    """).evaluate()

    urls = urls + modules

    collections = g.run("""
        MATCH (c:Collection)-[*1..2]-(owner:Member {login:'admin@historiana.eu'})
        RETURN collect({
            loc: '/historical-content/source-collections/'+apoc.text.slug(c.slug),
            lastmod: c.modified
        })
    """).evaluate()

    urls = urls + collections
    return response.text(xml(urls), content_type="application/xml")
