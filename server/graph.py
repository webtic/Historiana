"""global connect to Neo4j"""

import os
import time
from py2neo import Graph

class ConnectionError(Exception):
    pass

from config import CONFIG


retry = 10
while retry:

    try:
        g = Graph(CONFIG.DB_URL, name=CONFIG.DB_NAME, auth=CONFIG.DB_AUTH)
        break

    except ConnectionError:
        print(f"retry connect Neo4J {11-retry}/10")
        time.sleep(1)
        retry = retry - 1

    raise ConnectionError("Cannot connect to Graph")

# return an instance of a Graph connection for multi-threaded purposes
def graph():
    return Graph(CONFIG.DB_URL, name=CONFIG.DB_NAME, auth=CONFIG.DB_AUTH)
