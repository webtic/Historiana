from sanic import Sanic
from sanic.response import json, html
import logging
import hashlib
import os.path
import uuid

from graph import g

app = Sanic(__name__)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

app.static('/assets', '../historiana-thriving/dist/assets')
app.static('/', '../historiana-thriving/dist')

@app.route("/upload")
async def u(request):
    print("request: ", request.url)
    print(dir(request))

    print("## headers ##")
    print( json.dumps(request.headers,indent=4) )

    print("## body ##")
    #print( json.dumps("%s" % request.body))

    #print("files: ", request.files)
    if request.method=='POST':
        upload = request.files['image']
        #name = request.files['name']
        #print("name: ", name)
        print("filew: ", upload.type, type(upload.type))
        print("filew: ", upload.name)
        data = upload.body
        f=open("bla.dump","wb")
        f.write(data)
        f.close()

        sig = hashlib.sha512(data).hexdigest()
        print(sig)
        try:
            ra = request.headers['Remote-Addr'].split(':')[0]
        except:
            ra = None

        upload = {

            'digest':       sig,
            'filename':     upload.name,
            'remote_addr':  ra,
            'mimetype':     upload.type,
            'status':       'uploaded',
            'extension':    os.path.splitext(upload.name)[1],
            'uuid':         str(uuid.uuid4()),
            'owner':        'anonymous',
            'uploaded':     'timestamp()'
        }



        print("ra: ", upload)
        
        #print("keys: ", upload.keys())

    return html('<form action="" method="post" enctype="multipart/form-data"><input type="file" name="image"><input type="submit">')


#@app.route("/")
#async def test(request):
#    return json({"hello": "world"}) 
#app.static('/', '/web/sites/historiana/historiana-thriving/dist')



@app.route("/foo")
async def test(request):
    print("foo: ", request.headers)
    return json({"foo": "bar"})


# @app.route("/")
# async def homepage(request):
#     """homepage"""
#     return html(open("../historiana-thriving/dist/index.html").read())


async def handler(request, *params):
    print("rr, request.url)
    print("params: ", params)
    print("handler!!!")
    return json("df")


paths = g.run("MATCH (p:Path)-[:USE_TEMPLATE]-(t:Template) RETURN p.name as uri,t.file as template")
for p in paths:
    print("uri: ",p['uri'])
    app.router.add(uri=p['uri'], methods=['GET'], handler=handler)
    #            self.router.add(uri=uri, methods=methods, handler=handler).

#     print(p)
  
print("routers: ", app.router.routes_all)
app.run(host="0.0.0.0", port=9000, debug=True)

