"""
Historiana Global API configuration
this needs a major cleanup :)
"""

__version__ = (1, 0, 0)

import os

# print("config.py config for: {}".format(os.uname().nodename))
# print(os.uname().nodename)
# print('=========')


class Config:
    """
    Global configuration object
    from config import config
    HOME: used in /share-activity to create the correct link to a short url

    """

    CWD = os.getcwd()

    # NOTE: no trailing / in urls !
    CONFIGS = {
        # XXX url is op het moment de URL naar de assets in de API
        # betere naam verzinnen!
        "atom.webtic.net": {
            "comment": "local development",
            "url": "http://10.10.10.42:9000",
            "home": "http://localhost:8080",
            "remote": "http://92.108.93.241:8080",
            "shortener": "http://localhost:8080/hist",
            "development": True,
            "user_uploads": "/Users/paulj/Projects/Historiana/development/server/user_assets",
            "tasks": "/Users/paulj/Projects/Historiana/development/server/tasks",
            "mail_templates": "/Users/paulj/Projects/Historiana/development/server/email-templates",
            "DB_AUTH": ("neo4j", "s3cret42"),
            "DB_NAME": "historianadev",
            "DB_URL": "bolt://neo4j.tic:7687"


        },
        "dev.historiana.eu": {
            "comment": "online dev server dev.historiana.eu",
            "url": "https://dev.historiana.eu",
            "home": "https://dev.historiana.eu",
            "shortener": "http://dev.hi.st",
            "rollbar": "a3be502916464d8fa3683f4e35841adf",
            "ROLLBAR_ENV": "development",
            "utf8console": True,
            "user_uploads": "/web/sites/historiana.eu/site/user_assets",
            "tasks": "/web/sites/historiana.eu/api/tasks",
            "mail_templates": "/web/sites/historiana.eu/api/email-templates",
            "DB_AUTH": ("neo4j", "geen"),
            "DB_NAME": "historiana",
            "DB_URL": "bolt://localhost:7687"

        },
        "newdev": {
            "comment": "newdev server dev.historiana.eu",
            "url": "https://newdev.historiana.eu",
            "home": "https://newdev.historiana.eu",
            "shortener": "http://newdev.hi.st",
            "rollbar": "a3be502916464d8fa3683f4e35841adf",
            "ROLLBAR_ENV": "development",
            "utf8console": True,
            "user_uploads": "/web/sites/historiana.eu/site/user_assets",
            "tasks": "/web/sites/historiana.eu/api/tasks",
            "mail_templates": "/web/sites/Historiana/server/api/email-templates",
        },
        "test.historiana.eu": {
            "comment": "online test server dev.historiana.eu",
            "url": "https://test.historiana.eu",
            "home": "https://test.historiana.eu",
            "shortener": "http://test.hi.st",
            "rollbar": "a3be502916464d8fa3683f4e35841adf",
            "ROLLBAR_ENV": "test",
            "utf8console": True,
            "user_uploads": "/web/sites/historiana.eu/site/user_assets",
            "tasks": "/web/sites/historiana.eu/api/tasks",
            "mail_templates": "/web/sites/historiana.eu/api/email-templates",
        },

        "live.historiana.eu": {
            "comment": "Production server",
            "url": "https://live.historiana.eu",
            "home": "https://live.historiana.eu",
            "shortener": "http://hi.st",
            "rollbar": "a3be502916464d8fa3683f4e35841adf",
            "ROLLBAR_ENV": "production",
            "utf8console": True,
            "user_uploads": "/web/sites/historiana.eu/site/user_assets",
            "mail_templates": "/web/sites/historiana.eu/api/email-templates",
        },
        "hi.webtic.nl": {
            "url": "http://www.beta.historiana.eu",
            "home": "http://www.beta.historiana.eu",
            "shortener": "http://beta.hi.st",
            "user_uploads": os.getcwd() + "/user_assets",
        },
        "demo.historiana.eu": {
            "url": "http://demo.historiana.eu",
            "home": "http://demo.historiana.eu",
            "shortener": "http://demo.hi.st",
            "utf8console": True,
            "user_uploads": os.getcwd() + "/user_assets",
        },
    }

    # alias
    CONFIGS["atom.local"] = CONFIGS["atom.webtic.net"]

    # USER_UPLOADS = os.getcwd() + '/user_assets'
    USER_UPLOADS = CONFIGS.get(os.uname().nodename)["user_uploads"]
    DB_AUTH = CONFIGS.get(os.uname().nodename)["DB_AUTH"]
    DB_NAME = CONFIGS.get(os.uname().nodename)["DB_NAME"]
    DB_URL = CONFIGS.get(os.uname().nodename)["DB_URL"]
    USER_UPLOADS_URL = "/ua"
    SHORT_BASE = "https://hi.st/"
    SERVER = None

    # =========================================================
    def __init__(self):

        try:
            self.SERVER = os.uname().nodename
            self.SITE = self.CONFIGS.get(self.SERVER)
            self.SITE_URL = self.SITE["home"]
            self.development = self.SITE.get("development", False)
            self.opbeat = self.SITE.get("opbeat", False)
        except Exception as e:
            print("ABORT cannot get nodename")
            print("exception: ", e)

    # =========================================================
    def __repr__(self):
        return "{}".format(self.CONFIGS.get(self.SERVER))

    @property
    def HOME(self):
        return self.SITE["home"]


CONFIG = Config()
print("active config: ", CONFIG)
