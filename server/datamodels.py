
# datamodels for Historiana

import datetime
from dataclasses import dataclass

@dataclass
class EventLog:
	"""eventlog entry"""

	ts: datetime.datetime
	remote_addr: str
	source: str
	msg: str


@dataclass(init=False)
class Member:
	login: str
	can_publish_tl: bool
	password: str
	email: str
	memo: str
	created: datetime.datetime
	is_staff: bool
	can_create: bool
	avatar: str
	uuid: str
	is_teacher: bool
	joined: datetime.datetime
	oldpassword: str
	is_partner: bool
	show_beta: str
	lastLogin: str
	city: str
	is_admin: bool
	is_confirmed: bool
	name: str
	last_login: str

	_form = dict(name="form1")

	_field = {
		'name': 'fieldname',
		'type': 'text',
		'label': 'Field Name',
		'hint': 'A hint about this field'
	}





foo = dict(name="Testform", fields=[1,2,3])



