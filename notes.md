
Europeana APIs
https://pro.europeana.eu/page/apis

https://pro.europeana.eu/page/iiif
https://pro.europeana.eu/page/annotations
https://pro.europeana.eu/page/sparql
https://pro.europeana.eu/page/harvesting-and-downloads
https://pro.europeana.eu/page/entity
https://pro.europeana.eu/page/search
https://pro.europeana.eu/page/record

upgrade vuejs
https://github.com/vuejs/vue/blob/main/CHANGELOG.md#270-2022-07-01

package.json info
https://classic.yarnpkg.com/lang/en/docs/package-json/

nvm install
https://github.com/nvm-sh/nvm

migrate to vite
https://vueschool.io/articles/vuejs-tutorials/how-to-migrate-from-vue-cli-to-vite/
https://medium.com/nerd-for-tech/from-vue-cli-to-vitejs-648d2f5e031d

https://github.com/vitejs/vite/issues/305
