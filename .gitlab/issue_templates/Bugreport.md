# A bug 

We categorize bugs from the user's perspective and they can have two flavours:
1. **Visual**. A user can complete the chosen function, but something looks wrong with the application. For example, when in the Activity Builder items that shift from position when scrolling.
2. **Functional**. A functional bug means that the program does not work as intended. For example, a user clicks the Save button, but the data is not saved. Or an Index Card is duplicated 3 times instead of 1.

More inforamtion
- [How to add an issue](https://gitlab.com/webtic/Historiana/-/wikis/How-to-add-an-issue)
- [Bug, feature or frontend](https://gitlab.com/webtic/Historiana/-/wikis/Bug,-feature-or-frontend)
- [Structuring the site for testing](https://gitlab.com/webtic/Historiana/-/wikis/Structuring-the-site-for-testing)

## Type
_Is it visual or functional or both?_

## URL
https://test.historiana.eu/...

## Screenshot
_Attach ‘application crash’ screen shot.. IF any_

## Error message in console
_in Chrome: Display > Develope > Javascript Console_

## Environment 
_Describe your platform and OS._

## Description
_Describe what you wanted to achieve and what happens._

## Expected result 
_Describe what interaction you do (e.g clicking save button) and what should happen then._

## Steps To Reproduce
1. ...
2. ...
3. ...

## Additional thoughts
_If any, otherwise leave empty._
