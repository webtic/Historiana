
import uuid
import json
from collections import Mapping, Set, Sequence

with open("ww1_narrative.json") as d:
  data = json.load(d)

string_types = (str, unicode) if str is bytes else (str, bytes)
iteritems = lambda mapping: getattr(mapping, 'iteritems', mapping.items)()

def objwalk(obj, path=(), memo=None):
  if memo is None:
      memo = set()
  if isinstance(obj, Mapping):
      if id(obj) not in memo:
          memo.add(id(obj))
          obj['uuid'] = str(uuid.uuid4())

          for key, value in iteritems(obj):
              for child in objwalk(value, path + (key,), memo):
                  yield child
  elif isinstance(obj, (Sequence, Set)) and not isinstance(obj, string_types):
      if id(obj) not in memo:
          memo.add(id(obj))
          for index, value in enumerate(obj):
              for child in objwalk(value, path + (index,), memo):
                  yield child
  else:
      yield path, obj



for o in objwalk(data):
  print(o)


with open("ww1.json", "w") as d:
  json.dump(data, d, indent=4)

#
# for k in data:
#
# #
#    child = data[k]
#    t = type(child)
#    if t==list or t==dict:
#      for c in child:
#        print(c)


#  if not e.get('uuid'):
#    e['uuid'] = uuid.uuid4().hex