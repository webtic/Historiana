{
  uuid: "<uuid>",
	type: "narrative",
	title: "An exemplar document",
  slug: "an-examplar-document",
	teaser: "Teaser like text for cardview",
  summary: "Summary for narritive",
  acknowledgement: "Placeholder for partner and sponsor accreditation",
  icon: {
    url: "//path/to/image.jpg",
    uuid: "xxxx-yyyy-xxxxxxx",
    caption: "optional caption"
  },

	summary: [
		{
			type: "p"
			content: ["A short text representing the content."]
		}
	],

	content: [
		{
      type: "chapter",
			title: "Block Title",
      slug: "block-title",
      teaser: "Teaser like text for cardview",
      summary: "Summary for chapter",

			icon: {
				url: "//path/to/image.jpg",
				uuid: "xxxx-yyyy-xxxxxxx"
			}

      content: [
        {
          type: "p",
          text: "Bodytext for subchapter."
        },

        {
          type: "source",
          caption: "the caption of the source",
          description: "description of the source",
          asset: {
            type: "image|audio|pdf|video"
            url: "//path/to/image.jpg",
            uuid: "xxxx-yyyy-xxxxxxx"
          }
        },

        {
          type: "subchapter",
          title: "Sub Chapter Title",
          summary: "Summary for subchapter",
          content: [
            type: "p",
            text: "Bodytext for subchapter."
          ]
        }
      ]
    }
	],

  meta: {
    created: "2021-06-21",
    modified: "2021-06-23",
    owner: "<uuid>",
    authors: ["<uuid>", "<uuid>"],
    partners: ["<uuid>", "<uuid>"],
    tags: [],
    license: "<uuid>"

  }
}