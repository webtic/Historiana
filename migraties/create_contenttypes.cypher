create (:ContentType {name: "Image", icon: "image", uuid: "59c6a698-deb0-4b70-9e77-3435bbb46032"});
create (:ContentType {name: "Text", icon: "file-alt", uuid: "4cf0b1f1-ad3a-4b93-a99e-8a5cc026ccc5"});
create (:ContentType {name: "Video", icon: "video", uuid: "05cf4d68-64c3-423e-b265-8c156de3561a"});
create (:ContentType {name: "Sound", icon: "volume", uuid: "d3cee554-4f1b-4299-93c4-e2fc6f197adf"}); 
