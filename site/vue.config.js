// determine hostname and set API url appropriately

console.log("\x1b[32m")
console.log("#########################################")

var os = require("os")
var hostname = os.hostname()

console.log("\x1b[32mhost: ", hostname)

switch (hostname) {
  case "atom.webtic.net":
  case "atom.local":
  case "Niques-iMac.local":
  case "Niqs-iMac.local":
    API_HOST = "http://10.10.10.42:9000"
    break

  // change 'something' to your local hostname
  case "something":
    API_HOST = "http://va-fighters.historiana.eu:9000"
    break

  default:
    API_HOST = "https://test.historiana.eu"
    break
}

console.log(`Setting API to -> ${API_HOST}`)
console.log("#########################################")
console.log("\x1b[0m")

// options documented at https://github.com/webpack-contrib/webpack-bundle-analyzer
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin

module.exports = {

  //for development on iOS Safari
  chainWebpack: config => {
    if (process.env.NODE_ENV === 'development') {
      config
        .output
        .filename('[name].[hash].js')
        .end()
    }
  },

  runtimeCompiler: true,


  // configureWebpack: {
  //   plugins: [new BundleAnalyzerPlugin({ openAnalyzer: false })]
  // },

  devServer: {
    //contentBase: path.join(__dirname, 'dist'),

    proxy: {
      "/api/admin": {
        target: `${API_HOST}`,
        changeOrigin: true,
        pathRewrite: {
          "^/api": ""
        }
      },
      "/api": {
        target: `${API_HOST}/api`,
        changeOrigin: true,
        pathRewrite: {
          "^/api": ""
        }
      },
      "/objects": {
        target: `${API_HOST}/objects`,
        changeOrigin: true,
        pathRewrite: {
          "^/objects": ""
        }
      },
      "/uploads": {
        target: `${API_HOST}/uploads`,
        changeOrigin: true,
        pathRewrite: {
          "^/uploads": ""
        }
      },
      "/assets": {
        target: `${API_HOST}/assets`,
        changeOrigin: true,
        pathRewrite: {
          "^/assets": ""
        }
      },
      "/static": {
        target: `${API_HOST}/static`,
        changeOrigin: true,
        pathRewrite: {
          "^/static": ""
        }
      },
      // '/static': {
      //   target: 'http://10.10.10.42:9000/static',
      //   changeOrigin: true,
      //   pathRewrite: {
      //     '^/static': ''
      //   }
      // },
      "/ua": {
        target: `${API_HOST}/ua`,
        changeOrigin: true,
        pathRewrite: {
          "^/ua": ""
        }
      }
    }
  },

  pluginOptions: {
    quasar: {}
  },
  transpileDependencies: [/[\\\/]node_modules[\\\/]quasar[\\\/]/]
}
