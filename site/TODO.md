# TODO / TOFIX

- in een Tool: Add Sources > pop-up layer Save button werkt niet
- Partners > Partner > Collection > [select sources] > Add selection to MySources werkt niet

# admin error handling / admin index

# http://localhost:8080/#/admin/Dashboard

Wanneer je nu in GenericEditor een error triggert (Roles zonder velden laden)
Dan volgt er een \$router.go('Dashboard') om weg te navigeren van de fout.
Dat werkt alleen niet echt lekker, de highlite in navigatie links blijft op Role staan.
Valt samen met de admin index; eigenlijk zou hij op het dashboard moeten openen en niet op een lege pagina

# see how we can integrate something like

http://mark-rolich.github.io/Magnifier.js/

TAGS moeten op ieder ding kunnen en niet alleen een activity
Dit speelt bijv bij Sources

"share with others" moet een preview geven en dan de keuze bieden om hem toe te voegen aan de eigen collectie

# interface elementen gelijk trekken

Overkoepelend

- dialog windows: tekst en vormgeveing buttons gelijk trekken (deels al gedaan)
- tekst aanleveren voor de Help blokken
- sharing an activity: hover over button wrong design

Activity Builder:

- Bewerken = potloodje (geen button met add sources).
  vraag kan een potloodje (edit) naar één pop-up met meerdere acties leidden?
- Uploaden? Button of?

PartnerCollection.vue heeft showMeta en renderMeta toegevoegd als prototype
kijken hoe we dit overkoepelend kunnen maken.

zelfde voor Partner.vue daar zit een proto voor de meta van een Collection

Er is in de API ook meta voor Europeana spul en een hele generieke /api/meta call die te technisch is.

Alles moet gebundeld worden tot een enkel systeem, op basis van een data-description

# TODO 2020

my-filter werkt alleen bij MyShares maar staat ook bij: My eLA en MySources,
hij ontbreekt bij MyCollections. Alledrie de plekken zouden supported moeten zijn.
Is het de moeite waard om het generiek te maken?


# SEO verhaal controleren en eventueel bijwerken
# greylisting opvangen en afhandelen in de mail module

# teaching en learning bij Partners, wat doen we daar mee?
# werkt de excel upload bij collections goed?

# narratives onderzoek
https://livingwriter.com/ is dat wat?







