# Welcome to the Historiana project

This repository contains the front-end code and documentation for the Historiana project.

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn run serve
```

### Compiles and minifies for production

```
yarn run build
```

For more information please visit the [developer site](https://historiana.dev) or check the docs/ folder.

<!--### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).-->
