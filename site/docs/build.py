

# https://gist.github.com/jiffyclub/5015986



import sys
import os.path
import markdown
import glob
from jinja2 import Environment, PackageLoader, select_autoescape


def parse_args(args=None):
    d = 'Make a complete, styled HTML document from a Markdown file.'
    parser = argparse.ArgumentParser(description=d)
    parser.add_argument('mdfile', type=argparse.FileType('r'), nargs='?',
                        default=sys.stdin,
                        help='File to convert. Defaults to stdin.')
    parser.add_argument('-o', '--out', type=argparse.FileType('w'),
                        default=sys.stdout,
                        help='Output file name. Defaults to stdout.')
    return parser.parse_args(args)


if __name__ == '__main__':

    env = Environment(
        loader=PackageLoader('build', 'templates'),
        autoescape=select_autoescape(['html', 'xml'])
    )

    template = env.get_template('base.html.jinja2')
    extensions = ['extra', 'smarty', 'attr_list', 'codehilite']

    print("Generating HTML for Historiana Developer site...")

    for md_file in glob.glob('content/*.md'):
        print(f'-> {md_file}')
        base = os.path.splitext(os.path.basename(md_file))[0]
        with open(md_file) as file_:
            md = file_.read()
            html = markdown.markdown(md, extensions=extensions, output_format='html5')
            doc = template.render(content=html)

            with open(f'{base}.html','w') as out:
                out.write(doc)



    print("-> homepage")
    homepage = env.get_template('homepage.html.jinja2')
    doc = homepage.render(content='')
    with open('index.html','w') as out:
        out.write(doc)


    print('DONE')


