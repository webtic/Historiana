from gitignore_parser import parse_gitignore
matches = parse_gitignore('/Users/paulj/Projects/Historiana/development/site/.gitignore')


import os

root = '/Users/paulj/Projects/Historiana/development/site/src'

for path, dirs, files in os.walk(root, topdown=True):
	print("** ", path.replace(root,''))
	for name in files:
		fullname = os.path.join(path, name)
		print("fullname: ", fullname)
		if not matches(fullname):
			#print("- ", os.path.join(root, name))
			print(f"{path}/{name}")

print("dirs: ", dirs)
for name in dirs:
	#print("+ ", os.path.join(root, name))	
	print("+ ", name)