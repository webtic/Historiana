### Introduction

**The Historiana project has been around since early 2011 and is a partnership between [Euroclio](https://euroclio.eu/) and [Webtic](https://webtic.eu/). In 2019 we are opening up Historiana and are actively looking for partners to boost the speed of development and let the project grow from infant into a curious and lively 10 year old child :)**

Historiana is an online environment where teachers can create, share and use historical sources and where students can do online e-Learning Activities.

In the beginning we have been changing the technology stack a few times. Early versions were based on Django and the overall architecture was completely based on server side scripting. When more data needed to be interconnected, the relational model was swapped with Graph model as we ran into performance issues when more and more items were related.

At this moment the site is split in a VueJS based front-end and a REST API based back-end written in Python using Sanic. Neo4J and the filesystem are used as the datastores. Hosting is done on virtual machines; production, testing and development each have their own VM. Webtic does the management of these servers.

Frontend developers can choose to just run the frontend part on their local machine and communicate with the API on the remote server. This is the recommended method when no write operations are performed via the API.

When development work includes writing (experimental) data to the database it is recommended to develop on a local instance of the server. See the [Backend](backend.html) documentation on how to get a local version of the database and API running.

<figure>
<img src="static/Historiana System Overview.svg">
<figcaption>Schematic overview of the current architecture</figcaption>
</figure>

### Server side aka Backend

Although most of the server components are operating system agnostic we are using the most recent Ubuntu Server LTS release with stock components where possible.

Serverside we are using:

- [Ubuntu Server LTS](https://www.ubuntu.com/download/server)
- [NGINX](https://nginx.org/) webserver
- [Sanic](https://sanicframework.org/) Async Python API framework
- [Python v3](https://docs.python.org/3/) Python 3 general coding
- [Py2Neo](https://py2neo.org/v4/) Glue between code and data
- [Neo4J](https://neo4j.com/) Graph data store

All code is kept in a Gitlab [repository](https://gitlab.com/webtic/Historiana/tree/develop) and will be public May 2019.

Currently there is no Python package management, we will be implemention [pipenv](https://docs.pipenv.org/) later in 2019.

See the [backend](/backend.html) section for details.

### Client side aka Frontend

The current release is our first major step towards a Javascript based SPA. It has been quite a learning path when the back-end server side is the place were you have the most experience :)

For various reasons, where easy to grasp and being modern were major incitements, we picked the [Quasar](https://quasar.dev/) framework to build upon.

Within Vue we are using several third party libraries; see the [Frontend](/frontend.html) section for details.

---

For more information mail us at [developer@historiana.eu](mailto:developer@historiana.eu)
