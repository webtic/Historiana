<ul class="submenu">
    <li><a href="frontend.html" >Introduction</a></li>
    <li class="active"><a href="activity_builder.html">Activity Builder</a></li>
    <li><a href="build_a_block.html">Building a Block</a></li>

    <li><a href="design.html">Design</a></li>
    <li><a href="frontend_components.html" >VUE components</a></li>

</ul>
<br clear="all">
<br clear="all">

## Activity Builder

### Introduction

**Building Blocks are the ‘tools’ with which the educator can build an e-Learning Activity. Examples are a simple text, a question or an embed block. They’re the core blocks that can ‘connect’ the ‘historical thinking’ blocks together. Examples are the Sorting, the Prioritising and the Analysing Tool. Every Block contains once placed in the e-Learning Activity sequence line contains basic functionalities delete and edit. Clicking the edit icon opens the Activity Builder and leads to the Building Block. **

### Developing a Building Block

- Historiana is a based on a custom CMS build with the Neo4J Graph database.
- The front- and back-end are two separate projects.
- The front-end communicates with the back-end via a REST API interface which is developed in Python using Sanic.

#### Requirements for developing a Building Block

The most important requirement for external webdevelopers is that they know Javascript and HTML. All interaction within one single Building Block should be developed as part of the tool.

Tech specs

- Database: Neo4
- Backend: Python 3
- Frontend: Javascript (ES6), VueJS (a progressive javascript framework), Quasar

Links

- [https://neo4j.com](https://neo4j.com)
- [https://www.python.org](https://www.python.org)
- [https://vuejs.org](https://vuejs.org)
- [https://quasar.dev/](https://quasar.dev/)

### The anatomy of an Building Block frontend

The white area in the middle, the canvas, is developed by the external party. The orange buttons in the upper left corner are used to add interaction (Add images, Add background, etc.) to a Building Block. There are Building Blocks which work with multiple images or as in the Analysing Block that is based on one images where annotations can be added.

> Beware that the Builder is transitioning to a more conclusive interface. As soon as they are implemented in the code this section will be updated.

##### Page navigation

![Breadcrumbs and sidenav](../static/media/Visualdesign_Outsidecanvas.png)

##### Sizes

We aim for creating a more responsive site. The present maximal width is based on the idea that Activity Builder and eLearning Tools should be usable on a tablet and not on a mobile. In this screendump you also see the future plan for creating a new navigational layout. Pagination and next/previous on top, overarching buttons (add sources, assigments) on the right, interaction buttons on the canvas.

![](../static/media/Visualdesign_Canvassize.png)

On the wishlist:

![](../static/media/Read.png)

![](../static/media/How.png)

##### Clicking an action button (Add sources, Choose Background, ...) [Teacherview]

![](../static/media/Visualdesign_Actionbar.png)

##### Clicking 'Add source' [Teacherview]

![](../static/media/Visualdesign_Clickaddimage.png)

##### ...opens an image layer where a user can select a source

![](../static/media/Visualdesign_Opensource.png)

##### Clicking the magnifier icon on a source placed on the canvas...

![](../static/media/Visualdesign_Clicksource.png)

###### ...opens an image layer with information on the source

![](../static/media/Visualdesign_Addimages.png)

### The anatomy of an Building Block backend

A Building Block that is part of the e-Activity Builder is written in Vue.js.

**Questions to be documented**

- How will it work with getting images from the database?
- How and where should backgrounds be embedded?
- Who is responsible for what?

### The best workflow

We learned that building a ‘tool’ in javascript disconnected from Historiana gave us the opportunity to focus
solely on the concept. User testing and testing was simple and straight forward. As soon as the concept was
finalised Paul and the external developer started working together to find the best way to embed the code
within the Historiana environment.

### Lessons learned so far

The BuildingBlock environment was the first complex Vue implementation we did. We had quite some existing
code from our prototyping stage which we wanted to re-use. The fact that the code was jQuery based added an
additional layer of complexity. From all the hoops and loops we went through we have learned quite a few things. Later when most of the dust was settled and we had more insights we created a new block from scratch. The way that was done was a lot more efficient.

**Later we will work out an example based on the code of that block.**

### Things to note:

- Historiana is a Vue project using the standard Vue environment; this means webpack, babel and friends. Write
  ES6 and the setup will transpile to older browsers.
- Usage of JQuery is not preferred and not allowed for future extensions. If still used don’t use it to keep state, the DOM is just a representation of the truth, not a source.
- Your code can be “isolated” into a module with its internal state, just make sure your block can be initialised and report back the state via a JSON data structure.
- Vue is just Javascript ;-)
- If needed you can call the API of Historiana directly, if you need more functionality we want to know.
- Use vanilla Javascript if you can; remember a lot of jQuery usage was because the browser-world was a
  completely different scene back in the day. See http://youmightnotneedjquery.com/ for inspiration.
- Vue is your friend ;-) It is very simple to adapt in a simple project by just including it via CDN. Unless you have an existing code base try to setup your project with Vue. It will be simpler to integrate later and it gives a lot of functionality you can build on.
