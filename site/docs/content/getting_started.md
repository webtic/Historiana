## Getting started

Here we document the steps to get you start coding on the Historiana project. 

###Overview
The Historiana project is a website which consists of a frontend and a backend which are communicating via a REST based API.

Developers can choose to just run the frontend part on their local machine and communicate with the API on the remote server. This is the recommended method when no write operations are performed via the API.

When development work includes writing (experimental) data to the database it is recommended to develop on a local instance of the server. 

------
### Frontend development
The Historiana web-client is a [VueJS](https://vuejs.org) application which is published on Github. 

It is build using the vue-cli environment and some knowledge of the environment this builds on is required. Currently development is done on MacOS and Linux. 

To to get a local copy:

```git clone https://gitlab.com/webtic/Historiana```

All development is done in the **develop** branch!

Before you can actually run a local instance some tools need to be installed.

- `cd Historiana`

- Install **Yarn** using the [instructions](https://yarnpkg.com/en/docs/install) 
- Install **npm** : `sudo apt install npm`

- Initialise the environment: `yarn install`
		
- run the frontend: `yarn dev`

Your browser should open displaying the Historiana homepage.

**This document is a work in progress; currently most is not tested in a clean environment.**




------
### Backend development
*The backend is currently in a separate repository, this will change soon.*

The backend consists of an instance of the Neo4J graph database and a process which exposes a REST based API to both the graph and filesystem for storage of files and information.

This API  is implemented in Python 3 and uses Sanic as an async framework to implement the REST API. The javascript frontend communicates directly with this API.

It functions as a smart bridge between server and client and glues both the Graph database, File System and other resources together into a single resource used by the Frontend.

#### Installing Neo4J

For development purposes a free Enterprise version of Neo4J can be downloaded from their site. Although there are several server versions an installation of [Neo4J Desktop](https://neo4j.com/download/){:target="_blank"} is recommended. Their Desktop product is a wrapper around the server and eases the management of the database in a development environment.

To install it just download the version for your platform and run the installer.

Download the [Historiana Graph](/downloads/graph.db.tgz) 



------
### Documentation development
The documentation which you are reading now and is published on [Historiana Dev](https://historiana.dev){:target="blank"} and this directory /docs contains all sources to build the documentation site.

See docs/README.md for all the details; basically there is Markdown documentation in content/ which is combined with the documentation retrieved from the actual sourcecode with the *gen-vue.py* script.

For formatting code we use [Pygments](http://pygments.org/) and the additional [VueLexer](https://github.com/testdrivenio/vue-lexer)

The documentation site can be used locally:

```
cd docs
python3 -m http.server 8000

```

Then go to [http://localhost:8000](http://localhost:8000) to view the local instance.


The *push* script sends the local version to the remote https://historiana.dev/ site.
It boldly assumes there are valid ssh credentials.
