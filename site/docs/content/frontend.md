<ul class="submenu">
    <li class="active"><a href="frontend.html" >Introduction</a></li>
    <li><a href="activity_builder.html">Activity Builder</a></li>
    <li><a href="build_a_block.html">Building a Block</a></li>
    <li><a href="design.html">Design</a></li>
    <li><a href="frontend_components.html" >VUE components</a></li>

</ul>
<br clear="all">
<br clear="all">

# Frontend Documentation

The Frontend is at [https://historiana.eu](https://historiana.eu) and in the current iteration it is a VueJS single page application (SPA) using the [Vue Router](https://router.vuejs.org/) for URL routing and [Vuex](https://vuex.vuejs.org/) for state management.

![](/static/homepage.jpg)

## Internals and vision

On the CCS site of things we have removed the dependencies on Foundation and Elements.io and now use [Quasar](https://quasar.dev/).
Within the e-Activity we move away from jQuery.
For the backend in some parts ElementIO is used, other parts are prototyped using Vuetify. We need to settle on one specific library. We should re-evaluate the current VueJS UI library scene.

## Source tree overview

The Frontend lives in the Gitlab repository at [https://gitlab.com/webtic/Historiana](https://gitlab.com/webtic/Historiana).

All of the VueJS code is in the `src/` folder of this repo.

This tree still needs cleaning up; some materials are not used anymore and should be removed to clear the clutter.

Main elements are:

| file - folder          | description                                                  |
| ---------------------- | ------------------------------------------------------------ |
| admin/                 | Vue components which are solely used in the admin interface  |
| App.vue                | top level component, all others are childs in this one.      |
| assets/                | to be removed?                                               |
| components/            | Most Vue components which are actually a "page"              |
| components/generic     | remove?                                                      |
| config/                | configuration for the Vue Router and ActivityBuilder         |
| custom.scss            | used in Header.vue; contains new CSS which is not Foundation |
| docs/                  | older docs to be repurposed                                  |
| eabuilder/             | all files for the e-activity builder                         |
| element-variables.scss | settings for ElementIO UI Library                            |
| historiana.scss        | global settings and more (also imports element-ui? css)      |
| Historiana.vue         | Main header/content/footer layout                            |
| indexes/               | remove?                                                      |
| main.js                | main entry point, Vue is started here                        |
| mixins/                | globally used functionality, more needs to be moved here :)  |
| pages/                 | User facing pages should be here, currently it is a mix      |
| store/                 | Vuex definitions and functionality, the core of the App      |
| tags/                  | globally used Vue components, needs a better name :)         |
| tic.js                 | global Javascript functions                                  |
| views/                 | Admin pages, conflicts with admin/                           |

## Rename and cleanup

Parts of this source tree need a major cleanup. Naming of components/pages/tags need to be better. We should read the style guide and implement accordingly.

## Component level documentation

We are implementing the documentation on component level [here](frontend_components.html)

## Full tree of the current contents

This is a snapshot of the full src/ tree:

```
├── App.vue
├── App.vue.dist
├── Historiana.vue
├── _settings.scss
├── admin
│   ├── hiCheckbox.vue
│   ├── hiCopyright.vue
│   ├── hiDuration.vue
│   ├── hiStatus.vue
│   ├── hiWhyCheckbox.vue
│   ├── selectCopyright.vue
│   └── selectPartner.vue
├── assets
│   ├── font-awesome
│   │   ├── HELP-US-OUT.txt
│   │   ├── css
│   │   │   ├── font-awesome.css
│   │   │   └── font-awesome.min.css
│   │   ├── fonts
│   │   │   ├── FontAwesome.otf
│   │   │   ├── fontawesome-webfont.eot
│   │   │   ├── fontawesome-webfont.svg
│   │   │   ├── fontawesome-webfont.ttf
│   │   │   ├── fontawesome-webfont.woff
│   │   │   └── fontawesome-webfont.woff2
│   │   ├── less
│   │   │   ├── animated.less
│   │   │   ├── bordered-pulled.less
│   │   │   ├── core.less
│   │   │   ├── fixed-width.less
│   │   │   ├── font-awesome.less
│   │   │   ├── icons.less
│   │   │   ├── larger.less
│   │   │   ├── list.less
│   │   │   ├── mixins.less
│   │   │   ├── path.less
│   │   │   ├── rotated-flipped.less
│   │   │   ├── screen-reader.less
│   │   │   ├── stacked.less
│   │   │   └── variables.less
│   │   └── scss
│   │       ├── _animated.scss
│   │       ├── _bordered-pulled.scss
│   │       ├── _core.scss
│   │       ├── _fixed-width.scss
│   │       ├── _icons.scss
│   │       ├── _larger.scss
│   │       ├── _list.scss
│   │       ├── _mixins.scss
│   │       ├── _path.scss
│   │       ├── _rotated-flipped.scss
│   │       ├── _screen-reader.scss
│   │       ├── _stacked.scss
│   │       ├── _variables.scss
│   │       └── font-awesome.scss
│   ├── logo.png
│   ├── sequencing
│   │   ├── Circles01.png
│   │   ├── Circles01.svg
│   │   ├── bgapp_Circles.svg
│   │   ├── bgapp_Lineair.svg
│   │   ├── bgapp_Timeline.svg
│   │   ├── logo.png
│   │   └── versailles_1430693c.jpg
│   └── sorting
│       ├── Circles01.png
│       ├── Circles01.svg
│       ├── bgapp_Circles.svg
│       ├── bgapp_Lineair.svg
│       ├── bgapp_Timeline.svg
│       ├── logo.png
│       └── versailles_1430693c.jpg
├── components
│   ├── ABPanel.vue
│   ├── Body.vue
│   ├── Builder.vue
│   ├── CountrySelect.vue
│   ├── Footer.vue
│   ├── Header.vue
│   ├── LearningActivities.vue
│   ├── MyActivities.vue
│   ├── MyActivityLog.vue
│   ├── MyHistoriana.vue
│   ├── MyProfile.vue
│   ├── MyResources.vue
│   ├── MyShares.vue
│   ├── MySources.vue
│   ├── MyTags.vue
│   ├── SASEuropeana.vue
│   ├── SASExplore.vue
│   ├── SASExploreSlug.vue
│   ├── SASPartnerView.vue
│   ├── SASPartners.vue
│   ├── SelectSources.vue
│   ├── Upload.vue
│   ├── Welcome.vue
│   ├── generic
│   ├── hiCopyright.vue
│   ├── hiPartnerSelect.vue
│   └── histRedirect.vue
├── config
│   ├── blocktypes.js
│   ├── routes.js
│   └── routes.js.orig
├── custom.scss
├── docs
│   ├── README.md
│   ├── _coverpage.md
│   ├── _media
│   │   ├── 1280px--Fontanellen-_Ephemeral_Art_Construction_in_front_of_Lund_University_Main_Building.jpg
│   │   ├── 800px--Fontanellen-_Ephemeral_Art_Construction_in_front_of_Lund_University_Main_Building.jpg
│   │   ├── Confirm.png
│   │   ├── Gallery.png
│   │   ├── H.svg
│   │   ├── Index-Card.png
│   │   ├── Index-Cards.png
│   │   ├── Logo-H.svg
│   │   ├── background.png
│   │   ├── favicon.ico
│   │   └── icon.svg
│   ├── _sidebar.md
│   ├── components
│   │   ├── confirm.md
│   │   ├── gallery.md
│   │   └── index-cards.md
│   ├── gallery.md
│   ├── guide
│   │   ├── named-routes.md
│   │   └── tags.md
│   ├── index.html
│   ├── projectstructure.md
│   ├── research.md
│   ├── seethisautomatically.md
│   └── testing.md
├── element-variables.scss
├── historiana.scss
├── indexes
│   ├── index.dev.html
│   ├── index.html
│   ├── index.live.html
│   ├── index.local.html
│   └── nobeta-index.html
├── main.js
├── main.js.dist
├── mixins
│   └── clipboard.mixin.js
├── node_modules
├── notes.md
├── package-lock.json
├── pages
│   ├── About.vue
│   ├── BB-analysing.vue
│   ├── BB-analysing.vue.old
│   ├── BB-embed.vue
│   ├── BB-prioritizing.vue
│   ├── BB-question.vue
│   ├── BB-sorting.vue
│   ├── BB-text.vue
│   ├── BlockEditor.vue
│   ├── CaseStudy.vue
│   ├── Experiment.vue
│   ├── HCbySlug.vue
│   ├── HCbySlug.vue.bak
│   ├── HistoricalContent.vue
│   ├── Homepage.vue
│   ├── MediaLibrary.vue
│   ├── ResetPassword.vue
│   ├── Status.vue
│   ├── TeachingLearning.vue
│   ├── TestPage.vue
│   ├── analysing.js
│   ├── analysing.js.1
│   ├── elaRedirect.vue
│   └── elaStudentView.vue
├── plugins
│   └── axios.js
├── store
│   ├── aa.js
│   ├── index.js
│   └── modules
│       ├── admin.js
│       ├── eactivity.js
│       ├── feedback.js
│       └── user.js
├── style
│   └── zurb.css
├── styles.scss
├── tags
│   ├── Actionbar.vue
│   ├── Alert.vue
│   ├── Card.vue
│   ├── Confirm.vue
│   ├── Editable.vue
│   ├── Gallery.vue
│   ├── GenericMemberTags.vue
│   ├── HFilter.vue
│   ├── HFilter.vue.old
│   ├── LanguageSelect.vue
│   ├── MemberTags.vue
│   ├── Modal.vue
│   ├── MyLabel.vue
│   ├── OldGallery.vue
│   └── navBuildingBlock.vue
├── tic.js
└── views
    └── admin
        ├── Admin.vue
        ├── Collections.vue
        ├── Dashboard.vue
        ├── GenericEditor.vue
        ├── LearningActivityEditor.vue
        ├── ModEditor.vue
        ├── ModEditorChapter.vue
        ├── ModEditorIndex.vue
        ├── Partners.vue
        ├── Roles.vue
        ├── Style.scss
        ├── Users.vue
        └── admin.scss

```
