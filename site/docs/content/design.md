<ul class="submenu">
    <li><a href="frontend.html" >Introduction</a></li>
    <li><a href="activity_builder.html">Activity Builder</a></li>
    <li><a href="build_a_block.html">Building a Block</a></li>
    <li class="active"><a href="design.html">Design</a></li>
    <li><a href="frontend_components.html" >VUE components</a></li>
</ul>
<br clear="all">
<br clear="all">

## Activity Builder

## Design

This is a document in development. In the future

### Colors

There are 16 colors, 4 grays, black and white that can be cosen from.

<div class="row">
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #cc0000;background-color:#eb0000;">
      <div class="color" style="background-color:#ff0000;"></div>
    </div>
  </div>
  <div class="caption"><h3>#ff0000</h3>HEX: #ff0000<br />RGB: 255, 0, 0</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #cc5200;background-color:#eb5e00;">
      <div class="color" style="background-color:#ff6600;"></div>
    </div>
  </div>
  <div class="caption"><h3>#ff6600</h3>HEX: #ff6600<br />RGB: 255, 102, 0</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #bb9300;background-color:#daab00;">
      <div class="color" style="background-color:#eebb00;"></div>
    </div>
  </div>
  <div class="caption"><h3>#eebb00</h3>HEX: #eebb00<br />RGB: 238, 187, 0</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #bbbb00;background-color:#dada00;">
      <div class="color" style="background-color:#eeee00;"></div>
    </div>
  </div>
  <div class="caption"><h3>#eeee00</h3>HEX: #eeee00<br />RGB: 238, 238, 0</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #93bb00;background-color:#abda00;">
      <div class="color" style="background-color:#bbee00;"></div>
    </div>
  </div>
  <div class="caption"><h3>#bbee00</h3>HEX: #bbee00<br />RGB: 187, 238, 0</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #3e8800;background-color:#4ca700;">
      <div class="color" style="background-color:#55bb00;"></div>
    </div>
  </div>
  <div class="caption"><h3>#55bb00</h3>HEX: #55bb00<br />RGB: 85, 187, 0</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #007730;background-color:#00963c;">
      <div class="color" style="background-color:#00aa44;"></div>
    </div>
  </div>
  <div class="caption"><h3>#00aa44</h3>HEX: #00aa44<br />RGB: 0, 170, 68</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #00775f;background-color:#009678;">
      <div class="color" style="background-color:#00aa88;"></div>
    </div>
  </div>
  <div class="caption"><h3>#00aa88</h3>HEX: #00aa88<br />RGB: 0, 170, 136</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #007c88;background-color:#0097a7;">
      <div class="color" style="background-color:#00aabb;"></div>
    </div>
  </div>
  <div class="caption"><h3>#00aabb</h3>HEX: #00aabb<br />RGB: 0, 170, 187</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #0086bb;background-color:#009bda;">
      <div class="color" style="background-color:#00aaee;"></div>
    </div>
  </div>
  <div class="caption"><h3>#00aaee</h3>HEX: #00aaee<br />RGB: 0, 170, 238</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #004d99;background-color:#005cb8;">
      <div class="color" style="background-color:#0066cc;"></div>
    </div>
  </div>
  <div class="caption"><h3>#0066cc</h3>HEX: #0066cc<br />RGB: 0, 102, 204</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #0c2588;background-color:#0f2da7;">
      <div class="color" style="background-color:#1133bb;"></div>
    </div>
  </div>
  <div class="caption"><h3>#1133bb</h3>HEX: #1133bb<br />RGB: 17, 51, 187</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #200b55;background-color:#2b0e74;">
      <div class="color" style="background-color:#331188;"></div>
    </div>
  </div>
  <div class="caption"><h3>#331188</h3>HEX: #331188<br />RGB: 51, 17, 136</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #440a44;background-color:#630e63;">
      <div class="color" style="background-color:#771177;"></div>
    </div>
  </div>
  <div class="caption"><h3>#771177</h3>HEX: #771177<br />RGB: 119, 17, 119</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #77006b;background-color:#960087;">
      <div class="color" style="background-color:#aa0099;"></div>
    </div>
  </div>
  <div class="caption"><h3>#aa0099</h3>HEX: #aa0099<br />RGB: 170, 0, 153</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #aa005c;background-color:#c9006c;">
      <div class="color" style="background-color:#dd0077;"></div>
    </div>
  </div>
  <div class="caption"><h3>#dd0077</h3>HEX: #dd0077<br />RGB: 221, 0, 119</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #cccccc;background-color:#ebebeb;">
      <div class="color" style="background-color:#ffffff;"></div>
    </div>
  </div>
  <div class="caption"><h3>#ffffff</h3>HEX: #ffffff<br />RGB: 255, 255, 255</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #bbbbbb;background-color:#dadada;">
      <div class="color" style="background-color:#eeeeee;"></div>
    </div>
  </div>
  <div class="caption"><h3>#eeeeee</h3>HEX: #eeeeee<br />RGB: 238, 238, 238</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #999999;background-color:#b8b8b8;">
      <div class="color" style="background-color:#cccccc;"></div>
    </div>
  </div>
  <div class="caption"><h3>#cccccc</h3>HEX: #cccccc<br />RGB: 204, 204, 204</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #666666;background-color:#858585;">
      <div class="color" style="background-color:#999999;"></div>
    </div>
  </div>
  <div class="caption"><h3>#999999</h3>HEX: #999999<br />RGB: 153, 153, 153</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #333333;background-color:#525252;">
      <div class="color" style="background-color:#666666;"></div>
    </div>
  </div>
  <div class="caption"><h3>#666666</h3>HEX: #666666<br />RGB: 102, 102, 102</div>
</div>
<div class="swatch">
  <div class="outer">
    <div class="shadow" style="border:1px solid #000000;background-color:#000000;">
      <div class="color" style="background-color:#000000;"></div>
    </div>
  </div>
  <div class="caption"><h3>#000000</h3>HEX: #000000<br />RGB: 0, 0, 0</div>
</div>

</div>

#### Typography online

- Headers: Ubuntu
- Body text: Open Sans

Both font are already embedded in Historiana.
Use the following CSS rules to specify these families:

font-family: 'Open Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;

**Ubuntu**

- [https://fonts.google.com/specimen/Ubuntu](https://fonts.google.com/specimen/Ubuntu)
- [https://design.ubuntu.com/font/](https://design.ubuntu.com/font/)

** Open Sans**

[https://fonts.google.com/specimen/Open+Sans](https://fonts.google.com/specimen/Open+Sans)

### Icons

Icons that are needed for usage in a tool can be chosen from the Pro version from FontAwesome.
Only the solid and regular options are permitted. Do not use the Light, Duotone or Brands option.

- [https://fontawesome.com/](https://fontawesome.com/)

### Shape, design, placing etc. based on different functionality / actions

#### To be defined, please contact Webtic for consultation!

- Round buttons vs square buttons
- Usage of color (orange vs black vs grey etc.)
- Size of buttons
- Push buttons, shades.
- Pop-up layer different functionality (layer with info, layer with other buttons/interactions)
- Placing on the buttons on the canvas
- Placing of the buttons on a source

Consistency: now buttons that leads to meat information and bigger size of the picture are represented by an ‘i’ or a magnifier. We will be using only an ‘i’ in this case. The magnifier will be used for zooming in on images and PDFs.

…..
