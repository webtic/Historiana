## Historiana System Overview


The Historiana project is a website which consists of a frontend and a backend which are communicating via a REST based API.

Developers can choose to just run the frontend part on their local machine and communicate with the API on the remote server. This is the recommended method when no write operations are performed via the API.

When development work includes writing (experimental) data to the database it is recommended to develop on a local instance of the server. 

<img src ="static/Historiana System Overview.svg">


All code is kept in a Gitlab repository and public. 


## CLIENT


The Historiana web-client is a [VueJS](https://vuejs.org) application which is published on Github. 

It is build using the vue-cli environment and some knowledge of the environment this builds on is required. Currently development is done on MacOS and Linux. 

To to get a local copy:

```git clone https://github.com/Historiana/frontend```

Before you can actually run a local instance some tools need to be installed.

- `cd frontend`

- Install **Yarn** using the [instructions](https://yarnpkg.com/en/docs/install) 
- Install **npm** : `sudo apt install npm`

- Initialise the environment: `yarn install`
		
- run the frontend: `yarn dev`





