## Roadmap
While in the early days we shifted around databases and HTML frameworks for quite a bit we think we now have settled a stack which allows us to build the stuff we need. And we are looking for help.

The Historiana project is a collection of concepts, some of them are "done" while others are at their infancy. Currently the navigation of the site shows these elements:

- Historical Content
	- Key Moment
	- Unit
	- Source collection

- Teaching and Learning
	- Learning Activity
	- eLearning Activity

- Search
 	- Partners
	- Europeana

- e-Activity Builder

- My Historiana
 	- Profile
 	- Activities
 	- Sources
 	- Tags
 	- Shares

- Site Admin
	- Add collections
	
Most of these elements are implemented in a front/backend scheme. The actual data is stored on the server, the interaction with these concepts are implemented in VueJS.

All these elements use one or more of these implementations, more details in the specific [front](frontend.html)- and [backend](backend.html) documentation.

> We often classify content in the site between "Historical Content", "Builder" and "Sources" as there are logical seperations.

Currently we need to work on these elements:

Source collections: only way create them at the moment is to import a Powerpoint, Members also would want to create collections within My Historiana

Key moment/Unit: both use the same templates; we want to evolve to the new Text-tool

LearningActivity: better web-admin to edit/create them, remove the current over-complexity ; remodel using native Cypher. We originally used Neomodel back in the early Neo4J 2.0 days to make this. The model created is horrendous.

Builder: let 3rd parties create new blocks, document and improve the process of adding new functionalities, improve the state management which is currently buggy.

Search: Europeana API seems broken for certain results, debug and fix.
Search partners need to be integrated with a new "partners" system.


Overall we need to cleanup and revise the codebase, it shows that this was my first Vue project I ever coded. Move functionality into the store; cleanup and standardize Vue code using a sensible subset of the [styleguide](https://vuejs.org/v2/style-guide/). 




** TODO provide mapping between SITE NAV and the Actual UI elements in this doc **


### Timeline
(to be expanded ;-)

<div>
<div class="timeline-entry">
	<div class="timeline-date">December 2010</div>
	<div class="timeline-text"><b>First contact</b> Euroclio is looking for a new partner to create a first version of the Historiana vision. In a hurry as the original partner dropped out and the deadline is rapidply approaching.
	</div>
</div> 
<br clear="all">

<div class="timeline-entry">
	<div class="timeline-date">Januari 2019</div>
	<div class="timeline-text"><b>Start of OUH</b> Opening up Historiana.</div>
<div> 

</div>

