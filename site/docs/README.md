
## Historiana for Developers

This directory contains all the contents of the [Historiana for Developers site](https://historiana.dev) which is a work-in-progress to collect documentation on the Historiana project oriented towards  developers who want too collaborate in the project. 

The documentation site is hosted on [historiana.dev](https://historiana.dev) while the project itself lives on [historiana.eu](https://historiana.eu). The main repository is at [https://gitlab.com/Historiana/](Gitlab)


The documentation is split in two different parts; the [Frontend Components](/frontend_components.html) documentation is generated with *gen-vue.py* which scans all Frontend source code and retrieves documentation and meta information.

Other documentation is written in Markdown and stored in the `content/` folder. These docs are converted to HTML using *build.py* and are stored in the docs/ root.

To build the site we made a shell wrapper which runs both scripts:

```
sh make 
```

This runs *build.py* scans content/\*.md and generates the corresponding HTML files in the root using the Jinja2 code in templates/ 
Exception is index.html which is generated from templates/homepage.html; see `build.py` for details.

The *gen-vue.py* script scans all Vue code and collects comments and meta information from each file. This is rendered to HTML via the Jinja2 frontend_components.html.jinja2 template.

NOTE: Development is done in the 'development' branch of the project.

To view/work on this locally run this in the current folder:

```python3 -m http.server 8000```

Then go to [http://localhost:8000]() 


To create the HTML from the source you will need Python3 and the [Pygments](http://pygments.org/) syntax highlighter.
Additionally we install a [VueLexer](https://github.com/testdrivenio/vue-lexer) which implements the Vue syntax.

pip3 install pygments
pip3 install vue-lexer

You might want to change the default styling of the code via: 

pygmentize -S monokai -f html >static/pygment.css 

