# generate Components docs from the Vue sources

# this extracts the javascript of the <script>
# section; we really like to parse it but
# the work involved seems too much for now.
#
# see also https://github.com/Kronuz/esprima-python/tree/master/esprima
# when replacing export default { with a = { we can parse it via
# esprima.parseScript(code)
# but the esprima output lacks an easy way of getting
# the name and props values of the object

# from lxml import etree
# pageInMemory = open("Users.vue", "r").read()
# parsedPage = etree.HTML(pageInMemory)
# t = parsedPage.xpath("//script//text()")[0]
# t=t.replace("export default", "a=")
# source= esprima.parseScript(t)

import os
import yaml
import subprocess
import html

from pygments import highlight
from vue.lexer import VueLexer
from pygments.formatters import HtmlFormatter


from gitignore_parser import parse_gitignore

matches = parse_gitignore(
    "/Users/paulj/Projects/Historiana/development/site/.gitignore"
)

from jinja2 import Environment, PackageLoader, select_autoescape

# root of the sources to process
root = "/Users/paulj/Projects/Historiana/development/site/src"

# destination folder for documentation
destroot = "/Users/paulj/Projects/Historiana/development/site/docs"

process = []

for path, dirs, files in os.walk(root, topdown=True):
    relative_path = path.replace(root, "")
    if relative_path == "":
        relative_path = "/"

    for name in files:
        fullname = os.path.join(path, name)
        if not matches(fullname) and name.endswith(".vue"):

            # create destination HTML filename
            me = "/".join([relative_path, name]).replace("/", "_")
            me = me.replace(".vue", ".html")
            process.append(
                dict(
                    source=f"{path}/{name}",
                    relpath=relative_path,
                    name=name,
                    dest=f"{destroot}/components/{me}",
                    reldest=f"/components/{me}",
                )
            )

# setup Jinja2 environment
env = Environment(
    loader=PackageLoader("build", "templates"),
    autoescape=select_autoescape(["html", "xml"]),
)

template = env.get_template("frontend_components.html.jinja2")
extensions = ["extra", "smarty"]

# write the index
print("-> Generate INDEX frontend_components.html")
doc = template.render(title="Frontend Vue Components", files=process)
with open(f"frontend_components.html", "w") as out:
    out.write(doc)

for offset, file in enumerate(process):
    print(60 * "-")
    print("## Processing: ", file["name"])

    # name = file.replace(".vue", "")

    # get next/prev navigation info
    try:
        _prev = process[offset - 1]
    except:
        _prev = None

    try:
        _next = process[offset + 1]
    except:
        _next = None

    with open(file["source"], "r") as s:
        code = s.read()

        ## assume documentation is in the comment block before the <template> tag
        comment = code.split("<template>")[0]
        comment = comment.replace("<!--", "")
        comment = comment.replace("-->", "")
        print("COMMENT: ", comment)

        try:
            result = yaml.load(comment)
            print(type(result), result)

            try:
                # result['example'] = html.escape(result['example'])
                result["example"] = highlight(
                    result["example"], VueLexer(), HtmlFormatter(linenos=True)
                )
            except:
                pass

            # for (k) in result:
            #    a = result[k]
            #    print(k, type(a), result[k])

        except Exception as e:
            print(f"*** ERROR on {file['source']}: {e}")

            result = None
            pass

    # nice to have: parsing the <script> for props[]

    # grep for this file in the src/ tree
    # assume ag is installed and sourcetree is at ../src

    cmd = ["ag", "-l", "import " + file["name"].replace(".vue", ""), "../src"]
    try:
        refs = subprocess.check_output(cmd).decode()
        refs = refs.replace("../src", "")
        refs = refs.split("\n")
        refs = [
            dict(
                url=r.replace("/", "_").replace(".vue", ".html"),
                name=os.path.basename(r),
            )
            for r in refs
            if r
        ]

        # refs = [ os.path.basename(r.replace("../src","")) for r in refs.decode().split("\n") if r]
        # print(f"{file} --> {refs}")

    except Exception as e:
        # print(f"REFS: {file} | {e}")
        refs = []

    # print("-> ", refs)

    # create output filename
    doc = template.render(
        me=file["dest"].replace(root, ""),  # the current html file to generate
        title="",
        files=process,  # all the files, used for navigation on the left
        meta=result,  # comments parsed to Dict of current file
        name=file["name"].replace(".vue", ""),  # name of this file (no extension)
        prev=_prev,  # previous file, if any
        next=_next,  # next file, wraps to [0]
        refs=refs,  # all modules we found current module in
    )

    with open(file["dest"], "w") as out:
        print("WRITE ", file)
        out.write(doc)

    out.close()
