

// new eActivity module

export default {

	namespaced: true,

	state: {
		action: 'Create',
		mode: 'teacher', // teacher|student
		activity: {
			title: '',
			description: ''
		}
	},


	getters: {
		all (state) { return JSON.stringify(state) },
		activity (state) { return state.activity },
		action (state) { return state.action }

	},

	mutations: {
		SET_ACTION (state, value) {
			state.action = value
		},

		SET_ACTIVITY(state, act) {
			console.log("set activity: ", act)
			state.activity = Object.assign({}, state.activity, act)
		}

	}

}