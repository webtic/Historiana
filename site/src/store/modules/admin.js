import {API} from '../../tic'
import _ from 'lodash'


console.log("*** admin vuex")
// all things admin
const state = {
  version: '1.10',
  forms: {}

};

// ===========================================================================
const getters = {
  version: state => state.version,
  forms: state => state.forms,
  demo: (state) => (id) => {
    console.log("admin demo ", id)
    //return state.bblocks[id]
    return 1
  }
}

// ===========================================================================
const mutations = {

  iets (state, payload) {
    // set new title/description -- used in bb-prioritizing
    let edit = state.bblocks[payload.current].record.images.find(e => e.uuid===payload.uuid)
    edit.title = payload.item.title
    edit.description = payload.item.description
  },

  SET_FORMS (state, payload) {
    console.log("** SETFORM")
    state.forms = payload
  }

}


// ===========================================================================
const actions = {

  init: (ctx, vm) => {
    console.log("H-admin INIT")
    API.get('/admin/form-meta')
    .then((resp) => {
      ctx.commit('SET_FORMS', resp.data.data)
    })
    .catch(e => {
      console.log("ERROR: ", e)
      alert(`ERROR: ${e}`)
    })
  },

  saveStudentAnswers: (ctx, vm) => {
    console.log("save students answers for ", ctx.state.sa_uuid)
    console.log("store these: ", JSON.stringify(vm.answers))
    API.post('/save-student-answers', 
      {
        sa: ctx.state.sa_uuid,
        answers: vm.answers
      })
    .then(resp => {
      console.log("XXXXX: ", resp)
      // ctx.commit('editUrl', params.route.path)

      if (resp.data.isError) {
        vm.$message({
          type: 'error',
          message: `Error submitting answers<br>${resp.data.errorCode}`, 
          dangerouslyUseHTMLString: true,
          showClose: true,
          duration: 0     // keep error on screen
        })
      } else {
        vm.$message({
          type: 'success',
          message: 'Answers submitted'  
        })
      }
    })
    .catch((resp) => {
      console.log("CATCH ERROR resp: ", resp)
      console.debug("DEBUG: ", resp)
      vm.$message({
        type: 'error',
        message: `No answers submitted<br><br>${resp}`,
        dangerouslyUseHTMLString: true
      })       
    })
  }
}

export default {
  namespaced: true,
  actions,
  getters,
  state,
  mutations
};
