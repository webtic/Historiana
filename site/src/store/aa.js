// user and profile need merging
const store = new Vuex.Store({
  state: {
    feedback: {
      msg: '',
      status: 'ok'
    },
    count: 0,
    loggedIn: false,
    displayname: 'Anonymous',
    user: {},
    userTags: {},       // all user defined tags
    profile: {},
    activity: {
      next: dummy,
      prev: dummy,
      currentBlock: null,
      is_placeholder: true,
      image: '',
      title: '',
      description: '',
      tags: [],
      blockOrder: [],
      bblocks: {},
      from_store: true,
      items: 0
    }, 
  },

  getters: {
    getTags: state => {
      // return an array of true Tags.uuids for the current activity
      // return state.activity.tags.map( function (o) {
      //   return state.profile.user.tags[o].uuid
      // })
      var uuids = [];
      for (var o in state.activity.tags) {
        uuids.push(state.activity.tags[o].uuid);
      }
      return uuids
    },

    getActiveTags: state => {
      return state.activity.tags
    },

    getActivity: state => {
      return state.activity.uuid
    },

    getLoginState: state => {
      console.log("return getLoginState:", state.loggedIn)
      return state.loggedIn
    },
  },

  mutations: {

    removeTag (state, tagId) {
      console.log('mutate remove tag: ', tagId)
      state.activity.tags = state.activity.tags.filter(function(e) { return e.uuid !== tagId; });

    },

    addBlock (state, block) {
      state.activity.bblocks[block.id] = block
      state.activity.items = state.activity.items + 1
      console.debug(state.activity.bblocks)
    },

    deleteBlock (state, blockId) {
      console.log('deleteBlock',blockId)
      // remove from current blocks
      delete state.activity.bblocks[blockId]
      // and remove from order helper
      state.activity.blockOrder.splice(state.activity.blockOrder.indexOf(blockId), 1)
      state.activity.items = state.activity.items - 1
    },

    setBlockOrder (state, currentOrder) {
      state.activity.blockOrder = currentOrder
    },

    increment (state) {
      state.count++
    },

    logout (state) {
      // reset all state
      state.loggedIn = false
      state.user = {}
      state.activity = {
        next: dummy,
        prev: dummy,
        currentBlock: null,
        is_placeholder: true,
        image: '',
        title: '',
        description: '',
        tags: [],
        blockOrder: [],
        bblocks: {},
        from_store: true,
        items: 0
      } 
    },

    zapBuilder (state) {
      // reset e-Activity builder
      state.activity = {
        next: null,
        prev: null,
        currentBlock: null,
        is_placeholder: true,
        image: '',
        title: '',
        description: '',
        tags: [],
        blockOrder: [],
        bblocks: {},
        from_store: true,
        items: 0
      }       
    },

    loggedIn (state) {
      console.log("loggedIn store mutation")
      state.loggedIn = true
      // state.user = {}
    },

    setProfile (state, data) {
      console.log('=== SET PROFILE ===')
      state.profile = data
      state.loggedIn = true
      console.log("loggedin statexx: ", state.loggedIn)
    },

    setUserTags(state, data) {
      // console.log("** SET USER TAGS ")
      // read all tags from JSON into state.userTags
      // console.log(JSON.stringify(data))
      if (data.user.has_tags) {
        for (let t of data.user.tags) {
          if (!state.userTags.hasOwnProperty(t.tag)) {
            state.userTags[t.tag]= []
          }
          state.userTags[t.tag].push(t)
        }
      } else {
        console.log('notags')
      }
    },


    setUser (state, user) {
      state.user = user
      state.loggedIn = true
      state.displayname = function (e) {
        var a = [
          state.user.name,
          state.user.login,
          state.user.email
        ]
        return a.filter(n => n)[0] || 'Anonymous'
      }()

    }
  }
})
