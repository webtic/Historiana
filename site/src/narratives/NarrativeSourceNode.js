import { Node, mergeAttributes } from '@tiptap/core'
import { VueNodeViewRenderer } from '@tiptap/vue-2'
import Component from './NarrativeSource.vue'

export default Node.create({
  name: 'NarrativeSource',

  addAttributes() {
    // Return an object with attribute configuration
    return {
      title: { default:null },
      src: { default:null },
      type: { default:null },
      caption: { default:null },
      description: { default:null },
      related: { default:null }
    }
  },

  group: 'block',

  content: 'paragraph*',

  draggable: true,

  isolating: true,

  parseHTML() {
    return [
      {
        tag: 'narrative-source',
      },
    ]
  },

  renderHTML({ HTMLAttributes }) {
    return ['narrative-source', mergeAttributes(HTMLAttributes),0]
  },

  addNodeView() {
    return VueNodeViewRenderer(Component)
  },

})