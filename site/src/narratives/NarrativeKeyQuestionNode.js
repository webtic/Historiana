import { Node, mergeAttributes } from '@tiptap/core'
import { VueNodeViewRenderer } from '@tiptap/vue-2'
import Component from './NarrativeKeyQuestion.vue'

export default Node.create({
  name: 'NarrativeKeyQuestion',

  addAttributes() {
    // Return an object with attribute configuration
    return {
      text: { default:null },
      related: { default:null }
    }
  },

  group: 'block',

  content: 'inline*',

  draggable: true,

  parseHTML() {
    return [
      {
        tag: 'narrative-key-question',
      },
    ]
  },

  renderHTML({ HTMLAttributes }) {
    return ['narrative-key-question', mergeAttributes(HTMLAttributes),0]
  },

  addNodeView() {
    return VueNodeViewRenderer(Component)
  },

})