# Web Testing: A Complete guide about testing web applications


#Forms
[Testing the Activity Builder](https://docs.google.com/spreadsheets/d/1jtqL3rlIOxGHYWuBSbmL_q57w8KiXnxfvK-UXYNRba8/edit#gid=0)

---

[©Copyrights: Software Testing Help](http://www.softwaretestinghelp.com/web-application-testing/)

1. Functionality Testing
2. Usability testing
3. Interface testing
4. Compatibility testing
5. Performance testing
6. Security testing

--- 

# 1. Functionality Testing

Test for – all the links in web pages, database connection, forms used for submitting or getting information from user in the web pages, Cookie testing etc.

### Check all the links:

- Test the outgoing links from all the pages  to specific domain under test.
- Test all internal links.
- Test links jumping on the same pages.
- Test links used to send email to admin or other users from web pages.
- Test to check if there are any orphan pages.
- Finally link checking includes, check for broken links in all above-mentioned links.

### Test forms in all pages:
Forms are the integral part of any website. Forms are used for receiving information from users and to interact with them. So what should be checked on these forms?

- First check all the validations on each field.
- Check for default values of the fields.
- Wrong inputs in the forms to the fields in the forms.
- Options to create forms if any, form delete, view or modify the forms.

Let’s take example of the search engine project currently I am working on, in this project we have advertiser and affiliate signup steps. Each sign up step is different but its dependent on the other steps. So sign up flow should get executed correctly. There are different field validations like email Ids, User financial info validations etc. All these validations should get checked in manual or automated web testing.

### Cookies Testing:
Cookies are small files stored on the user machine. These are basically used to maintain the session- mainly the login sessions. Test the application by enabling or disabling the cookies in your browser options. Test if the cookies are encrypted before writing to user machine. If you are testing the session cookies (i.e. cookies that expire after the session ends) check for login sessions and user stats after session ends. Check effect on application security by deleting the cookies. (I will soon write a separate article on cookie testing as well)

### Validate your HTML/CSS:
If you are optimizing your site for Search engines then HTML/CSS validation is the most important one. Mainly validate the site for HTML syntax errors. Check if  the site is crawlable to different search engines.

### Database testing:
Data consistency is also very important in web application. Check for data integrity and errors while you edit, delete, modify the forms or do any DB related functionality.
Check if all the database queries are executing correctly, data is retrieved and also updated correctly. More on database testing could be load on DB,we will address this in web load or performance testing below.

# 2. Usability Testing

### Test for navigation
Navigation means how an user surfs the web pages, different controls like buttons, boxes or how the user uses the links on the pages to surf different pages.
Usability testing includes the following:

- Website should be easy to use.
- Instructions provided should be very clear.
- Check if the instructions provided are perfect to satisfy its purpose.
- Main menu should be provided on each page.
- It should be consistent enough.


### Content checking:

Content should be logical and easy to understand. Check for spelling errors. Usage  of dark colours annoys the users and should not be used in the site theme. You can follow some standard colours that are used for web page and content building. These are the common accepted standards like what I mentioned above about annoying colours, fonts, frames etc.
Content should be meaningful. All the anchor text links should be working properly. Images should be placed properly with proper sizes.
These are some of basic important standards that should be followed in web development. Your task is to validate all for UI testing.

### Other user information for user help:
Like search option, sitemap also help files etc. Sitemap should be present with all the links in websites with proper tree view of navigation. Check for all links on the sitemap.

“Search in the site” option will help users to find content pages that they are looking for easily and quickly. These are all optional items and if present they should be validated.

# 3. Interface Testing
The main interfaces are:
Web server and application server interface
Application server and Database server interface.

Check if all the interactions between these servers are executed and errors are handled properly. If database or web server returns any error message for any query by application server then application server should catch and display these error messages appropriately to the users. Check what happens if user interrupts any transaction in-between? Check what happens if connection to the web server is reset in between?

# 4. Compatibility Testing
Compatibility of your website is a very important testing aspect. See which compatibility test to be executed:

- Browser compatibility
- Operating system compatibility
- Mobile browsing
- Printing options

### Browser compatibility:
In my web-testing career I have experienced this as the most influencing part on web site testing.
Some applications are very dependent on browsers. Different browsers have different configurations and settings that your web page should be compatible with. Your website coding should be a cross browser platform compatible. If you are using java scripts or AJAX calls for UI functionality, performing security checks or validations then give more stress on browser compatibility testing of your web application.Test web application on different browsers like Internet explorer, Firefox, Netscape navigator, AOL, Safari, Opera browsers with different versions.

### OS compatibility:
Some functionality in your web application is that it may not be compatible with all operating systems. All new technologies used in web development like graphic designs, interface calls like different API’s may not be available in all Operating Systems.
Hence test your web application on different operating systems like Windows, Unix, MAC, Linux, Solaris with different OS flavors.

### Mobile browsing:
We are in new technology  era. So in future Mobile browsing will rock. Test your web pages on mobile browsers. Compatibility issues may be there on mobile devices as well.

### Printing options:
If you are giving page-printing options then make sure fonts, page alignment, page graphics etc., are  getting printed properly. Pages should  fit to the paper size or as per the size mentioned in the printing option.

# 5. Performance testing
Web application should sustain to heavy load. Web performance testing should include:

- Web Load Testing
- Web Stress Testing

Test application performance on different internet connection speed.
Web load testing: You need to test if many users are accessing or requesting the same page. Can system sustain in peak load times? Site should handle many simultaneous user requests, large input data from users, simultaneous connection to DB, heavy load on specific pages etc.

**Web Stress** testing: Generally stress means stretching the system beyond its specified limits. Web stress testing is performed to break the site by giving stress and its checked as how the system reacts to stress and how it  recovers from crashes. Stress is generally given on input fields, login and sign up areas.

In web performance testing website functionality on different operating systems and different hardware platforms is checked for software and hardware memory leakage errors.

# 6. Security Testing

Following are some of the test cases for web security testing:

- Test by pasting internal URL directly onto the browser address bar without login. Internal pages should not open.
- If you are logged in using username and password and browsing internal pages then try changing URL options directly. I.e. If you are checking some publisher site statistics with publisher site ID= 123. Try directly changing the URL site ID parameter to different site ID which is not related to the logged in user. Access should be denied for this user to view others stats.
- Try some invalid inputs in input fields like login username, password, input text boxes etc. Check the systems reaction on all invalid inputs.
- Web directories or files should not be accessible directly unless they are given download option.
- Test the CAPTCHA for automates script logins.
- Test if SSL is used for security measures. If used proper message should get displayed when user switch from non-secure http:// pages to secure https:// pages and vice versa.
- All transactions, error messages, security breach attempts should get logged in log files somewhere on the web server.



