# Tags

## Definition
Components are reusable modules which give access to a certain functionality. Using them is almost as easy as typing a HTML tag.

### Select tags
```
<ul class="menu simple tags select">
  <li v-for="clas,i in classes">
    <input :id="'clas'+i" :value="clas" v-model="activeFilters" type="checkbox"><label :for="'clas'+i">{{clas.name}}</label>
  </li>
</ul>

```

### Remove tags
```
<ul class="menu simple tags manage">
  <li v-for="keyword in keywords" title="Click to remove" @click="removeTag(keyword.uuid)">
    <i class="fa fa-1x fa-remove warn" aria-hidden="true"></i>&nbsp;{{keyword.name}}</li>
  </li>
</ul>

```

### Add new tags
```

<div class="input-group input-tag">
  <span class="input-group-label">Keyword</span>
  <input class="input-group-field small-12 medium-8 large-6"  v-model="inKeyword" @keyup.enter="addTag(inKeyword, 'MyKeywords'); inKeyword=''" type="text" placeholder="Add new keywords">
    <div class="input-group-button">
      <a @click="addTag(inKeyword, 'MyKeywords'); inKeyword=''" :class="{ok:inKeyword}" class="addnew button small"><i class="fa fa-plus" aria-hidden="true"></i></a>
    </div>
</div>


```  


## Browse
Based on tags


```
<ul class="dropdown menu" data-dropdown-menu>
        <li><a href="javascript:"><i class="fa fa-list"></i> Year</a>
          <ul class="menu simple tags select">
            <li v-for="year,i in years" class="">
              <input :id="'year'+i" :value="year" v-model="activeFilters" type="checkbox"> <label :for="'year'+i">{{year.name}}</label>
            </li>
          </ul>
        </li>
        <li><a href="javascript:"><i class="fa fa-list"></i> Class</a>
          <ul class="menu simple tags select">
            <li v-for="clas,i in classes">
              <input :id="'clas'+i" :value="clas" v-model="activeFilters" type="checkbox"><label :for="'clas'+i">{{clas.name}}</label>
            </li>
          </ul>
        </li>

        <li><a href="javascript:"><i class="fa fa-list"></i> Keywords</a>
          <ul class="menu simple tags select">
            <li v-for="keyword,i in keywords">
              <input :id="'keyword'+i" :value="keyword" v-model="activeFilters" type="checkbox"><label :for="'keyword'+i">{{keyword.name}}</label>
            </li>
          </ul>
        </li>

        <li><a href="javascript:"><i class="fa fa-list"></i> Language</a>
          <ul class="menu simple tags select">
            <li v-for="lang,i in languages">
              <input :id="'lang'+i" :value="lang" v-model="activeFilters" type="checkbox"><label :for="'lang'+i">{{lang.name}}</label>
            </li>
          </ul>
        </li>
      </ul>
```

### Show selected tags from Browse


```
<div class="row">
    <div class="column">
      <ul class="menu simple tags manage">
        <li v-for="active in activeFilters" @click="activeFilters.splice(activeFilters.indexOf(active),1)"><i class="fa fa-1x fa-remove warn"></i> {{active.name}}</li>
      </ul>
    </div>
  </div>
```


