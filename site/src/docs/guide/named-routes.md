# Named routes 

In router/index.js definieer je een naam: 

```
      {
        path: '/teaching-learning',
        component: TL,
        meta: { class: 'is-teaching-learning' },
        children: [
          {
            name: 'tl-learning-activities',
            path: 'activities',
            component: LearningActivities,
            meta: { class: 'is-teaching-learning' }
          }
        ]
      },
```
en in een template refereer je naar de naam:       

```
        <router-link 
          :to="{name:'tl-learning-activities'}" 
          data-filter=".learningactivities" 
          id="learningactivities">Learning Activities
        </router-link>

```

De hierarchie wordt dan automatisch overgenomen.
