# Research and notes

## editor
!> contenteditable heeft kennelijk issues zie
https://github.com/vuejs/vue/issues/47

en mogelijk moeten we gewoon dit gebruiken
https://github.com/FranzSkuffka/vue-medium-editor

uiteindelijk zijn we Quill gaan gebruiken

## to Research

?> *TODO* - inifinite loop tool: 
https://github.com/lookstudios/vue-loop

?> *TODO* - magnifier:
http://mark-rolich.github.io/Magnifier.js/

andere plugin voor Dropzone
https://vuejsfeed.com/blog/upload-files-via-droply-dropzone-wrapper-for-vue-js-2

grappige dingen op: https://vuejsfeed.com/

# routes and subnavigation
active-class is not given to named routes if the URL does not include the trailing slash / #826 https://github.com/vuejs/vue-router/issues/826




https://forum.vuejs.org/t/router-active-class-on-submenu-parent/7027

There's no drect support for this in vue-router, so you have to build something on your own, foir example:
```
<li class="nav__dropdown" :class="{'router-link-active': subIsActive('/parent')}">
router-link-active
methods: {
  subIsActive(input) {
    const paths = Array.isArray(input) ? input : [input]
    return paths.some(path => {
      return this.$route.path.indexOf(path) === 0 // current path starts with this path string
    })
  }
}
```
now you can define an arbitrary path that should be matched, for example if all your submenus' paths start with /products, you would do subIsActive('/products'). if some do start with /cart, you could do subIsActive(['/products', '/cart'])


interessante editor: http://editor.ory.am/
https://github.com/ory/editor?branch=master

alt: https://draftjs.org/docs/overview.html#content


uuid naar mime en terug; voor tracking:

```python
import base64
import uuid

def get_a_uuid():
    # Use the uuid library to get a uuid
    new_uuid       = uuid.uuid4()
    # Get the foundation byte array for the uuid
    new_uuid       = new_uuid.bytes
    # Encode the bytes of the uuid as a url-safe base 64 string 
    new_uuid       = base64.urlsafe_b64encode(new_uuid)
    # Remove the trailing "="'s signs that the base 64 encoding adds because they can mess up urls
    new_uuid       = new_uuid.replace('=', '')
```
def get_a_uuid():
	 # Compact version:

    return base64.urlsafe_b64encode(uuid.uuid4().bytes).replace('=', '')

```

# Two-way Computed Property
uitproberen en in model verwerken?
https://vuex.vuejs.org/en/forms.html

```computed: {
  message: {
    get () {
      return this.$store.state.obj.message
    },
    set (value) {
      this.$store.commit('updateMessage', value)
    }
  }
```

# dispatch in store; complex spul : 
https://github.com/vuejs/vuex/issues/130
https://stackoverflow.com/questions/40390411/vuex-2-0-dispatch-versus-commit
https://github.com/vuejs/vuex/issues/38

* * * 
mogelijke admin interface
https://github.com/vue-bulma

# building blocks
https://h5p.org/content-types-and-applications
Waar ik stiekem op hoopte is dat we een deel van de code zouden kunnen gebruiken voor nieuwe building blocks. 

Bijvoorbeeld: 

https://h5p.org/image-hotspots
https://h5p.org/interactive-video

Maar wellicht dat dit wishful thinking is. :-$


# python-deamon
Slecht gedocumenteerd maar alle params zijn hier te vinden: 
https://pagure.io/python-daemon/blob/master/f/daemon/daemon.py

Let er op dat .runner() inmiddels deprecated is; veel docs die je vind hebben het hier nog over.

Eventueel alternatief: http://supervisord.org/



