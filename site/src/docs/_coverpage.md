<!-- _coverpage.md -->

![logo](_media/H.svg)

# Historiana Thriving <small>0.8</small>

"Fontanellen" Ephemeral Art Construction in front of Lund University Main Building | Francois Polito (Source [Europeana](http://www.europeana.eu/portal/nl/record/2058704/File__22Fontanellen_22_Ephemeral_Art_Construction_in_front_of_Lund_University_Main_Building_jpg.html))



[GitLab (login required)](https://gitlab.com/webtic/Historiana)
[Get Started](README.md)

<!-- background image -->
<!--![](_media/1280px--Fontanellen-_Ephemeral_Art_Construction_in_front_of_Lund_University_Main_Building.jpg)-->


<!-- background color -->
![color](#fefefe)
