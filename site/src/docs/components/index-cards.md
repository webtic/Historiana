# Index Cards
Collectable cards that link to the different kind of content and are fillterable and sortable. The contain the meta-information title, description, tags and more information. Cards can be shared, deleted and duplicated. When deleted or duplicated their content is directly connected to the action.

![Index Card](../_media/Index-Card.png)

## Input

|property|description|
|:-|:-|
|title|The text displayed in H3|
|action|name of action as displayed in the feedback message|

## Emits

|event|description|
|:-|:-|
|closeConfirm|user clicked cancel
|confirmed|user clicked confirm


*TODO*
Make the icon dynamic; currently it is hardcoded to `fa-trash-o`
<i class="fa fa-trash-o"></i>

## Sample

```
<confirm action="Delete Block" 
  v-show="showConfirmDeleteBlock"
  title="Confirm delete"
  @closeConfirm="showConfirmDeleteBlock=!showConfirmDeleteBlock" 
  @confirmed="deleteBlock">Are you sure you want to delete this block?
</confirm>

```

## Requirements
The v-show variable must be in the local context as a data propery
```
  data () {
    return {
      showConfirmDeleteBlock: false
    }
  }
```

![Index Cards](../_media/Index-Cards.png)


* * *