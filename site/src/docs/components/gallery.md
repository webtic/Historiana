# Gallery
Display an item; currently Activity or Source, with an image to the left and title and description to the right.  
There is an actionbar via which an edit can be triggered. The content currently editable is the title and the text.

Also sets-up keyboard to listen for Escape (close) and cursorkeys <i class="fa fa-arrow-left"></i> left and right <i class="fa fa-arrow-right"></i> for navigating previous/next item in the set.

![Gallery](_media/Gallery.png)

## Input

|property|description|use
|:-|:-|
|item|the item to show, a JS object with title, description and url for image
|total|number of items in array|zz in nn/zz display
|current|offset of current item in array|nn in nn/zz display
|isView|toggle the view/edit of the gallery


## Emits

|event|description|
|:-|:-|
|prev|make previous item the current item
|next|make next item the current item
|close|hide the gallery
|toggleEdit|user toggled edit

?> *TODO*
Tags need to be shown (and also editable?)

## Sample

```
<gallery v-show="showGallery"
     :item="item"
     :total="sources.length"
     :current="currentFocus"
     :isView="isView"
     @prev="prev"
     @next="next"
     @close="showGallery=!showGallery; isView=true"
     @toggleEdit="isView=!isView" 
></gallery> 

```

## Requirements
!> The v-show variable must be in the local context as a data propery
```
  data () {
    return {
      showConfirmDeleteBlock: false
    }
  }
```

* * *