# Confirm
Display a text in a modal screen and wait for the user to give either a Cancel or OK response.  
Also sets-up keyboard to listen for Escape (C ancel) and Enter (OK)

![Conform](../_media/Confirm.png)

## Input

|property|description|
|:-|:-|
|title|The text displayed in H3|
|action|name of action as displayed in the feedback message|

## Emits

|event|description|
|:-|:-|
|closeConfirm|user clicked cancel
|confirmed|user clicked confirm


*TODO*
Make the icon dynamic; currently it is hardcoded to `fa-trash-o`
<i class="fa fa-trash-o"></i>

## Sample

```
<confirm action="Delete Block" 
  v-show="showConfirmDeleteBlock"
  title="Confirm delete"
  @closeConfirm="showConfirmDeleteBlock=!showConfirmDeleteBlock" 
  @confirmed="deleteBlock">Are you sure you want to delete this block?
</confirm>

```

## Requirements
The v-show variable must be in the local context as a data propery
```
  data () {
    return {
      showConfirmDeleteBlock: false
    }
  }
```

* * *