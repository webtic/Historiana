<!-- docs/_sidebar.md -->
- [<svg xmlns="http://www.w3.org/2000/svg" id="logo" data-name="logo" width="72px" height="100px" viewBox="0 0 72.89 100" class="logo svg replaced-svg"><title>Logo-H</title><path d="M53.36,0V26.86H19.53V0H0V100H19.24l4.47-24.23h-.07L19.82,63.93h0a17.19,17.19,0,1,1,33.24,0h0L49.26,75.77h-.08L53.65,100H72.89V0Z" class="logo" fill="#55bb00"></path></svg>](/)

- Getting started
  - [ReadMe](README.md)
    
- Components
  - [Confirm](components/confirm.md)
  - [Gallery](components/gallery.md)
  - [Index Cards](components/Index-Cards.md)
  
- Guide
  - [Routes/URLs](guide/named-routes.md)
  - [Tags](guide/tags.md)
  
- Misc 
  - [Project structure](projectstructure.md)
  - [Research](research.md)
  - [Testing](testing.md)
  
<!--  - [Changelog](changelog.md)-->