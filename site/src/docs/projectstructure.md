# Project structure

/
```
README.md             generic VUE readme
Start.sh              doubleclick script for Nique
Style.md
build                 webpack configuration files
config                vue-webpack config 
dist                  output of `npm run build` process
index.html            basic index for the project, injection point for Vue
node_modules          npm storage directory
package.json          site- and development javascript productions
src                   all source materials of Historiana
static                static materials
sync_to_dev           script to sync to dev.historiana.eu
sync_to_live          script to sync build to beta.historiana.eu
```

/src
```
App.vue               main entrypoint
App.vue.dist          -
Historiana.vue        main template for site
_settings.scss    
assets                assets packed by webpack
components            Vue components
config                Vue configuration settings, router
docs                  these docs
main.js               Vue main entry point
pages                 move-into-components
router                Vue router settings
store                 Vue global store, contains most Application logic
tags                  move-into-components
tic.js                global custom functions
webpack.dev.conf.js   ? to we need this
``` 