oude start van Nique:
cd /Users/nique/Sites/HistorianaLive/src
nohup docsify serve docs &
npm run dev

fontawesome reference info:
https://blog.logrocket.com/full-guide-to-using-font-awesome-icons-in-vue-js-apps-5574c74d9b2d

discovery dev notes
https://docs.google.com/document/d/1Ij63ndsPhuufK-Kr5-37sf7BNlCP99QBlPJoujawfT4/edit#

# Named routes

In routes.js definieer je een naam:

```
      {
        path: '/teaching-learning',
        component: TL,
        meta: { class: 'is-teaching-learning' },
        children: [
          {
            name: 'tl-learning-activities',
            path: 'activities',
            component: LearningActivities,
            meta: { class: 'is-teaching-learning' }
          }
        ]
      },
```

en in een template refereer je naar de naam:

```
        <router-link :to="{name:'tl-learning-activities'}"
                     data-filter=".learningactivities"
                     id="learningactivities">Learning Activities</router-link>
```

De hierarchie wordt dan automatisch overgenomen.

===
Student View issues

the entrypoint is http://localhost:8080/#/student/view/7d6e2fc6-5015-474a-bb3a-6771bfbf6e00/
but we need to redirect this to http://localhost:8080/#/student/view/7d6e2fc6-5015-474a-bb3a-6771bfbf6e00/{{type}}/{{block}} to let it work correctly. This is done via elaRedirect.vue; the record is fetched at that moment

Consequence is that deeplinking does not fetch the record and a reload therefore fails.

Additionally http://localhost:8080/#/student/view/7d6e2fc6-5015-474a-bb3a-6771bfbf6e00 without slash also fails.

===

### TODO : er is besproken dat er een IndexCard voor external link moet komen om op die manier bijv.

de oude Casestudies te ontsluiten. Bekijken hoe dit kan en wat de gevolgen zijn.

curl https://intake.opbeat.com/api/v1/organizations/a166ad7ff1c745ca87021dc82e199e43/apps/967364a7fc/releases/ \
-H "Authorization: Bearer da050859826a3c6892a184b5035f26eb90e8b658" \
-d rev=`git log -n 1 --pretty=format:%H` \
-d branch=`git rev-parse --abbrev-ref HEAD` \
-d status=completed

App id: 967364a7fc
Organization id: a166ad7ff1c745ca87021dc82e199e43
Secret token: da050859826a3c6892a184b5035f26eb90e8b658

# FRONTEND OPBEAT

<script src="https://d3tvtfb6518e3e.cloudfront.net/3/opbeat.min.js"
    data-org-id="a166ad7ff1c745ca87021dc82e199e43"
    data-app-id="84bdf14774">
</script>

RELEASE FRONTEND:

curl https://intake.opbeat.com/api/v1/organizations/a166ad7ff1c745ca87021dc82e199e43/apps/84bdf14774/releases/ \
-H "Authorization: Bearer da050859826a3c6892a184b5035f26eb90e8b658" \
-d rev=`git log -n 1 --pretty=format:%H` \
-d branch=`git rev-parse --abbrev-ref HEAD` \
-d status=completed

## dependencies:

pip3 install py2neo
pip3 install sanic
pip3 install rollbar
pip3 install dnspython
pip3 install python-pptx
pip3 install python-slugify
pip3 install hjson
python3 -mpip install openpyxl

```
[Unit]
Description=Historiana API
After=syslog.target
After=network.target

[Service]
Type=simple
User=www-data
Group=daemon
ExecStart=/usr/bin/python3 /web/sites/historiana.eu/api/server.py
WorkingDirectory=/web/sites/historiana.eu/api/
Environment="PYTHONPATH=/web/sites/historiana.eu/api/:/web/sites/historiana.eu/api/tic"

# Give the script some time to startup
TimeoutSec=300

[Install]
WantedBy=multi-user.target

```

stats:

https://quasar.dev/quasar-cli/developing-capacitor-apps/managing-google-analytics#Implementing-this-into-application

shortlist: https://docs.google.com/document/d/1-OLdJNmspIGlj4dIXe_9Tjk0naTqCkzhdJvNT02eQCE/edit#heading=h.7o54wofgyhpz

issues doc: https://docs.google.com/document/d/1lEeQkkOeZ9Ni93990TZrvn36l4Yy_fvL3nV9GxoN6CE/edit#


http://localhost:8080/admin/teams

Vue Dynamically Adding Components on the Fly
https://codepen.io/glikoz/pen/qYEZEV?editors=1010
https://forum.quasar-framework.org/topic/866/qform-form-fields-generator


# tags

L&T tags

https://docs.google.com/spreadsheets/d/1rp8Hh20bNPe7AdyGpHb_r62LwktFP-oUm3Z8znVLZUQ/edit#gid=0


