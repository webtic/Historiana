// this version is not used 
// bb-blocks are configured as childs here instead of the root level

import Historiana from '../Historiana'
import Builder from '../components/Builder'
import SelectSources from '../components/SelectSources'
import MyHistoriana from '../components/MyHistoriana'

import LearningActivities from '../components/LearningActivities'

import MyActivities from '../components/MyActivities'
import MySources from '../components/MySources'
import MyResources from '../components/MyResources'
import MyTags from '../components/MyTags'
import MyProfile from '../components/MyProfile'
import MyShares from '../components/MyShares'
// import MyActivityLog from '../components/MyActivityLog'
import Upload from '../components/Upload'

import SASExplore from '../components/SASExplore'
import SASExploreSlug from '../components/SASExploreSlug'
import SASPartners from '../components/SASPartners'
import SASPartnerView from '../components/SASPartnerView'
import SASEuropeana from '../components/SASEuropeana'

// Pages
import HC from '../pages/HistoricalContent'
import HCbySlug from '../pages/HCbySlug'
import TL from '../pages/TeachingLearning'
import HOME from '../pages/Homepage'
import MediaLibrary from '../pages/MediaLibrary'
import CaseStudy from '../pages/CaseStudy'

// BuildingBlock Editor
import BlockEditor from '../pages/BlockEditor'
import BBtext from '../pages/BB-text'
import BBquestion from '../pages/BB-question'
import BBembed from '../pages/BB-embed'
import BBsorting from '../pages/BB-sorting'
import BBprioritizing from '../pages/BB-prioritizing'
import BBanalysing from '../pages/BB-analysing'
import BBcomparing from '../pages/BB-comparing'

// Admin functions
import Admin from '../views/admin/Admin'
import Collections from '../views/admin/Collections'
import Dashboard from '../views/admin/Dashboard'
import Roles from '../views/admin/Roles'
import GenericEditor from '../views/admin/GenericEditor'
import Partners from '../views/admin/Partners'

// Test Page
import TestPage from '../pages/TestPage'
import Experiment from '../pages/Experiment'
import Status from '../pages/Status'

// Student View
import elaStudentView from '../pages/elaStudentView'
import elaRedirect from '../pages/elaRedirect'

import histRedirect from '../components/histRedirect'


import LearningActivityEditor from '../views/admin/LearningActivityEditor'
import ModEditor from '../views/admin/ModEditor'
import ModEditorChapter from '../views/admin/ModEditorChapter'
import ModEditorIndex from '../views/admin/ModEditorIndex'


import ResetPassword from '../pages/ResetPassword'


export const routes = [
    {
      name: 'error',
      path: '/error',
      component: () => import('../components/Error.vue')
    },
    {
      name: 'reset-password',
      component: ResetPassword,
      path: '/reset-password/:challenge',
      props: true
    },

    {
      name: 'admin',
      path: '/admin',
      component: Admin,
      children: [{
          name: 'modeditor',
          plural: 'Modules',
          path: 'modeditor',
          component: ModEditorIndex,
          children: [{
              name: 'modeditormodchapter',
              plural: 'Modules',
              path: ':mod/:chapter',
              component: ModEditorChapter,
              props: true,
              hide: true
            },
            {
              name: 'modeditormod',
              plural: 'Modules',
              path: ':mod',
              component: ModEditor,
              props: true,
              hide: true
            }
          ]
        },
        {
          name: 'Partner',
          plural: 'Partners',
          path: 'partner',
          component: Partners
        },
        {
          name: 'Collection',
          plural: 'Collections',
          path: 'collection',
          component: Collections
        },
        {
          name: 'Dashboard',
          path: 'dashboard',
          component: Dashboard
        },
        {
          name: 'HistoricalThinking',
          plural: 'HistoricalThinking',
          path: 'HistoricalThinking',
          component: GenericEditor
        },
        {
          name: 'Language',
          plural: 'Languages',
          path: 'language',
          component: GenericEditor
        },
        {
          name: 'LearningActivity',
          plural: 'LearningActivities',
          path: 'LearningActivity',
          component: LearningActivityEditor
        },
        {
          name: 'License',
          plural: 'Licenses',
          path: 'license',
          component: GenericEditor
        },
        {
          name: 'Role',
          plural: 'Roles',
          path: 'role',
          component: GenericEditor
        },
        {
          name: 'Redirect',
          plural: 'Redirects',
          path: 'redirect',
          component: GenericEditor
        },
        {
          name: 'TeachingChallenges',
          plural: 'TeachingChallenges',
          path: 'TeachingChallenges',
          component: GenericEditor
        },
        {
          name: 'TeachingMethods',
          plural: 'TeachingMethods',
          path: 'TeachingMethods',
          component: GenericEditor
        }

      ]
    },

    {
      // http://localhost/hist/ for local development redirects
      // online we use the http://hi.st/ service
      name: 'histRedirect',
      path: '/hist/:shortCode',
      component: histRedirect
    },
    {
      name: 'histRedirectIndex',
      path: '/hist',
      component: histRedirect
    },
    {
      name: 'elaredirect',
      path: '/student/view/:uuid',
      component: elaRedirect,
      meta: {
        class: 'is-activity-builder'
      },
      props: {
        mode: 'student'
      }
    },

    {
      name: 'elashareredirect',
      path: '/share/view/:uuid',
      component: elaRedirect,
      meta: {
        class: 'is-activity-builder'
      },
      props: {
        mode: 'share'
      }
    },

    {
      name: 'elaviewer',
      path: '/student/view/:uuid/:currentBlock',
      component: elaStudentView,
      meta: {
        class: 'is-activity-builder'
      },
      props: {
        viewMode: 'student'
      },

      children: [{
          name: 'studentbbText',
          path: '/student/view/:uuid/text/:currentBlock',
          component: BBtext,
          meta: {
            class: 'is-bb-text'
          }
        },
        {
          name: 'studentbbQuestion',
          path: '/student/view/:uuid/question/:currentBlock',
          component: BBquestion,
          meta: {
            class: 'is-bb-question'
          }
        },
        {
          name: 'studentbbEmbed',
          path: '/student/view/:uuid/embed/:currentBlock',
          component: BBembed,
          meta: {
            class: 'is-bb-embed'
          }
        },
        {
          name: 'studentbbSorting',
          path: '/student/view/:uuid/sorting/:currentBlock',
          component: BBsorting,
          meta: {
            class: 'is-bb-sorting'
          }
        },
        {
          name: 'studentbbPrioritizinging',
          path: '/student/view/:uuid/prioritizing/:currentBlock',
          component: BBprioritizing,
          meta: {
            class: 'is-bb-prioritizing'
          }
        },
        {
          name: 'studentbbAnalysing',
          path: '/student/view/:uuid/analysing/:currentBlock',
          component: BBanalysing,
          meta: {
            class: 'is-bb-analysing'
          }
        }
      ]
    },

    {
      name: 'elashareviewer',
      path: '/share/view/:uuid/:currentBlock',
      component: elaStudentView,
      props: {
        viewMode: 'share'
      },
      meta: {
        class: 'is-activity-builder'
      },
      children: [{
          name: 'sharebbText',
          path: '/share/view/:uuid/text/:currentBlock',
          component: BBtext,
          props: true,
          meta: {
            class: 'is-bb-text'
          }
        },
        {
          name: 'sharebbQuestion',
          path: '/share/view/:uuid/question/:currentBlock',
          component: BBquestion,
          meta: {
            class: 'is-bb-question'
          }
        },
        {
          name: 'sharebbEmbed',
          path: '/share/view/:uuid/embed/:currentBlock',
          component: BBembed,
          meta: {
            class: 'is-bb-embed'
          }
        },
        {
          name: 'sharebbSorting',
          path: '/share/view/:uuid/sorting/:currentBlock',
          component: BBsorting,
          meta: {
            class: 'is-bb-sorting'
          }
        },
        {
          name: 'sharebbPrioritizinging',
          path: '/share/view/:uuid/prioritizing/:currentBlock',
          component: BBprioritizing,
          meta: {
            class: 'is-bb-prioritizing'
          }
        },
        {
          name: 'sharebbAnalysing',
          path: '/share/view/:uuid/analysing/:currentBlock',
          component: BBanalysing,
          meta: {
            class: 'is-bb-analysing'
          }
        }
      ]
    },

    {
      name: 'TestPage',
      path: '/TestPage',
      component: TestPage
    },
    {
      name: 'Experiment',
      path: '/Experiment',
      component: Experiment
    },
    {
      name: 'MediaLibrary',
      path: '/MediaLibrary',
      component: MediaLibrary,
      meta: {
        class: 'is-media-library'
      }
    },
    {
      path: '/',
      name: 'HomePage',
      component: resolve => resolve(Historiana),
      meta: {
        class: 'is-homepage'
      },
      children: [{
          name: 'About',
          path: '/about',
          component: () => import("../pages/About.vue")
        },
        {
          component: HOME,
          name: 'homepage',
          path: '/',
          meta: {
            class: 'is-homepage'
          }
        },
        {
          name: 'CaseStudy',
          path: '/case-study',
          component: CaseStudy,
          meta: {
            class: 'is-case-study'
          }
        },
        {
          name: 'historical-content-slug-source',
          path: '/historical-content/source-collections/:slug',
          component: SASExploreSlug,
          meta: {
            class: 'is-historical-content'
          },
          props: true
        },

        {
          name: 'historical-content-slug-chapter',
          path: '/historical-content/:type/:slug/:chapter',
          component: HCbySlug,
          meta: {
            class: 'is-historical-content'
          },
          props: true
        },
        {
          name: 'historical-content-slug',
          path: '/historical-content/:type/:slug',
          component: HCbySlug,
          meta: {
            class: 'is-historical-content'
          },
          props: true
        },
        {
          name: 'historical-content',
          path: '/historical-content',
          component: HC,
          meta: {
            class: 'is-historical-content'
          },
          props: true
        },
        {
          path: '/teaching-learning',
          component: TL,
          meta: {
            class: 'is-teaching-learning'
          },
          children: [{
            name: 'tl-learning-activities',
            path: 'activities',
            component: LearningActivities,
            meta: {
              class: 'is-teaching-learning'
            }
          }]
        },
        {
          name: 'learning-activity',
          path: '/learning-activity/:slug',
          component: LearningActivities,
          meta: {
            class: 'is-teaching-learning'
          },
          props: true
        },
        {
          name: 'sas',
          path: '/sas',
          component: SelectSources,
          meta: {
            class: 'is-select-sources'
          },
          children: [{
              name: 'explore',
              path: 'explore',
              component: SASExplore,
              meta: {
                class: 'is-select-sources'
              }
            },
            {
              name: 'exploreslug',
              path: 'explore/:slug',
              component: SASExploreSlug,
              meta: {
                class: 'is-select-sources'
              }
            },
            {
              name: 'partners',
              path: 'partners',
              component: SASPartners,
              meta: {
                class: 'is-select-sources'
              }
            },
            {
              // we need the uuid because there is data duplication
              // TODO: cleanup SearchPartner entries; make slug an unique
              // MIGRATE slug:
              // match (p:SearchPartner) set p.slug=apoc.text.slug(toLower(p.name))
              name: 'explore-partner',
              path: 'partners/:slug/:uuid',
              component: SASPartnerView,
              props: true,
              meta: {
                class: 'is-select-sources'
              }
            },
            {
              name: 'search',
              path: 'search',
              component: SASEuropeana,
              meta: {
                class: 'is-select-sources'
              }
            }
          ]
        },
        {
          name: 'elabuilder',
          path: '/ela/:uuid',
          component: Builder,
          // beforeEnter: (to, from, next) => {
          // //return console.log('beforeenter')
          //   console.log("** elabuilder **")
          //   console.log("from: ", from)
          //   console.log("to: ", to)
          //   console.log("next: ", next)
          //   console.log("** end elabuilder **")
          // },
          //store.dispatch('initApp').then(response => {
          // the above state is not available here, since it
          // it is resolved asynchronously in the store action
          //}, error => {
          // handle error here
          //})                   
          //},
          meta: {
            class: 'is-activity-builder'
          }
        },
        {
          name: 'my',
          path: '/my',
          // we need /my as parent for the active-link magic to work correctly
          // but we redirect to get activities as the first active page instead
          // of the empty my- one.
          redirect: '/my/activities',
          component: MyHistoriana,
          meta: {
            class: 'is-my-historiana'
          },
          children: [{
              name: 'my-activities',
              path: 'activities',
              component: MyActivities,
              meta: {
                class: 'is-my-historiana'
              }
            },
            {
              name: 'my-sources',
              path: 'sources',
              component: MySources,
              meta: {
                class: 'is-my-historiana'
              }
              // beforeEnter (to, from, next) {
              //   console.log("before entry my sources")
              //   next()
              //   $('#main-menu-my-historiana').addClass('selected')

              // }

            },
            {
              name: 'my-resources',
              path: 'resources',
              component: MyResources,
              meta: {
                class: 'is-my-historiana'
              }
            },
            {
              name: 'my-tags',
              path: 'tags',
              component: MyTags,
              meta: {
                class: 'is-my-historiana'
              }
            },
            {
              name: 'my-profile',
              path: 'profile',
              component: MyProfile,
              meta: {
                class: 'is-my-historiana'
              }
            },
            {
              name: 'my-shares',
              path: 'shares',
              component: MyShares,
              meta: {
                class: 'is-my-historiana'
              }
            },
            {
              name: 'upload',
              path: 'upload',
              component: Upload,
              meta: {
                class: 'is-my-historiana'
              }
            }]
          },
            //          {
            //            name: 'my-activitylog',
            //            path: 'activitylog',
            //            component: MyActivityLog,
            //            meta: { class: 'is-my-historiana' }
            //          }

            {
              name: 'builderIndex',
              path: '/builder',
              component: Builder,
              props: true,
              meta: {
                class: 'is-activity-builder'
              }
            },
        
            {
              name: 'builder',
              path: '/builder/:uuid',
              props: true,
              component: Builder,
              meta: {
                class: 'is-activity-builder'
              },
        
              children: [{
                name: 'bbeditor',
                path: '/builder/:uuid/edit',
                props: true,
                component: BlockEditor,
                meta: {
                  class: 'is-my-historiana'
                },
                children: [{
                    name: 'bbText',
                    path: '/builder/:uuid/edit/text/:block',
                    beforeEnter: (to, from, next) => { 
                      console.log(" router BB TEXT *************")
                    },
                    component: BBtext,
                    meta: {
                      class: 'is-bb-text',
                      type: 'text',
                      mode: 'edit'
                    }
                  },
                  {
                    name: 'bbQuestion',
                    path: '/builder/:uuid/edit/question/:block',
                    component: BBquestion,
                    meta: {
                      class: 'is-bb-question'
                    }
                  },
                  {
                    name: 'bbEmbed',
                    path: '/builder/:uuid/edit/embed/:block',
                    component: BBembed,
                    meta: {
                      class: 'is-bb-embed'
                    }
                  },
                  {
                    name: 'bbSorting',
                    path: '/builder/:uuid/edit/sorting/:block',
                    component: BBsorting,
                    meta: {
                      class: 'is-bb-sorting'
                    }
                  },
                  {
                    name: 'bbPrioritizinging',
                    path: '/builder/:uuid/edit/prioritizing/:block',
                    component: BBprioritizing,
                    meta: {
                      class: 'is-bb-prioritizing'
                    }
                  },
                  {
                    name: 'bbAnalysing',
                    path: '/builder/:uuid/edit/analysing/:block',
                    component: BBanalysing,
                    meta: {
                      class: 'is-bb-analysing'
                    }
                  },
                  {
                    name: 'bbComparing',
                    path: '/builder/:uuid/edit/comparing/:block',
                    component: BBcomparing,
                    meta: {
                      class: 'is-bb-comparing'
                    }
                  }
                ]
              }]
        }
      ]
    }
    ]
    
    export default routes