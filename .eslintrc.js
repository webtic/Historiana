module.exports = {
  "parserOptions": {
      "parser": "babel-eslint",
      "ecmaVersion": 2017,
      "sourceType": "module"
  },
  extends: [
    // add more generic rulesets here, such as:
    // 'eslint:recommended',
    //'plugin:vue/strongly-recommended'
  ],
  rules: {
    // override/add rules settings here, such as:
    // 'vue/no-unused-vars': 'error'
  'no-console': 'off',
  'prettier/semi': 'false',
  "prettier/prettier": ["error", { "singleQuote": true }],
  'vue/html-indent': 'error',
  'vue/singleline-html-element-content-newline': 'off',
  'vue/multiline-html-element-content-newline': 'off',
  'vue/max-attributes-per-line': 'off',
  'vue/html-closing-bracket-spacing': 'off',
  'vue/html-indent': 'off',
  "vue/html-self-closing": "off",
  "vue/html-closing-bracket-newline": "off"
  }
}
